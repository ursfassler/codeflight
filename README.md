# CodeFlight

CodeFlight is a free and open source tool to gain insights about the high level structure of your software.
It supports various metrics, graphs and statistics to find problematic areas in your code base and keep track of them.

See the [examples](http://ursfassler.gitlab.io/codeflight-examples/index.html) to get an impression.

## Use Cases

CodeFlight supports the software architect and developer in various ways.
For once, it provides metrics and graphs to identify problematic areas, thus guiding the development team in the cleanup / refactoring work.
Integrated in the continuous integration pipeline it is a early warning system for code rot.
If executed with the testing suite it can alarm about a policy violation, e.g. introduction of cyclic dependencies between packages.


## Output

CodeFlight generates the metrics, graphs and more as html.
This allows you to browse it right away or include it in your CI or any other system.

## Input 

CodeFlight takes a simplified AST of the project as input.
The AST consist of just a few different nodes an references between the nodes.

    <project>
        <package name="">
            <class name="ClassA" id="1">
                <method name="method1" id="2">
                    <reference target="3"/>
                </method>
                <field name="field1">
            </class>
            <function name="foo">
                <reference target="1"/>
                <reference target="2"/>
            </function>
        </package>
    </project>

For C++, the simplified AST can be generated with [cpp-ast](https://gitlab.com/ursfassler/cpp-ast).

## Compilation

codeflight requires the following packages to be installed on your system:

* libtinyxml-dev
* libtclap-dev

It can be compiled using cmake:

    $ mkdir build && cd build
    $ cmake ..
    $ make -j`nproc`

See also `run-tests/docker-test.sh` how it is built and tests are run.

## Usage

CodeFlight is an easy to use command line tool.
You provide the simplified AST and the output is generated, for example:

    $ codeflight my-program.ast my-program

This will output an HTML report in the *my-program* folder. More options are available, see *--help* argument:

    USAGE:

       ./codeflight  [-r <directory>] [--] [--version] [-h] <filename> <output
                     directory>


    Where:

       -r <directory>,  --resources <directory>
         directory for resources to add to the output

       --,  --ignore_rest
         Ignores the rest of the labeled arguments following this flag.

       --version
         Displays version information and exits.

       -h,  --help
         Displays usage information and exits.

       <filename>
         (required)  file containing the AST

       <output directory>
         (required)  where to place the output


       generating artefacts and metrics
