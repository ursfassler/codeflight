cmake_minimum_required (VERSION 3.8)


add_library(_options INTERFACE)

target_compile_options(_options INTERFACE
    -Wall
    -Wextra
    -pedantic
    -pedantic-errors
    -Werror
)

target_compile_features(_options INTERFACE
    cxx_std_20
)
