# language: en

Feature: http GET
  As a user of the tool
  I want the pages to be generated when I access them
  In order to save time and space


Scenario: return the index page
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <package name="p1"/>
        </package>
    </project>
    """
  And I parse the input
  And I start the server

  When I send the request:
    """
    GET / HTTP/1.1


    """

  Then I expect the response:
    """
    HTTP/1.0 200 OK
    Content-Type: text/html
    Content-Length: 989

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>The Super Project</title>
        </head>
        <body>
            <h1>Project</h1>
            <p>
                <ul>
                    <li>
                        <a href="all-packages.html">All packages</a>
                    </li>
                    <li>
                        <a href="all-classes.html">All classes</a>
                    </li>
                    <li>
                        <a href="all-functions.html">All functions</a>
                    </li>
                    <li>
                        <a href="all-variables.html">All variables</a>
                    </li>
                    <li>
                        <a href="packages-with-cyclic-dependencies.html">Packages with cylic dependencies</a>
                    </li>
                    <li>
                        <a href="stable-dependency-principle-violations.html">Stable dependency principle violations</a>
                    </li>
                </ul>
            </p>
        </body>
    </html>

    """


Scenario: return a sub page
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <package name="p1"/>
        </package>
    </project>
    """
  And I parse the input
  And I start the server

  When I send the request:
    """
    GET /-/p1/index.html HTTP/1.1


    """

  Then I expect the response:
    """
    HTTP/1.0 200 OK
    Content-Type: text/html
    Content-Length: 892

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>p1</title>
        </head>
        <body>
            <nav>
                <a href="../../index.html">The Super Project</a>
                &gt;
                <a href="../index.html">::</a>
                &gt;
                <a href="index.html">p1</a>
            </nav>
            <h1>p1</h1>
            <p>
                <ul>
                    <li>
                        <a href="content-dependencies.svg">Package content dependencies</a>
                    </li>
                    <li>
                        <a href="package-cycles.html">Package cycles</a>
                    </li>
                    <li>
                        <a href="neighbors.svg">Package neighbors</a>
                    </li>
                    <li>
                        <a href="package-tree.svg">Package tree</a>
                    </li>
                </ul>
            </p>
        </body>
    </html>

    """


Scenario: return a SVG
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <package name="p1"/>
        </package>
    </project>
    """
  And I parse the input
  And I start the server

  When I send the request:
    """
    GET /-/p1/neighbors.svg HTTP/1.1


    """

  Then I expect the response:
    """
    HTTP/1.0 200 OK
    Content-Type: image/svg+xml
    Content-Length: 32

    generated from -/p1/neighbors.gv
    """


Scenario: return a page with encoded characters in url
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <package name="Hello <World>?"/>
        </package>
    </project>
    """
  And I parse the input
  And I start the server

  When I send the request:
    """
    GET /-/Hello%20%3CWorld%3E%3f/index.html HTTP/1.1


    """

  Then I expect the response:
    """
    HTTP/1.0 200 OK
    Content-Type: text/html
    Content-Length: 946

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>Hello &lt;World&gt;?</title>
        </head>
        <body>
            <nav>
                <a href="../../index.html">The Super Project</a>
                &gt;
                <a href="../index.html">::</a>
                &gt;
                <a href="index.html">Hello &lt;World&gt;?</a>
            </nav>
            <h1>Hello &lt;World&gt;?</h1>
            <p>
                <ul>
                    <li>
                        <a href="content-dependencies.svg">Package content dependencies</a>
                    </li>
                    <li>
                        <a href="package-cycles.html">Package cycles</a>
                    </li>
                    <li>
                        <a href="neighbors.svg">Package neighbors</a>
                    </li>
                    <li>
                        <a href="package-tree.svg">Package tree</a>
                    </li>
                </ul>
            </p>
        </body>
    </html>

    """


Scenario: return an error when an artefact is not found
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <package name="p1"/>
        </package>
    </project>
    """
  And I parse the input
  And I start the server

  When I send the request:
    """
    GET /-/p2/index.html HTTP/1.1


    """

  Then I expect the response:
    """
    HTTP/1.0 404 Not Found
    Content-Type: text/html
    Content-Length: 317

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>404 Not Found</title>
        </head>
        <body>
            <h1>Not Found</h1>
            <p>Artefact does not exist</p>
            <p>
                <a href="../../index.html">Back to the project page.</a>
            </p>
        </body>
    </html>

    """


Scenario: return an error when the request url has an error
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <package name="p1"/>
        </package>
    </project>
    """
  And I parse the input
  And I start the server

  When I send the request:
    """
    GET /hello%a.html HTTP/1.1


    """

  Then I expect the response:
    """
    HTTP/1.0 400 Bad Request
    Content-Type: text/html
    Content-Length: 270

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>400 Bad Request</title>
        </head>
        <body>
            <h1>Bad Request</h1>
            <p>Your browser sent a request that this server could not understand.</p>
        </body>
    </html>

    """


Scenario: return ressources
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <package name="p1"/>
        </package>
    </project>
    """
  And I have the file "input/res/style1.css":
    """
    // style 1
    """
  And I have the file "input/res/script.js":
    """
    console.log("Hello World")
    """
  And I enable all metrics
  And I set the resources directory to "input/res/"
  And I parse the input
  And I start the server

  When I send the request:
    """
    GET /all-packages.html HTTP/1.1


    """

  Then I expect the response:
    """
    HTTP/1.0 200 OK
    Content-Type: text/html
    Content-Length: 1486

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <script src="resources/script.js"></script>
            <link rel="stylesheet" href="resources/style1.css" />
            <title>All packages</title>
        </head>
        <body>
            <h1>All packages</h1>
            <table class="string-numbers">
                <tr>
                    <th>Name</th>
                    <th>Packages</th>
                    <th>Classes</th>
                    <th>Functions</th>
                    <th>Variables</th>
                    <th>LCOM4</th>
                    <th>fan-in</th>
                    <th>fan-out</th>
                    <th>instability</th>
                    <th>has cycles</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/index.html">::</a>
                    </td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0.00</td>
                    <td>false</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/p1/index.html">p1</a>
                    </td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0.00</td>
                    <td>false</td>
                </tr>
            </table>
        </body>
    </html>

    """

  When I send the request:
    """
    GET /resources/style1.css HTTP/1.1


    """

  Then I expect the response:
    """
    HTTP/1.0 200 OK
    Content-Type: text/css
    Content-Length: 10

    // style 1
    """

  When I send the request:
    """
    GET /resources/script.js HTTP/1.1


    """

  Then I expect the response:
    """
    HTTP/1.0 200 OK
    Content-Type: text/javascript
    Content-Length: 26

    console.log("Hello World")
    """
