/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "mock/MemoryFiles.h"
#include "mock/MemoryFilesystem.h"
#include "mock/MemoryProcess.h"

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>



namespace features
{


class OsFs
{
public:
  mock::MemoryFiles files{};
  mock::MemoryProcess os{files};
  mock::MemoryFilesystem fs{files};

};


}
