/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "mock/MemoryLogger.h"

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>


namespace features
{
namespace
{


THEN("^I expect the log output:$")
{
  REGEX_PARAM(std::string, expected);

  cucumber::ScenarioScope<mock::MemoryLogger> logger;
  ASSERT_EQ(expected, logger->str());
}


}
}
