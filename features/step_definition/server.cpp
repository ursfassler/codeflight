/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Codeflight.h"
#include "OsFs.h"
#include "component/http/Connection.h"
#include "component/http/Http.h"
#include "mock/MemoryLogger.h"

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>


namespace features
{
namespace
{


class Server
{
public:
  std::unique_ptr<component::http::Http> http{};
  std::unique_ptr<component::http::Connection> connection{};
  std::string response{};

};


GIVEN("^I start the server$")
{
  cucumber::ScenarioScope<Codeflight> allContext{};
  cucumber::ScenarioScope<mock::MemoryLogger> logger{};
  cucumber::ScenarioScope<OsFs> osfs{};
  cucumber::ScenarioScope<Server> serverContext{};

  serverContext->http = std::make_unique<component::http::Http>(
        *allContext->codeflight,
        *allContext->web,
        "\n",
        "",
        osfs->fs,
        *logger
        );

  const auto sender = [](const std::string& response) -> std::optional<std::size_t>{
    cucumber::ScenarioScope<Server> context{};
    context->response = response;
    return { response.size() };
  };

  serverContext->connection = serverContext->http->produceConnection(sender);
}


WHEN("^I send the request:$")
{
  REGEX_PARAM(std::string, request);

  cucumber::ScenarioScope<Server> context{};

  context->connection->received(request);
}


THEN("^I expect the response:$")
{
  REGEX_PARAM(std::string, expected);

  cucumber::ScenarioScope<Server> context{};

  ASSERT_EQ(expected, context->response);
}


}
}
