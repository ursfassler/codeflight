/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Codeflight.h"
#include "OsFs.h"
#include "component/astparser/astparser.h"
#include "component/codeflight/artefact/factory/Factory.h"
#include "component/driver/Driver.h"
#include "mock/MemoryLogger.h"
#include <memory>
#include <sstream>

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>


namespace features
{
namespace
{

const std::string DefaultInputFile{"input.ast"};


GIVEN("^I have the input file:$")
{
  REGEX_PARAM(std::string, input);

  cucumber::ScenarioScope<OsFs> osfs;
  osfs->files.write(DefaultInputFile, input);
}

GIVEN("^I enable all metrics$")
{
  cucumber::ScenarioScope<Codeflight> context;
  context->metricRepoFactory = {};
}

GIVEN("^I set the resources directory to \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, resourcesDirectory);

  cucumber::ScenarioScope<Codeflight> context;
  context->resourcesDirectory = resourcesDirectory;
}

WHEN("^I parse the input$")
{
  cucumber::ScenarioScope<Codeflight> context;
  cucumber::ScenarioScope<mock::MemoryLogger> logger;
  cucumber::ScenarioScope<OsFs> osfs;

  const std::filesystem::path outdir = {};

  auto project = component::astparser::load(
        DefaultInputFile,
        context->xmlReader,
        osfs->fs,
        *logger
        );

  context->codeflight = component::codeflight::Codeflight::produce(
        project,
        {},
        context->metricRepoFactory
        );

  context->web = component::web::Web::produce(
        *context->codeflight,
        context->xmlFactory,
        context->resourcesDirectory,
        outdir,
        osfs->fs,
        osfs->os
        );

  context->driver = std::make_unique<component::driver::Driver>(
        *context->codeflight,
        *context->web,
        *logger
        );
}

WHEN("^I process the file \"([^\"]*)\" with the output directory \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, inputFile);
  REGEX_PARAM(std::string, outputDirectory);

  cucumber::ScenarioScope<Codeflight> context;
  cucumber::ScenarioScope<OsFs> osfs;
  cucumber::ScenarioScope<mock::MemoryLogger> logger;

  auto project = component::astparser::load(
        inputFile,
        context->xmlReader,
        osfs->fs,
        *logger
        );

  const auto codeflight = component::codeflight::Codeflight::produce(
        project,
        {},
        {}
        );

  const auto web = component::web::Web::produce(
        *codeflight,
        context->xmlFactory,
        context->resourcesDirectory,
        outputDirectory,
        osfs->fs,
        osfs->os
        );

  component::driver::Driver driver{*codeflight, *web, *logger};
  driver.processAll();
}

WHEN("^I process the queue until empty$")
{
  cucumber::ScenarioScope<Codeflight> context;
  context->driver->processAll();
}

WHEN("^I generate the file \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, urlString);

  cucumber::ScenarioScope<Codeflight> context;
  const auto url = component::web::library::Url::parse(urlString);
  if (!url) {
    throw std::runtime_error("invalid URL: " + urlString);
  }
  auto artefactUrl = context->web->getWebGenerator().maybeGenerate(*url);
  if (!artefactUrl) {
    throw std::runtime_error("artefact generator not found for: " + urlString);
  }
}

std::set<std::filesystem::path> parseFilenames(const std::vector<std::map<std::string,std::string>>& hashes)
{
  std::set<std::filesystem::path> result{};
  for (const auto& row : hashes) {
    const auto& filename = row.at("filename");
    result.insert(filename);
  }
  return result;
}

THEN("^I expect to have exactly these files:$")
{
  TABLE_PARAM(filesTable);
  const auto expected = parseFilenames(filesTable.hashes());

  cucumber::ScenarioScope<OsFs> osfs;
  const auto actual = osfs->files.listOfFiles();

  ASSERT_EQ(expected, actual);
}

THEN("^I expect to have at least these files:$")
{
  TABLE_PARAM(filesTable);
  const auto expected = parseFilenames(filesTable.hashes());

  cucumber::ScenarioScope<OsFs> osfs;
  const auto all = osfs->files.listOfFiles();

  std::set<std::filesystem::path> actual{};
  set_intersection(expected.begin(),expected.end(),all.begin(),all.end(), std::inserter(actual, actual.begin()));

  ASSERT_EQ(expected, actual);
}


}
}
