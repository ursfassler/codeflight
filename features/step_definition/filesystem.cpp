/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "OsFs.h"

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>


namespace features
{
namespace
{


GIVEN("^I have the file \"([^\"]*)\":$")
{
  REGEX_PARAM(std::string, filename);
  REGEX_PARAM(std::string, content);

  cucumber::ScenarioScope<OsFs> osfs;
  osfs->files.write(filename, content);
}

THEN("^I expect to get the output file \"([^\"]*)\":$")
{
  REGEX_PARAM(std::string, filename);
  REGEX_PARAM(std::string, content);

  cucumber::ScenarioScope<OsFs> osfs;
  const auto actual = osfs->files.read(filename);
  ASSERT_EQ(content, actual);
}


}
}
