/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/adapter/xml/reader/TinyReader.h"
#include "component/adapter/xml/writer/TinyWriter.h"
#include "component/codeflight/Codeflight.h"
#include "component/codeflight/metric/mock/MinimalRepository.h"
#include "component/driver/Driver.h"
#include "component/web/Web.h"
#include "component/web/generator/WebGenerator.h"
#include "component/web/output/WebWriter.h"
#include "component/web/resource/Resource.h"
#include <memory>
#include <sstream>

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>


namespace features
{


class Codeflight
{
public:
  std::optional<component::codeflight::MetricRepoFactory> metricRepoFactory = [](){
    return std::make_unique<component::codeflight::metric::mock::MinimalRepository>();
  };
  std::optional<std::filesystem::path> resourcesDirectory;

  component::adapter::xml::reader::TinyReader xmlReader{};
  component::adapter::xml::writer::TinyFactory xmlFactory{};
  std::unique_ptr<component::driver::Driver> driver{};

  std::unique_ptr<component::codeflight::Codeflight> codeflight{};
  std::unique_ptr<component::web::Web> web{};

};


}
