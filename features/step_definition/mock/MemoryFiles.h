/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <filesystem>
#include <map>
#include <set>
#include <string>

namespace features::mock
{


class MemoryFiles
{
public:
  std::string read(const std::string&) const;
  void write(const std::string& name, const std::string& content);
  std::set<std::filesystem::path> listOfFiles() const;

private:
  std::map<std::filesystem::path, std::string> fs{};

};


}
