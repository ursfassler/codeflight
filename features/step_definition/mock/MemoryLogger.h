/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/astparser/io/Logger.h"
#include "component/driver/io/Logger.h"
#include "component/http/io/Logger.h"
#include <algorithm>
#include <vector>


namespace features::mock
{


class MemoryLogger :
    public component::astparser::io::Logger,
    public component::driver::io::Logger,
    public component::http::io::Logger
{
public:
  void info(const std::string& message) const override;
  void warning(const std::string& message) const override;

  std::string str() const;

private:
  mutable std::vector<std::string> messages{};

  void log(const std::string& severity, const std::string& message) const;

};


}
