/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/astparser/io/Filesystem.h"
#include "component/web/io/Filesystem.h"
#include <set>
#include <string>

namespace features::mock
{

class MemoryFiles;


class MemoryFilesystem :
    public component::astparser::io::Filesystem,
    public component::web::io::Filesystem
{
public:
  MemoryFilesystem(
      MemoryFiles&
      );

  std::set<std::filesystem::path> files(const std::filesystem::path&) const override;
  void read(const std::filesystem::path&, const std::function<void(std::istream&)>&) const override;
  bool fileExists(const std::filesystem::path&) const override;
  void write(const std::filesystem::path&, const std::function<void(std::ostream&)>&) override;

private:
  MemoryFiles& fs;

};


}
