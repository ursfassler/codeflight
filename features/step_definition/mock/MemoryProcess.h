/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/web/io/Process.h"

namespace features::mock
{

class MemoryFiles;


class MemoryProcess :
    public component::web::io::Process
{
public:
  MemoryProcess(
      MemoryFiles&
      );

  void execute(const std::string& command) override;

private:
  MemoryFiles& fs;

};


}
