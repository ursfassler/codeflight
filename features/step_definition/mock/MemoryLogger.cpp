/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MemoryLogger.h"

namespace features::mock
{


void MemoryLogger::info(const std::string &message) const
{
  log("INFO", message);
}

void MemoryLogger::warning(const std::string &message) const
{
  log("WARNING", message);
}

std::string MemoryLogger::str() const
{
  std::string result {};
  std::for_each(messages.begin(), messages.end(), [&result](const auto& itr){
    result += itr + "\n";
  });
  return result;

}

void MemoryLogger::log(const std::string& severity, const std::string& message) const
{
  messages.push_back(severity + ": " + message);
}


}
