/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MemoryProcess.h"
#include "MemoryFiles.h"
#include <sstream>

namespace features::mock
{


MemoryProcess::MemoryProcess(
    MemoryFiles& fs_
    ) :
  fs{fs_}
{
}

void MemoryProcess::execute(const std::string &command)
{
  const std::string call{"dot -T svg -o \""};
  if (command.substr(0, call.size()) == call) {
    const std::string separator{"\" \""};
    const std::string end{"\""};
    const auto startOfSvgName = call.size();
    const auto endOfSvgName = command.find(separator, startOfSvgName);
    const auto startOfGvName = endOfSvgName + separator.size();
    const auto endOfGvName = command.find(end, startOfGvName);
    const auto gvFile = command.substr(startOfGvName, endOfGvName - startOfGvName);
    const auto svgFile = command.substr(startOfSvgName, endOfSvgName - startOfSvgName);
    fs.write(svgFile, "generated from " + gvFile);
  }
}


}
