/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MemoryFiles.h"
#include <stdexcept>

namespace features::mock
{


std::string MemoryFiles::read(const std::string &name) const
{
  const auto idx = fs.find(name);
  if (idx == fs.end()) {
    throw std::invalid_argument("file not found: " + name);
  }
  return idx->second;
}

void MemoryFiles::write(const std::string& name, const std::string& content)
{
  fs[name] = content;
}

std::set<std::filesystem::path> MemoryFiles::listOfFiles() const
{
  std::set<std::filesystem::path> result{};
  for (const auto& itr : fs) {
    const auto filename = itr.first;
    result.insert(filename);
  }
  return result;
}


}
