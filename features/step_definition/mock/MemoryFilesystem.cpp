/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MemoryFilesystem.h"
#include "MemoryFiles.h"
#include <sstream>

namespace features::mock
{


MemoryFilesystem::MemoryFilesystem(
    MemoryFiles& fs_
    ) :
  fs{fs_}
{
}

std::set<std::filesystem::path> MemoryFilesystem::files(const std::filesystem::path& root) const
{
  std::set<std::filesystem::path> result{};

  const auto files = fs.listOfFiles();
  for (const auto& file : files) {
    const auto relative = std::filesystem::relative(file, root);
    const auto parent_path = relative.parent_path();
    if (parent_path.empty()) {
      result.insert(relative);
    }
  }

  return result;
}

void MemoryFilesystem::read(const std::filesystem::path& name, const std::function<void(std::istream &)>& reader) const
{
  std::stringstream stream{fs.read(name)};
  reader(stream);
}

bool MemoryFilesystem::fileExists(const std::filesystem::path& name) const
{
  const auto list = fs.listOfFiles();
  const auto idx = list.find(name);
  return idx != list.end();
}

void MemoryFilesystem::write(const std::filesystem::path& name, const std::function<void(std::ostream&)>& writer)
{
  std::stringstream stream{};
  writer(stream);
  fs.write(name, stream.str());
}


}
