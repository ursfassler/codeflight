# language: en

Feature: ast cleanup
  As a user of the tool
  I want to be able to work with an invalid ast
  In order to get good results


@wip
Scenario: add references from class under method to method
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="m1">
                    <class name="ClassA.ClassB" id="1">
                        <reference target="2"/>
                    </class>
                </method>
            </class>
            <function name="f1">
                <reference target="1"/>
            </function>
            <function name="f2" id="2"/>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/ClassA/neighbors.gv"

  Then I expect the log output:
    """
    WARNING: class "ClassA.ClassB" (1) at invalid position, merging with all children into method "m1"

    """
  And I expect to get the output file "-/ClassA/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="bold"
                label="ClassA"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                style="rounded"
                label="f2"
                URL="../f2/index.html"
            ]
            _node2 [
                shape="box"
                style="rounded"
                label="f1"
                URL="../f1/index.html"
            ]
        }
        _node0 -> _node1
        _node2 -> _node0
    }

    """


Scenario: handle all child elements under invalid node
  Given I have the input file:
    """
    <project>
        <package name="">
            <function name="f1">
                <reference target="1"/>
            </function>
            <function name="f2" id="2"/>
            <function name="f3">
                <package name="p2">
                    <class name="ClassA">
                        <method name="m1" id="1">
                            <reference target="2"/>
                        </method>
                    </class>
                </package>
            </function>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/f3/neighbors.gv"

  Then I expect the log output:
    """
    WARNING: package "p2" at invalid position, merging with all children into function "f3"

    """
  And I expect to get the output file "-/f3/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="rounded, bold"
                label="f3"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                style="rounded"
                label="f2"
                URL="../f2/index.html"
            ]
            _node2 [
                shape="box"
                style="rounded"
                label="f1"
                URL="../f1/index.html"
            ]
        }
        _node0 -> _node1
        _node2 -> _node0
    }

    """


Scenario: remove invalid class under method
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <method name="m1">
                    <class name="ClassA.ClassB"/>
                </method>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "all-classes.html"

  Then I expect the log output:
    """
    WARNING: class "ClassA.ClassB" at invalid position, merging with all children into method "m1"

    """
  And I expect to get the output file "all-classes.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>All classes</title>
        </head>
        <body>
            <h1>All classes</h1>
            <table>
                <tr>
                    <th>Name</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/ClassA/index.html">ClassA</a>
                    </td>
                </tr>
            </table>
        </body>
    </html>

    """


Scenario: handle references in references
  Given I have the input file:
    """
    <project>
        <package name="">
            <function name="f1">
                <reference target="2"/>
                    <reference target="3">
                </reference>
            </function>
            <function name="f2" id="2"/>
            <function name="f3" id="3"/>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/f1/neighbors.gv"

  Then I expect the log output:
    """

    """
  And I expect to get the output file "-/f1/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="rounded, bold"
                label="f1"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                style="rounded"
                label="f2"
                URL="../f2/index.html"
            ]
            _node2 [
                shape="box"
                style="rounded"
                label="f3"
                URL="../f3/index.html"
            ]
        }
        _node0 -> _node1
        _node0 -> _node2
    }

    """


Scenario: handle invalid node under package that has references
  Given I have the input file:
    """
    <project>
        <package>
            <method name="m1" id="1">
                <reference target="2"/>
            </method>
            <function name="f1">
                <reference target="1"/>
            </function>
            <function name="f2" id="2"/>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/f1/neighbors.gv"
  And I generate the file "-/f2/neighbors.gv"

  Then I expect the log output:
    """
    WARNING: method "m1" (1) at invalid position, merging with all children into package ""
    WARNING: move all references from method "m1" (1) to anchestor package "": references not allowed under target node, ignoring references
    WARNING: change target of reference function "f1" -> method "m1" (1) to package "": references to target node are not allowed, ignoring reference

    """
  And I expect to get the output file "-/f1/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="rounded, bold"
                label="f1"
                URL="index.html"
            ]
        }
    }

    """
  And I expect to get the output file "-/f2/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="rounded, bold"
                label="f2"
                URL="index.html"
            ]
        }
    }

    """


Scenario: handle invalid node within invalid node
  Given I have the input file:
    """
    <project>
        <package name="">
            <function name="f1">
                <reference target="1"/>
            </function>
            <function name="f2" id="2"/>
            <function name="f3">
                <package name="p2">
                    <class name="ClassA">
                        <method name="m1">
                            <function name="f4" id="1">
                                <reference target="2"/>
                            </function>
                        </method>
                    </class>
                </package>
            </function>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/f3/neighbors.gv"

  Then I expect the log output:
    """
    WARNING: package "p2" at invalid position, merging with all children into function "f3"

    """
  And I expect to get the output file "-/f3/neighbors.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="::"
            URL="../index.html"
            _node0 [
                shape="box"
                style="rounded, bold"
                label="f3"
                URL="index.html"
            ]
            _node1 [
                shape="box"
                style="rounded"
                label="f2"
                URL="../f2/index.html"
            ]
            _node2 [
                shape="box"
                style="rounded"
                label="f1"
                URL="../f1/index.html"
            ]
        }
        _node0 -> _node1
        _node2 -> _node0
    }

    """
