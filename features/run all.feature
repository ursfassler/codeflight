# language: en

Feature: run all
  As a user of the tool
  I want easily get some information about my code
  In order have a successful first interaction with the tool


Scenario: generate all usefull files
  Given I have the file "input/test.ast":
    """
    <project>
        <package name="">
            <package name="p1">
                <class name="ClassA">
                    <method name="methodA">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
            <variable name="variableA" id="1"/>
            <function name="functionA">
                <reference target="1"/>
            </variable>
            <variable name="variableC"/>
        </package>
    </project>
    """

  When I process the file "input/test.ast" with the output directory "output"

  Then I expect to have at least these files:
    | filename                                             |
    | input/test.ast                                       |
    | output/index.html                                    |
    | output/-/content-dependencies.svg                    |
    | output/-/package-cycles.html                         |
    | output/dependencies-between-packages/-~p1,-.svg      |
    | output/-/p1/content-dependencies.svg                 |
    | output/-/p1/package-cycles.html                      |
    | output/-/p1/ClassA/intra-dependencies.svg            |
    | output/-/content-dependencies.svg                    |
    | output/-/p1/content-dependencies.svg                 |


Scenario: print the filename that will be generated
  Given I have the file "input/test.ast":
    """
    <project>
        <package name="">
            <package name="p1">
                <class name="ClassA">
                    <method name="methodA">
                        <reference target="1"/>
                    </method>
                </class>
            </package>
            <variable name="variableA" id="1"/>
            <function name="functionA">
                <reference target="1"/>
            </variable>
            <variable name="variableC"/>
        </package>
    </project>
    """

  When I process the file "input/test.ast" with the output directory "output"

  Then I expect the log output:
    """
    INFO: generate: :index
    INFO: generate: :all-packages
    INFO: generate: :all-classes
    INFO: generate: :all-functions
    INFO: generate: :all-variables
    INFO: generate: :packages-with-cyclic-dependencies
    INFO: generate: :stable-dependency-principle-violations
    INFO: generate: /:index
    INFO: generate: //p1:index
    INFO: generate: //p1/ClassA:index
    INFO: generate: //functionA:index
    INFO: generate: //variableA:index
    INFO: generate: /:content-dependencies
    INFO: generate: /:package-cycles
    INFO: generate: /:neighbors
    INFO: generate: /:package-tree
    INFO: generate: //p1:content-dependencies
    INFO: generate: //p1:package-cycles
    INFO: generate: //p1:neighbors
    INFO: generate: //p1:package-tree
    INFO: generate: //p1/ClassA:intra-dependencies
    INFO: generate: //p1/ClassA:neighbors
    INFO: generate: //functionA:neighbors
    INFO: generate: //variableA:neighbors
    INFO: generate: :dependencies-between-packages://p1:/

    """
