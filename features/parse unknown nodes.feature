# language: en

Feature: parse unknown nodes
  As a user of the tool
  I want to get notified about unknown nodes
  In order to fix my input file


Scenario: report an error for functions in classes
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <function name="Field2"/>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the log output:
    """
    WARNING: function "Field2" at invalid position, merging with all children into class "ClassA"

    """


Scenario: report an error for anything but references in fields
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <field name="Field1" id="f1">
                    <class name="1"/>
                    <reference target="f1"/>
                    <field name="3" id="_3"/>
                    <method name="4"/>
                </field>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the log output:
    """
    WARNING: class "1" at invalid position, merging with all children into field "Field1" (f1)
    WARNING: field "3" (_3) at invalid position, merging with all children into field "Field1" (f1)
    WARNING: method "4" at invalid position, merging with all children into field "Field1" (f1)

    """


Scenario: report an error for anything but references in functions
  Given I have the input file:
    """
    <project>
        <package name="">
            <function name="f1" id="3">
                <class name="1"/>
                <reference target="3"/>
                <field name="3"/>
                <method name="4"/>
            </function>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the log output:
    """
    WARNING: class "1" at invalid position, merging with all children into function "f1" (3)
    WARNING: field "3" at invalid position, merging with all children into function "f1" (3)
    WARNING: method "4" at invalid position, merging with all children into function "f1" (3)

    """


Scenario: report an error for references in packages
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="p1">
                <reference target="1"/>
            </package>
            <function name="foo" id="1"/>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the log output:
    """
    WARNING: reference not allowed under package "p1"

    """


Scenario: report an error for unknown elements
  Given I have the input file:
    """
    <project>
        <package name="the package">
            <lala name="nonono"/>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the log output:
    """
    WARNING: ignore unknown element lala under package "the package" (3:9)

    """
