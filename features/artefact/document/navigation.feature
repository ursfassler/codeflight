# language: en

Feature: document navigation
  As a user of the tool
  I want to naviagte between artefacts
  In order to easily go to an parent package


Scenario: print a breadcrumb navigation
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <class name="ClassA"/>
            <class name="ClassB"/>
            <package name="package1">
                <class name="ClassC"/>
                <package name="package2">
                    <class name="ClassD"/>
                    <class name="ClassE"/>
                </package>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/package1/package2/ClassE/index.html"

  Then I expect to get the output file "-/package1/package2/ClassE/index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>package1::package2::ClassE</title>
        </head>
        <body>
            <nav>
                <a href="../../../../index.html">The Super Project</a>
                &gt;
                <a href="../../../index.html">::</a>
                &gt;
                <a href="../../index.html">package1</a>
                &gt;
                <a href="../index.html">package2</a>
                &gt;
                <a href="index.html">ClassE</a>
            </nav>
            <h1>package1::package2::ClassE</h1>
            <p>
                <ul>
                    <li>
                        <a href="intra-dependencies.svg">Member dependencies</a>
                    </li>
                    <li>
                        <a href="neighbors.svg">Class neighbors</a>
                    </li>
                </ul>
            </p>
        </body>
    </html>

    """
