# language: en

Feature: document language features
  As a user of the tool
  I want to see language specific output
  In order to have a familiar look


Scenario Outline: use language specific separators
  Given I have the input file:
    """
    <project name="The Super Project" language="<language>">
        <package name="">
            <class name="ClassA"/>
            <class name="ClassB"/>
            <package name="package1">
                <class name="ClassC"/>
                <package name="package2">
                    <class name="ClassD"/>
                    <class name="ClassE"/>
                </package>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "all-classes.html"
  And I process the queue until empty

  Then I expect to get the output file "all-classes.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>All classes</title>
        </head>
        <body>
            <h1>All classes</h1>
            <table>
                <tr>
                    <th>Name</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/ClassA/index.html">ClassA</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/ClassB/index.html">ClassB</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/ClassC/index.html">package1<sep>ClassC</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/package2/ClassD/index.html">package1<sep>package2<sep>ClassD</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/package2/ClassE/index.html">package1<sep>package2<sep>ClassE</a>
                    </td>
                </tr>
            </table>
        </body>
    </html>

    """
    And I expect to get the output file "-/ClassA/index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>ClassA</title>
        </head>
        <body>
            <nav>
                <a href="../../index.html">The Super Project</a>
                &gt;
                <a href="../index.html"><sep></a>
                &gt;
                <a href="index.html">ClassA</a>
            </nav>
            <h1>ClassA</h1>
            <p>
                <ul>
                    <li>
                        <a href="intra-dependencies.svg">Member dependencies</a>
                    </li>
                    <li>
                        <a href="neighbors.svg">Class neighbors</a>
                    </li>
                </ul>
            </p>
        </body>
    </html>

    """

  Examples:
    | language   | sep |
    | C++        | ::  |
    | Python     | .   |
    | TypeScript | /   |
