# language: en

Feature: graph language features
  As a user of the tool
  I want to see language specific output
  In order to have a familiar look


Scenario Outline: use language specific separators
  Given I have the input file:
    """
    <project language="<language>">
        <package name="">
            <package name="package1">
                <class name="ClassA">
                    <method name="Method1" id="m1"/>
                </class>
                <package name="package2">
                    <class name="ClassB">
                        <method name="Method2">
                            <reference target="m1"/>
                        </method>
                    </class>
                </package>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "dependencies-between-packages/-~package1~package2,-~package1.gv"

  Then I expect to get the output file "dependencies-between-packages/-~package1~package2,-~package1.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="package1<sep>package2"
            URL="../-/package1/package2/index.html"
            _node0 [
                shape="box"
                label="ClassB"
                URL="../-/package1/package2/ClassB/index.html"
            ]
        }
        subgraph cluster_1 {
            label="package1"
            URL="../-/package1/index.html"
            _node1 [
                shape="box"
                label="ClassA"
                URL="../-/package1/ClassA/index.html"
            ]
        }
        _node0 -> _node1 [
            label="1"
            URL="-~package1~package2,-~package1.svg"
        ]
    }

    """

  Examples:
    | language   | sep |
    | C++        | ::  |
    | Python     | .   |
    | TypeScript | /   |
