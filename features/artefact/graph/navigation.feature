# language: en

Feature: graph navigation
  As an use of the tool
  I want to naviagte between artefacts
  In order to easily reach the information I need


Scenario: use correct links in sub package
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="p1">
                <package name="p2">
                    <class name="ClassA"/>
                </package>
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/p1/p2/content-dependencies.gv"

  Then I expect to get the output file "-/p1/p2/content-dependencies.gv":
    """
    digraph {
        rankdir=LR
        label="p1::p2"
        _node0 [
            shape="box"
            label="ClassA"
            URL="ClassA/index.html"
        ]
    }

    """
