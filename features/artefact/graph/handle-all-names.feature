# language: en

Feature: handle all names
  As an user of the tool
  I want to get the output independent of the use names
  In order to not care about the implementation of the tool


Scenario: correctly return dependencies between packages when the package name has a dot in it
  Given I have the input file:
    """
    <project name="The Super Project">
        <package name="">
            <package name="p1.1">
            </package>
            <package name="p2.2">
            </package>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "dependencies-between-packages/-~p1.1,-~p2.2.svg"

  Then I expect to get the output file "dependencies-between-packages/-~p1.1,-~p2.2.svg":
    """
    generated from dependencies-between-packages/-~p1.1,-~p2.2.gv
    """
