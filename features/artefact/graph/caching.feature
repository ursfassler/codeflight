# language: en

Feature: caching
  As a user of the tool
  I want a fast tool
  So that I don't have to wait too long for the results


Scenario: don't run dot when the graph didn't change
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="p1">
                <class name="ClassA" id="1"/>
            </package>
            <package name="p2">
                <class name="ClassB">
                    <reference target="1"/>
                </class>
            </package>
        </package>
    </project>
    """
  And I have the file "dependencies-between-packages/-~p1,-~p2.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="p1"
            URL="../-/p1/index.html"
            _node0 [
                shape="box"
                label="ClassA"
                URL="../-/p1/ClassA/index.html"
            ]
        }
        subgraph cluster_1 {
            label="p2"
            URL="../-/p2/index.html"
            _node1 [
                shape="box"
                label="ClassB"
                URL="../-/p2/ClassB/index.html"
            ]
        }
        _node1 -> _node0 [
            label="1"
            URL="-~p2,-~p1.svg"
        ]
    }

    """
  And I have the file "dependencies-between-packages/-~p1,-~p2.svg":
    """
    old content
    """

  When I parse the input
  And I generate the file "dependencies-between-packages/-~p1,-~p2.svg"

  Then I expect to get the output file "dependencies-between-packages/-~p1,-~p2.svg":
    """
    old content
    """


Scenario: run dot when the graph didn't change but there is no svg
  Given I have the input file:
    """
    <project>
        <package name="">
            <package name="p1">
                <class name="ClassA" id="1"/>
            </package>
            <package name="p2">
                <class name="ClassB">
                    <reference target="1"/>
                </class>
            </package>
        </package>
    </project>
    """
  And I have the file "dependencies-between-packages/-~p1,-~p2.gv":
    """
    digraph {
        rankdir=LR
        subgraph cluster_0 {
            label="p1"
            URL="../-/p1/index.html"
            _node0 [
                shape="box"
                label="ClassA"
                URL="../-/p1/ClassA/index.html"
            ]
        }
        subgraph cluster_1 {
            label="p2"
            URL="../-/p2/index.html"
            _node1 [
                shape="box"
                label="ClassB"
                URL="../-/p2/ClassB/index.html"
            ]
        }
        _node1 -> _node0 [
            label="1"
        ]
    }

    """

  When I parse the input
  And I generate the file "dependencies-between-packages/-~p1,-~p2.svg"

  Then I expect to get the output file "dependencies-between-packages/-~p1,-~p2.svg":
    """
    generated from dependencies-between-packages/-~p1,-~p2.gv
    """
