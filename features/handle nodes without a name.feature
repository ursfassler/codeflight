# language: en

Feature: handle nodes without a name
  As a user of the tool
  I want to navigate through nodes without names
  In order to analyse every project


Scenario: generate a name for a nameless class
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name=""/>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "all-classes.html"
  And I process the queue until empty
  Then I expect to get the output file "all-classes.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>All classes</title>
        </head>
        <body>
            <h1>All classes</h1>
            <table>
                <tr>
                    <th>Name</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/-unnamed1-/index.html">-unnamed1-</a>
                    </td>
                </tr>
            </table>
        </body>
    </html>

    """
  And I expect to get the output file "-/-unnamed1-/index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <title>-unnamed1-</title>
        </head>
        <body>
            <nav>
                <a href="../../index.html">Project</a>
                &gt;
                <a href="../index.html">::</a>
                &gt;
                <a href="index.html">-unnamed1-</a>
            </nav>
            <h1>-unnamed1-</h1>
            <p>
                <ul>
                    <li>
                        <a href="intra-dependencies.svg">Member dependencies</a>
                    </li>
                    <li>
                        <a href="neighbors.svg">Class neighbors</a>
                    </li>
                </ul>
            </p>
        </body>
    </html>

    """
