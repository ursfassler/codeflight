# language: en

Feature: parse invalid references
  As a user of the tool
  I want to get notified about invalid references
  In order to fix my input file


Scenario: report an invalid reference
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <reference target="x"/>
            </class>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the log output:
    """
    WARNING: reference target not found: x

    """


Scenario: print graph without invalid reference
  Given I have the input file:
    """
    <project>
        <package name="">
            <class name="ClassA">
                <reference target="x"/>
            </class>
        </package>
    </project>
    """

  When I parse the input
  And I generate the file "-/content-dependencies.gv"

  Then I expect to get the output file "-/content-dependencies.gv":
    """
    digraph {
        rankdir=LR
        label="::"
        _node0 [
            shape="box"
            label="ClassA"
            URL="ClassA/index.html"
        ]
    }

    """


Scenario: report not supported reference to package
  Given I have the input file:
    """
    <project language="Python">
        <package name="top">
            <class name="ClassA">
                <method name="m1">
                    <reference target="1"/>
                </method>
            </class>
            <package name="p1" id="1"/>
        </package>
    </project>
    """

  When I parse the input

  Then I expect the log output:
    """
    WARNING: reference to package is not supported, target: 1

    """
