# language: en

Feature: add CSS an JavaScript
  As a user of the tool
  I want the result presented in a nice way
  In order see the relevant information easily


Scenario: add a provided CSS and JavaScript
  Given I have the file "input/test.ast":
    """
    <project name="The Super Project">
        <package name="">
            <class name="ClassA"/>
            <class name="ClassB"/>
            <package name="package1">
                <class name="ClassC"/>
            </package>
            <package name="package2"/>
        </package>
    </project>
    """
  And I have the file "input/res/style1.css":
    """
    // style 1
    """
  And I have the file "input/res/style2.css":
    """
    """
  And I have the file "input/res/other.txt":
    """
    """
  And I have the file "input/res/script1.js":
    """
    """
  And I set the resources directory to "input/res/"

  When I process the file "input/test.ast" with the output directory "output"

  Then I expect to get the output file "output/all-packages.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <script src="resources/script1.js"></script>
            <link rel="stylesheet" href="resources/style1.css" />
            <link rel="stylesheet" href="resources/style2.css" />
            <title>All packages</title>
        </head>
        <body>
            <h1>All packages</h1>
            <table class="string-numbers">
                <tr>
                    <th>Name</th>
                    <th>Packages</th>
                    <th>Classes</th>
                    <th>Functions</th>
                    <th>Variables</th>
                    <th>LCOM4</th>
                    <th>fan-in</th>
                    <th>fan-out</th>
                    <th>instability</th>
                    <th>has cycles</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/index.html">::</a>
                    </td>
                    <td>2</td>
                    <td>2</td>
                    <td>0</td>
                    <td>0</td>
                    <td>2</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0.00</td>
                    <td>false</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/index.html">package1</a>
                    </td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0.00</td>
                    <td>false</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package2/index.html">package2</a>
                    </td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0.00</td>
                    <td>false</td>
                </tr>
            </table>
        </body>
    </html>

    """
  And I expect to get the output file "output/-/ClassA/index.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <script src="../../resources/script1.js"></script>
            <link rel="stylesheet" href="../../resources/style1.css" />
            <link rel="stylesheet" href="../../resources/style2.css" />
            <title>ClassA</title>
        </head>
        <body>
            <nav>
                <a href="../../index.html">The Super Project</a>
                &gt;
                <a href="../index.html">::</a>
                &gt;
                <a href="index.html">ClassA</a>
            </nav>
            <h1>ClassA</h1>
            <p>
                <ul>
                    <li>
                        <a href="intra-dependencies.svg">Member dependencies</a>
                    </li>
                    <li>
                        <a href="neighbors.svg">Class neighbors</a>
                    </li>
                </ul>
            </p>
        </body>
    </html>

    """
  And I expect to get the output file "output/resources/style1.css":
    """
    // style 1
    """
  And I expect to have at least these files:
    | filename                                             |
    | output/index.html                                    |
    | output/resources/style1.css                          |
    | output/resources/style2.css                          |
    | output/resources/script1.js                          |


Scenario: provide information about the table type
  Given I have the file "input/test.ast":
    """
    <project name="The Super Project">
        <package name="">
            <package name="package1">
                <class name="ClassA" id="1">
                    <reference target="2"/>
                </class>
            </package>
            <package name="package2">
                <class name="ClassB" id="2">
                    <reference target="1"/>
                </class>
            </package>
        </package>
    </project>
    """
  And I have the file "input/res/style.css":
    """
    """
  And I set the resources directory to "input/res/"

  When I process the file "input/test.ast" with the output directory "output"

  Then I expect to get the output file "output/all-packages.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <link rel="stylesheet" href="resources/style.css" />
            <title>All packages</title>
        </head>
        <body>
            <h1>All packages</h1>
            <table class="string-numbers">
                <tr>
                    <th>Name</th>
                    <th>Packages</th>
                    <th>Classes</th>
                    <th>Functions</th>
                    <th>Variables</th>
                    <th>LCOM4</th>
                    <th>fan-in</th>
                    <th>fan-out</th>
                    <th>instability</th>
                    <th>has cycles</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/index.html">::</a>
                    </td>
                    <td>2</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0.00</td>
                    <td>true</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/index.html">package1</a>
                    </td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0.50</td>
                    <td>false</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package2/index.html">package2</a>
                    </td>
                    <td>0</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                    <td>0.50</td>
                    <td>false</td>
                </tr>
            </table>
        </body>
    </html>

    """
  And I expect to get the output file "output/packages-with-cyclic-dependencies.html":
    """
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8" />
            <link rel="stylesheet" href="resources/style.css" />
            <title>Packages with cyclic dependencies</title>
        </head>
        <body>
            <h1>Packages with cyclic dependencies</h1>
            <table class="string-numbers">
                <tr>
                    <th>metric</th>
                    <th>value</th>
                </tr>
                <tr>
                    <td>Has cycles</td>
                    <td>true</td>
                </tr>
                <tr>
                    <td>Number of involved packages</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>Total number of packages</td>
                    <td>3</td>
                </tr>
                <tr>
                    <td>Packages in cycles ratio</td>
                    <td>0.667</td>
                </tr>
                <tr>
                    <td>Number of involved dependencies</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>Total number of dependencies</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>Dependencies in cycles ratio</td>
                    <td>1.000</td>
                </tr>
                <tr>
                    <td>Number of cycles</td>
                    <td>1</td>
                </tr>
            </table>
            <h2>Cycle cluster 1</h2>
            <h3>Involved packages</h3>
            <table class="string-numbers">
                <tr>
                    <th>Package</th>
                    <th>cycle fan-in</th>
                    <th>cycle fan-out</th>
                    <th>in cycles</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/index.html">package1</a>
                    </td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package2/index.html">package2</a>
                    </td>
                    <td>1</td>
                    <td>1</td>
                    <td>1</td>
                </tr>
            </table>
            <h3>Involved dependencies</h3>
            <table class="string-string-numbers">
                <tr>
                    <th>from</th>
                    <th>to</th>
                    <th>references</th>
                    <th>stability</th>
                    <th>in cycles</th>
                </tr>
                <tr>
                    <td>
                        <a href="-/package1/index.html">package1</a>
                    </td>
                    <td>
                        <a href="-/package2/index.html">package2</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~package1,-~package2.svg">1</a>
                    </td>
                    <td>0.000</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>
                        <a href="-/package2/index.html">package2</a>
                    </td>
                    <td>
                        <a href="-/package1/index.html">package1</a>
                    </td>
                    <td>
                        <a href="dependencies-between-packages/-~package2,-~package1.svg">1</a>
                    </td>
                    <td>0.000</td>
                    <td>1</td>
                </tr>
            </table>
        </body>
    </html>

    """
