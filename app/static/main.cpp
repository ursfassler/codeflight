/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "component/adapter/io/findSystemResources.h"
#include "component/adapter/io/StdFilesystem.h"
#include "component/adapter/io/StdProcess.h"
#include "component/adapter/io/StreamLogger.h"
#include "component/adapter/xml/reader/TinyReader.h"
#include "component/adapter/xml/writer/TinyWriter.h"
#include "component/astparser/astparser.h"
#include "component/codeflight/Codeflight.h"
#include "component/codeflight/artefact/common/package-cycles/Settings.h"
#include "component/codeflight/ast/Project.h"
#include "component/driver/Driver.h"
#include "component/web/Web.h"
#include <tclap/CmdLine.h>
#include <vector>


namespace app::static_
{


int main(int argc, char* argv[])
{
  std::filesystem::path filename{};
  std::filesystem::path outdir{};
  std::optional<std::filesystem::path> resources{};

  try {
    TCLAP::CmdLine cmd{"generating artefacts and metrics"};

    TCLAP::UnlabeledValueArg<std::string> astArg{"ast-file", "file containing the AST", true, {}, "filename"};
    cmd.add(astArg);

    TCLAP::UnlabeledValueArg<std::string> outArg{"output-directory", "where to place the output", true, {}, "output directory"};
    cmd.add(outArg);

    TCLAP::ValueArg<std::string> resArg("r", "resources", "directory for resources to add to the output", false, {}, "directory");
    cmd.add(resArg);

    cmd.parse(argc, argv);

    filename = astArg.getValue();
    outdir = outArg.getValue();
    resources = resArg.isSet() ? resArg.getValue() : resources;
  } catch (TCLAP::ArgException &e) {
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    return -1;
  }

  if (!resources) {
    resources = component::adapter::io::findSystemResources("codeflight-static");
  }

  component::adapter::io::StdFilesystem fs{};
  component::adapter::io::StdProcess process{};
  component::adapter::io::StreamLogger logger{std::cout, std::cerr};
  component::adapter::xml::reader::TinyReader xmlReader{};
  component::adapter::xml::writer::TinyFactory xmlFactory{};

  auto project = component::astparser::load(filename, xmlReader, fs, logger);

  const auto codeflight = component::codeflight::Codeflight::produce(
        project,
        {},
        {}
        );

  const auto web = component::web::Web::produce(
        *codeflight,
        xmlFactory,
        resources,
        outdir,
        fs,
        process
        );

  component::driver::Driver driver{*codeflight, *web, logger};
  driver.processAll();

  return 0;
}


}


int main(int argc, char* argv[])
{
  return app::static_::main(argc, argv);
}
