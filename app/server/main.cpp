/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "component/codeflight/Codeflight.h"
#include "EventServer.h"
#include "SignalHandler.h"
#include "TcpServer.h"
#include "component/adapter/io/StdFilesystem.h"
#include "component/adapter/io/StdProcess.h"
#include "component/adapter/io/StreamLogger.h"
#include "component/adapter/io/findSystemResources.h"
#include "component/adapter/xml/reader/TinyReader.h"
#include "component/adapter/xml/writer/TinyWriter.h"
#include "component/astparser/astparser.h"
#include "component/codeflight/artefact/common/package-cycles/Settings.h"
#include "component/codeflight/artefact/factory/Factory.h"
#include "component/codeflight/artefact/generator/Generator.h"
#include "component/codeflight/ast/Project.h"
#include "component/codeflight/ast/repository/Neighbor.h"
#include "component/codeflight/metric/AllRepository.h"
#include "component/http/Http.h"
#include "component/web/Web.h"
#include "component/web/io/ResourceFilesystem.h"
#include "component/web/library/Url.h"
#include "component/web/resource/Factory.h"
#include "component/web/resource/Resource.h"
#include "tmpdir.h"
#include <tclap/CmdLine.h>
#include <vector>


namespace app::server
{
namespace
{


void serve(
    const std::filesystem::path& filename,
    const std::optional<std::filesystem::path>& resourcesDirectory,
    component::adapter::io::StdFilesystem& fs,
    component::adapter::io::StdProcess& process,
    const component::adapter::io::StreamLogger& logger
    )
{
  const std::filesystem::path outdir = createTemporaryDirectory();
  logger.info("use directory: " + outdir.string());

  component::adapter::xml::reader::TinyReader xmlReader{};
  component::adapter::xml::writer::TinyFactory xmlFactory{};

  auto project = component::astparser::load(filename, xmlReader, fs, logger);

  const auto codeflight = component::codeflight::Codeflight::produce(
        project,
        {},
        {}
        );

  const auto web = component::web::Web::produce(
        *codeflight,
        xmlFactory,
        resourcesDirectory,
        outdir,
        fs,
        process
        );

  component::http::Http http{
    *codeflight,
    *web,
    "\r\n",
    outdir,
    fs,
    logger
  };

  EventServer eventServer{logger};
  TcpServer tcpServer{http, eventServer, logger};
  eventServer.addListener(tcpServer.getFd(), &tcpServer);
  SignalHandler signalHandler{[&eventServer](int){
      eventServer.stop();
    }};
  eventServer.addListener(signalHandler.getFd(), &signalHandler);

  eventServer.run();

  eventServer.removeListener(signalHandler.getFd());
  tcpServer.closeConnections();
  eventServer.removeListener(tcpServer.getFd());
  std::filesystem::remove_all(outdir);
}


}


int main(int argc, char* argv[])
{
  std::filesystem::path filename{};
  std::optional<std::filesystem::path> resources{};

  try {
    TCLAP::CmdLine cmd{"serve artefacts and metrics"};

    TCLAP::UnlabeledValueArg<std::string> astArg{"ast-file", "file containing the AST", true, {}, "filename"};
    cmd.add(astArg);

    TCLAP::ValueArg<std::string> resArg("r", "resources", "directory for resources to add to the output", false, {}, "directory");
    cmd.add(resArg);

    cmd.parse(argc, argv);

    filename = astArg.getValue();
    resources = resArg.isSet() ? resArg.getValue() : resources;
  } catch (TCLAP::ArgException &e) {
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    return -1;
  }

  if (!resources) {
    resources = component::adapter::io::findSystemResources("codeflight-server");
  }

  component::adapter::io::StdFilesystem fs{};
  component::adapter::io::StdProcess process{};
  component::adapter::io::StreamLogger logger{std::cout, std::cerr};

  serve(filename, resources, fs, process, logger);

  return 0;
}


}


int main(int argc, char* argv[])
{
  return app::server::main(argc, argv);
}
