/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "SignalHandler.h"

#include <csignal>
#include <cstdlib>
#include <stdexcept>
#include <sys/signalfd.h>
#include <unistd.h>


namespace app::server
{
namespace
{


int createFd()
{
  sigset_t mask{};

  sigemptyset(&mask);
  sigaddset(&mask, SIGINT);
  sigaddset(&mask, SIGQUIT);
  sigaddset(&mask, SIGTERM);
  sigaddset(&mask, SIGHUP);

  // Block signals so that they aren't handled according to their default dispositions.
  const auto spm = sigprocmask(SIG_BLOCK, &mask, nullptr);
  if (spm < 0) {
    throw std::runtime_error("unable to set sigprocmask: " + std::to_string(errno));
  }

  const auto sfd = signalfd(-1, &mask, 0);
  if (sfd < 0) {
    throw std::runtime_error("unable to create signalfd: " + std::to_string(errno));
  }

  return sfd;
}


}


SignalHandler::SignalHandler(const SignalListener& listener_) :
  fd{createFd()},
  listener{listener_}
{
}

void SignalHandler::onEventIn(int)
{
  struct signalfd_siginfo fdsi{};
  const auto s = read(fd, &fdsi, sizeof(fdsi));
  if (s != sizeof(fdsi)) {
    throw std::runtime_error("unable to read signal: " + std::to_string(errno));
  }

  listener(fdsi.ssi_signo);
}

void SignalHandler::onEventOut(int)
{
}

int SignalHandler::getFd() const
{
  return fd;
}


}
