/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "tmpdir.h"
#include <random>


namespace app::server
{


std::filesystem::path createTemporaryDirectory()
{
  const auto tmp_dir = std::filesystem::temp_directory_path();
  std::random_device dev;
  std::mt19937 prng(dev());
  std::uniform_int_distribution<uint64_t> rand(0);

  const auto MaxAttempts = 1000;
  for (std::size_t i = 0; i < MaxAttempts; i++) {
    std::stringstream ss;
    ss << std::hex << rand(prng);
    auto path = tmp_dir / ss.str();
    if (std::filesystem::create_directory(path)) {
      return path;
    }
  }

  throw std::runtime_error("could not find non-existing directory");
}


}
