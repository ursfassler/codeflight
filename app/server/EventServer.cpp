/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "EventServer.h"
#include "component/adapter/io/StreamLogger.h"
#include <stdexcept>
#include <sys/epoll.h>
#include <unistd.h>


namespace app::server
{
namespace
{


constexpr auto EventIn = EPOLLIN;
constexpr auto EventOut = EPOLLOUT;

bool isSet(unsigned int value, unsigned int mask)
{
  return (value & mask) != 0;
}

unsigned int clear(unsigned int value, unsigned int mask)
{
  return value & ~mask;
}


}


EventServer::EventServer(
    const component::adapter::io::StreamLogger& logger_
    ) :
  logger{logger_},
  epollfd{epoll_create1(0)}
{
  if (epollfd == -1) {
    throw std::runtime_error("unable to create epoll");
  }
}

EventServer::~EventServer()
{
  close(epollfd);
}

void EventServer::run() const
{
  while (running) {
    struct epoll_event event{};

    const auto nfds = epoll_wait(epollfd, &event, 1, -1);
    if (nfds == -1) {
      throw std::runtime_error("unable to wait epoll");
    }

    if (nfds == 1) {
      auto events = event.events;
      const auto fd = event.data.fd;

      if (isSet(events, EventIn)) {
        const auto idx = listeners.find(fd);
        if (idx != listeners.end()) {
          events = clear(events, EventIn);
          idx->second->onEventIn(fd);
        }
      }
      if (isSet(events, EventOut)) {
        const auto idx = listeners.find(fd);
        if (idx != listeners.end()) {
          events = clear(events, EventOut);
          idx->second->onEventOut(fd);
        }
      }

      if (events == event.events) {
        logger.error("unhandled events \"" + std::to_string(events) + "\" for fd \"" + std::to_string(fd) + "\"");
      }
    }
  }
}

void EventServer::stop()
{
  running = false;
}

void EventServer::addListener(int fd, EventListener* listener)
{
  control(EPOLL_CTL_ADD, EventIn, fd);
  listeners[fd] = listener;
}

void EventServer::removeListener(int fd)
{
  listeners.erase(fd);
  control(EPOLL_CTL_DEL, {}, fd);
}

void EventServer::enableEventOut(int fd)
{
  control(EPOLL_CTL_MOD, EventIn | EventOut, fd);
}

void EventServer::disableEventOut(int fd)
{
  control(EPOLL_CTL_MOD, EventIn, fd);
}

void EventServer::control(int operation, uint32_t events, int fd)
{
  struct epoll_event ev{};
  ev.events = events;
  ev.data.fd = fd;
  const auto ret = epoll_ctl(epollfd, operation, fd, &ev);
  if (ret == -1) {
    throw std::runtime_error("unable to modify epoll");
  }
}


}
