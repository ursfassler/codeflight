/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "EventServer.h"
#include <functional>


namespace app::server
{


using SignalListener = std::function<void(int)>;


class SignalHandler :
    public EventListener
{
public:
  SignalHandler(const SignalListener&);
  void onEventIn(int) override;
  void onEventOut(int) override;
  int getFd() const;

private:
  const int fd;
  const SignalListener listener;

};


}
