/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */


#include <filesystem>


namespace app::server
{


std::filesystem::path createTemporaryDirectory();


}
