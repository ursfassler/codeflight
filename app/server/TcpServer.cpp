/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "TcpServer.h"
#include "component/adapter/io/StreamLogger.h"
#include "component/http/Http.h"
#include <cstdlib>
#include <cstring>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>


namespace app::server
{
namespace
{


void errnoException(std::string message)
{
  char buffer[100];
  const auto ret = ::strerror_r(errno, buffer, sizeof(buffer));
  message += ": " + std::string(ret);
  throw std::runtime_error(message);
}

int createServerFd(uint16_t port)
{
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd == -1) {
    errnoException("Failed to create socket");
  }

  int reuseaddr = 1;
  const auto reopt = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (const void *)&reuseaddr, sizeof(int));
  if (reopt < 0) {
    errnoException("Failed to set reuse option");
  }

  sockaddr_in sockaddr{};
  sockaddr.sin_family = AF_INET;
  sockaddr.sin_addr.s_addr = INADDR_ANY;
  sockaddr.sin_port = htons(port);
  if (bind(sockfd, (struct sockaddr*)&sockaddr, sizeof(sockaddr)) < 0) {
    errnoException("Failed to bind to port " + std::to_string(port));
  }

  const auto MaxConnectionsInQueue = 0;
  if (listen(sockfd, MaxConnectionsInQueue) < 0) {
    errnoException("Failed to listen on socket");
  }

  return sockfd;
}


}


TcpServer::TcpServer(
    const component::http::Http& http_,
    EventServer& eventServer_,
    const component::adapter::io::StreamLogger& logger_
    ) :
  fd{createServerFd(port)},
  http{http_},
  eventServer{eventServer_},
  logger{logger_}
{
  logger.info("Listening on port: " + std::to_string(port));
}

TcpServer::~TcpServer()
{
  const auto ret = close(fd);
  if (ret < 0) {
    logger.error("Failed to shutdown server");
  } else {
    logger.info("server shutdown");
  }
}

int TcpServer::getFd() const
{
  return fd;
}

int TcpServer::acceptConnection() const
{
  int connection = accept(fd, nullptr, nullptr);
  if (connection < 0) {
    errnoException("Failed to accept connection");
  }

  return connection;
}

void TcpServer::closeClientConnection(int fd)
{
  eventServer.removeListener(fd);

  const auto sht = shutdown(fd, SHUT_RDWR);
  if (sht < 0) {
    logger.error("Failed to shutdown connection");
  }

  connections.erase(fd);
}

void TcpServer::onEventIn(int eventFd)
{
  if (eventFd == fd) {
    int connectionFd = acceptConnection();

    const auto sender = [connectionFd, this](const std::string& data) -> int {
      const auto sent = ::send(connectionFd, data.data(), data.size(), 0);
      eventServer.enableEventOut(connectionFd);
      return sent;
    };

    connections.emplace(connectionFd, http.produceConnection(sender));

    eventServer.addListener(connectionFd, this);
  } else {
    const auto idx = connections.find(eventFd);
    if (idx == connections.end()) {
      logger.error("Event in for unknown fd: " + std::to_string(eventFd));
      return;
    }
    std::unique_ptr<component::http::Connection>& connection = idx->second;

    char buffer[4096];
    auto bytesRead = ::read(eventFd, buffer, sizeof(buffer));
    if (bytesRead <= 0) {
      closeClientConnection(eventFd);
    } else {
      connection->received(std::string(buffer, bytesRead));

      if (connection->isFinished()) {
        closeClientConnection(eventFd);
      }
    }
  }
}

void TcpServer::onEventOut(int eventFd)
{
  if (eventFd == fd) {
    return;
  } else {
    const auto idx = connections.find(eventFd);
    if (idx == connections.end()) {
      logger.error("Event out for unknown fd: " + std::to_string(eventFd));
      return;
    }
    std::unique_ptr<component::http::Connection>& connection = idx->second;

    eventServer.disableEventOut(eventFd);

    connection->sent();

    if (connection->isFinished()) {
      closeClientConnection(eventFd);
    }
  }
}

void TcpServer::closeConnections()
{
  while (true) {
    const auto idx = connections.begin();
    if (idx == connections.end()) {
      return;
    }

    closeClientConnection(idx->first);
  }
}


}
