/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "EventServer.h"
#include "component/http/Connection.h"
#include <cstdint>
#include <string>

namespace component::adapter::io
{

class StreamLogger;

}

namespace component::http
{

class Http;

}

namespace app::server
{


class TcpServer :
    public EventListener
{
public:
  TcpServer(
      const component::http::Http&,
      EventServer&,
      const component::adapter::io::StreamLogger&
  );
  ~TcpServer() override;

  int getFd() const;
  void onEventIn(int) override;
  void onEventOut(int) override;

  void closeConnections();

private:
  const uint16_t port{6789};
  const int fd;
  const component::http::Http& http;
  EventServer& eventServer;
  const component::adapter::io::StreamLogger& logger;

  int acceptConnection() const;
  std::map<int, std::unique_ptr<component::http::Connection>> connections{};
  void closeClientConnection(int);

};


}
