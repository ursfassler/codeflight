/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <cstdint>
#include <map>

namespace component::adapter::io
{

class StreamLogger;

}

namespace app::server
{


class EventListener
{
public:
  virtual ~EventListener() = default;

  virtual void onEventIn(int) = 0;
  virtual void onEventOut(int) = 0;
};


class EventServer
{
public:
  EventServer(
      const component::adapter::io::StreamLogger&
  );
  ~EventServer();

  void run() const;
  void stop();
  void addListener(int, EventListener*);
  void removeListener(int);
  void enableEventOut(int);
  void disableEventOut(int);

private:
  const component::adapter::io::StreamLogger& logger;
  const int epollfd;
  bool running{true};
  std::map<int, EventListener*> listeners{};

  void control(int operation, std::uint32_t events, int fd);
};


}
