/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ErrorPage.h"
#include "RequestHandler.h"
#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"
#include "component/web/generator/WebGenerator.h"
#include <filesystem>

namespace component::web::io
{

class ResourceFilesystem;

}

namespace component::codeflight
{

class Codeflight;

}

namespace component::web
{

class Web;

}

namespace component::http
{


class Http
{
public:
  Http(
      codeflight::Codeflight&,
      web::Web&,
      std::string newline_,
      std::filesystem::path outdir,
      web::io::ResourceFilesystem&,
      const io::Logger&
      );

  std::unique_ptr<Connection> produceConnection(const Connection::Sender&) const;

private:
  codeflight::Codeflight& codeflight;
  web::Web& web;
  std::string newline;
  ErrorPage errorPage;
  RequestHandler requestHandler;
  const io::Logger& logger;
};


}
