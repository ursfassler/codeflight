/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Status.h"
#include "Response.h"
#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"
#include "component/web/library/Url.h"
#include <optional>

namespace component::web::output::html
{

class WriterFactory;
class Writer;

}

namespace component::http
{


class ErrorPage
{
public:
  ErrorPage(
      const codeflight::artefact::interface::ArtefactIdentifier&,
      const web::output::html::WriterFactory&
      );

  http::Response generate(http::Status, const std::optional<web::library::Url>&) const;

private:
  const codeflight::artefact::interface::ArtefactIdentifier rootAi;
  const web::output::html::WriterFactory& htmlFactory;

  void printDocument(http::Status, bool hasUrl, web::output::html::Writer&) const;
};


}
