/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace component::http
{


enum class Status
{
  _200_OK,
  _400_Bad_Request,
  _404_Not_Found,
};


uint_least16_t getStatusCode(Status);
std::string getStatusText(Status);


}
