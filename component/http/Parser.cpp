/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Parser.h"


namespace component::http
{


std::optional<web::library::Url> parse(const std::string& request)
{
  const auto start = request.find(' ', 0);
  const auto end = request.find(' ', start+1);
  const auto urlPart = request.substr(start+1, end-start-1);
  auto url = web::library::Url::parse(urlPart);
  return url;
}


}
