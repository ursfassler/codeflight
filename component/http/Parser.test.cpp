/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Parser.h"
#include <gmock/gmock.h>


namespace component::http::unit_test
{
namespace
{

using namespace testing;


TEST(HttpParser_Test, get_root_url)
{
  const std::string request =
      "GET / HTTP/1.1\r\n"
      "\r\n"
      "\r\n";

  const auto actual = parse(request);

  ASSERT_EQ(web::library::Url{}, actual);
}

TEST(HttpParser_Test, get_larger_url)
{
  const std::string request =
      "GET /hello/world.html HTTP/1.1\r\n"
      "\r\n"
      "\r\n";

  const auto actual = parse(request);

  ASSERT_EQ(web::library::Url::parse("/hello/world.html"), actual);
}


}
}
