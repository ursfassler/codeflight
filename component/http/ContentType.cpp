/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ContentType.h"
#include <stdexcept>


namespace component::http
{


std::string getType(ContentType type)
{
  switch (type) {
    case ContentType::Html:
      return "text/html";
    case ContentType::Svg:
      return "image/svg+xml";
    case ContentType::Css:
      return "text/css";
    case ContentType::JavaScript:
      return "text/javascript";
  }

  throw std::runtime_error("unhandled content type: " + std::to_string(static_cast<int>(type)));
}


}
