/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/web/library/Url.h"
#include <optional>
#include <string>

namespace component::http
{


std::optional<web::library::Url> parse(const std::string&);


}
