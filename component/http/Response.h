/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ContentType.h"
#include "Status.h"
#include <string>

namespace component::http
{


struct Content
{
  ContentType type;
  std::string data;
};

struct Response
{
  Status status;
  Content content{};
};


}
