/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "RequestHandler.h"
#include "ErrorPage.h"
#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"
#include "component/web/generator/WebGenerator.h"
#include "component/web/io/ResourceFilesystem.h"
#include "component/web/library/urlFromAi.h"

namespace component::http
{
namespace
{


ContentType typeFromRepresentation(const std::string& representation)
{
  if (representation == ".html") {
    return http::ContentType::Html;
  }

  if (representation == ".svg") {
    return http::ContentType::Svg;
  }

  if (representation == ".css") {
    return http::ContentType::Css;
  }

  if (representation == ".js") {
    return http::ContentType::JavaScript;
  }

  throw std::runtime_error("unhandled representation: " + representation);
}


}


RequestHandler::RequestHandler(
    const web::generator::WebGenerator& generator_,
    const web::library::Url& rootUrl_,
    const ErrorPage& errorPage_,
    const std::filesystem::path& root_,
    web::io::ResourceFilesystem& filesystem_
    ) :
  generator{generator_},
  rootUrl{rootUrl_},
  errorPage{errorPage_},
  root{root_},
  filesystem{filesystem_}
{
}

http::Response RequestHandler::handle(const web::library::Url& url) const
{
  const auto requestUrl = url.getPath().empty() ? rootUrl : url;

  const auto generated = generator.maybeGenerate(requestUrl);

  if (!generated) {
    return errorPage.generate(http::Status::_404_Not_Found, url);
  }

  const auto contentType = typeFromRepresentation(requestUrl.getRepresentation());

  std::string response{};

  const auto filename = root / requestUrl.filename();
  filesystem.read(filename, [&response](std::istream& stream){
    const std::string content(std::istreambuf_iterator<char>(stream), {});
    response = content;
  });

  return { http::Status::_200_OK, { contentType, response } };
}


}
