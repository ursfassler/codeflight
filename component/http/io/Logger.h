/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace component::http::io
{


class Logger
{
public:
  virtual ~Logger() = default;

  virtual void info(const std::string&) const = 0;
  virtual void warning(const std::string&) const = 0;
};


}
