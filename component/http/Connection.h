/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/web/library/Url.h"
#include "Response.h"
#include <functional>
#include <optional>
#include <string>

namespace component::http::io
{

class Logger;

}

namespace component::http
{

class ErrorPage;


class Connection
{
public:
  class Handler
  {
  public:
    virtual ~Handler() = default;

    virtual Response handle(const web::library::Url&) const = 0;
  };

  using Sender = std::function<std::optional<std::size_t>(const std::string&)>;

  Connection(
      const Handler&,
      const Sender&,
      const ErrorPage&,
      std::string newline,
      const io::Logger&
      );

  void received(const std::string&);
  void sent();

  bool isFinished() const;

private:
  const Handler& handler;
  const Sender sender;
  const ErrorPage& errorPage;
  const std::string newline;
  const io::Logger& logger;

  enum class State
  {
    ReceivingRequest,
    SendingResponse,
    Finished,
  };

  State state{State::ReceivingRequest};
  std::string request{};
  std::string response{};

  Response badRequest() const;
};


}
