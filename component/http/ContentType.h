/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace component::http
{


enum class ContentType
{
  Html,
  Svg,
  Css,
  JavaScript,
};


std::string getType(ContentType);


}
