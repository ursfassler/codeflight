/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Connection.h"
#include "Response.h"
#include <filesystem>
#include <string>

namespace component::web::io
{

class ResourceFilesystem;

}

namespace component::web::generator
{

class WebGenerator;

}

namespace component::http
{

class ErrorPage;


class RequestHandler :
    public Connection::Handler
{
public:
  RequestHandler(
      const web::generator::WebGenerator&,
      const web::library::Url&,
      const ErrorPage&,
      const std::filesystem::path&,
      web::io::ResourceFilesystem&
      );

  http::Response handle(const web::library::Url&) const override;

private:
  const web::generator::WebGenerator& generator;
  const web::library::Url rootUrl;
  const ErrorPage& errorPage;
  const std::filesystem::path root;
  const web::io::ResourceFilesystem& filesystem;

};


}
