/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Connection.h"
#include "ErrorPage.h"
#include "Parser.h"
#include "io/Logger.h"


namespace component::http
{


Connection::Connection(
    const Handler& handler_,
    const Sender& sender_,
    const ErrorPage& errorPage_,
    std::string newline_,
    const io::Logger& logger_
    ) :
  handler{handler_},
  sender{sender_},
  errorPage{errorPage_},
  newline{std::move(newline_)},
  logger{logger_}
{
}

void Connection::received(const std::string& data)
{
  if (state != State::ReceivingRequest) {
    logger.warning("receiving data but none expected");
  }

  request += data;
  const auto endMarker = newline+newline;
  const auto end = request.find(endMarker);
  if (end != std::string::npos) {
    const auto url = http::parse(request);
    request.erase(0, end + endMarker.size());

    if (url) {
      logger.info("request received for: " + url->escaped());
    } else {
      logger.warning("received bad request");
    }

    const auto response = url ? handler.handle(*url) : badRequest();

    this->response.clear();

    const auto statusCode = getStatusCode(response.status);
    const auto statusText = getStatusText(response.status);
    this->response +=
        "HTTP/1.0 " + std::to_string(statusCode) + " " + statusText + newline;

    const auto contentType = getType(response.content.type);
    const auto contentLength = std::to_string(response.content.data.length());
    this->response +=
        "Content-Type: " + contentType + newline +
        "Content-Length: " + contentLength + newline;

    this->response +=
        newline;

    this->response +=
        response.content.data;

    state = State::SendingResponse;
    sent();
  }
}

http::Response Connection::badRequest() const
{
  return errorPage.generate(http::Status::_400_Bad_Request, {});
}

bool Connection::isFinished() const
{
  return state == State::Finished;
}

void Connection::sent()
{
  if (state != State::SendingResponse) {
    logger.warning("received send response but not expected");
    return;
  }

  const ssize_t size = response.size();
  if (size <= 0) {
    state = State::Finished;
    return;
  }

  const auto sent = sender(response);
  if (!sent) {
    state = State::Finished;
    logger.info("send error");
    return;
  }

  response.erase(0, *sent);
}


}
