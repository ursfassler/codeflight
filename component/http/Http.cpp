/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Http.h"
#include "component/codeflight/Codeflight.h"
#include "component/web/Web.h"
#include "component/web/output/WebWriter.h"

namespace component::http
{


Http::Http(
    codeflight::Codeflight& codeflight_,
    web::Web& web_,
    std::string newline_,
    std::filesystem::path outdir,
    web::io::ResourceFilesystem& fs,
    const io::Logger& logger_
    ) :
  codeflight{codeflight_},
  web{web_},
  newline{newline_},
  errorPage{
    codeflight.rootAi(),
    web.getWriterFactory()
  },
  requestHandler{
    web.getWebGenerator(),
    web.rootUrl(),
    errorPage,
    outdir,
    fs
  },
  logger{logger_}
{
}

std::unique_ptr<Connection> Http::produceConnection(const Connection::Sender& sender) const
{
  return std::make_unique<Connection>(
        requestHandler,
        sender,
        errorPage,
        newline,
        logger
        );
}


}
