/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ErrorPage.h"
#include "component/web/library/LinkHook.h"
#include "component/web/output/html/Writer.h"
#include "component/web/output/html/WriterFactory.h"

namespace component::http
{
namespace
{

std::string getStatusDescription(http::Status value)
{
  switch (value) {
    case http::Status::_200_OK:
      return "Everything is OK";
    case http::Status::_400_Bad_Request:
      return "Your browser sent a request that this server could not understand.";
    case http::Status::_404_Not_Found:
      return "Artefact does not exist";
  }

  throw std::runtime_error("unhandled status code: " + std::to_string(static_cast<int>(value)));
}


class NullLinkHook :
    public web::library::LinkHook
{
public:
  void referenced(const codeflight::artefact::interface::ArtefactIdentifier&)
  {
  }
};


}


ErrorPage::ErrorPage(
    const codeflight::artefact::interface::ArtefactIdentifier& rootAi_,
    const web::output::html::WriterFactory& htmlFactory_
    ) :
  rootAi{rootAi_},
  htmlFactory{htmlFactory_}
{
}

http::Response ErrorPage::generate(http::Status status, const std::optional<web::library::Url>& url) const
{
  const auto root = url ? *url : web::library::Url{};

  std::stringstream output;

  NullLinkHook hook{};
  auto xw = htmlFactory.produce(root, hook);

  printDocument(status, url.has_value(), *xw);

  xw->serialize(output);

  return { status, { http::ContentType::Html, output.str() } };
}

void ErrorPage::printDocument(http::Status status, bool hasUrl, web::output::html::Writer& xw) const
{
  const auto message = http::getStatusText(status);
  const auto code = std::to_string(http::getStatusCode(status));

  xw.beginDocument(code + " " + message, {});

  xw.beginSection(message);

  xw.beginParagraph();
  xw.text(getStatusDescription(status));
  xw.endParagraph();

  if (hasUrl) {
    xw.beginParagraph();
    xw.link("Back to the project page.", rootAi);
    xw.endParagraph();
  }

  xw.endSection();

  xw.endDocument();
}


}
