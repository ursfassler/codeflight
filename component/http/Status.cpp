/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Status.h"
#include <stdexcept>


namespace component::http
{


uint_least16_t getStatusCode(Status value)
{
  switch (value) {
    case Status::_200_OK:
      return 200;
    case Status::_400_Bad_Request:
      return 400;
    case Status::_404_Not_Found:
      return 404;
  }

  throw std::runtime_error("unhandled status code: " + std::to_string(static_cast<int>(value)));
}

std::string getStatusText(Status value)
{
  switch (value) {
    case Status::_200_OK:
      return "OK";
    case Status::_400_Bad_Request:
      return "Bad Request";
    case Status::_404_Not_Found:
      return "Not Found";
  }

  throw std::runtime_error("unhandled status code: " + std::to_string(static_cast<int>(value)));
}


}
