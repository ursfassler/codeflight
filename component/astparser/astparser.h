/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <filesystem>
#include <memory>

namespace component::codeflight::ast
{

class Project;

}

namespace component::astparser::xml
{

class Reader;

}

namespace component::astparser::io
{

class Filesystem;
class Logger;

}

namespace component::astparser
{


std::unique_ptr<codeflight::ast::Project> load(
    const std::filesystem::path& filename,
    const xml::Reader&,
    io::Filesystem&,
    const io::Logger&
    );


}
