/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <filesystem>
#include <functional>

namespace component::astparser::io
{


class Filesystem
{
public:
  virtual ~Filesystem() = default;

  virtual void read(const std::filesystem::path&, const std::function<void(std::istream&)>&) const = 0;

};


}
