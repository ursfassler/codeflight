/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/library/OrderedMap.h"


namespace component::codeflight::ast
{

class Project;
class Node;

}

namespace component::astparser
{


using NodesAndParent = codeflight::library::OrderedMap<codeflight::ast::Node*, codeflight::ast::Node*>;


NodesAndParent getInvalidNodes(codeflight::ast::Project&);


}
