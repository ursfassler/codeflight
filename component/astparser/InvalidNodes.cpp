/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "InvalidNodes.h"
#include "component/codeflight/ast/Class.h"
#include "component/codeflight/ast/Field.h"
#include "component/codeflight/ast/Function.h"
#include "component/codeflight/ast/Method.h"
#include "component/codeflight/ast/MutableVisitor.h"
#include "component/codeflight/ast/Node.h"
#include "component/codeflight/ast/Package.h"
#include "component/codeflight/ast/Project.h"
#include "component/codeflight/ast/Variable.h"
#include "component/codeflight/ast/specification/Class.h"
#include "component/codeflight/ast/specification/Field.h"
#include "component/codeflight/ast/specification/Function.h"
#include "component/codeflight/ast/specification/Method.h"
#include "component/codeflight/ast/specification/Package.h"
#include "component/codeflight/ast/specification/Project.h"
#include "component/codeflight/ast/specification/Variable.h"
#include "component/codeflight/ast/specification/operator.h"


namespace component::astparser
{
namespace
{


using codeflight::ast::specification::operator||;


const codeflight::ast::NodeSpecification None = [](const codeflight::ast::Node*){
  return false;
};


class InvalidNodes :
    public codeflight::ast::MutableVisitor
{
public:
  using InvalidPosition = std::function<void(codeflight::ast::Node* node, codeflight::ast::Node* parent)>;


  InvalidNodes(
      const InvalidPosition& invalidPosition_
      ) :
    invalidPosition{invalidPosition_}
  {
  }

  void visit(codeflight::ast::Variable& node) override
  {
    verifyAllowedChildren(node, None);
  }

  void visit(codeflight::ast::Function& node) override
  {
    verifyAllowedChildren(node, None);
  }

  void visit(codeflight::ast::Field& node) override
  {
    verifyAllowedChildren(node, None);
  }

  void visit(codeflight::ast::Method& node) override
  {
    verifyAllowedChildren(node, None);
  }

  void visit(codeflight::ast::Class& node) override
  {
    verifyAllowedChildren(node,
                          codeflight::ast::specification::Field() ||
                          codeflight::ast::specification::Method() ||
                          codeflight::ast::specification::Class()
                          );
  }

  void visit(codeflight::ast::Package& node) override
  {
    verifyAllowedChildren(node,
                          codeflight::ast::specification::Class() ||
                          codeflight::ast::specification::Package() ||
                          codeflight::ast::specification::Function() ||
                          codeflight::ast::specification::Variable()
                          );
  }

  void visit(codeflight::ast::Project& node) override
  {
    verifyAllowedChildren(node, codeflight::ast::specification::Package());
  }

private:
  const InvalidPosition invalidPosition;

  void verifyAllowedChildren(codeflight::ast::Node& node, const codeflight::ast::NodeSpecification& spec)
  {
    for (const auto& child : node.children) {
      const auto chptr = child.get();
      if (!spec(chptr)) {
        invalidPosition(chptr, &node);
      }
    }
  }
};


void recursive(codeflight::ast::Node& node, codeflight::ast::MutableVisitor& visitor, const std::function<bool(codeflight::ast::Node&)>& recurse)
{
  node.accept(visitor);

  if (recurse(node)) {
    for (auto& child : node.children) {
      recursive(*child, visitor, recurse);
    }
  }
}


}


NodesAndParent getInvalidNodes(codeflight::ast::Project& project)
{
  NodesAndParent invalid{};

  const auto storeInvalid = [&invalid](codeflight::ast::Node* node, codeflight::ast::Node* parent){
    invalid.add(node, parent);
  };

  InvalidNodes verifier{storeInvalid};
  recursive(project, verifier, [&invalid](codeflight::ast::Node& node){
    return !invalid.contains(&node);
  });

  return invalid;
}


}
