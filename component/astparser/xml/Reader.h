/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <optional>
#include <string>

namespace component::astparser::xml
{


class Element
{
public:
  virtual ~Element() = default;

  virtual std::string nodeName() const = 0;
  virtual std::optional<std::string> getAttribute(const std::string&) const = 0;
  virtual std::unique_ptr<Element> firstChildElement() const = 0;
  virtual std::unique_ptr<Element> nextSiblingElement() const = 0;
  virtual std::size_t row() const = 0;
  virtual std::size_t column() const = 0;
};

class Document
{
public:
  virtual ~Document() = default;

  virtual std::unique_ptr<Element> documentElement() const = 0;
};

class Reader
{
public:
  virtual ~Reader() = default;

  virtual std::unique_ptr<Document> load(std::istream&) const = 0;
};


}
