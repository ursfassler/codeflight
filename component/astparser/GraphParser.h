/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <functional>
#include <map>
#include <memory>
#include <optional>
#include <set>

namespace component::astparser::xml
{

class Element;
class Document;

}

namespace component::astparser::io
{

class Logger;

}

namespace component::astparser
{

class Builder;


class GraphParser
{
  public:
    enum class Type {
      Package,
      Class,
      Field,
      Method,
      Variable,
      Function,
      Reference,
    };
    struct Node {
      Type type;
      std::string name;
      std::optional<std::string> id;
    };


    GraphParser(
        Builder&,
        const io::Logger&
        );

    void parse(const std::unique_ptr<xml::Document>&);

  private:
    Builder& builder;
    const io::Logger& logger;

    const std::map<Type, std::function<void(const Node&, const std::unique_ptr<xml::Element>&)>> Parser;

    void parseReference(const Node&, const std::unique_ptr<xml::Element>&);
    void parseGraphNode(const Node&, const std::unique_ptr<xml::Element>&);
    void parseChildren(const std::unique_ptr<xml::Element>&, const Node& parent);
    void parseElement(const std::unique_ptr<xml::Element>&, const Node& parent);
    std::optional<Node> parseNode(const std::unique_ptr<xml::Element>&) const;
    std::optional<Type> parseType(const std::unique_ptr<xml::Element>&) const;
    std::optional<std::string> parseName(const std::unique_ptr<xml::Element>&) const;
    std::optional<std::string> parseId(const std::unique_ptr<xml::Element>&) const;
};


}
