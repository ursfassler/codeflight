/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AstBuilder.h"
#include "AstFixer.h"
#include "component/codeflight/ast/Class.h"
#include "component/codeflight/ast/Field.h"
#include "component/codeflight/ast/Function.h"
#include "component/codeflight/ast/Method.h"
#include "component/codeflight/ast/Package.h"
#include "component/codeflight/ast/Project.h"
#include "component/codeflight/ast/Variable.h"
#include "component/codeflight/ast/specification/Package.h"
#include "io/Logger.h"
#include <cassert>


namespace component::astparser
{


AstBuilder::AstBuilder(
    const io::Logger& logger_
    ) :
  logger{logger_}
{
}

void AstBuilder::beginProject(const Name& name, const Language& language)
{
  assert(stack.empty());

  root = std::make_unique<codeflight::ast::Project>();
  root->name = name;
  root->language = language;

  stack.push_back(root.get());
}

void AstBuilder::endProject()
{
  assert(!stack.empty());
  stack.pop_back();

  for (const auto& edge : edges) {
    const auto source = edge.first;
    for (const auto& dst : edge.second) {
      const auto destination = idToElement[dst];
      if (destination) {
        if (codeflight::ast::specification::Package()(destination)) {
          logger.warning("reference to package is not supported, target: " + dst);
        } else {
          source->references.push_back(destination);
        }
      } else {
        logger.warning("reference target not found: " + dst);
      }
    }
  }

  const auto getId = [this](const codeflight::ast::Node* node) -> std::optional<std::string>{
    const auto idx = nodeToId.find(node);
    if (idx == nodeToId.end()) {
      return {};
    } else {
      return idx->second;
    }
  };
  AstFixer fixer{getId, logger};
  fixer.fix(root);

  assert(stack.empty());
}

void AstBuilder::beginNode(AstBuilder::Type type, const Name& name, const std::optional<Id>& id)
{
  assert(!stack.empty());

  auto node = produce(type);
  node->name = name;
  if (id) {
    idToElement[*id] = node.get();
    nodeToId[node.get()] = *id;
  }

  const auto parent = stack.back();
  node->parent = parent;

  const auto ptr = node.get();
  parent->children.push_back(std::move(node));
  stack.push_back(ptr);
}

void AstBuilder::endNode()
{
  assert(!stack.empty());
  stack.pop_back();
  assert(!stack.empty());
}

void AstBuilder::addEdgeTo(const AstBuilder::Id& id)
{
  edges[stack.back()].push_back(id);
}

std::unique_ptr<codeflight::ast::Project> AstBuilder::getMaybeProject()
{
  assert(root);
  assert(stack.empty());
  return std::move(root);
}

std::unique_ptr<codeflight::ast::Node> AstBuilder::produce(Type type) const
{
  switch (type) {
    case Type::Class: return std::make_unique<codeflight::ast::Class>();
    case Type::Field: return std::make_unique<codeflight::ast::Field>();
    case Type::Method: return std::make_unique<codeflight::ast::Method>();
    case Type::Package: return std::make_unique<codeflight::ast::Package>();
    case Type::Function: return std::make_unique<codeflight::ast::Function>();
    case Type::Variable: return std::make_unique<codeflight::ast::Variable>();
  }
  assert(false);
  return {};
}


}
