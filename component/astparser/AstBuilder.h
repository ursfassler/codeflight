/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Builder.h"
#include <map>
#include <memory>
#include <vector>


namespace component::codeflight::ast
{

class Project;
class Node;

}

namespace component::astparser::io
{

class Logger;

}

namespace component::astparser
{


class AstBuilder :
    public Builder
{
public:
  AstBuilder(const io::Logger&);

  void beginProject(const Name&, const Language&) override;
  void endProject() override;

  void beginNode(Type, const Name&, const std::optional<Id>&) override;
  void endNode() override;

  void addEdgeTo(const Id&) override;

  std::unique_ptr<codeflight::ast::Project> getMaybeProject();

private:
  const io::Logger& logger;
  std::unique_ptr<codeflight::ast::Project> root{};

  std::vector<codeflight::ast::Node*> stack{};

  std::map<Id, codeflight::ast::Node*> idToElement{};
  std::map<const codeflight::ast::Node*, Id> nodeToId{};
  std::map<codeflight::ast::Node*, std::vector<Id>> edges{};
  std::unique_ptr<codeflight::ast::Node> produce(Type) const;

};


}
