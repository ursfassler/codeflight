/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <optional>
#include <string>

namespace component::astparser
{


class Builder
{
public:
  using Id = std::string;
  using Name = std::string;
  using Language = std::string;
  enum class Type
  {
    Package,
    Class,
    Field,
    Method,
    Variable,
    Function,
  };

  virtual ~Builder() = default;

  virtual void beginProject(const Name&, const Language&) = 0;
  virtual void endProject() = 0;

  virtual void beginNode(Type, const Name&, const std::optional<Id>&) = 0;
  virtual void endNode() = 0;

  virtual void addEdgeTo(const Id&) = 0;

};


}
