/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "astparser.h"
#include "AstBuilder.h"
#include "GraphParser.h"
#include "component/codeflight/ast/Project.h"
#include "io/Filesystem.h"
#include "xml/Reader.h"


namespace component::astparser
{
namespace
{


std::unique_ptr<codeflight::ast::Project> parse(
    const std::unique_ptr<xml::Document>& document,
    const io::Logger& logger
    )
{
  AstBuilder builder{logger};
  GraphParser gp{builder, logger};
  gp.parse(document);
  return builder.getMaybeProject();
}


}


std::unique_ptr<codeflight::ast::Project> load(
  const std::filesystem::path& filename,
  const xml::Reader& xmlReader,
  io::Filesystem& os,
  const io::Logger& logger
)
{
  std::unique_ptr<xml::Document> doc{};
  os.read(filename, [&doc, &xmlReader](std::istream& stream){
    doc = xmlReader.load(stream);
  });
  return parse(doc, logger);
}


}
