/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/library/OrderedMap.h"
#include <functional>
#include <memory>
#include <optional>


namespace component::codeflight::ast
{

class Project;
class Node;

}

namespace component::astparser::io
{

class Logger;

}

namespace component::astparser
{


using IdGetter = std::function<std::optional<std::string>(const codeflight::ast::Node*)>;


class AstFixer
{
public:
  AstFixer(
      const IdGetter&,
      const io::Logger&
      );

  void fix(const std::unique_ptr<codeflight::ast::Project>&);

private:
  const IdGetter getId;
  const io::Logger& logger;

  using NodesAndParent = codeflight::library::OrderedMap<codeflight::ast::Node*, codeflight::ast::Node*>;

  void warnAboutInvalidNodes(const NodesAndParent&) const;
  void moveReferencesFromInvalidNodesToParent(const NodesAndParent&) const;
  void moveReferenceInvalidTargetNodesToParent(codeflight::ast::Project&, const NodesAndParent&) const;
  void removeInvalidNodes(const NodesAndParent&) const;
  void warnNodesWithReferencesWhereNotAllowed(const codeflight::ast::Project&) const;
  std::string to_string(const codeflight::ast::Node*) const;
};


}
