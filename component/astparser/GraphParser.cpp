/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "GraphParser.h"
#include "Builder.h"
#include "io/Logger.h"
#include "xml/Reader.h"


namespace component::astparser
{
namespace
{


const std::map<std::string, GraphParser::Type> TypeMap{
  {"class", GraphParser::Type::Class},
  {"field", GraphParser::Type::Field},
  {"function", GraphParser::Type::Function},
  {"method", GraphParser::Type::Method},
  {"package", GraphParser::Type::Package},
  {"reference", GraphParser::Type::Reference},
  {"variable", GraphParser::Type::Variable},
};

std::string to_string(GraphParser::Type type)
{
  for (const auto& itr : TypeMap) {
    if (type == itr.second) {
      return itr.first;
    }
  }
  throw std::runtime_error("unhandled type: " + std::to_string((int)type));
}

Builder::Type toBuilder(GraphParser::Type type)
{
  switch (type) {
    case GraphParser::Type::Class: return Builder::Type::Class;
    case GraphParser::Type::Field: return Builder::Type::Field;
    case GraphParser::Type::Function: return Builder::Type::Function;
    case GraphParser::Type::Method: return Builder::Type::Method;
    case GraphParser::Type::Package: return Builder::Type::Package;
    case GraphParser::Type::Variable: return Builder::Type::Variable;
    case GraphParser::Type::Reference: throw std::runtime_error("can not convert reference");
  }
  throw std::runtime_error("unhandled type: " + std::to_string((int)type));
}

std::string to_string(const GraphParser::Node& value)
{
  std::string result{};

  result += to_string(value.type);

  result += " ";
  result += "\"" + value.name + "\"";

  if (value.id) {
    result += " ";
    result += "(" + *value.id + ")";
  }

  return result;
}

std::string to_pos_string(const std::unique_ptr<xml::Element>& element)
{
  return std::to_string(element->row()) + ":" + std::to_string(element->column());
}


}


GraphParser::GraphParser(
    Builder& builder_,
    const io::Logger& logger_
    ) :
  builder{builder_},
  logger{logger_},
  Parser
  {
    {Type::Class, std::bind(&GraphParser::parseGraphNode, this, std::placeholders::_1, std::placeholders::_2)},
    {Type::Field, std::bind(&GraphParser::parseGraphNode, this, std::placeholders::_1, std::placeholders::_2)},
    {Type::Package, std::bind(&GraphParser::parseGraphNode, this, std::placeholders::_1, std::placeholders::_2)},
    {Type::Function, std::bind(&GraphParser::parseGraphNode, this, std::placeholders::_1, std::placeholders::_2)},
    {Type::Method, std::bind(&GraphParser::parseGraphNode, this, std::placeholders::_1, std::placeholders::_2)},
    {Type::Variable, std::bind(&GraphParser::parseGraphNode, this, std::placeholders::_1, std::placeholders::_2)},
    {Type::Reference, std::bind(&GraphParser::parseReference, this, std::placeholders::_1, std::placeholders::_2)},
  }
{
}

void GraphParser::parse(const std::unique_ptr<xml::Document>& document)
{
  const auto child = document->documentElement();
  if (!child) {
    return;
  }

  const auto cname = child->getAttribute("name");
  const std::string name = cname ? *cname : "";
  const auto clanguage = child->getAttribute("language");
  const std::string language = clanguage ? *clanguage : "";

  builder.beginProject(name, language);

  const auto packageNodeName = to_string(Type::Package);
  const auto topPackageNode = child->firstChildElement();
  if (!topPackageNode || (topPackageNode->nodeName() != packageNodeName)) {
    logger.warning("missing top level package");
    builder.endProject();
    return;
  }
  const auto nextTopPackageNode = topPackageNode->nextSiblingElement();
  if (nextTopPackageNode) {
    logger.warning("multiple top level packages");
  }

  parseElement(topPackageNode, {Type::Package, "", {}});

  builder.endProject();
}

void GraphParser::parseReference(const Node&, const std::unique_ptr<xml::Element>& element)
{
  const auto target = element->getAttribute("target");
  if (!target) {
    logger.warning("node without target: " + element->nodeName());
    return;
  }
  builder.addEdgeTo(*target);
}

void GraphParser::parseGraphNode(const Node& node, const std::unique_ptr<xml::Element>& element)
{
  builder.beginNode(toBuilder(node.type), node.name, node.id);
  parseChildren(element, node);
  builder.endNode();
}

void GraphParser::parseChildren(const std::unique_ptr<xml::Element>& element, const Node& parent)
{
  for (auto child = element->firstChildElement(); child; child = child->nextSiblingElement()) {
    parseElement(child, parent);
  }
}

void GraphParser::parseElement(const std::unique_ptr<xml::Element>& child, const Node& parent)
{
  const auto optNode = parseNode(child);
  if (!optNode) {
    const std::string type = child->nodeName();
    logger.warning("ignore unknown element " + type + " under " + to_string(parent) + " (" + to_pos_string(child) + ")");
    return;
  }
  const auto node = *optNode;

  const auto idx = Parser.find(node.type);
  if (idx == Parser.end()) {
    throw std::runtime_error("no parser found for " + to_string(node.type));
  }
  const auto parser = idx->second;

  parser(node, child);
}

std::optional<GraphParser::Node> GraphParser::parseNode(const std::unique_ptr<xml::Element>& element) const
{
  const auto type = parseType(element);
  if (type) {
    const auto name = parseName(element);
    const auto id = parseId(element);
    return {{*type, name ? *name : "", id}};
  } else {
    return {};
  }
}

std::optional<GraphParser::Type> GraphParser::parseType(const std::unique_ptr<xml::Element>& element) const
{
  const auto value = element->nodeName();
  const auto idx = TypeMap.find(value);
  if (idx == TypeMap.end()) {
    return {};
  } else {
    return idx->second;
  }
}

std::optional<std::string> GraphParser::parseName(const std::unique_ptr<xml::Element>& element) const
{
  auto name = element->getAttribute("name");
  return name;
}

std::optional<std::string> GraphParser::parseId(const std::unique_ptr<xml::Element>& element) const
{
  const auto id = element->getAttribute("id");
  return id;
}


}
