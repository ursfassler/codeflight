/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AstFixer.h"
#include "InvalidNodes.h"
#include "component/codeflight/ast/Node.h"
#include "component/codeflight/ast/Project.h"
#include "component/codeflight/ast/name/typename.h"
#include "component/codeflight/ast/specification/Package.h"
#include "component/codeflight/ast/specification/Project.h"
#include "component/codeflight/ast/specification/operator.h"
#include "component/codeflight/ast/traverse/traverse.h"
#include "io/Logger.h"
#include <set>


namespace component::astparser
{
namespace
{


using codeflight::ast::specification::operator||;


const codeflight::ast::NodeSpecification NoReferencesAllowed =
    codeflight::ast::specification::Package() ||
    codeflight::ast::specification::Project();


}


AstFixer::AstFixer(
    const IdGetter& getId_,
    const io::Logger& logger_
    ) :
  getId{getId_},
  logger{logger_}
{
}

void AstFixer::fix(const std::unique_ptr<codeflight::ast::Project>& project)
{
  if (project) {
    const auto invalid = getInvalidNodes(*project);
    warnAboutInvalidNodes(invalid);
    moveReferencesFromInvalidNodesToParent(invalid);
    moveReferenceInvalidTargetNodesToParent(*project, invalid);
    removeInvalidNodes(invalid);

    warnNodesWithReferencesWhereNotAllowed(*project);
  }
}

void AstFixer::warnAboutInvalidNodes(const NodesAndParent& invalid) const
{
  for (const auto node : invalid.keys()) {
    const auto parent = invalid.value(node);
    logger.warning(to_string(node) + " at invalid position, merging with all children into " + to_string(parent));
  }
}

void AstFixer::moveReferencesFromInvalidNodesToParent(const NodesAndParent& invalid) const
{
  for (const auto& itr : invalid) {
    const auto node = itr.first;
    const auto target = itr.second;

    if (NoReferencesAllowed(target)){
      const auto fromName = to_string(node);
      const auto toName = to_string(target);
      logger.warning("move all references from " + fromName + " to anchestor " + toName + ": references not allowed under target node, ignoring references");
    } else {
      codeflight::ast::traverse::recursiveMutable(*node, [&target](codeflight::ast::Node& node){
        target->references.insert(target->references.end(), node.references.begin(), node.references.end());
        node.references.clear();
      });
    }
  }
}

void AstFixer::moveReferenceInvalidTargetNodesToParent(codeflight::ast::Project& project, const NodesAndParent& invalid) const
{
  const auto topInvalidParent = [&invalid](codeflight::ast::Node* node){
    std::optional<codeflight::ast::Node*> result = {};

    for (auto itr = std::optional(node); itr; itr = (*itr)->parent) {
      const auto pos = invalid.find(*itr);
      if (pos != invalid.end()) {
        result = pos->second;
      }
    }

    return result;
  };

  codeflight::ast::traverse::recursiveMutable(project, [&topInvalidParent, this](codeflight::ast::Node& node){
    std::set<codeflight::ast::Node*> unfixable{};

    for (auto& ref : node.references) {
      const auto parent = topInvalidParent(ref);
      if (parent) {
        auto target = parent.value();
        if (NoReferencesAllowed(target)){
          const auto fromName = to_string(&node);
          const auto toName = to_string(ref);
          const auto targetName = to_string(target);
          logger.warning("change target of reference " + fromName + " -> " + toName + " to " + targetName + ": references to target node are not allowed, ignoring reference");
          unfixable.insert(ref);
        } else {
          ref = target;
        }
      }
    }

    auto&v  = node.references;
    v.erase(std::remove_if(v.begin(), v.end(), [&unfixable](codeflight::ast::Node* itr) {
              return unfixable.contains(itr);
            }), v.end());

  });
}

void AstFixer::removeInvalidNodes(const NodesAndParent& invalid) const
{
  for (const auto& itr : invalid) {
    const auto parent = itr.second;
    auto& v = parent->children;
    v.erase(std::remove_if(v.begin(), v.end(), [&invalid](std::unique_ptr<codeflight::ast::Node>& child) {
              return invalid.contains(child.get());
            }), v.end());
  }
}

void AstFixer::warnNodesWithReferencesWhereNotAllowed(const codeflight::ast::Project& project) const
{
  codeflight::ast::traverse::recursive(project, [this](const codeflight::ast::Node& node){
    if (NoReferencesAllowed(&node) && !node.references.empty()) {
      logger.warning("reference not allowed under " + to_string(&node));
    }
  });
}

std::string AstFixer::to_string(const codeflight::ast::Node* node) const
{
  std::string result{};

  result += codeflight::ast::name::typeName(node);

  result += " ";
  result += "\"" + node->name + "\"";

  const auto id = getId(node);
  if (id) {
    result += " ";
    result += "(" + *id + ")";
  }

  return result;
}


}
