/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Queue.h"
#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"
#include "component/web/library/aiString.h"
#include "component/web/library/urlFromAi.h"
#include "io/Logger.h"

namespace component::driver
{


Queue::Queue(
    const Generate& generate_,
    const io::Logger& logger_
    ) :
  generate{generate_},
  logger{logger_}
{
}

void Queue::push(const codeflight::artefact::interface::ArtefactIdentifier& value)
{
  queue.push(value);
}

void Queue::processAll()
{
  while (hasWork()) {
    processNext();
  }
}

void Queue::processNext()
{
  const auto ai = queue.front();
  queue.pop();

  if (!isGenerated(ai)) {
    logger.info("generate: " + web::library::aiString(ai));
    generate(ai);
    addGenerated(ai);
  }
}

bool Queue::hasWork() const
{
  return !queue.empty();
}

void Queue::addGenerated(const codeflight::artefact::interface::ArtefactIdentifier& artefact)
{
  generated.add(artefact);
}

bool Queue::isGenerated(const codeflight::artefact::interface::ArtefactIdentifier& artefact) const
{
  return generated.contains(artefact);
}


}
