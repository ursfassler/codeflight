/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Driver.h"
#include "component/codeflight/Codeflight.h"
#include "component/codeflight/ast/Project.h"
#include "component/web/Web.h"
#include "component/web/resource/Artefact.h"
#include <memory>


namespace component::driver
{


Driver::Driver(
    codeflight::Codeflight& codeflight_,
    web::Web& web_,
    const io::Logger& logger
    ) :
  codeflight{codeflight_},
  web{web_},
  queue{std::bind(&Driver::generate, this, std::placeholders::_1), logger}
{
  web.addLinkHook(this);

  queue.push(codeflight.rootAi());
}

void Driver::referenced(const codeflight::artefact::interface::ArtefactIdentifier& ai)
{
  queue.push(ai);
}

void Driver::generate(const codeflight::artefact::interface::ArtefactIdentifier& ai)
{
  web.getWebGenerator().maybeGenerate(ai);
}

void Driver::processAll()
{
  web.getResources().foreach([this](const component::web::resource::Artefact& resource){
    web.getWriter().resource(resource);
  });
  queue.processAll();
}


}
