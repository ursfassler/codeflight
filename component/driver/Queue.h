/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/library/Set.h"
#include <functional>
#include <queue>

namespace component::codeflight::artefact::interface
{

struct ArtefactIdentifier;

}

namespace component::driver::io
{

class Logger;

}

namespace component::driver
{


class Queue
{
public:
  using Generate = std::function<void(const codeflight::artefact::interface::ArtefactIdentifier&)>;

  Queue(
      const Generate&,
      const io::Logger&
      );

  void push(const codeflight::artefact::interface::ArtefactIdentifier&);
  void processAll();
  void processNext();

private:
  const Generate generate;
  const io::Logger& logger;

  std::queue<codeflight::artefact::interface::ArtefactIdentifier> queue;
  codeflight::library::Set<codeflight::artefact::interface::ArtefactIdentifier> generated{};

  bool hasWork() const;
  bool isGenerated(const codeflight::artefact::interface::ArtefactIdentifier&) const;
  void addGenerated(const codeflight::artefact::interface::ArtefactIdentifier&);
};


}
