/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace component::driver::io
{


class Logger
{
public:
  virtual ~Logger() = default;

  virtual void info(const std::string&) const = 0;
};


}
