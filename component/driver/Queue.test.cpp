/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Queue.h"
#include "component/codeflight/artefact/interface/Artefact.h"
#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"
#include "io/Logger.h"
#include <gmock/gmock.h>


namespace component::driver::unit_test
{
namespace
{

using namespace testing;


class NullLogger :
    public io::Logger
{
public:
  void info(const std::string&) const override
  {
  }
};


class Queue_Test :
    public Test
{
  public:
    NullLogger logger{};
    Queue queue{std::bind(&Queue_Test::handle, this, std::placeholders::_1), logger};

    codeflight::artefact::interface::ArtefactIdentifier a(const std::string& name)
    {
      return id(name);
    }

    std::map<std::string, unsigned> counter{};

    unsigned generatedTimes(const std::string& name) const
    {
      return counter.at(name);
    }

    void handle(const codeflight::artefact::interface::ArtefactIdentifier& ai)
    {
      counter[ai.artefact]++;
    }

    codeflight::artefact::interface::ArtefactIdentifier id(const std::string& name) const
    {
      return {{}, name, {}};
    }
};


TEST_F(Queue_Test, generates_artefact_in_queue)
{
  auto artefact = a("a");
  queue.push(artefact);

  queue.processAll();

  EXPECT_EQ(1, generatedTimes("a"));
}

TEST_F(Queue_Test, generates_all_artefact_in_queue)
{
  auto artefact1 = a("1");
  queue.push(artefact1);
  auto artefact2 = a("2");
  queue.push(artefact2);
  auto artefact3 = a("3");
  queue.push(artefact3);

  queue.processAll();

  EXPECT_EQ(1, generatedTimes("1"));
  EXPECT_EQ(1, generatedTimes("2"));
  EXPECT_EQ(1, generatedTimes("3"));
}

TEST_F(Queue_Test, does_not_generate_equal_artefact_twice)
{
  auto artefact1 = a("equal_name");
  queue.push(artefact1);
  auto artefact2 = a("equal_name");
  queue.push(artefact2);

  queue.processAll();

  EXPECT_EQ(1, generatedTimes("equal_name"));
}


}
}
