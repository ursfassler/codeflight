/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Queue.h"
#include "component/web/library/LinkHook.h"

namespace component::codeflight
{

class Codeflight;

}

namespace component::web
{

class Web;

}

namespace component::driver::io
{

class Logger;

}

namespace component::driver
{


class Driver :
    private web::library::LinkHook
{
public:
  Driver(
      codeflight::Codeflight&,
      web::Web&,
      const io::Logger&
      );

  void processAll();

private:
  codeflight::Codeflight& codeflight;
  web::Web& web;
  Queue queue;

  void referenced(const codeflight::artefact::interface::ArtefactIdentifier&) override;
  void generate(const codeflight::artefact::interface::ArtefactIdentifier&);
};


}
