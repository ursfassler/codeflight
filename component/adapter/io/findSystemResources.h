/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <filesystem>
#include <optional>
#include <string>


namespace component::adapter::io
{


std::optional<std::filesystem::path> findSystemResources(const std::string& appName);


}
