/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "StdFilesystem.h"
#include <filesystem>
#include <fstream>

namespace component::adapter::io
{


void StdFilesystem::write(const std::filesystem::path& name, const std::function<void(std::ostream&)>& writer)
{
  const auto directory = name.parent_path();
  std::filesystem::create_directories(directory);

  std::ofstream output{name};
  writer(output);
}

bool StdFilesystem::fileExists(const std::filesystem::path& name) const
{
  return std::filesystem::exists(name);
}

void StdFilesystem::read(const std::filesystem::path& name, const std::function<void (std::istream &)>& reader) const
{
  std::ifstream input{name};
  reader(input);
}

std::set<std::filesystem::path> StdFilesystem::files(const std::filesystem::path& root) const
{
  if (!std::filesystem::exists(root)) {
    return {};
  }

  std::set<std::filesystem::path> result{};

  for(const auto& file: std::filesystem::directory_iterator(root)) {
    if (std::filesystem::is_regular_file(file)) {
      const auto relative = std::filesystem::relative(file, root);
      result.insert(relative);
    }
  }

  return result;
}


}
