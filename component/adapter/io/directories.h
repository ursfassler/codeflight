/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <filesystem>
#include <vector>

namespace component::adapter::io
{


std::vector<std::filesystem::path> defaultResourcesDirectories();


}
