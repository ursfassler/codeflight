/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "findSystemResources.h"
#include "directories.h"

namespace component::adapter::io
{


std::optional<std::filesystem::path> findSystemResources(const std::string& appName)
{
  const auto resPaths{defaultResourcesDirectories()};

  for (const auto& path : resPaths) {
    const auto fullPath = path / appName / "resources";
    if (std::filesystem::exists(fullPath) && std::filesystem::is_directory(fullPath)) {
      return fullPath;
    }
  }

  return {};
}


}
