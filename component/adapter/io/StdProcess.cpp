/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "StdProcess.h"

namespace component::adapter::io
{


void StdProcess::execute(const std::string &command)
{
  std::system(command.c_str());
}


}
