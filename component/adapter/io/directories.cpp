/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "directories.h"
#include <optional>
#include <pwd.h>
#include <unistd.h>

namespace component::adapter::io
{
namespace
{


std::optional<std::string> homedir()
{
  const auto myuid = getuid();
  const auto mypasswd = getpwuid(myuid);

  if (mypasswd) {
    return {mypasswd->pw_dir};
  } else {
    return {};
  }
}


}


std::vector<std::filesystem::path> defaultResourcesDirectories()
{
  std::vector<std::filesystem::path> result{};

  const auto home = homedir();
  if (home) {
    result.emplace_back(home.value() + "/.local/share");
  }
  result.emplace_back("/usr/local/share");
  result.emplace_back("/usr/share");

  return result;
}


}
