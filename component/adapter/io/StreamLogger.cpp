/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "StreamLogger.h"

namespace component::adapter::io
{


StreamLogger::StreamLogger(std::ostream &standard_, std::ostream &error_) :
    standardStream{standard_},
    errorStream{error_}
{
}

void StreamLogger::info(const std::string &message) const
{
    standardStream << message << std::endl;
}

void StreamLogger::warning(const std::string &message) const
{
    errorStream << message << std::endl;
}

void StreamLogger::error(const std::string &message) const
{
    errorStream << message << std::endl;
}


}
