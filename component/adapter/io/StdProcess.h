/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "component/web/io/Process.h"

namespace component::adapter::io
{


class StdProcess :
    public component::web::io::Process
{
public:
  void execute(const std::string&) override;

};


}
