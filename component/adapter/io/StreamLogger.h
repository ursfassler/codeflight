/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "component/astparser/io/Logger.h"
#include "component/driver/io/Logger.h"
#include "component/http/io/Logger.h"
#include <iostream>

namespace component::adapter::io
{


class StreamLogger :
    public component::astparser::io::Logger,
    public component::driver::io::Logger,
    public component::http::io::Logger
{
public:
  StreamLogger(std::ostream& standard_, std::ostream& error_);

  void info(const std::string& message) const override;
  void warning(const std::string& message) const override;
  void error(const std::string& message) const;

private:
  std::ostream& standardStream;
  std::ostream& errorStream;

};


}
