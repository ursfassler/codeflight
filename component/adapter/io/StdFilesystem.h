/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "component/astparser/io/Filesystem.h"
#include "component/web/io/Filesystem.h"

namespace component::adapter::io
{


class StdFilesystem :
    public component::astparser::io::Filesystem,
    public component::web::io::Filesystem
{
public:
  void write(const std::filesystem::path&, const std::function<void(std::ostream&)>&) override;
  bool fileExists(const std::filesystem::path&) const override;
  void read(const std::filesystem::path&, const std::function<void(std::istream&)>&) const override;
  std::set<std::filesystem::path> files(const std::filesystem::path&) const override;

};


}
