/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/web/xml/Writer.h"
#include <memory>
#include <ostream>
#include <tinyxml.h>


namespace component::adapter::xml::writer
{


class TinyWriter :
    public web::xml::Writer
{
public:
  void text(const std::string&) override;
  void attribute(const std::string& name, const std::string& value) override;
  void beginNode(const std::string&) override;
  void endNode() override;

  void serialize(std::ostream&) const override;

private:
  TiXmlDocument doc{};
  TiXmlNode* current{&doc};
};


class TinyFactory :
    public web::xml::WriterFactory
{
public:
  std::unique_ptr<web::xml::Writer> produce() const override;
};


}
