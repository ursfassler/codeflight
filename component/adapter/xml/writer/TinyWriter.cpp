/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "TinyWriter.h"


namespace component::adapter::xml::writer
{


void TinyWriter::text(const std::string &nodeValue)
{
  current->InsertEndChild(TiXmlText{nodeValue});
}

void TinyWriter::attribute(const std::string &name, const std::string &value)
{
  current->ToElement()->SetAttribute(name, value);
}

void TinyWriter::beginNode(const std::string &nodeName)
{
  current = current->InsertEndChild(TiXmlElement{nodeName});
}

void TinyWriter::endNode()
{
  current = current->Parent();
}

void TinyWriter::serialize(std::ostream &output) const
{
  TiXmlPrinter printer{};
  doc.Accept(&printer);

  output << "<!DOCTYPE html>" << std::endl;
  output << printer.Str();
}


std::unique_ptr<web::xml::Writer> TinyFactory::produce() const
{
  return std::make_unique<TinyWriter>();
}


}
