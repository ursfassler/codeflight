/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/astparser/xml/Reader.h"
#include <tinyxml.h>

namespace component::adapter::xml::reader
{


class TinyElement :
    public astparser::xml::Element
{
public:
  TinyElement(const TiXmlElement*);

  std::string nodeName() const override;
  std::optional<std::string> getAttribute(const std::string&) const override;
  std::unique_ptr<Element> firstChildElement() const override;
  std::unique_ptr<Element> nextSiblingElement() const override;

  std::size_t row() const override;
  std::size_t column() const override;

private:
  const TiXmlElement* const element;
};

class TinyDocument :
    public astparser::xml::Document
{
public:
  TinyDocument(std::istream&);

  std::unique_ptr<astparser::xml::Element> documentElement() const override;

private:
  TiXmlDocument document{};
};

class TinyReader :
    public astparser::xml::Reader
{
public:
  std::unique_ptr<astparser::xml::Document> load(std::istream&) const override;
};


}
