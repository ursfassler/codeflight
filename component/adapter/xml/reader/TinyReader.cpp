/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "TinyReader.h"


namespace component::adapter::xml::reader
{


TinyElement::TinyElement(const TiXmlElement* element_) :
  element{element_}
{
}

std::string TinyElement::nodeName() const
{
  return element->ValueStr();
}

std::optional<std::string> TinyElement::getAttribute(const std::string& name) const
{
  const auto value = element->Attribute(name);
  if (value) {
    return {*value};
  } else {
    return {};
  }
}

std::unique_ptr<astparser::xml::Element> TinyElement::firstChildElement() const
{
  const auto value = element->FirstChildElement();
  if (!value) {
    return {};
  }
  return std::make_unique<TinyElement>(value);
}

std::unique_ptr<astparser::xml::Element> TinyElement::nextSiblingElement() const
{
  const auto value = element->NextSiblingElement();
  if (!value) {
    return {};
  }
  return std::make_unique<TinyElement>(value);
}

std::size_t TinyElement::row() const
{
  return element->Row();
}

std::size_t TinyElement::column() const
{
  return element->Column();
}

TinyDocument::TinyDocument(std::istream& stream)
{
  stream >> document;
}

std::unique_ptr<astparser::xml::Element> TinyDocument::documentElement() const
{
  const auto value = document.RootElement();
  if (!value) {
    return {};
  }
  return std::make_unique<TinyElement>(value);
}

std::unique_ptr<astparser::xml::Document> TinyReader::load(std::istream& stream) const
{
  return std::make_unique<TinyDocument>(stream);
}


}
