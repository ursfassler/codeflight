# language: en

Feature: draw the dependencies of one package
  As an architect
  I want to know the dependencies within one package
  In order to see missing or to strong connections


Scenario: reduce method and field dependencies to class dependencies
  Given I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'Method1' -> 123
                method 'Method2' -> 2
            class 'ClassB'
                method 'Method1' 123
                field 'Field1' 2
    """

  When I generate the content-dependencies for ""

  Then I expect the output:
    """
    graph /''

    class /'ClassA' index:/''/'ClassA'
    class /'ClassB' index:/''/'ClassB'

    /'ClassA' -> /'ClassB'

    """


Scenario: reduce duplicated dependencies
  Given I have the AST:
    """
    project
        package
            class 'A' 1
                method 'AM1' 2
            class 'B'
                method 'BM1' -> 1
                method 'BM2' -> 4 2
                method 'BM3' -> 4
                field 'BF1' 3 -> 1
                field 'BF2' 4
    """

  When I generate the content-dependencies for ""

  Then I expect the output:
    """
    graph /''

    class /'A' index:/''/'A'
    class /'B' index:/''/'B'

    /'B' -> /'A'

    """


Scenario: remove dependencies to itself
  Given I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'Method1' -> 2
                method 'Method2' 2
    """

  When I generate the content-dependencies for ""

  Then I expect the output:
    """
    graph /''

    class /'ClassA' index:/''/'ClassA'


    """


Scenario: reduce encapsulated class dependencies
  Given I have the AST:
    """
    project
        package
            class 'ClassA'
                class 'ClassA_A' 1
            class 'ClassB'
                class 'ClassB_B' -> 1
        """

  When I generate the content-dependencies for ""

  Then I expect the output:
    """
    graph /''

    class /'ClassA' index:/''/'ClassA'
    class /'ClassB' index:/''/'ClassB'

    /'ClassB' -> /'ClassA'

    """


Scenario: show packages
  Given I have the AST:
    """
    project
        package
            class 'ClassA'
                class 'ClassA_A' 1
            package 'pac'
                class 'ClassB' -> 1
            """

  When I generate the content-dependencies for ""

  Then I expect the output:
    """
    graph /''

    class   /'ClassA' index:/''/'ClassA'
    package /'pac'    index:/''/'pac'

    /'pac' -> /'ClassA' dependencies-between-packages::/''/'pac':/''

    """


Scenario: add link to class
  Given I have the AST:
    """
    project
        package
            class 'ClassA'
    """

  When I generate the content-dependencies for ""

  Then I expect the output:
    """
    graph /''

    class /'ClassA' index:/''/'ClassA'


    """


Scenario: add link to function
  Given I have the AST:
    """
    project
        package
            function 'funcA'
    """

  When I generate the content-dependencies for ""

  Then I expect the output:
    """
    graph /''

    function /'funcA' index:/''/'funcA'


    """


Scenario: add link to variable
  Given I have the AST:
    """
    project
        package
            variable 'varA'
    """

  When I generate the content-dependencies for ""

  Then I expect the output:
    """
    graph /''

    variable /'varA' index:/''/'varA'


    """


Scenario: add link between packages
  Given I have the AST:
    """
    project
        package
            package 'a'
                function 'aa' -> 1
                function 'ab' -> 0 1 2 3
                function 'ac' 0
            package 'b'
                class 'ba'
                    field 'baa' 1
                    field 'bab' 2
                function 'bc' 3
        """

  When I generate the content-dependencies for ""

  Then I expect the output:
    """
    graph /''

    package /'a' index:/''/'a'
    package /'b' index:/''/'b'

    /'a' -> /'b' '3' dependencies-between-packages::/''/'a':/''/'b'

    """


Scenario: add link between package element parents
  Given I have the AST:
    """
    project
        package
            package 'a'
                function 'aa' 1 -> 3
            package 'b'
                class 'ba' 2
            function 'c' 3 -> 2
            variable 'd' 4 -> 3
        """

  When I generate the content-dependencies for ""

  Then I expect the output:
    """
    graph /''

    package  /'a' index:/''/'a'
    package  /'b' index:/''/'b'
    function /'c' index:/''/'c'
    variable /'d' index:/''/'d'

    /'a' -> /'c' dependencies-between-packages::/''/'a':/''
    /'c' -> /'b' dependencies-between-packages::/'':/''/'b'
    /'d' -> /'c'

    """


Scenario: show dependencies between direct child elements
  Given I have the AST:
    """
    project
        package
            function 'a' -> 1
            variable 'b' 1
    """

  When I generate the content-dependencies for ""

  Then I expect the output:
    """
    graph /''

    function /'a' index:/''/'a'
    variable /'b' index:/''/'b'

    /'a' -> /'b'

    """


Scenario: create artefact for sub-package
  Given I have the AST:
    """
    project
        package
            package 'a'
                package 'b'
                    class 'X' -> 1
                    class 'Y' 1
    """

  When I generate the content-dependencies for "/a/b"

  Then I expect the output:
    """
    graph /''/'a'/'b'

    class /'X' index:/''/'a'/'b'/'X'
    class /'Y' index:/''/'a'/'b'/'Y'

    /'X' -> /'Y'

    """
