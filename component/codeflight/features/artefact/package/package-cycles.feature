# language: en

Feature: list cycles between elements of one package
  As an architect
  I want to identify the cycles in the software
  In order to reduce the cyclic dependencies


Scenario: list cycles from sub-packages
  Given I have the AST:
    """
    project
        package
            package 'a'
                package 'a'
                    function 'funcA' 1 -> 2 3
                    class 'ClassA' 4 -> 1 3
            package 'b'
                class 'ClassB' 2 -> 3 5
            package 'c'
                class 'ClassC' 3 -> 1 4
            package 'd'
                class 'ClassB' 5
    """

  When I generate the package-cycles for ""

  Then I expect the output:
    """
    document Package cycles

    = Package cycles

    metric                             value
    Has cycles                         true
    Number of involved packages        3
    Total number of packages           5
    Packages in cycles ratio           0.600
    Number of involved dependencies    4
    Total number of dependencies       5
    Dependencies in cycles ratio       0.800
    Number of cycles                   2

    == Cycle cluster 1

    === Involved packages

    Package               cycle fan-in    cycle fan-out    in cycles
    /'a' index:/''/'a'    2               3                2
    /'b' index:/''/'b'    1               1                1
    /'c' index:/''/'c'    3               2                2

    === Involved dependencies

    from                  to                    references                                          in cycles
    /'a' index:/''/'a'    /'b' index:/''/'b'    1 dependencies-between-packages::/''/'a':/''/'b'    1
    /'a' index:/''/'a'    /'c' index:/''/'c'    2 dependencies-between-packages::/''/'a':/''/'c'    1
    /'b' index:/''/'b'    /'c' index:/''/'c'    1 dependencies-between-packages::/''/'b':/''/'c'    1
    /'c' index:/''/'c'    /'a' index:/''/'a'    2 dependencies-between-packages::/''/'c':/''/'a'    2


    """


Scenario: include elements of this package
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA' 1
                    method 'a' -> 2
                    field 'b' -> 3
                function 'funcB' 2 -> 3 1
                package 'c'
                    function 'funcC' 3 -> 1
    """

  When I generate the package-cycles for "/a"

  Then I expect the output:
    """
    document Package cycles

    = Package cycles

    metric                             value
    Has cycles                         true
    Number of involved packages        2
    Total number of packages           2
    Packages in cycles ratio           1.000
    Number of involved dependencies    2
    Total number of dependencies       2
    Dependencies in cycles ratio       1.000
    Number of cycles                   1

    == Cycle cluster 1

    === Involved packages

    Package                   cycle fan-in    cycle fan-out    in cycles
     index:/''/'a'            1               2                1
    /'c' index:/''/'a'/'c'    2               1                1

    === Involved dependencies

    from                      to                        references                                              in cycles
     index:/''/'a'            /'c' index:/''/'a'/'c'    2 dependencies-between-packages::/''/'a':/''/'a'/'c'    1
    /'c' index:/''/'a'/'c'     index:/''/'a'            1 dependencies-between-packages::/''/'a'/'c':/''/'a'    1


    """


Scenario: list separate cycles
  Given I have the AST:
    """
    project
        package
            package 'a'
                function 'funcA' a -> b c d
            package 'b'
                function 'funcB' b -> a c d
            package 'c'
                function 'funcC' c -> d
            package 'd'
                function 'funcD' d -> c
    """

  When I generate the package-cycles for ""

  Then I expect the output:
    """
    document Package cycles

    = Package cycles

    metric                             value
    Has cycles                         true
    Number of involved packages        4
    Total number of packages           5
    Packages in cycles ratio           0.800
    Number of involved dependencies    4
    Total number of dependencies       8
    Dependencies in cycles ratio       0.500
    Number of cycles                   2

    == Cycle cluster 1

    === Involved packages

    Package               cycle fan-in    cycle fan-out    in cycles
    /'a' index:/''/'a'    1               1                1
    /'b' index:/''/'b'    1               1                1

    === Involved dependencies

    from                  to                    references                                          in cycles
    /'a' index:/''/'a'    /'b' index:/''/'b'    1 dependencies-between-packages::/''/'a':/''/'b'    1
    /'b' index:/''/'b'    /'a' index:/''/'a'    1 dependencies-between-packages::/''/'b':/''/'a'    1

    == Cycle cluster 2

    === Involved packages

    Package               cycle fan-in    cycle fan-out    in cycles
    /'c' index:/''/'c'    1               1                1
    /'d' index:/''/'d'    1               1                1

    === Involved dependencies

    from                  to                    references                                          in cycles
    /'c' index:/''/'c'    /'d' index:/''/'d'    1 dependencies-between-packages::/''/'c':/''/'d'    1
    /'d' index:/''/'d'    /'c' index:/''/'c'    1 dependencies-between-packages::/''/'d':/''/'c'    1


    """
