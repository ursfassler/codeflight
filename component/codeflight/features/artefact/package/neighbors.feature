# language: en

Feature: draw package neighbors
  As an architect
  I want to see the neighbors of a package
  In order to see all dependencies


Scenario: draw the neighbors of the package
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'Class'
                    method 'm' -> 1
            package 'b'
                class 'Class'
                    method 'm' 1 -> 2
            package 'c'
                class 'Class' 2
    """

  When I generate the neighbors for "/b"

  Then I expect the output:
    """
    graph

    package /''/'b' ! index:/''/'b'
    package /''/'c'   index:/''/'c'
    package /''/'a'   index:/''/'a'

    /''/'b' -> /''/'c' '1' dependencies-between-packages::/''/'b':/''/'c'
    /''/'a' -> /''/'b' '1' dependencies-between-packages::/''/'a':/''/'b'

    """


Scenario: draw only the neighbors of the package
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'm1' -> b
                    method 'm2' -> d
            package 'b'
                class 'ClassB' b
            package 'c'
                class 'ClassC' c
            package 'd'
                class 'ClassD' d
                    method 'm3' -> c
    """

  When I generate the neighbors for "/a"

  Then I expect the output:
    """
    graph

    package /''/'a' ! index:/''/'a'
    package /''/'b'   index:/''/'b'
    package /''/'d'   index:/''/'d'

    /''/'a' -> /''/'b' '1' dependencies-between-packages::/''/'a':/''/'b'
    /''/'a' -> /''/'d' '1' dependencies-between-packages::/''/'a':/''/'d'

    """


Scenario: don't draw dependencies between neighbors of interested package
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'm1' -> b
                    method 'm2' -> c
            package 'b'
                class 'ClassB' b
            package 'c'
                class 'ClassC' c
                    method 'm3' -> b
    """

  When I generate the neighbors for "/a"

  Then I expect the output:
    """
    graph

    package /''/'a' ! index:/''/'a'
    package /''/'b'   index:/''/'b'
    package /''/'c'   index:/''/'c'

    /''/'a' -> /''/'b' '1' dependencies-between-packages::/''/'a':/''/'b'
    /''/'a' -> /''/'c' '1' dependencies-between-packages::/''/'a':/''/'c'

    """


Scenario: label duplicated connections
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'm1' -> 1
                class 'ClassB'
                    method 'm1' -> 2
            package 'b'
                class 'ClassA' 1
                class 'ClassB' 2
    """

  When I generate the neighbors for "/a"

  Then I expect the output:
    """
    graph

    package /''/'a' ! index:/''/'a'
    package /''/'b'   index:/''/'b'

    /''/'a' -> /''/'b' '2' dependencies-between-packages::/''/'a':/''/'b'

    """


Scenario: don't draw connection to itself
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'm1' -> 2
                class 'ClassB'
                    method 'm2' 2
    """

  When I generate the neighbors for "/a"

  Then I expect the output:
    """
    graph

    package /''/'a' ! index:/''/'a'


    """


Scenario: draw cycle
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'm1' 1 -> 2
            package 'b'
                class 'ClassB'
                    method 'm2' 2 -> 1
    """

  When I generate the neighbors for "/a"

  Then I expect the output:
    """
    graph

    package /''/'a' ! index:/''/'a'
    package /''/'b'   index:/''/'b'

    /''/'a' -> /''/'b' '1' dependencies-between-packages::/''/'a':/''/'b'
    /''/'b' -> /''/'a' '1' dependencies-between-packages::/''/'b':/''/'a'

    """


Scenario: draw the neighbors of the sub-packages to dig into dependencies between packages and their children
  Given I have the AST:
    """
    project
        package
            package 'a'
                package 'b'
                    function 'foo' -> 1
            package 'x'
                package 'y'
                    function 'bar' 1 -> 2
            package 'c'
                package 'd'
                    function 'zzz' 2
        """

  When I generate the neighbors for "/x"

  Then I expect the output:
    """
    graph

    package /''/'x' ! index:/''/'x'
    package /''/'c'   index:/''/'c'
    package /''/'a'   index:/''/'a'

    /''/'x' -> /''/'c' '1' dependencies-between-packages::/''/'x':/''/'c'
    /''/'a' -> /''/'x' '1' dependencies-between-packages::/''/'a':/''/'x'

    """


Scenario: neighbor can be a package element
  Given I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'foo' -> 1
            package 'x'
                package 'y'
                    function 'bar' 1 -> 2
            variable 'c' 2
    """

  When I generate the neighbors for "/x"

  Then I expect the output:
    """
    graph

    package  /''/'x'      ! index:/''/'x'
    variable /''/'c'        index:/''/'c'
    class    /''/'ClassA'   index:/''/'ClassA'

    /''/'x'      -> /''/'c' '1'
    /''/'ClassA' -> /''/'x' '1'

    """


Scenario: neighbor can be a package element from a different package
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'foo' -> 1
            package 'x'
                package 'y'
                    function 'bar' 1
        """

  When I generate the neighbors for "/x/y"

  Then I expect the output:
    """
    graph

    package /''/'x'/'y' ! index:/''/'x'/'y'
    package /''/'a'       index:/''/'a'

    /''/'a' -> /''/'x'/'y' '1' dependencies-between-packages::/''/'a':/''/'x'/'y'

    """


Scenario: sub elements are not neighbors of a package
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'foo' 2 -> 1
                package 'x'
                    package 'y'
                        function 'bar' 1
                            package 'z'
                    variable 'w' -> 2
        """

  When I generate the neighbors for "/a"

  Then I expect the output:
    """
    graph

    package /''/'a' ! index:/''/'a'


    """


Scenario: count each package element once
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'm1' 1 -> 4 5
                    method 'm2' 2 -> 5 5
                package 'b'
                    class 'ClassB' 3 -> 4
                        function 'f1' -> 5
                    function 'f2' -> 4 4 5
            package 'x'
                function 'f1' 4
                function 'f2' 5
            package 'y'
                variable 'z' -> 1 2 3 1 2
    """

  When I generate the neighbors for "/a"

  Then I expect the output:
    """
    graph

    package /''/'a' ! index:/''/'a'
    package /''/'x'   index:/''/'x'
    package /''/'y'   index:/''/'y'

    /''/'a' -> /''/'x' '6' dependencies-between-packages::/''/'a':/''/'x'
    /''/'y' -> /''/'a' '2' dependencies-between-packages::/''/'y':/''/'a'

    """


Scenario: no neighbors
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                class 'ClassB'
            package 'x'
                function 'f1'
    """

  When I generate the neighbors for "/a"

  Then I expect the output:
    """
    graph

    package /''/'a' ! index:/''/'a'


    """
