# language: en

Feature: generate an package overview
  As a user of the tool
  I want to easily see the most important information about a package
  In order to get used to the tool and see all possible information


Scenario: print a simple package overview
  Given I have the AST:
    """
    project 'The Super Project'
        package
            class 'ClassA'
            class 'ClassB'
            package 'package1'
                class 'ClassC'
                package 'package2'
                    class 'ClassD'
                    class 'ClassE'
    """

  When I generate the index for "/package1/package2"

  Then I expect the output:
    """
    document /''/'package1'/'package2'

    = /''/'package1'/'package2'

    - Package content dependencies content-dependencies:/''/'package1'/'package2'
    - Package cycles package-cycles:/''/'package1'/'package2'
    - Package neighbors neighbors:/''/'package1'/'package2'
    - Package tree package-tree:/''/'package1'/'package2'

    """
