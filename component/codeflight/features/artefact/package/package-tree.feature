# language: en

Feature: draw packages as tree
  As an architect
  I want to see the tree of packages
  In order see the structure of the software


Background:
  Given I have the AST:
    """
    project
        package
            package 'p1'
                package 'p1_1'
                    package 'p1_1_1'
                    package 'p1_1_2'
            package 'p2'
                package 'p2_1'
                package 'p2_2'
    """


Scenario: draw the children, parents and siblings

  When I generate the package-tree for "/p2"

  Then I expect the output:
    """
    graph

    package /'p2'   ! index:/''/'p2'
    package /''       index:/''
    package /'p1'     index:/''/'p1'
    package /'p2_1'   index:/''/'p2'/'p2_1'
    package /'p2_2'   index:/''/'p2'/'p2_2'

    /''   -> /'p2'
    /''   -> /'p1'
    /'p2' -> /'p2_1'
    /'p2' -> /'p2_2'

    """


Scenario: don't draw siblings of parent

  When I generate the package-tree for "/p1/p1_1"

  Then I expect the output:
    """
    graph

    package /'p1_1'   ! index:/''/'p1'/'p1_1'
    package /'p1'       index:/''/'p1'
    package /''         index:/''
    package /'p1_1_1'   index:/''/'p1'/'p1_1'/'p1_1_1'
    package /'p1_1_2'   index:/''/'p1'/'p1_1'/'p1_1_2'

    /''     -> /'p1'
    /'p1'   -> /'p1_1'
    /'p1_1' -> /'p1_1_1'
    /'p1_1' -> /'p1_1_2'

    """


Scenario: don't draw grandchildren

  When I generate the package-tree for "/p1"

  Then I expect the output:
    """
    graph

    package /'p1'   ! index:/''/'p1'
    package /''       index:/''
    package /'p2'     index:/''/'p2'
    package /'p1_1'   index:/''/'p1'/'p1_1'

    /''   -> /'p1'
    /''   -> /'p2'
    /'p1' -> /'p1_1'

    """


Scenario: does work for root package

  When I generate the package-tree for ""

  Then I expect the output:
    """
    graph

    package /''   ! index:/''
    package /'p1'   index:/''/'p1'
    package /'p2'   index:/''/'p2'

    /'' -> /'p1'
    /'' -> /'p2'

    """


