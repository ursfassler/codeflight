# language: en

Feature: generate an variable overview
  As a user of the tool
  I want to easily see all variables of my project
  In order to get used to the tool and see all possible information


Scenario: print a simple variable overview
  Given I have the AST:
    """
    project 'The Super Project'
        package
            variable 'varA'
    """

  When I generate the index for "/varA"

  Then I expect the output:
    """
    document /''/'varA'

    = /''/'varA'

    - Variable neighbors neighbors:/''/'varA'

    """
