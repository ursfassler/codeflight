# language: en

Feature: draw variable neighbors
  As an architect
  I want to see the neighbors of a variable
  In order to understand the role of the variable and the dependencies


Scenario: draw variable neighbors
  Given I have the AST:
    """
    project
        package
            variable 'varA' 1 -> 2
            class 'ClassB'
                method 'm1' 2
            class 'ClassC'
                method 'm1' -> 1
    """

  When I generate the neighbors for "/varA"

  Then I expect the output:
    """
    graph

    cluster  /''         index:/''
    variable /'varA'   ! index:/''/'varA'
    class    /'ClassB'   index:/''/'ClassB'
    class    /'ClassC'   index:/''/'ClassC'


    /'varA'   -> /'ClassB'
    /'ClassC' -> /'varA'

    """
