# language: en

Feature: generate an class overview
  As a user of the tool
  I want to easily see all output for a class
  In order to get used to the tool and see all possible information


Scenario: print a simple class overview
  Given I have the AST:
    """
    project
        package
            class 'ClassA'
            class 'ClassB'
            package 'package1'
                class 'ClassC'
                package 'package2'
                    class 'ClassD'
                    class 'ClassE'
    """

  When I generate the index for "/ClassA"

  Then I expect the output:
    """
    document /''/'ClassA'

    = /''/'ClassA'

    - Member dependencies intra-dependencies:/''/'ClassA'
    - Class neighbors neighbors:/''/'ClassA'

    """
