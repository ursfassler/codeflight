# language: en

Feature: draw class neighbors
  As an architect
  I want to see the neighbors of a class
  In order to see all dependencies


Scenario: draw only the neighbors of the class
  Given I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'm1' -> b
                method 'm2' -> d
            class 'ClassB' b
            class 'ClassC' c
            class 'ClassD' d
                method 'm3' -> c
    """

  When I generate the neighbors for "/ClassA"

  Then I expect the output:
    """
    graph

    cluster /''         index:/''
    class   /'ClassA' ! index:/''/'ClassA'
    class   /'ClassB'   index:/''/'ClassB'
    class   /'ClassD'   index:/''/'ClassD'


    /'ClassA' -> /'ClassB'
    /'ClassA' -> /'ClassD'

    """


Scenario: don't draw dependencies between neighbors of interested class
  Given I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'm1' -> b
                method 'm2' -> c
            class 'ClassB' b
            class 'ClassC' c
                method 'm3' -> b
    """

  When I generate the neighbors for "/ClassA"

  Then I expect the output:
    """
    graph

    cluster /''         index:/''
    class   /'ClassA' ! index:/''/'ClassA'
    class   /'ClassB'   index:/''/'ClassB'
    class   /'ClassC'   index:/''/'ClassC'


    /'ClassA' -> /'ClassB'
    /'ClassA' -> /'ClassC'

    """


Scenario: draw dependencies to other packages
  Given I have the AST:
    """
    project
        package
            class 'ClassA' a
                method 'm1' -> b
                method 'm2' -> c
            package 'p1'
                class 'ClassB' b
                package 'p2'
                    class 'ClassC' c
                class 'ClassD' d
                    method 'm3' -> a
    """

  When I generate the neighbors for "/ClassA"

  Then I expect the output:
    """
    graph

    cluster /''             index:/''
    class   /'ClassA'     ! index:/''/'ClassA'

    cluster /''/'p1'        index:/''/'p1'
    class   /'ClassB'       index:/''/'p1'/'ClassB'
    class   /'ClassD'       index:/''/'p1'/'ClassD'

    cluster /''/'p1'/'p2'   index:/''/'p1'/'p2'
    class   /'ClassC'       index:/''/'p1'/'p2'/'ClassC'


    /'ClassA' -> /'ClassB' dependencies-between-packages::/'':/''/'p1'
    /'ClassA' -> /'ClassC' dependencies-between-packages::/'':/''/'p1'/'p2'
    /'ClassD' -> /'ClassA' dependencies-between-packages::/''/'p1':/''

    """


Scenario: draw the encapsulated class neighbor of the class
  Given I have the AST:
    """
    project
        package
            class 'ClassA' 1
                class 'ClassB' -> 1 2
                class 'ClassC' 2
    """

  When I generate the neighbors for "/ClassA/ClassB"

  Then I expect the output:
    """
    graph

    cluster /''                  index:/''
    class   /'ClassA'/'ClassB' ! index:/''/'ClassA'/'ClassB'
    class   /'ClassA'            index:/''/'ClassA'
    class   /'ClassA'/'ClassC'   index:/''/'ClassA'/'ClassC'


    /'ClassA'/'ClassB' -> /'ClassA'
    /'ClassA'/'ClassB' -> /'ClassA'/'ClassC'

    """


Scenario: don't draw duplicated connections
  Given I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'm1' -> b b
            class 'ClassB' b
    """

  When I generate the neighbors for "/ClassA"

  Then I expect the output:
    """
    graph

    cluster /''         index:/''
    class   /'ClassA' ! index:/''/'ClassA'
    class   /'ClassB'   index:/''/'ClassB'


    /'ClassA' -> /'ClassB'

    """


Scenario: don't draw connection to itself
  Given I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'm1' -> 2
                method 'm2' 2
    """

  When I generate the neighbors for "/ClassA"

  Then I expect the output:
    """
    graph

    cluster /''         index:/''
    class   /'ClassA' ! index:/''/'ClassA'



    """


Scenario: draw cycles within package
  Given I have the AST:
    """
    project
        package
            class 'ClassA' a
                method 'm1' -> b
            class 'ClassB' b
                method 'm2' -> a
    """

  When I generate the neighbors for "/ClassA"

  Then I expect the output:
    """
    graph

    cluster /''         index:/''
    class   /'ClassA' ! index:/''/'ClassA'
    class   /'ClassB'   index:/''/'ClassB'


    /'ClassA' -> /'ClassB'
    /'ClassB' -> /'ClassA'

    """


Scenario: draw cycles with other package
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA' a
                    method 'm1' -> b
            package 'b'
                class 'ClassB' b
                    method 'm2' -> a
    """

  When I generate the neighbors for "/a/ClassA"

  Then I expect the output:
    """
    graph

    cluster /''/'a'     index:/''/'a'
    class   /'ClassA' ! index:/''/'a'/'ClassA'

    cluster /''/'b'     index:/''/'b'
    class   /'ClassB'   index:/''/'b'/'ClassB'


    /'ClassA' -> /'ClassB' dependencies-between-packages::/''/'a':/''/'b'
    /'ClassB' -> /'ClassA' dependencies-between-packages::/''/'b':/''/'a'

    """


Scenario: draw function neighbors
  Given I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'm1' 1 -> 2
            function 'funcB' 2
            function 'funcC' -> 1
    """

  When I generate the neighbors for "/ClassA"

  Then I expect the output:
    """
    graph

    cluster  /''         index:/''
    class    /'ClassA' ! index:/''/'ClassA'
    function /'funcB'    index:/''/'funcB'
    function /'funcC'    index:/''/'funcC'


    /'ClassA' -> /'funcB'
    /'funcC'  -> /'ClassA'

    """
