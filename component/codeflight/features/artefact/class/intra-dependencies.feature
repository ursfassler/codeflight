# language: en

Feature: draw intra class dependencies
  As an architect
  I want to see which methods access which fields
  In order to see if I can split the class


Scenario: draw a simple class
  Given I have the AST:
    """
    project
        package
            class 'ClassA'
                field 'Field1' f1
                method 'Method1' -> f2
                method 'Method2' m2 -> f1
                field 'Field2' f2
                method 'Method3' -> m2
    """

  When I generate the intra-dependencies for "/ClassA"

  Then I expect the output:
    """
    graph /''/'ClassA'

    field  /'Field1'
    method /'Method1'
    method /'Method2'
    field  /'Field2'
    method /'Method3'

    /'Method1' -> /'Field2'
    /'Method2' -> /'Field1'
    /'Method3' -> /'Method2'

    """


Scenario: draw classes in packages
  Given I have the AST:
    """
    project
        package
            package 'Package1'
                class 'ClassA' c1
                    field 'Field1'
                package 'Package2'
                    class 'ClassB'
                        method 'Method1' -> c1
        """

  When I generate the intra-dependencies for "/Package1/ClassA"

  Then I expect the output:
    """
    graph /''/'Package1'/'ClassA'

    field /'Field1'


    """

  When I generate the intra-dependencies for "/Package1/Package2/ClassB"

  Then I expect the output:
    """
    graph /''/'Package1'/'Package2'/'ClassB'

    method /'Method1'


    """


Scenario: draw a dependency to a class definition inside a class
  Given I have the AST:
    """
    project
        package
            class 'ClassA'
                class 'ClassB' 1
                field 'Field1' -> 1
                method 'Method1' -> 1
    """

  When I generate the intra-dependencies for "/ClassA"

  Then I expect the output:
    """
    graph /''/'ClassA'

    class  /'ClassB'  index:/''/'ClassA'/'ClassB'
    field  /'Field1'
    method /'Method1'

    /'Field1'  -> /'ClassB'
    /'Method1' -> /'ClassB'

    """


Scenario: draw dependency only once
  Given I have the AST:
    """
    project
        package
            class 'ClassA'
                field 'Field1' 1
                method 'Method1' -> 1 1 1
    """

  When I generate the intra-dependencies for "/ClassA"

  Then I expect the output:
    """
    graph /''/'ClassA'

    field  /'Field1'
    method /'Method1'

    /'Method1' -> /'Field1'

    """


Scenario: use correct link for a class definition inside a class in a sub package
  Given I have the AST:
    """
    project
        package
            package 'sub'
                class 'ClassA'
                    class 'ClassB'
    """

  When I generate the intra-dependencies for "/sub/ClassA"

  Then I expect the output:
    """
    graph /''/'sub'/'ClassA'

    class /'ClassB' index:/''/'sub'/'ClassA'/'ClassB'


    """
