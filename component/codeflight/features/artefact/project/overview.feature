# language: en

Feature: generate an overview
  As a user of the tool
  I want to have an entry point to the analysis
  In order to get used to the tool


Scenario: print project overview
  Given I have the AST:
    """
    project 'The Super Project'
        package
            class 'ClassA'
            class 'ClassB'
            package 'package1'
                class 'ClassC'
                package 'package2'
                    class 'ClassD'
                    class 'ClassE'
    """

  When I generate the index

  Then I expect the output:
    """
    document The Super Project

    = Project

    - All packages all-packages:
    - All classes all-classes:
    - All functions all-functions:
    - All variables all-variables:
    - Packages with cylic dependencies packages-with-cyclic-dependencies:
    - Stable dependency principle violations stable-dependency-principle-violations:

    """
