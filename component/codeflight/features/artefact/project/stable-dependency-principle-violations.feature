# language: en

Feature: Dependencies which violate the stable dendency principle
  As an architect
  I want easily see if and which dependencies violate the stable dendency principle
  In order to move some classes or fix the problem otherwise


Scenario: print a list of dependencies and packages that violate the SDP
  Given I have the AST:
    """
    project
        package
            package 'instable1'
                class 'A' -> stable
            package 'instable2'
                class 'A' -> stable
            package 'stable'
                class 'A' stable -> flexible
                class 'B' -> flexible
            package 'flexible'
                class 'A' flexible -> x
                class 'B' -> x
                class 'C' -> x
            package 'x'
                class 'A' x
    """

  When I generate the stable-dependency-principle-violations

  Then I expect the output:
    """
    document Stable dependency principle violations

    = Stable dependency principle violations

    metric                         value
    Number of violations           1
    Number of dependencies         4
    Violation ratio                0.250
    Number of involved packages    2

    == Dependencies

    from                                                                       to                                                                           stability
    /''/'stable' dependencies-between-packages::/''/'stable':/''/'flexible'    /''/'flexible' dependencies-between-packages::/''/'stable':/''/'flexible'    -0.100

    == Packages

    Package                                in violations    instability
    /''/'stable' index:/''/'stable'        1                0.500
    /''/'flexible' index:/''/'flexible'    1                0.600


    """


Scenario: reduce output when no violations are found
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA' -> 2
            package 'b'
                class 'ClassB' 2
    """

  When I generate the stable-dependency-principle-violations

  Then I expect the output:
    """
    document Stable dependency principle violations

    = Stable dependency principle violations

    metric                         value
    Number of violations           0
    Number of dependencies         1
    Violation ratio                0.000
    Number of involved packages    0


    """


Scenario: handle empty project
  Given I have the AST:
    """
    project
    """

  When I generate the stable-dependency-principle-violations

  Then I expect the output:
    """
    document Stable dependency principle violations

    = Stable dependency principle violations

    metric                         value
    Number of violations           0
    Number of dependencies         0
    Violation ratio                0.000
    Number of involved packages    0


    """
