# language: en

Feature: all classes
  As a user of the tool
  I want to easily see all classes in my project
  In order to get an overview of the classes


Scenario: print all classes
  Given I have the AST:
    """
    project 'The Super Project'
        package
            class 'ClassA'
                method 'm1' -> f2 m2
                field 'f1'
                field 'f2' f2
                method 'm2' m2 -> f3
                field 'f3' f3
            package 'package'
                class 'ClassA'
                    method 'm1' -> f3 func3
                function 'f1' -> func3
                package 'package1'
                variable 'v1' v1
                function 'bar' bar -> foo
                variable 'v2'
                function 'f3' func3 -> v1
                package 'package2'
                    function 'foo' foo -> bar
    """

  When I generate the all-classes

  Then I expect the output:
    """
    document All classes

    = All classes

    Name                                                   Methods    Fields    LCOM4    fan-in    fan-out    instability
    /''/'ClassA' index:/''/'ClassA'                        2          3         2        1         0          0.00
    /''/'package'/'ClassA' index:/''/'package'/'ClassA'    1          0         1        0         2          1.00


    """


Scenario: print class in class
  Given I have the AST:
    """
    project 'test'
        package
            class 'ClassA'
                class 'ClassB'
    """

  When I generate the all-classes

  Then I expect the output:
    """
    document All classes

    = All classes

    Name                                                 Methods    Fields    LCOM4    fan-in    fan-out    instability
    /''/'ClassA' index:/''/'ClassA'                      0          0         1        0         0          0.00
    /''/'ClassA'/'ClassB' index:/''/'ClassA'/'ClassB'    0          0         0        0         0          0.00


    """
