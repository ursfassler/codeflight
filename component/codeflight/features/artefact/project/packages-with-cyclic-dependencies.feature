# language: en

Feature: Packages with cyclic dependencies
  As an architect
  I want easily see if and which packages have cyclic dependencies
  In order to remove the cycles


Scenario: print a list of packages with cyclic dependencies
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA' 1 -> 2
                function 'foo' -> 2 bar
            package 'b'
                class 'ClassB' 2 -> 1
                function 'bar' bar
            package 'c'
                class 'ClassC' -> 1
    """

  When I generate the packages-with-cyclic-dependencies

  Then I expect the output:
    """
    document Packages with cyclic dependencies

    = Packages with cyclic dependencies

    metric                             value
    Has cycles                         true
    Number of involved packages        2
    Total number of packages           4
    Packages in cycles ratio           0.500
    Number of involved dependencies    2
    Total number of dependencies       3
    Dependencies in cycles ratio       0.667
    Number of cycles                   1

    == Cycle cluster 1

    === Involved packages

    Package                  cycle fan-in    cycle fan-out    in cycles
    /''/'a' index:/''/'a'    1               3                1
    /''/'b' index:/''/'b'    3               1                1

    === Involved dependencies

    from                     to                       references                                          stability    in cycles
    /''/'a' index:/''/'a'    /''/'b' index:/''/'b'    3 dependencies-between-packages::/''/'a':/''/'b'    0.350        1
    /''/'b' index:/''/'b'    /''/'a' index:/''/'a'    1 dependencies-between-packages::/''/'b':/''/'a'    -0.350       1


    """


Scenario: reduce output when no cycles are found
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA' -> 2
            package 'b'
                class 'ClassB' 2
    """

  When I generate the packages-with-cyclic-dependencies

  Then I expect the output:
    """
    document Packages with cyclic dependencies

    = Packages with cyclic dependencies

    metric                             value
    Has cycles                         false
    Number of involved packages        0
    Total number of packages           3
    Packages in cycles ratio           0.000
    Number of involved dependencies    0
    Total number of dependencies       1
    Dependencies in cycles ratio       0.000
    Number of cycles                   0


    """


Scenario: include global package
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA' 1 -> 2
            class 'ClassB' 2 -> 1
    """

  When I generate the packages-with-cyclic-dependencies

  Then I expect the output:
    """
    document Packages with cyclic dependencies

    = Packages with cyclic dependencies

    metric                             value
    Has cycles                         true
    Number of involved packages        2
    Total number of packages           2
    Packages in cycles ratio           1.000
    Number of involved dependencies    2
    Total number of dependencies       2
    Dependencies in cycles ratio       1.000
    Number of cycles                   1

    == Cycle cluster 1

    === Involved packages

    Package                  cycle fan-in    cycle fan-out    in cycles
    /'' index:/''            1               1                1
    /''/'a' index:/''/'a'    1               1                1

    === Involved dependencies

    from                     to                       references                                      stability    in cycles
    /'' index:/''            /''/'a' index:/''/'a'    1 dependencies-between-packages::/'':/''/'a'    0.000        1
    /''/'a' index:/''/'a'    /'' index:/''            1 dependencies-between-packages::/''/'a':/''    0.000        1


    """


Scenario: detect cyclic dependency with 3 packages
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA' 1 -> 2
            package 'b'
                class 'ClassB' 2 -> 3
            package 'c'
                class 'ClassC' 3 -> 1
    """

  When I generate the packages-with-cyclic-dependencies

  Then I expect the output:
    """
    document Packages with cyclic dependencies

    = Packages with cyclic dependencies

    metric                             value
    Has cycles                         true
    Number of involved packages        3
    Total number of packages           4
    Packages in cycles ratio           0.750
    Number of involved dependencies    3
    Total number of dependencies       3
    Dependencies in cycles ratio       1.000
    Number of cycles                   1

    == Cycle cluster 1

    === Involved packages

    Package                  cycle fan-in    cycle fan-out    in cycles
    /''/'a' index:/''/'a'    1               1                1
    /''/'b' index:/''/'b'    1               1                1
    /''/'c' index:/''/'c'    1               1                1

    === Involved dependencies

    from                     to                       references                                          stability    in cycles
    /''/'a' index:/''/'a'    /''/'b' index:/''/'b'    1 dependencies-between-packages::/''/'a':/''/'b'    0.000        1
    /''/'b' index:/''/'b'    /''/'c' index:/''/'c'    1 dependencies-between-packages::/''/'b':/''/'c'    0.000        1
    /''/'c' index:/''/'c'    /''/'a' index:/''/'a'    1 dependencies-between-packages::/''/'c':/''/'a'    0.000        1


    """


Scenario: detect 2 independent cycles
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA' 1 -> 2
            package 'b'
                class 'ClassB' 2 -> 1
            package 'c'
                class 'ClassC' 3 -> 4
            package 'd'
                class 'ClassD' 4 -> 3
    """

  When I generate the packages-with-cyclic-dependencies

  Then I expect the output:
    """
    document Packages with cyclic dependencies

    = Packages with cyclic dependencies

    metric                             value
    Has cycles                         true
    Number of involved packages        4
    Total number of packages           5
    Packages in cycles ratio           0.800
    Number of involved dependencies    4
    Total number of dependencies       4
    Dependencies in cycles ratio       1.000
    Number of cycles                   2

    == Cycle cluster 1

    === Involved packages

    Package                  cycle fan-in    cycle fan-out    in cycles
    /''/'a' index:/''/'a'    1               1                1
    /''/'b' index:/''/'b'    1               1                1

    === Involved dependencies

    from                     to                       references                                          stability    in cycles
    /''/'a' index:/''/'a'    /''/'b' index:/''/'b'    1 dependencies-between-packages::/''/'a':/''/'b'    0.000        1
    /''/'b' index:/''/'b'    /''/'a' index:/''/'a'    1 dependencies-between-packages::/''/'b':/''/'a'    0.000        1

    == Cycle cluster 2

    === Involved packages

    Package                  cycle fan-in    cycle fan-out    in cycles
    /''/'c' index:/''/'c'    1               1                1
    /''/'d' index:/''/'d'    1               1                1

    === Involved dependencies

    from                     to                       references                                          stability    in cycles
    /''/'c' index:/''/'c'    /''/'d' index:/''/'d'    1 dependencies-between-packages::/''/'c':/''/'d'    0.000        1
    /''/'d' index:/''/'d'    /''/'c' index:/''/'c'    1 dependencies-between-packages::/''/'d':/''/'c'    0.000        1


    """


Scenario: detect cycle with sub-package
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA' 1 -> 2
                package 'b'
                    class 'ClassB' 2 -> 3
                    package 'c'
                        class 'ClassC' 3 -> 1
    """

  When I generate the packages-with-cyclic-dependencies

  Then I expect the output:
    """
    document Packages with cyclic dependencies

    = Packages with cyclic dependencies

    metric                             value
    Has cycles                         true
    Number of involved packages        3
    Total number of packages           4
    Packages in cycles ratio           0.750
    Number of involved dependencies    3
    Total number of dependencies       3
    Dependencies in cycles ratio       1.000
    Number of cycles                   1

    == Cycle cluster 1

    === Involved packages

    Package                                  cycle fan-in    cycle fan-out    in cycles
    /''/'a' index:/''/'a'                    1               1                1
    /''/'a'/'b' index:/''/'a'/'b'            1               1                1
    /''/'a'/'b'/'c' index:/''/'a'/'b'/'c'    1               1                1

    === Involved dependencies

    from                                     to                                       references                                                      stability    in cycles
    /''/'a' index:/''/'a'                    /''/'a'/'b' index:/''/'a'/'b'            1 dependencies-between-packages::/''/'a':/''/'a'/'b'            0.000        1
    /''/'a'/'b' index:/''/'a'/'b'            /''/'a'/'b'/'c' index:/''/'a'/'b'/'c'    1 dependencies-between-packages::/''/'a'/'b':/''/'a'/'b'/'c'    0.000        1
    /''/'a'/'b'/'c' index:/''/'a'/'b'/'c'    /''/'a' index:/''/'a'                    1 dependencies-between-packages::/''/'a'/'b'/'c':/''/'a'        0.000        1


    """


Scenario: print info in how many cycles a node and edge is
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA' a -> b c
            package 'b'
                class 'ClassB' b -> c d
            package 'c'
                class 'ClassC' c -> d
            package 'd'
                class 'ClassD' d -> a
    """

  When I generate the packages-with-cyclic-dependencies

  Then I expect the output:
    """
    document Packages with cyclic dependencies

    = Packages with cyclic dependencies

    metric                             value
    Has cycles                         true
    Number of involved packages        4
    Total number of packages           5
    Packages in cycles ratio           0.800
    Number of involved dependencies    6
    Total number of dependencies       6
    Dependencies in cycles ratio       1.000
    Number of cycles                   3

    == Cycle cluster 1

    === Involved packages

    Package                  cycle fan-in    cycle fan-out    in cycles
    /''/'a' index:/''/'a'    1               2                3
    /''/'b' index:/''/'b'    1               2                2
    /''/'c' index:/''/'c'    2               1                2
    /''/'d' index:/''/'d'    2               1                3

    === Involved dependencies

    from                     to                       references                                          stability    in cycles
    /''/'a' index:/''/'a'    /''/'b' index:/''/'b'    1 dependencies-between-packages::/''/'a':/''/'b'    0.000        2
    /''/'a' index:/''/'a'    /''/'c' index:/''/'c'    1 dependencies-between-packages::/''/'a':/''/'c'    0.333        1
    /''/'b' index:/''/'b'    /''/'c' index:/''/'c'    1 dependencies-between-packages::/''/'b':/''/'c'    0.333        1
    /''/'b' index:/''/'b'    /''/'d' index:/''/'d'    1 dependencies-between-packages::/''/'b':/''/'d'    0.333        1
    /''/'c' index:/''/'c'    /''/'d' index:/''/'d'    1 dependencies-between-packages::/''/'c':/''/'d'    0.000        2
    /''/'d' index:/''/'d'    /''/'a' index:/''/'a'    1 dependencies-between-packages::/''/'d':/''/'a'    -0.333       3


    """


Scenario: don't count cycles when graph exceeds maximum number of nodes or dependencies
  Given I set the maximal number of nodes to 2 and vertices to 5 for cycle count
  And I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA' 1 -> 2
            package 'b'
                class 'ClassB' 2 -> 1
            package 'c'
                class 'ClassC' 3 -> 4
            package 'd'
                class 'ClassD' 4 -> 3 5
            package 'e'
                class 'ClassE' 5 -> 3 4
    """

  When I generate the packages-with-cyclic-dependencies

  Then I expect the output:
    """
    document Packages with cyclic dependencies

    = Packages with cyclic dependencies

    metric                             value
    Has cycles                         true
    Number of involved packages        5
    Total number of packages           6
    Packages in cycles ratio           0.833
    Number of involved dependencies    7
    Total number of dependencies       7
    Dependencies in cycles ratio       1.000

    == Cycle cluster 1

    === Involved packages

    Package                  cycle fan-in    cycle fan-out    in cycles
    /''/'a' index:/''/'a'    1               1                1
    /''/'b' index:/''/'b'    1               1                1

    === Involved dependencies

    from                     to                       references                                          stability    in cycles
    /''/'a' index:/''/'a'    /''/'b' index:/''/'b'    1 dependencies-between-packages::/''/'a':/''/'b'    0.000        1
    /''/'b' index:/''/'b'    /''/'a' index:/''/'a'    1 dependencies-between-packages::/''/'b':/''/'a'    0.000        1

    == Cycle cluster 2

    === Involved packages

    Package                  cycle fan-in    cycle fan-out
    /''/'c' index:/''/'c'    2               1
    /''/'d' index:/''/'d'    2               2
    /''/'e' index:/''/'e'    1               2

    === Involved dependencies

    from                     to                       references                                          stability
    /''/'c' index:/''/'c'    /''/'d' index:/''/'d'    1 dependencies-between-packages::/''/'c':/''/'d'    -0.167
    /''/'d' index:/''/'d'    /''/'c' index:/''/'c'    1 dependencies-between-packages::/''/'d':/''/'c'    0.167
    /''/'d' index:/''/'d'    /''/'e' index:/''/'e'    1 dependencies-between-packages::/''/'d':/''/'e'    -0.167
    /''/'e' index:/''/'e'    /''/'c' index:/''/'c'    1 dependencies-between-packages::/''/'e':/''/'c'    0.333
    /''/'e' index:/''/'e'    /''/'d' index:/''/'d'    1 dependencies-between-packages::/''/'e':/''/'d'    0.167


    """
