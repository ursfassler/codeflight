# language: en

Feature: all packages
  As a user of the tool
  I want to easily see all packages in my project
  In order to get an overview of the packages


Scenario: print all packages
  Given I have the AST:
    """
    project 'The Super Project'
        package
            class 'ClassA'
                method 'm1' -> f2 m2
                field 'f1'
                field 'f2' f2
                method 'm2' m2 -> f3
                field 'f3' f3
            package 'package'
                class 'ClassA'
                    method 'm1' -> f3 func3
                function 'f1' -> func3
                package 'package1'
                variable 'v1' v1
                function 'bar' bar -> foo
                variable 'v2'
                function 'f3' func3 -> v1
                package 'package2'
                    function 'foo' foo -> bar
    """

  When I generate the all-packages

  Then I expect the output:
    """
    document All packages

    = All packages

    Name                                                       Packages    Classes    Functions    Variables    LCOM4    fan-in    fan-out    instability    has cycles
    /'' index:/''                                              1           1          0            0            1        1         0          0.00           false
    /''/'package' index:/''/'package'                          2           1          3            2            3        1         2          0.67           true
    /''/'package'/'package1' index:/''/'package'/'package1'    0           0          0            0            0        0         0          0.00           false
    /''/'package'/'package2' index:/''/'package'/'package2'    0           0          1            0            1        1         1          0.50           false


    """
