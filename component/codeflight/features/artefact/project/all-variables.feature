# language: en

Feature: all variables
  As a user of the tool
  I want to easily see all variables in my project
  In order to get an overview of the variables


Scenario: print the variables
  Given I have the AST:
    """
    project 'The Super Project'
        package
            class 'ClassA'
                method 'm1' -> f2 m2
                field 'f1'
                field 'f2' f2
                method 'm2' m2 -> f3
                field 'f3' f3
            package 'package'
                class 'ClassA'
                    method 'm1' -> f3 func3
                function 'f1' -> func3
                package 'package1'
                variable 'v1' v1
                function 'bar' bar -> foo
                variable 'v2'
                function 'f3' func3 -> v1
                package 'package2'
                    function 'foo' foo -> bar
    """

  When I generate the all-variables

  Then I expect the output:
    """
    document All variables

    = All variables

    Name                                           fan-in    fan-out    instability
    /''/'package'/'v1' index:/''/'package'/'v1'    1         0          0.00
    /''/'package'/'v2' index:/''/'package'/'v2'    0         0          0.00


    """
