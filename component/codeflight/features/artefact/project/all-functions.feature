# language: en

Feature: all functions
  As a user of the tool
  I want to easily see all functions in my project
  In order to get an overview of the functions


Scenario: print the functions
  Given I have the AST:
    """
    project 'The Super Project'
        package
            class 'ClassA'
                method 'm1' -> f2 m2
                field 'f1'
                field 'f2' f2
                method 'm2' m2 -> f3
                field 'f3' f3
            package 'package'
                class 'ClassA'
                    method 'm1' -> f3 func3
                function 'f1' -> func3
                package 'package1'
                variable 'v1' v1
                function 'bar' bar -> foo
                variable 'v2'
                function 'f3' func3 -> v1
                package 'package2'
                    function 'foo' foo -> bar
    """

  When I generate the all-functions

  Then I expect the output:
    """
    document All functions

    = All functions

    Name                                                                   fan-in    fan-out    instability
    /''/'package'/'f1' index:/''/'package'/'f1'                            0         1          1.00
    /''/'package'/'bar' index:/''/'package'/'bar'                          1         1          0.50
    /''/'package'/'f3' index:/''/'package'/'f3'                            2         1          0.33
    /''/'package'/'package2'/'foo' index:/''/'package'/'package2'/'foo'    1         1          0.50


    """
