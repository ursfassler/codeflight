# language: en

Feature: draw the dependencies between packages
  As an architect
  I want to know the dependencies between packages
  In order to reduce them


Scenario: generate a graph with the dependencies between 2 packages
  Given I have the AST:
    """
    project
        package
            package 'package1'
                class 'ClassA'
                    method 'Method1' m1
                package 'package2'
                    class 'ClassB'
                        method 'Method2' -> m1
    """

  When I generate the dependencies-between-packages with "/package1/package2, /package1"

  Then I expect the output:
    """
    graph

    cluster /''/'package1'/'package2' index:/''/'package1'/'package2'
    class   /'ClassB'                 index:/''/'package1'/'package2'/'ClassB'

    cluster /''/'package1'            index:/''/'package1'
    class   /'ClassA'                 index:/''/'package1'/'ClassA'


    /'ClassB' -> /'ClassA' '1' dependencies-between-packages::/''/'package1'/'package2':/''/'package1'

    """


Scenario: don't draw duplicated edges
  Given I have the AST:
    """
    project
        package
            package 'package1'
                class 'ClassA'
                    method 'Method1' m1
                    field 'Field1' f1
            package 'package2'
                class 'ClassB'
                    method 'Method2' -> m1
                    method 'Field2' -> f1
    """

  When I generate the dependencies-between-packages with "/package1, /package2"

  Then I expect the output:
    """
    graph

    cluster /''/'package1' index:/''/'package1'
    class   /'ClassA'      index:/''/'package1'/'ClassA'

    cluster /''/'package2' index:/''/'package2'
    class   /'ClassB'      index:/''/'package2'/'ClassB'


    /'ClassB' -> /'ClassA' '1' dependencies-between-packages::/''/'package2':/''/'package1'

    """


Scenario: don't draw edges to itself
  Given I have the AST:
    """
    project
        package
            package 'package1'
                class 'ClassA'
                    method 'Method1' -> f1
                    field 'Field1' f1
            package 'package2'
                class 'ClassB'
                    method 'Field2' -> f1
    """

  When I generate the dependencies-between-packages with "/package1, /package2"

  Then I expect the output:
    """
    graph

    cluster /''/'package1' index:/''/'package1'
    class   /'ClassA'      index:/''/'package1'/'ClassA'

    cluster /''/'package2' index:/''/'package2'
    class   /'ClassB'      index:/''/'package2'/'ClassB'


    /'ClassB' -> /'ClassA' '1' dependencies-between-packages::/''/'package2':/''/'package1'

    """


Scenario: generate a graph with the dependencies between 2 packages with variables in them
  Given I have the AST:
    """
    project
        package
            package 'package1'
                variable 'v1' v1
                package 'package2'
                    function 'f1' -> v1
    """

  When I generate the dependencies-between-packages with "/package1/package2, /package1"

  Then I expect the output:
    """
    graph

    cluster  /''/'package1'/'package2' index:/''/'package1'/'package2'
    function /'f1'                     index:/''/'package1'/'package2'/'f1'

    cluster  /''/'package1'            index:/''/'package1'
    variable /'v1'                     index:/''/'package1'/'v1'


    /'f1' -> /'v1' '1' dependencies-between-packages::/''/'package1'/'package2':/''/'package1'

    """


Scenario: generate a graph with the dependencies between root and 2 sub package
  Given I have the AST:
    """
    project
        package
            package 'p1'
                class 'ClassB'
                    method 'Method1' m1 -> m2
            package 'p2'
                class 'ClassC'
                    method 'Method3' -> m1
            class 'ClassA'
                method 'Method2' m2
    """

  When I generate the dependencies-between-packages with "/p1, "

  Then I expect the output:
    """
    graph

    cluster /''/'p1'  index:/''/'p1'
    class   /'ClassB' index:/''/'p1'/'ClassB'

    cluster /''       index:/''
    class   /'ClassA' index:/''/'ClassA'
    package /'p2'     index:/''/'p2'


    /'ClassB' -> /'ClassA' '1' dependencies-between-packages::/''/'p1':/''
    /'p2'     -> /'ClassB' '1' dependencies-between-packages::/''/'p2':/''/'p1'

    """

  When I generate the dependencies-between-packages with "/p2, /p1"

  Then I expect the output:
    """
    graph

    cluster /''/'p2'  index:/''/'p2'
    class   /'ClassC' index:/''/'p2'/'ClassC'

    cluster /''/'p1'  index:/''/'p1'
    class   /'ClassB' index:/''/'p1'/'ClassB'


    /'ClassC' -> /'ClassB' '1' dependencies-between-packages::/''/'p2':/''/'p1'

    """


Scenario: only include nodes that are involved in the dependencies between packages
  Given I have the AST:
    """
    project
        package
            package 'p1'
                class 'C1' 1
                class 'C1A' -> 1
            package 'p2'
                class 'C2' -> 1
            package 'p3'
                class 'C3'
    """

  When I generate the dependencies-between-packages with "/p1"
  Then I expect the output:
    """
    graph

    cluster /''/'p1' index:/''/'p1'



    """

  When I generate the dependencies-between-packages with "/p1, /p2"
  Then I expect the output:
    """
    graph

    cluster /''/'p1' index:/''/'p1'
    class   /'C1'    index:/''/'p1'/'C1'

    cluster /''/'p2' index:/''/'p2'
    class   /'C2'    index:/''/'p2'/'C2'


    /'C2' -> /'C1' '1' dependencies-between-packages::/''/'p2':/''/'p1'

    """

  When I generate the dependencies-between-packages with "/p1, /p2, /p3"
  Then I expect the output:
    """
    graph

    cluster /''/'p1' index:/''/'p1'
    class   /'C1'    index:/''/'p1'/'C1'

    cluster /''/'p2' index:/''/'p2'
    class   /'C2'    index:/''/'p2'/'C2'

    cluster /''/'p3' index:/''/'p3'


    /'C2' -> /'C1' '1' dependencies-between-packages::/''/'p2':/''/'p1'

    """


Scenario: include sub-packages in dependency graph
  Given I have the AST:
    """
    project
        package
            package 'a'
                package 'b'
                    package 'c'
                        class 'D' 1
                function 'e' -> 2
            package 'x'
                package 'y'
                    class 'z' -> 1
                    variable 'w' 2
    """

  When I generate the dependencies-between-packages with "/a, /x"

  Then I expect the output:
    """
    graph

    cluster  /''/'a' index:/''/'a'
    function /'e'    index:/''/'a'/'e'
    package  /'b'    index:/''/'a'/'b'

    cluster  /''/'x' index:/''/'x'
    package  /'y'    index:/''/'x'/'y'


    /'e' -> /'y' '1' dependencies-between-packages::/''/'a':/''/'x'/'y'
    /'y' -> /'b' '1' dependencies-between-packages::/''/'x'/'y':/''/'a'/'b'

    """


Scenario: include global package with cyclic dependency
  Given I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA' 1 -> 2
            class 'ClassB' 2 -> 1
    """

  When I generate the dependencies-between-packages with "/a, "

  Then I expect the output:
    """
    graph

    cluster /''/'a'   index:/''/'a'
    class   /'ClassA' index:/''/'a'/'ClassA'

    cluster /''       index:/''
    class   /'ClassB' index:/''/'ClassB'


    /'ClassA' -> /'ClassB' '1' dependencies-between-packages::/''/'a':/''
    /'ClassB' -> /'ClassA' '1' dependencies-between-packages::/'':/''/'a'

    """


Scenario: show number of dependencies between elements of packages
  Given I have the AST:
    """
    project
        package
            package 'a'
                package 'b'
                    package 'c'
                        class 'D' 1
                        function 'f1' -> 2
                    function 'f2' -> 3 1
                function 'f3' -> 3 1 4 5
            package 'x'
                package 'y'
                    package 'yy'
                        class 'z' 2 -> 1 1 1
                        variable 'w' 3 -> 1 1
                class 'v'
                    method 'm1' 4
                    method 'm2' 5
    """

  When I generate the dependencies-between-packages with "/a, /x"

  Then I expect the output:
    """
    graph

    cluster  /''/'a' index:/''/'a'
    package  /'b'    index:/''/'a'/'b'
    function /'f3'   index:/''/'a'/'f3'

    cluster  /''/'x' index:/''/'x'
    package  /'y'    index:/''/'x'/'y'
    class    /'v'    index:/''/'x'/'v'


    /'b'  -> /'y' '2' dependencies-between-packages::/''/'a'/'b':/''/'x'/'y'
    /'y'  -> /'b' '2' dependencies-between-packages::/''/'x'/'y':/''/'a'/'b'
    /'f3' -> /'y' '1' dependencies-between-packages::/''/'a':/''/'x'/'y'
    /'f3' -> /'v' '1' dependencies-between-packages::/''/'a':/''/'x'

    """


Scenario: add link between packages
  Given I have the AST:
    """
    project
        package
            package 'a'
                package 'b'
                    function 'f1' -> 1
            package 'x'
                package 'y'
                    variable 'v1' 1
    """

  When I generate the dependencies-between-packages with "/a, /x"

  Then I expect the output:
    """
    graph

    cluster /''/'a' index:/''/'a'
    package /'b'    index:/''/'a'/'b'

    cluster /''/'x' index:/''/'x'
    package /'y'    index:/''/'x'/'y'


    /'b' -> /'y' '1' dependencies-between-packages::/''/'a'/'b':/''/'x'/'y'

    """
