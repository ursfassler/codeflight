# language: en

Feature: draw function neighbors
  As an architect
  I want to see the neighbors of a function
  In order to understand the role of the function and the dependencies


Scenario: draw the neighbors of the function
  Given I have the AST:
    """
    project
        package
            function 'funcA' -> b d
            function 'funcB' b
            function 'funcC' c
            function 'funcD' d -> c
    """

  When I generate the neighbors for "/funcA"

  Then I expect the output:
    """
    graph

    cluster  /''        index:/''
    function /'funcA' ! index:/''/'funcA'
    function /'funcB'   index:/''/'funcB'
    function /'funcD'   index:/''/'funcD'


    /'funcA' -> /'funcB'
    /'funcA' -> /'funcD'

    """
