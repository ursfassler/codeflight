# language: en

Feature: generate an function overview
  As a user of the tool
  I want to easily see all functions of my project
  In order to get used to the tool and see all possible information


Scenario: print a simple functions overview
  Given I have the AST:
    """
    project 'The Super Project'
        package
            function 'funcA'
    """

  When I generate the index for "/funcA"

  Then I expect the output:
    """
    document /''/'funcA'

    = /''/'funcA'

    - Function neighbors neighbors:/''/'funcA'

    """
