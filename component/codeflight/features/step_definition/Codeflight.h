/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/Codeflight.h"
#include <memory>

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>


namespace component::codeflight::features
{


class Codeflight
{
public:
  artefact::common::package_cycles::Settings cycleSettings{};
  std::unique_ptr<codeflight::Codeflight> codeflight{};
  std::string output{};
};


}
