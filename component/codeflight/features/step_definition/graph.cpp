/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "component/codeflight/ast/graph/graph.h"
#include "component/codeflight/ast/Package.h"
#include "component/codeflight/ast/graph/cycle/dfsHasCycles.h"
#include "component/codeflight/ast/graph/cycle/johnson.h"
#include "component/codeflight/ast/graph/cycle/tarjan.h"
#include "component/codeflight/library/util.h"
#include <algorithm>
#include <sstream>

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>


namespace component::codeflight::features
{
namespace
{


class Graph
{
  public:
    std::map<std::string, std::unique_ptr<component::codeflight::ast::Package>> nodes{};
    component::codeflight::ast::graph::Graph graph{};
    std::vector<std::vector<const component::codeflight::ast::Node*>> cycles{};
    bool hasCycles{false};

    void add(const std::string& source, const std::string& destination)
    {
      auto sn = node(source);
      auto dn = node(destination);
      graph.add(sn, dn);
    }

    std::vector<std::string> cyclesString() const
    {
      std::vector<std::string> string{};

      for (const std::vector<const component::codeflight::ast::Node*>& set : cycles) {
        std::vector<std::string> nodeString{};
        for (const auto& node : set) {
          nodeString.push_back(node->name);
        }
        string.push_back(component::codeflight::library::join(nodeString, ", "));
      }

      return string;
    }

  private:
    component::codeflight::ast::Package* node(const std::string& name)
    {
      const auto idx = nodes.find(name);
      if (idx != nodes.end()) {
        return idx->second.get();
      }

      auto node = std::make_unique<component::codeflight::ast::Package>();
      const auto ptr = node.get();
      node->name = name;
      nodes[name] = std::move(node);

      graph.add(ptr);

      return ptr;
    }
};


GIVEN("^I have a graph with the edges$")
{
  TABLE_PARAM(edgeTable);

  cucumber::ScenarioScope<Graph> context;

  for (const auto& row : edgeTable.hashes()) {
    const auto& source = row.at("source");
    const auto& destination = row.at("destination");
    context->add(source, destination);
  }
}

WHEN("^I run the johnson simple cycle detection algorithm on the graph$")
{
  cucumber::ScenarioScope<Graph> context;

  context->cycles.clear();

  component::codeflight::ast::graph::cycle::johnson(context->graph, [&context](const component::codeflight::ast::graph::cycle::NodeList& cycle){
    context->cycles.push_back(cycle);
  });
}

WHEN("^I run the dfs has cycle detection algorithm on the graph$")
{
  cucumber::ScenarioScope<Graph> context;

  context->hasCycles = component::codeflight::ast::graph::cycle::hasCycles(context->graph);
}

WHEN("^I run the tarjan's strongly connected components algorithm on the graph$")
{
  cucumber::ScenarioScope<Graph> context;

  const auto components = component::codeflight::ast::graph::cycle::tarjan(context->graph);

  context->cycles.clear();

  for (const auto& scc : components) {
    component::codeflight::ast::graph::cycle::NodeList nodes{scc.begin(), scc.end()};
    std::sort(nodes.begin(), nodes.end(), [](const component::codeflight::ast::Node* left, const component::codeflight::ast::Node* right){
      return left->name < right->name;
    });
    context->cycles.push_back(nodes);
  }

  std::sort(context->cycles.begin(), context->cycles.end(), [](const component::codeflight::ast::graph::cycle::NodeList& left, const component::codeflight::ast::graph::cycle::NodeList& right){
    return left.at(0)->name < right.at(0)->name;
  });
}

THEN("^I expect the resulting cycles$")
{
  TABLE_PARAM(setTable);

  std::vector<std::string> expected{};
  for (const auto& row : setTable.hashes()) {
    const auto& set = row.at("nodes");
    expected.push_back(set);
  }

  cucumber::ScenarioScope<Graph> context;
  ASSERT_EQ(expected, context->cyclesString());
}

THEN("^I expect to have cycles$")
{
  cucumber::ScenarioScope<Graph> context;
  ASSERT_TRUE(context->hasCycles);
}

THEN("^I expect to have no cycles$")
{
  cucumber::ScenarioScope<Graph> context;
  ASSERT_FALSE(context->hasCycles);
}


}
}
