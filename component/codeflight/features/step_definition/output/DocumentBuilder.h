/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/DocumentBuilder.h"
#include "Table.h"
#include <string>

namespace component::codeflight::features::output
{


class DocumentBuilder :
    public artefact::interface::DocumentBuilder
{
public:
  void beginDocument(const library::Path&, const ast::Node*) override;
  void beginDocument(const std::string&, const std::optional<const ast::Node*>&) override;
  void endDocument() override;
  void beginSection(const library::Path&) override;
  void beginSection(const std::string&) override;
  void endSection() override;
  void beginParagraph() override;
  void endParagraph() override;
  void beginUnnumberedList() override;
  void endUnnumberedList() override;
  void beginListItem() override;
  void endListItem() override;
  void link(const std::string&, const artefact::interface::ArtefactIdentifier&) override;
  void beginTable(const Row&, TableType) override;
  void endTable() override;
  void beginTableRow() override;
  void endTableRow() override;
  void tableCell(const library::Path&, const artefact::interface::ArtefactIdentifier&) override;
  void tableCell(const std::string&, const std::optional<artefact::interface::ArtefactIdentifier>&) override;

  std::string str() const;

private:
  std::string output{};
  Table table{4};
  Row row{};
  std::size_t section{0};
};


}
