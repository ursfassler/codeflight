/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/ArtefactVisitor.h"
#include <string>

namespace component::codeflight::features::output
{


class Builder :
    public artefact::interface::ArtefactVisitor
{
public:
  void visit(const artefact::interface::GraphArtefact& graph) override;
  void visit(const artefact::interface::DocumentArtefact&) override;

  std::string output{};
};


}
