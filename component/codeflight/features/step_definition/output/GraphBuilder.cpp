/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "GraphBuilder.h"
#include "component/codeflight/ast/name/typename.h"
#include "format.h"
#include <stdexcept>

namespace component::codeflight::features::output
{


void GraphBuilder::beginGraph(const std::optional<library::Path>& name)
{
  Table::Row row{};
  row.push_back("graph");
  if (name) {
    row.push_back(format(*name));
  }
  header.row(row);
}

void GraphBuilder::endGraph()
{
}

void GraphBuilder::beginContainer(const library::Path& label, const std::optional<artefact::interface::ArtefactIdentifier>& link)
{
  Table::Row row{"cluster", format(label), ""};

  if (link) {
    const auto linkString = format(*link);
    row.push_back(linkString);
  }

  nodes.row(row);
}

void GraphBuilder::endContainer()
{
  nodes.row({});
}

void GraphBuilder::node(const ast::Node* node, const library::Path& label, bool emphasize, const std::optional<artefact::interface::ArtefactIdentifier>& link)
{
  const std::string emph = emphasize ? "!" : "";

  const auto labelString = format(label);
  labels[node] = labelString;

  Table::Row row{ast::name::typeName(node), labelString, emph};

  if (link) {
    const auto linkString = format(*link);
    row.push_back(linkString);
  }

  nodes.row(row);
}

void GraphBuilder::edge(const ast::Node* from, const ast::Node* to, const std::optional<std::string>& label, const std::optional<artefact::interface::ArtefactIdentifier>& link)
{
  Table::Row row{labels.at(from), "->", labels.at(to)};
  if (label) {
    row.push_back("'" + *label + "'");
  }
  if (link) {
    const auto linkString = format(*link);
    row.push_back(linkString);
  }
  edges.row(row);
}

std::string GraphBuilder::str() const
{
  return
      header.str() +
      '\n' +
      nodes.str() +
      '\n' +
      edges.str();
}


}
