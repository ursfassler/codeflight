/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DocumentBuilder.h"
#include "format.h"
#include <stdexcept>

namespace component::codeflight::features::output
{


void DocumentBuilder::beginDocument(const library::Path& title, const ast::Node* link)
{
  beginDocument(format(title), link);
}

void DocumentBuilder::beginDocument(const std::string& title, const std::optional<const ast::Node*>&)
{
  output += "document " + title + '\n';
  output += '\n';
}

void DocumentBuilder::endDocument()
{
}

void DocumentBuilder::beginSection(const library::Path& title)
{
  beginSection(format(title));
}

void DocumentBuilder::beginSection(const std::string& title)
{
  section++;
  std::string secstr(section, '=');
  output += secstr + " " + title + '\n';
  output += '\n';
}

void DocumentBuilder::endSection()
{
  section--;
}

void DocumentBuilder::beginParagraph()
{
}

void DocumentBuilder::endParagraph()
{
}

void DocumentBuilder::beginUnnumberedList()
{
}

void DocumentBuilder::endUnnumberedList()
{
}

void DocumentBuilder::beginListItem()
{
  output += "- ";
}

void DocumentBuilder::endListItem()
{
}

void DocumentBuilder::link(const std::string& text, const artefact::interface::ArtefactIdentifier& link)
{
  output += text + " " + format(link);
  output += '\n';
}

void DocumentBuilder::beginTable(const Row& header, TableType)
{
  table.clear();
  table.row(header);
}

void DocumentBuilder::endTable()
{
  output += table.str();
  output += '\n';
  table.clear();
}

void DocumentBuilder::beginTableRow()
{
  row.clear();
}

void DocumentBuilder::endTableRow()
{
  table.row(row);
  row.clear();
}

void DocumentBuilder::tableCell(const library::Path& path, const artefact::interface::ArtefactIdentifier& link)
{
  tableCell(format(path), link);
}

void DocumentBuilder::tableCell(const std::string& text, const std::optional<artefact::interface::ArtefactIdentifier>& link)
{
  auto cell = text;
  if (link) {
    cell += " " + format(*link);
  }
  row.push_back(cell);
}

std::string DocumentBuilder::str() const
{
  return output;
}


}
