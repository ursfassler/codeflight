/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace component::codeflight::library
{

class Path;

}

namespace component::codeflight::artefact::interface
{

struct ArtefactIdentifier;

}

namespace component::codeflight::features::output
{


std::string format(const library::Path&);
std::string format(const artefact::interface::ArtefactIdentifier&);


}
