/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Table.h"
#include "component/codeflight/artefact/interface/GraphBuilder.h"
#include <map>
#include <string>

namespace component::codeflight::features::output
{


class GraphBuilder :
    public artefact::interface::GraphBuilder
{
public:
  void beginGraph(const std::optional<library::Path>& name) override;
  void endGraph() override;
  void beginContainer(const library::Path&, const std::optional<artefact::interface::ArtefactIdentifier>&) override;
  void endContainer() override;
  void node(const ast::Node* node, const library::Path& label, bool emphasize, const std::optional<artefact::interface::ArtefactIdentifier>& link) override;
  void edge(const ast::Node* from, const ast::Node* to, const std::optional<std::string>&, const std::optional<artefact::interface::ArtefactIdentifier>&) override;

  std::string str() const;

private:
  Table header{};
  Table nodes{};
  Table edges{};
  std::map<const ast::Node*, std::string> labels{};
};


}
