/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <vector>

namespace component::codeflight::features::output
{


class Table
{
public:
  using Row = std::vector<std::string>;

  Table(std::size_t sepSize = 1);

  void clear();
  void row(const Row& column);
  std::string str() const;

private:
  const std::size_t sepSize;
  std::vector<Row> rows{};

};


}
