/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Builder.h"
#include "DocumentBuilder.h"
#include "GraphBuilder.h"
#include "component/codeflight/artefact/interface/DocumentArtefact.h"
#include "component/codeflight/artefact/interface/GraphArtefact.h"
#include <stdexcept>

namespace component::codeflight::features::output
{


void Builder::visit(const artefact::interface::GraphArtefact& artefact)
{
  GraphBuilder builder{};
  artefact.build(builder);
  output = builder.str();
}

void Builder::visit(const artefact::interface::DocumentArtefact& artefact)
{
  DocumentBuilder builder{};
  artefact.build(builder);
  output = builder.str();
}


}
