/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "format.h"
#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"
#include "component/codeflight/library/Path.h"
#include <stdexcept>

namespace component::codeflight::features::output
{


std::string format(const library::Path& path)
{
  std::string result{};

  for (const auto& part : path.raw()) {
    result += "/'" + part + "'";
  }

  return result;
}

std::string format(const artefact::interface::ArtefactIdentifier& value)
{
  std::string result{};

  result += value.artefact;
  result += ":" + format(value.path);
  for (const auto& arg : value.arguments) {
    result += ":" + format(arg);
  }

  return result;
}


}
