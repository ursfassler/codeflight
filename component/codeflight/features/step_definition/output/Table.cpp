/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Table.h"
#include <map>
#include <set>

namespace component::codeflight::features::output
{


Table::Table(std::size_t sepSize_) :
  sepSize{sepSize_}
{
}

void Table::clear()
{
  rows.clear();
}

void Table::row(const Row& column)
{
  rows.push_back(column);
}

std::string Table::str() const
{
  std::map<std::size_t, std::size_t> widths{};
  std::set<std::size_t> columns{};

  for (const Row& row : rows) {
    for (std::size_t column = 0; column< row.size(); column++) {
      if (!row.at(column).empty()) {
        widths[column] = std::max(widths[column], row.at(column).size());
        columns.insert(column);
      }
    }
  }

  std::string output{};

  for (const Row& row : rows) {
    std::size_t lastColumn = 0;
    for (std::size_t i = 0; i < row.size(); i++) {
      if (columns.contains(i)) {
        if (!row.at(i).empty()) {
          lastColumn = i;
        }
      }
    }

    std::string line{};
    for (const auto column : columns) {
      const auto hasColumn = row.size() > column;
      if (!hasColumn) {
        continue;
      }

      const auto& cell = row.at(column);
      line += cell;

      const auto isLastColumn = column == lastColumn;
      if (!isLastColumn) {
        const std::size_t fill = widths.at(column) - cell.size() + sepSize;
        line += std::string(fill, ' ');
      }
    }
    output += line + '\n';
  }

  return output;
}


}
