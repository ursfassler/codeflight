/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "component/codeflight/metric/generic/Lcom4.h"
#include "Codeflight.h"
#include "component/codeflight/ast/query/find.h"
#include "component/codeflight/library/util.h"
#include "component/codeflight/metric/generic/Instability.h"
#include "component/codeflight/metric/package/Fan.h"
#include "component/codeflight/metric/package/HasCycles.h"
#include "component/codeflight/metric/packageelement/Fan.h"
#include <sstream>

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>


namespace component::codeflight::features
{
namespace
{


std::vector<std::string> parseAstPath(const std::string& value)
{
  auto path = (value == "::") ? std::vector<std::string>{""} : component::codeflight::library::split("::" + value, "::");
  return path;
}

const component::codeflight::ast::Node* getNode(const std::string& stringPath)
{
  const auto path = parseAstPath(stringPath);
  cucumber::ScenarioScope<Codeflight> context;
  const auto node = component::codeflight::ast::query::find(path, context->codeflight->getProject());

  if (!node) {
    throw std::runtime_error("node not found: " + stringPath);
  }

  return node;
}


THEN("^I expect the package LCOM4 for \"([^\"]*)\" to be (\\d+)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(std::size_t, expectedValue);

  const auto node = getNode(path);

  const auto actualValue = component::codeflight::metric::generic::packageLcom4(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

THEN("^I expect the class LCOM4 for \"([^\"]*)\" to be (\\d+)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(std::size_t, expectedValue);

  const auto node = getNode(path);

  const auto actualValue = component::codeflight::metric::generic::classLcom4(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

THEN("^I expect the package element fan-in for \"([^\"]*)\" to be (\\d+)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(std::size_t, expectedValue);

  const auto node = getNode(path);

  cucumber::ScenarioScope<Codeflight> context;
  component::codeflight::metric::packageelement::FanIn fan{context->codeflight->getNeighbors()};
  const auto actualValue = fan.count(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

THEN("^I expect the package element fan-out for \"([^\"]*)\" to be (\\d+)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(std::size_t, expectedValue);

  const auto node = getNode(path);

  cucumber::ScenarioScope<Codeflight> context;
  component::codeflight::metric::packageelement::FanOut fan{context->codeflight->getNeighbors()};
  const auto actualValue = fan.count(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

THEN("^I expect the package element instability for \"([^\"]*)\" to be (\\d+.\\d+)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(double, expectedValue);

  const auto node = getNode(path);

  cucumber::ScenarioScope<Codeflight> context;
  component::codeflight::metric::packageelement::FanIn fanIn{context->codeflight->getNeighbors()};
  component::codeflight::metric::packageelement::FanOut fanOut{context->codeflight->getNeighbors()};
  component::codeflight::metric::generic::Instability m{
    std::bind(&component::codeflight::metric::generic::Fan::count, &fanIn, std::placeholders::_1),
    std::bind(&component::codeflight::metric::generic::Fan::count, &fanOut, std::placeholders::_1)
  };
  const auto actualValue = m.value(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

THEN("^I expect the package has cycles for \"([^\"]*)\" to be (false|true)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(std::string, expectedRaw);
  const auto node = getNode(path);
  const auto expectedValue = expectedRaw == "true";

  const auto actualValue = component::codeflight::metric::package::hasCycles(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

THEN("^I expect the package fan-in for \"([^\"]*)\" to be (\\d+)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(std::size_t, expectedValue);

  const auto node = getNode(path);

  cucumber::ScenarioScope<Codeflight> context;
  component::codeflight::metric::package::FanIn fan{context->codeflight->getNeighbors()};
  const auto actualValue = fan.count(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

THEN("^I expect the package fan-out for \"([^\"]*)\" to be (\\d+)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(std::size_t, expectedValue);

  const auto node = getNode(path);

  cucumber::ScenarioScope<Codeflight> context;
  component::codeflight::metric::package::FanOut fan{context->codeflight->getNeighbors()};
  const auto actualValue = fan.count(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

THEN("^I expect the package instability for \"([^\"]*)\" to be (\\d+.\\d+)$")
{
  REGEX_PARAM(std::string, path);
  REGEX_PARAM(double, expectedValue);

  const auto node = getNode(path);

  cucumber::ScenarioScope<Codeflight> context;
  component::codeflight::metric::package::FanIn fanIn{context->codeflight->getNeighbors()};
  component::codeflight::metric::package::FanOut fanOut{context->codeflight->getNeighbors()};
  component::codeflight::metric::generic::Instability m{
    std::bind(&component::codeflight::metric::generic::Fan::count, &fanIn, std::placeholders::_1),
    std::bind(&component::codeflight::metric::generic::Fan::count, &fanOut, std::placeholders::_1)
  };
  const auto actualValue = m.value(*node);

  ASSERT_EQ(expectedValue, actualValue);
}

}
}
