/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Codeflight.h"
#include "parser/parser.h"

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>


namespace component::codeflight::features
{
namespace
{


GIVEN("^I have the AST:$")
{
  REGEX_PARAM(std::string, astContent);

  auto project = parser::parse(astContent);

  cucumber::ScenarioScope<Codeflight> context;
  context->codeflight = codeflight::Codeflight::produce(
        project,
        context->cycleSettings,
        {}
        );
}


}
}
