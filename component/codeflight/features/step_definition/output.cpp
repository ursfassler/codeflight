/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Codeflight.h"

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>


namespace component::codeflight::features
{
namespace
{


THEN("^I expect the output:$")
{
  REGEX_PARAM(std::string, expected);

  cucumber::ScenarioScope<Codeflight> context;

  ASSERT_EQ(expected, context->output);
}


}
}
