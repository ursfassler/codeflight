/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <string>

namespace component::codeflight::ast
{

class Project;

}

namespace component::codeflight::features::parser
{


std::unique_ptr<ast::Project> parse(const std::string&);


}
