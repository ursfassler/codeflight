/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Parser.h"
#include "component/codeflight/ast/Class.h"
#include "component/codeflight/ast/Field.h"
#include "component/codeflight/ast/Function.h"
#include "component/codeflight/ast/Method.h"
#include "component/codeflight/ast/Package.h"
#include "component/codeflight/ast/Variable.h"

namespace component::codeflight::features::parser
{


Parser::Parser(Stream<Token>& tokenizer_) :
  tokenizer{tokenizer_}
{
}

void Parser::parse()
{
  project = parseProject();

  std::vector<ast::Node*> stack{};
  stack.push_back(project.get());

  while (tokenizer.hasNext()) {
    auto level = parseNewline();
    auto node = parseNode();

    if (level > stack.size()) {
      throw std::runtime_error("node is too deep at: " + to_string(tokenizer.position()));
    }

    while (level < stack.size()) {
      stack.pop_back();
    }

    auto parent = stack.back();
    auto child = node.get();

    child->parent = parent;
    stack.push_back(child);
    parent->children.push_back(std::move(node));
  }

  link();
}

std::unique_ptr<ast::Project> Parser::getProject()
{
  return std::move(project);
}

void Parser::parseReferences(ast::Node* node)
{
  while (nextIs(TokenType::Identifier)) {
    const auto id = next(TokenType::Identifier);
    referencedIds[node].push_back(id);
  }
}

std::unique_ptr<ast::Node> Parser::createNode(const std::string& type)
{
  if (type == "project") {
    return std::make_unique<ast::Project>();
  } else if (type == "package") {
    return std::make_unique<ast::Package>();
  } else if (type == "class") {
    return std::make_unique<ast::Class>();
  } else if (type == "method") {
    return std::make_unique<ast::Method>();
  } else if (type == "field") {
    return std::make_unique<ast::Field>();
  } else if (type == "function") {
    return std::make_unique<ast::Function>();
  } else if (type == "variable") {
    return std::make_unique<ast::Variable>();
  } else {
    throw std::runtime_error("unknown type: " + type);
  }
}

std::unique_ptr<ast::Node> Parser::parseNode()
{
  const auto type = next(TokenType::Identifier);
  auto node = createNode(type);

  if (nextIs(TokenType::Literal)) {
    node->name = next(TokenType::Literal);
  }

  if (nextIs(TokenType::Identifier)) {
    const auto value = next(TokenType::Identifier);

    if (value == "->") {
      parseReferences(node.get());
    } else {
      nodeById[value] = node.get();
      if (nextIs(TokenType::Identifier)) {
        const auto value = next(TokenType::Identifier);
        if (value == "->") {
          parseReferences(node.get());
        }
      }
    }
  }

  return node;
}

std::unique_ptr<ast::Project> Parser::parseProject()
{
  const auto id = next(TokenType::Identifier);
  if (id != "project") {
    throw std::runtime_error("expected the identifier \"project\" " + to_string(tokenizer.position()));
  }

  auto project = std::make_unique<ast::Project>();

  if (nextIs(TokenType::Literal)) {
    project->name = next(TokenType::Literal);
  }

  return project;
}

std::size_t Parser::parseNewline()
{
  const auto value = next(TokenType::Newline);

  const auto len = value.length();
  if (len % 4 != 0) {
    throw std::runtime_error("not multiple of 4 (" + std::to_string(len) + ") at start of line at: " + to_string(tokenizer.position()));
  }
  const auto level = len / 4;

  return level;
}

void Parser::link()
{
  for (auto referenced : referencedIds) {
    auto node = referenced.first;
    for (const auto& id : referenced.second) {
      auto ref = nodeById.find(id);
      if (ref == nodeById.end()) {
        throw std::runtime_error("referenced node id not found: " + id);
      }
      node->references.push_back(ref->second);
    }
  }
}

std::string Parser::next(TokenType type)
{
  const auto token = tokenizer.next();
  if (token.type != type) {
    throw std::runtime_error("expected type " + std::to_string(static_cast<int>(type)) + " at " + to_string(tokenizer.position()));
  }
  return token.value;
}

bool Parser::nextIs(TokenType type) const
{
  const auto token = tokenizer.peek();
  return token.type == type;
}

std::string Parser::to_string(const Position& pos) const
{
  return std::to_string(pos.line) + ":" + std::to_string(pos.column);
}


}
