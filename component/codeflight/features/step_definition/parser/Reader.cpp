/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Reader.h"

namespace component::codeflight::features::parser
{


Reader::Reader(const std::string& data_) :
  data{data_}
{
}

char Reader::peek() const
{
  return data.at(index);
}

char Reader::next()
{
  char sym = data.at(index);

  if (sym == '\n') {
    pos.line++;
    pos.column = 0;
  } else {
    pos.column++;
  }

  index++;
  return sym;
}

bool Reader::hasNext() const
{
  return index < data.size();
}

Position Reader::position() const
{
  return pos;
}


}
