/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Stream.h"
#include <string>


namespace component::codeflight::features::parser
{


enum class TokenType
{
  Identifier,
  Literal,
  Newline,
  End,
};

struct Token
{
  TokenType type;
  std::string value;
};

class Tokenizer :
    public Stream<Token>
{
public:
  Tokenizer(Stream<char>&);

  Token next() override;
  Token peek() const override;
  bool hasNext() const override;
  Position position() const override;

private:
  Token peeked{};
  Stream<char>& reader;

  Token readNext();

};


}
