/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <cstdint>

namespace component::codeflight::features::parser
{


struct Position
{
  std::size_t line;
  std::size_t column;
};


template<typename T>
class Stream
{
public:
  virtual ~Stream() = default;

  virtual T peek() const = 0;
  virtual T next() = 0;
  virtual bool hasNext() const = 0;
  virtual Position position() const = 0;

};


}
