/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "parser.h"
#include "Parser.h"
#include "Reader.h"
#include "Tokenizer.h"

namespace component::codeflight::features::parser
{


std::unique_ptr<component::codeflight::ast::Project> parse(const std::string& content)
{
  Reader reader{content};
  Tokenizer tokenizer{reader};
  Parser parser{tokenizer};
  parser.parse();
  return parser.getProject();
}


}
