/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Stream.h"
#include "Tokenizer.h"
#include "component/codeflight/ast/Project.h"
#include <map>
#include <memory>

namespace component::codeflight::features::parser
{


class Parser
{
public:
  Parser(Stream<Token>& tokenizer_);

  void parse();
  std::unique_ptr<ast::Project> getProject();

private:
  void parseReferences(ast::Node* node);
  std::unique_ptr<ast::Node> createNode(const std::string& type);
  std::unique_ptr<ast::Node> parseNode();
  std::unique_ptr<ast::Project> parseProject();
  std::size_t parseNewline();

  void link();
  std::string next(TokenType type);
  bool nextIs(TokenType type) const;
  std::string to_string(const Position& pos) const;
  Stream<Token>& tokenizer;

  std::unique_ptr<ast::Project> project{};
  std::map<std::string, ast::Node*> nodeById{};
  std::map<ast::Node*, std::vector<std::string>> referencedIds{};

};



}
