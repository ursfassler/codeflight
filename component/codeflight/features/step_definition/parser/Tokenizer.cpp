/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Tokenizer.h"

namespace component::codeflight::features::parser
{


Tokenizer::Tokenizer(Stream<char>& reader_) :
  reader{reader_}
{
  peeked = readNext();
}

Token Tokenizer::next()
{
  Token current = peeked;
  peeked = readNext();
  return current;
}

Token Tokenizer::readNext()
{
  if (!reader.hasNext()) {
    return {TokenType::End, {}};
  }

  const auto peek = reader.peek();
  if (peek == '\n') {
    reader.next();

    std::string value{};
    while (reader.peek() == ' ') {
      value += reader.next();
    }
    return { TokenType::Newline, value };
  }

  if (peek == '\'') {
    reader.next();

    std::string value{};
    while (reader.peek() != '\'') {
      value += reader.next();
    }

    reader.next();
    while (reader.hasNext() && (reader.peek() == ' ')) {
      reader.next();
    }

    return { TokenType::Literal, value };
  }

  std::string value{};
  while (reader.hasNext()) {
    auto sym = reader.peek();
    switch (sym) {
      case '\n':
        return { TokenType::Identifier, value };
      case ' ':
        while (reader.peek() == ' ') {
          reader.next();
        }
        return { TokenType::Identifier, value };
      default:
        value += reader.next();
        break;
    }
  }
  return { TokenType::Identifier, value };
}

Token Tokenizer::peek() const
{
  return peeked;
}

bool Tokenizer::hasNext() const
{
  return reader.hasNext() || (peeked.type != TokenType::End);
}

Position Tokenizer::position() const
{
  return reader.position();
}


}
