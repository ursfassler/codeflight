/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Stream.h"
#include <string>

namespace component::codeflight::features::parser
{


class Reader :
    public Stream<char>
{
public:
  Reader(const std::string&);

  char peek() const override;
  char next() override;
  bool hasNext() const override;
  Position position() const override;

private:
  const std::string data;
  std::size_t index{0};
  Position pos{1, 0};

};


}
