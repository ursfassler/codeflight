/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Codeflight.h"
#include "component/codeflight/artefact/generator/Generator.h"
#include "component/codeflight/library/util.h"
#include "output/Builder.h"
#include "output/format.h"

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>


namespace component::codeflight::features
{
namespace
{

void generate(const artefact::interface::ArtefactIdentifier& ai)
{
  cucumber::ScenarioScope<Codeflight> context;

  artefact::generator::Generator generator{context->codeflight->getProject(), context->codeflight->getFactory()};
  auto artefact = generator.getMaybeArtefact(ai);
  if (!artefact) {
    throw std::runtime_error("unable to generate " + output::format(ai));
  }

  output::Builder builder{};
  artefact->accept(builder);
  context->output = builder.output;
}

GIVEN("^I set the maximal number of nodes to (\\d+) and vertices to (\\d+) for cycle count$")
{
  REGEX_PARAM(std::size_t, maxNumberOfNodes);
  REGEX_PARAM(std::size_t, maxNumberOfVertices);

  cucumber::ScenarioScope<Codeflight> context;
  context->cycleSettings.maxNumberOfNodes = maxNumberOfNodes;
  context->cycleSettings.maxNumberOfVertices = maxNumberOfVertices;
}

WHEN("^I generate the ([^ ]*)$")
{
  REGEX_PARAM(std::string, artefactType);
  artefact::interface::ArtefactIdentifier ai{{}, artefactType};

  generate(ai);
}

WHEN("^I generate the ([^ ]*) for \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, artefactType);
  REGEX_PARAM(std::string, stringPath);
  const auto parts = library::split(stringPath, "/");
  library::Path path{parts};
  artefact::interface::ArtefactIdentifier ai{path, artefactType};

  generate(ai);
}

WHEN("^I generate the ([^ ]*) with \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, artefactType);
  REGEX_PARAM(std::string, pathsString);
  const auto stringPaths = library::split(pathsString, ", ");
  library::Paths paths{};
  for (const auto& stringPath : stringPaths) {
    library::Path path{library::split(stringPath, "/")};
    paths.push_back(path);
  }
  artefact::interface::ArtefactIdentifier ai{{}, artefactType, paths};

  generate(ai);
}


}
}
