# language: en

Feature: calculate the fan-out metric for package elements
  As an architect
  I want to know which elements have a low/high fan-out
  In order to fix my project


Scenario: simple fan-out calculation
  When I have the AST:
    """
    project
        package
            class 'ClassA' 1
            class 'ClassB'
                method 'm1' -> 1
                method 'm2' -> 2
            class 'ClassC'
                method 'mc' 2
    """

  Then I expect the package element fan-out for "ClassB" to be 2


Scenario: does not count self-reference
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'ma' -> 1
                field 'fa' 1
    """

  Then I expect the package element fan-out for "ClassA" to be 0


Scenario: count multiple references to the same destination only once
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'm1' 1
                method 'm2' 2
            class 'ClassB'
                method 'm1' -> 1
                method 'm2' -> 2
    """

  Then I expect the package element fan-out for "ClassB" to be 1


Scenario: count reference to different package
  When I have the AST:
    """
    project
        package
            package 'pa'
                class 'ClassA'
                    method 'm1' 1
            package 'pb'
                class 'ClassB'
                    method 'm1' -> 1
    """

  Then I expect the package element fan-out for "pb::ClassB" to be 1


Scenario: count reference to function
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'mb' -> 1
            function 'f' 1
    """

  Then I expect the package element fan-out for "ClassA" to be 1
