# language: en

Feature: calculate the instability metric for package elements
  As an architect
  I want to know the instability of the elements
  In order to check if it is ok and find stable elements that depend on instable ones


Scenario: a simple stable, instable and in between elements
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'ma' -> 1
            class 'ClassB'
                method 'mb' 1 -> 2
            class 'ClassC'
                method 'mc' 2
    """

  Then I expect the package element instability for "ClassA" to be 1.0
  And I expect the package element instability for "ClassB" to be 0.5
  And I expect the package element instability for "ClassC" to be 0.0


Scenario: instability of a class with no neighbors
  When I have the AST:
    """
    project
        package
            class 'ClassA'
    """

  Then I expect the package element instability for "ClassA" to be 0.0


Scenario: more than one in and out
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'm1' -> 1
            class 'ClassB'
                method 'm1' -> 1
            class 'ClassC'
                method 'm1' -> 1
            class 'ClassD'
                method 'm1' 1 -> 2
            class 'ClassE'
                method 'm1' 2
    """

  And I expect the package element instability for "ClassD" to be 0.25


Scenario: doesn't count multiple connection between the same classes
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'm1' -> 1
                method 'm2' -> 1
                method 'm3' -> 2
            class 'ClassB'
                method 'm1' 1 -> 3
                method 'm2' 2 -> 3
            class 'ClassC'
                method 'm1' 3
    """

  And I expect the package element instability for "ClassB" to be 0.5
