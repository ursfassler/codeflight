# language: en

Feature: calculate the fan-in metric for package elements
  As an architect
  I want to know which elements have a low/high fan-in
  In order to fix my project


Scenario: simple fan-in calculation
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'ma' -> 1
            class 'ClassB'
                method 'mb' 1
            class 'ClassC'
                method 'mc' -> 1
    """

  Then I expect the package element fan-in for "ClassB" to be 2


Scenario: does not count self-reference
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'ma' -> 1
                field 'fa' 1
    """

  Then I expect the package element fan-in for "ClassA" to be 0


Scenario: count multiple references from the same source only once
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'm1' -> 1
                method 'm2' -> 1
            class 'ClassB'
                method 'mb' 1
    """

  Then I expect the package element fan-in for "ClassB" to be 1


Scenario: count reference from different package
  When I have the AST:
    """
    project
        package
            package 'pa'
                class 'ClassA'
                    method 'm1' -> 1
            package 'pb'
                class 'ClassB'
                    method 'mb' 1
    """

  Then I expect the package element fan-in for "pb::ClassB" to be 1


Scenario: count reference from function
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'mb' 1
            function 'f' -> 1
    """

  Then I expect the package element fan-in for "ClassA" to be 1
