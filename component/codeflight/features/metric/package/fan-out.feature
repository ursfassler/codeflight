# language: en

Feature: calculate the fan-out metric for packages
  As an architect
  I want to know which packages have a low/high fan-out
  In order to fix my project


Scenario: simple fan-out calculation
  When I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA' 1
            package 'b'
                class 'ClassB'
                    method 'm1' -> 1
                    method 'm2' -> 2
            package 'c'
                class 'ClassC'
                    method 'mc' 2
    """

  Then I expect the package fan-out for "b" to be 2


Scenario: does not count self-reference
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'ma' -> 1
            class 'ClassB'
                field 'fa' 1
    """

  Then I expect the package fan-out for "::" to be 0


Scenario: count multiple references from the same class to the same class only once
  When I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'm1' 1
                    method 'm2' 2
            package 'b'
                class 'ClassB'
                    method 'm1' -> 1
                    method 'm2' -> 2
    """

  Then I expect the package fan-out for "b" to be 1


Scenario: count multiple connection between the same packages
  When I have the AST:
    """
    project
        package
            package 'b'
                class 'ClassA'
                    method 'ba' 1 -> 3
                class 'ClassB'
                    method 'bb' 2 -> 3
            package 'c'
                class 'ClassA'
                    method 'ca' 3
    """

  And I expect the package fan-out for "b" to be 2


Scenario: count reference to sub-package
  When I have the AST:
    """
    project
        package
            package 'pa'
                class 'ClassA'
                    method 'm1' 1
            class 'ClassB'
                method 'm1' -> 1
    """

  Then I expect the package fan-out for "::" to be 1


Scenario: count reference to function
  When I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'mb' -> 1
            package 'f'
                function 'f' 1
    """

  Then I expect the package fan-out for "a" to be 1
