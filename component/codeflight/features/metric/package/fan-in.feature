# language: en

Feature: calculate the fan-in metric for packages
  As an architect
  I want to know which packages have a low/high fan-in
  In order to fix my project


Scenario: simple fan-in calculation
  When I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'ma' -> 1
            package 'b'
                class 'ClassB'
                    method 'mb' 1
            package 'c'
                class 'ClassC'
                    method 'mc' -> 1
    """

  Then I expect the package fan-in for "b" to be 2


Scenario: does not count self-reference
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'ma' -> 1
                field 'fa' 1
            class 'ClassB'
                method 'mb' -> 1
    """

  Then I expect the package fan-in for "::" to be 0


Scenario: count multiple references from the same class only once
  When I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'm1' -> 1
                    method 'm2' -> 1
            package 'b'
                class 'ClassB'
                    method 'mb' 1
    """

  Then I expect the package fan-in for "b" to be 1


Scenario: count all references from the same package
  When I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'm1' -> 1
                class 'ClassB'
                    method 'm1' -> 1
                class 'ClassC'
                    method 'm1' -> 2
                class 'ClassD'
                    method 'm1' -> 1
            package 'b'
                class 'ClassA'
                    method 'mb' 1
                class 'ClassB'
                    method 'mb' 2
    """

  Then I expect the package fan-in for "b" to be 4


Scenario: count reference from function
  When I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'mb' 1
            package 'b'
                function 'f' -> 1
    """

  Then I expect the package fan-in for "a" to be 1
