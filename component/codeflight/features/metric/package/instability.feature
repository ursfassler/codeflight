# language: en

Feature: calculate the instability metric for packages
  As an architect
  I want to know the instability of the packages
  In order to check if it is ok and find stable packages that depend on instable ones


Scenario: simple stable, instable and in between packages
  When I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'ma' -> 1
            package 'b'
                class 'ClassB'
                    method 'mb' 1 -> 2
            package 'c'
                class 'ClassC'
                    method 'mc' 2
    """

  Then I expect the package instability for "a" to be 1.0
  And I expect the package instability for "b" to be 0.5
  And I expect the package instability for "c" to be 0.0


Scenario: instability of a package with no neighbors
  When I have the AST:
    """
    project
        package ''
    """

  Then I expect the package instability for "::" to be 0.0


Scenario: more than one in and out
  When I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'm1' -> 1
            package 'b'
                class 'ClassB'
                    method 'm1' -> 1
            package 'c'
                class 'ClassC'
                    method 'm1' -> 1
            package 'd'
                class 'ClassD'
                    method 'm1' 1 -> 2
            package 'e'
                class 'ClassE'
                    method 'm1' 2
    """

  And I expect the package instability for "d" to be 0.25


Scenario: count multiple connection between the same packages
  When I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'm' -> 1
                class 'ClassB'
                    method 'm' -> 1
                class 'ClassC'
                    method 'm' -> 2
            package 'b'
                class 'ClassA'
                    method 'm' 1 -> 3
                class 'ClassB'
                    method 'm' 2 -> 3
            package 'c'
                class 'ClassA'
                    method 'm' 3
    """

  And I expect the package instability for "b" to be 0.4


Scenario: don't count multiple connection between the same classes
  When I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA'
                    method 'm1' -> 1
                    method 'm2' -> 1
                    method 'm3' -> 2
            package 'b'
                class 'ClassA'
                    method 'm1' 1 -> 3
                    method 'm2' 2 -> 3
            package 'c'
                class 'ClassA'
                    method 'm1' 3
    """

  And I expect the package instability for "b" to be 0.5
