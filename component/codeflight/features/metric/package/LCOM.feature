# language: en

Feature: calculate the LCOM4 metric for packages
  As an architect
  I want to know which packages have a lack of cohesion
  In order to fix my project


Scenario: simple LCOM4 calculation
  When I have the AST:
    """
    project
        package
            class 'ClassA'
            class 'ClassB'
    """

  Then I expect the package LCOM4 for "::" to be 2


Scenario: LCOM4 calculation between all connected classes
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'methodA' -> methodB methodD
            class 'ClassB'
                method 'methodB' methodB
            class 'ClassC'
                method 'methodC' -> methodB
            class 'ClassD'
                method 'methodD' methodD
    """

  Then I expect the package LCOM4 for "::" to be 1


Scenario: LCOM4 calculation in different packages
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'methodA' -> methodB methodD
            class 'ClassB'
                method 'methodB' methodB
            package 'package1'
                class 'ClassC'
                    method 'methodC' -> methodB
                class 'ClassD'
                    method 'methodD' methodD
                package 'package2'
    """

  Then I expect the package LCOM4 for "::" to be 1
  And I expect the package LCOM4 for "package1" to be 2
  And I expect the package LCOM4 for "package1::package2" to be 0


Scenario: consider only relations withing the same package
  When I have the AST:
    """
    project
        package
            function 'functionA' -> functionC
            function 'functionB' -> functionC
            package 'package1'
                function 'functionC' functionC
    """

  Then I expect the package LCOM4 for "::" to be 2


Scenario: include functions in calculation
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'methodA' -> functionA
            function 'functionA' functionA
            function 'functionB' -> functionA
            function 'functionC'
    """

  Then I expect the package LCOM4 for "::" to be 2


Scenario: include variables in calculation
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'methodA' -> variableA
            variable 'variableA' variableA
            variable 'variableB' -> variableA
            variable 'variableC'
    """

  Then I expect the package LCOM4 for "::" to be 2


Scenario: LCOM4 calculation for class in class
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                class 'ClassB'
    """

  Then I expect the package LCOM4 for "::" to be 1
