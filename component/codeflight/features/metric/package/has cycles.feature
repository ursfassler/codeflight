# language: en

Feature: calculate if the sub packages have cycles
  As an architect
  I want to know which packages have cycles
  In order to fix my project


Scenario: simple cycle calculation
  When I have the AST:
    """
    project
        package
            package 'a'
                class 'ClassA' 1
                    method 'a' -> 2
                    field 'b' -> 3
                function 'funcB' 2 -> 3
                package 'c'
                    function 'funcC' 3 -> 1
    """

  Then I expect the package has cycles for "::" to be false
  And I expect the package has cycles for "a" to be true
  And I expect the package has cycles for "a::c" to be false
