# language: en

Feature: calculate the LCOM metric classes
  As an architect
  I want to know which classes have a lack of cohesion
  In order to fix my project


Scenario: simple LCOM4 calculation
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                field 'field1' f1
                field 'field2' f2
                field 'field3' f3
                method 'method1' -> f1 f2
                method 'method2' -> f3
    """

  Then I expect the class LCOM4 for "ClassA" to be 2


Scenario: LCOM4 calculation in different classes
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'methodA' -> f1 f2
                field 'field1' f1
            package 'package1'
                class 'ClassB'
                    method 'methodB'
                    field 'field2' f2
    """

  Then I expect the class LCOM4 for "ClassA" to be 1
  And I expect the class LCOM4 for "package1::ClassB" to be 2


Scenario: functions are not considered in calculation
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                method 'methodA' -> f1
                method 'methodB' mB
            function 'function1' f1 -> mB
    """

  Then I expect the class LCOM4 for "ClassA" to be 2


Scenario: LCOM4 calculation of class in class
  When I have the AST:
    """
    project
        package
            class 'ClassA'
                field 'field1'
                class 'ClassB'
                    method 'methodB'
    """

  Then I expect the class LCOM4 for "ClassA" to be 2
  And I expect the class LCOM4 for "ClassA::ClassB" to be 1
