# language: en

Feature: cycle detection in graphs (johnson simple cycles)
  As a developer of the tool
  I want easy usable code to detect cycles in a graph
  In order to use it on the packages


Scenario: detect a trivial cycle
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | B      | A           |

  When I run the johnson simple cycle detection algorithm on the graph

  Then I expect the resulting cycles
    | nodes    |
    | A, B     |


Scenario: return nothing when no cycle is present
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | B      | C           |

  When I run the johnson simple cycle detection algorithm on the graph

  Then I expect the resulting cycles
    | nodes    |


Scenario: detect 2 connected trivial cycles
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | B      | A           |
    | A      | C           |
    | C      | A           |

  When I run the johnson simple cycle detection algorithm on the graph

  Then I expect the resulting cycles
    | nodes    |
    | A, C     |
    | A, B     |


Scenario: detect 2 connected trivial cycles, starting at an outer node
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | B      | A           |
    | B      | C           |
    | C      | B           |

  When I run the johnson simple cycle detection algorithm on the graph

  Then I expect the resulting cycles
    | nodes    |
    | A, B     |
    | B, C     |


Scenario: detect separate but connected cycles
  Given I have a graph with the edges
    | source | destination |
    | C      | D           |
    | D      | C           |
    | A      | B           |
    | B      | A           |
    | E      | F           |
    | F      | E           |
    | A      | C           |
    | D      | F           |

  When I run the johnson simple cycle detection algorithm on the graph

  Then I expect the resulting cycles
    | nodes    |
    | C, D     |
    | A, B     |
    | E, F     |


Scenario: detect separate but connected cycles with common node
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | A      | E           |
    | B      | D           |
    | C      | B           |
    | D      | C           |
    | D      | E           |
    | E      | D           |

  When I run the johnson simple cycle detection algorithm on the graph

  Then I expect the resulting cycles
    | nodes    |
    | B, D, C  |
    | E, D     |


Scenario: detect separate but connected cycles with 2 common nodes
  Given I have a graph with the edges
    | source | destination |
    | A      | D           |
    | B      | A           |
    | C      | A           |
    | D      | B           |
    | D      | C           |

  When I run the johnson simple cycle detection algorithm on the graph

  Then I expect the resulting cycles
    | nodes    |
    | A, D, C  |
    | A, D, B  |


Scenario: detect cycles in a fully connected graph
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | A      | C           |
    | B      | A           |
    | B      | C           |
    | C      | A           |
    | C      | B           |

  When I run the johnson simple cycle detection algorithm on the graph

  Then I expect the resulting cycles
    | nodes    |
    | A, C, B  |
    | A, C     |
    | A, B, C  |
    | A, B     |
    | B, C     |


Scenario: detect one cycle
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | A      | C           |
    | B      | D           |
    | B      | E           |
    | C      | D           |
    | C      | E           |
    | D      | F           |
    | E      | F           |
    | F      | A           |

  When I run the johnson simple cycle detection algorithm on the graph

  Then I expect the resulting cycles
    | nodes      |
    | A, C, E, F |
    | A, C, D, F |
    | A, B, E, F |
    | A, B, D, F |
