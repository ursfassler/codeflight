# language: en

Feature: simple cycle detection in graphs (dfs)
  As a developer of the tool
  I want easy, usable and fast code to detect cycles in a graph
  In order to use it on the packages


Scenario: detect a trivial cycle
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | B      | A           |

  When I run the dfs has cycle detection algorithm on the graph

  Then I expect to have cycles


Scenario: detect no cycle
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | B      | C           |

  When I run the dfs has cycle detection algorithm on the graph

  Then I expect to have no cycles


Scenario: detect 2 connected trivial cycles
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | B      | A           |
    | A      | C           |
    | C      | A           |

  When I run the dfs has cycle detection algorithm on the graph

  Then I expect to have cycles


Scenario: detect no cycle on triangle
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | B      | C           |
    | A      | C           |

  When I run the dfs has cycle detection algorithm on the graph

  Then I expect to have no cycles


Scenario: detect no cycle on diamond
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | B      | D           |
    | A      | C           |
    | C      | D           |

  When I run the dfs has cycle detection algorithm on the graph

  Then I expect to have no cycles


Scenario: detect cycle within lot of other nodes
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | C      | D           |
    | E      | F           |
    | F      | G           |
    | G      | E           |
    | I      | H           |
    | X      | Z           |

  When I run the dfs has cycle detection algorithm on the graph

  Then I expect to have cycles
