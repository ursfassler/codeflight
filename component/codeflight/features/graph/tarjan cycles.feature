# language: en

Feature: tarjan cycle detection in graphs (SCC)
  As a developer of the tool
  I want fast code to detect cycles in a graph
  In order to use it on the packages


Scenario: detect a trivial cycle
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | B      | A           |

  When I run the tarjan's strongly connected components algorithm on the graph

  Then I expect the resulting cycles
    | nodes    |
    | A, B     |


Scenario: return nothing when no cycle is present
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | B      | C           |

  When I run the tarjan's strongly connected components algorithm on the graph

  Then I expect the resulting cycles
    | nodes    |


Scenario: detect 2 connected trivial cycles
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | B      | A           |
    | A      | C           |
    | C      | A           |

  When I run the tarjan's strongly connected components algorithm on the graph

  Then I expect the resulting cycles
    | nodes    |
    | A, B, C  |


Scenario: detect 2 connected trivial cycles, starting at an outer node
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | B      | A           |
    | B      | C           |
    | C      | B           |

  When I run the tarjan's strongly connected components algorithm on the graph

  Then I expect the resulting cycles
    | nodes    |
    | A, B, C  |


Scenario: detect separate but connected cycles
  Given I have a graph with the edges
    | source | destination |
    | C      | D           |
    | D      | C           |
    | A      | B           |
    | B      | A           |
    | E      | F           |
    | F      | E           |
    | A      | C           |
    | D      | F           |

  When I run the tarjan's strongly connected components algorithm on the graph

  Then I expect the resulting cycles
    | nodes    |
    | A, B     |
    | C, D     |
    | E, F     |


Scenario: detect separate but connected cycles with common node
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | A      | E           |
    | B      | D           |
    | C      | B           |
    | D      | C           |
    | D      | E           |
    | E      | D           |

  When I run the tarjan's strongly connected components algorithm on the graph

  Then I expect the resulting cycles
    | nodes            |
    | B, C, D, E       |


Scenario: multiple cycles intersecting with each other
  Given I have a graph with the edges
    | source | destination |
    | A      | B           |
    | B      | C           |
    | C      | E           |
    | D      | A           |
    | D      | B           |
    | D      | F           |
    | E      | D           |
    | F      | E           |

  When I run the tarjan's strongly connected components algorithm on the graph

  Then I expect the resulting cycles
    | nodes            |
    | A, B, C, D, E, F |
