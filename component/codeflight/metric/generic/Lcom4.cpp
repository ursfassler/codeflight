/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Lcom4.h"
#include "component/codeflight/ast/graph/graph.h"
#include "component/codeflight/ast/specification/All.h"
#include "component/codeflight/ast/specification/PackageElement.h"
#include <cstdint>


namespace component::codeflight::metric::generic
{
namespace
{


std::size_t value(const ast::Node& node, const ast::NodeSpecification& spec)
{
  const auto graph = ast::graph::buildDirectChildrenGraph(node, spec);
  const auto count = graph.countConnectedComponents();
  return count;
}


}


std::size_t packageLcom4(const ast::Node& node)
{
  return value(node, ast::specification::PackageElement());
}

std::size_t classLcom4(const ast::Node& node)
{
  return value(node, ast::specification::All());
}


}
