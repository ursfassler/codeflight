/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <functional>

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::metric::generic
{


class Instability
{
  public:
    using FanCount = std::function<std::size_t(const ast::Node&)>;

    Instability(
        const FanCount& fanInCount,
        const FanCount& fanOutCount
        );

    double value(const ast::Node&) const;

  private:
    const FanCount fanInCount;
    const FanCount fanOutCount;

};


}
