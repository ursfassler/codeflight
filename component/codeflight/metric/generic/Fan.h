/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/ast/repository/Neighbor.h"
#include <cstdint>

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::metric::generic
{


class Fan
{
  public:
    using NeigborSpec = ast::repository::Neighbor::Specification;

    Fan(const ast::repository::Neighbor&, const NeigborSpec&);

    std::size_t count(const ast::Node&) const;

  private:
    const ast::repository::Neighbor& neighbor;
    const NeigborSpec& spec;
};


}
