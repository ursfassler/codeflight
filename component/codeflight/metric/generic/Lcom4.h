/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <cstdint>


namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::metric::generic
{


std::size_t packageLcom4(const ast::Node&);
std::size_t classLcom4(const ast::Node&);


}
