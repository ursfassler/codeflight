/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Instability.h"
#include "Fan.h"


namespace component::codeflight::metric::generic
{


Instability::Instability(
    const FanCount& fanInCount_,
    const FanCount& fanOutCount_
    ) :
  fanInCount{fanInCount_},
  fanOutCount{fanOutCount_}
{
}

double Instability::value(const ast::Node& node) const
{
  const auto fi = fanInCount(node);
  const auto fo = fanOutCount(node);

  const auto divisor = fi+fo;
  const double instability = (divisor == 0) ? 0.0 : double(fo) / divisor;

  return instability;
}


}
