/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Fan.h"
#include "component/codeflight/ast/repository/Neighbor.h"


namespace component::codeflight::metric::generic
{


Fan::Fan(const ast::repository::Neighbor& neighbor_, const NeigborSpec& spec_) :
  neighbor{neighbor_},
  spec{spec_}
{
}

std::size_t Fan::count(const ast::Node& node) const
{
  std::size_t count = 0;
  neighbor.foreach(spec, &node, [&count, &node](const ast::Node* neighbor){
    if (&node != neighbor) {
      count++;
    }
  });
  return count;
}


}
