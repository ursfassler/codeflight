/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/ast/traverse/type.h"


namespace component::codeflight::metric::generic
{


template<typename T>
std::size_t count(const ast::Node& node)
{
  std::size_t count = 0;
  ast::traverse::children<T>(node, [&count](const T&){
    count++;
  });
  return count;
}


}
