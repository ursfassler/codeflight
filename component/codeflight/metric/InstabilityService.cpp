/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "InstabilityService.h"
#include "component/codeflight/ast/Dependency.h"


namespace component::codeflight::metric
{


InstabilityService::InstabilityService(const PackageInstability& instabilityFunc_) :
  instabilityFunc{instabilityFunc_}
{
}

double InstabilityService::stability(const ast::Dependency& dependency) const
{
  const auto si = instability(dependency.from);
  const auto di = instability(dependency.to);
  const auto stability = si - di;
  return stability;
}

double InstabilityService::instability(const ast::Node* node) const
{
  const auto idx = cache.find(node);
  if (idx == cache.end()) {
    const auto instab = instabilityFunc(*node);
    cache[node] = instab;
    return instab;
  } else {
    return idx->second;
  }
}


}
