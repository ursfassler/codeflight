/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <functional>
#include <map>

namespace component::codeflight::ast
{

class Node;
struct Dependency;

}

namespace component::codeflight::metric
{


class InstabilityService
{
  public:
    using PackageInstability = std::function<double(const ast::Node&)>;

    InstabilityService(const PackageInstability&);

    double instability(const ast::Node*) const;
    double stability(const ast::Dependency&) const;

  private:
    PackageInstability instabilityFunc;

    mutable std::map<const ast::Node*, double> cache{};

};


}
