/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "AllRepository.h"
#include "component/codeflight/library/util.h"
#include "generic/Count.h"
#include "generic/Lcom4.h"
#include "package/HasCycles.h"


namespace component::codeflight::metric
{


AllRepository::AllRepository(const ast::repository::Neighbor& neighbors) :
  packageFanIn{neighbors},
  packageFanOut{neighbors},
  packageInstability{&packageFanIn, &packageFanOut},
  packageMetrics
  {
    {"Packages", std::bind(&calcWhole, generic::count<ast::Package>, std::placeholders::_1)},
    {"Classes", std::bind(&calcWhole, generic::count<ast::Class>, std::placeholders::_1)},
    {"Functions", std::bind(&calcWhole, generic::count<ast::Function>, std::placeholders::_1)},
    {"Variables", std::bind(&calcWhole, generic::count<ast::Variable>, std::placeholders::_1)},
    {"LCOM4", std::bind(&calcWhole, generic::packageLcom4, std::placeholders::_1)},
    {"fan-in", std::bind(&calcWhole, packageInstability.fanInFunc, std::placeholders::_1)},
    {"fan-out", std::bind(&calcWhole, packageInstability.fanOutFunc, std::placeholders::_1)},
    {"instability", std::bind(&calcReal, packageInstability.instabilityFunc, std::placeholders::_1)},
    {"has cycles", std::bind(&calcBool, package::hasCycles, std::placeholders::_1)},
  },
  packageelementFanIn{neighbors},
  packageelementFanOut{neighbors},
  packageelementInstability{&packageelementFanIn, &packageelementFanOut},
  classMetrics
  {
    {"Methods", std::bind(&calcWhole, generic::count<ast::Method>, std::placeholders::_1)},
    {"Fields", std::bind(&calcWhole, generic::count<ast::Field>, std::placeholders::_1)},
    {"LCOM4", std::bind(&calcWhole, generic::classLcom4, std::placeholders::_1)},
    {"fan-in", std::bind(&calcWhole, packageelementInstability.fanInFunc, std::placeholders::_1)},
    {"fan-out", std::bind(&calcWhole, packageelementInstability.fanOutFunc, std::placeholders::_1)},
    {"instability", std::bind(&calcReal, packageelementInstability.instabilityFunc, std::placeholders::_1 )},
  },
  functionMetrics
  {
    {"fan-in", std::bind(&calcWhole, packageelementInstability.fanInFunc, std::placeholders::_1)},
    {"fan-out", std::bind(&calcWhole, packageelementInstability.fanOutFunc, std::placeholders::_1)},
    {"instability", std::bind(&calcReal, packageelementInstability.instabilityFunc, std::placeholders::_1 )},
  },
  variableMetrics
  {
    {"fan-in", std::bind(&calcWhole, packageelementInstability.fanInFunc, std::placeholders::_1)},
    {"fan-out", std::bind(&calcWhole, packageelementInstability.fanOutFunc, std::placeholders::_1)},
    {"instability", std::bind(&calcReal, packageelementInstability.instabilityFunc, std::placeholders::_1 )},
  }
{
}

AllRepository::Instabilty::Instabilty(const generic::Fan* fanIn, const generic::Fan* fanOut) :
  fanInFunc{std::bind(&generic::Fan::count, fanIn, std::placeholders::_1)},
  fanOutFunc{std::bind(&generic::Fan::count, fanOut, std::placeholders::_1)},
  instability{fanInFunc, fanOutFunc},
  instabilityFunc{std::bind(&generic::Instability::value, &instability, std::placeholders::_1)}
{
}

std::string AllRepository::calcWhole(const WholeCalculation& func, const ast::Node& node)
{
  const auto value = func(node);
  return std::to_string(value);
}

std::string AllRepository::calcReal(const RealCalculation& func, const ast::Node& node)
{
  const auto value = func(node);
  return library::toString(value, 2);
}

std::string AllRepository::calcBool(const BoolCalculation& func, const ast::Node& node)
{
  const auto value = func(node);
  return library::toString(value);
}

const Metrics& AllRepository::package() const
{
  return packageMetrics;
}

const Metrics& AllRepository::clazz() const
{
  return classMetrics;
}

const Metrics& AllRepository::function() const
{
  return functionMetrics;
}

const Metrics& AllRepository::variable() const
{
  return variableMetrics;
}

Repository::RealCalculation AllRepository::packageInstabilityFunction() const
{
  return packageInstability.instabilityFunc;
}


}
