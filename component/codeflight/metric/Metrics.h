/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"
#include <functional>
#include <optional>
#include <string>
#include <vector>

namespace component::codeflight::ast
{

class Node;
struct Dependency;

}

namespace component::codeflight::metric
{


template<typename T>
struct GenericDescription
{
  using Calculation = std::function<std::string(const T&)>;
  using Link = std::function<artefact::interface::ArtefactIdentifier(const T&)>;

  std::string name;
  Calculation value;
  std::optional<Link> link{};
};


using Description = GenericDescription<ast::Node>;
using Metrics = std::vector<Description>;


using DependencyDescription = GenericDescription<ast::Dependency>;
using DependencyMetrics = std::vector<DependencyDescription>;


}
