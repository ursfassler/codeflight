/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "HasCycles.h"
#include "component/codeflight/ast/graph/cycle/dfsHasCycles.h"
#include "component/codeflight/ast/graph/graph.h"


namespace component::codeflight::metric::package
{


std::size_t hasCycles(const ast::Node& node)
{
  ast::graph::Graph graph = ast::graph::buildPackageDependencyGraph(node);
  graph.removeDuplicatedEdges();
  graph.removeEdgeToItself();
  return ast::graph::cycle::hasCycles(graph);
}


}
