/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Fan.h"


namespace component::codeflight::metric::package
{


FanIn::FanIn(const ast::repository::Neighbor& neighbor) :
  Fan{neighbor, neighbor.incomingPackage}
{
}


FanOut::FanOut(const ast::repository::Neighbor& neighbor) :
  Fan{neighbor, neighbor.outgoingPackage}
{
}


}
