/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <cstdint>

namespace component::codeflight::ast
{

class Node;

}


namespace component::codeflight::metric::package
{


std::size_t hasCycles(const ast::Node&);


}
