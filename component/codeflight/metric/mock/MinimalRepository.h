/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/metric/Repository.h"

namespace component::codeflight::metric::mock
{


class MinimalRepository :
    public Repository
{
  public:
    const Metrics& package() const override;
    const Metrics& clazz() const override;
    const Metrics& function() const override;
    const Metrics& variable() const override;

    RealCalculation packageInstabilityFunction() const override;

  private:
    Metrics empty{};
};


}
