/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MinimalRepository.h"

namespace component::codeflight::metric::mock
{


const Metrics &MinimalRepository::package() const
{
  return empty;
}

const Metrics &MinimalRepository::clazz() const
{
  return empty;
}

const Metrics &MinimalRepository::function() const
{
  return empty;
}

const Metrics &MinimalRepository::variable() const
{
  return empty;
}

Repository::RealCalculation MinimalRepository::packageInstabilityFunction() const
{
  return [](const ast::Node&) {
    return 0;
  };
}


}
