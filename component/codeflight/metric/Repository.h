/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/metric/Metrics.h"


namespace component::codeflight::metric
{


class Repository
{
  public:
    using RealCalculation = std::function<double(const ast::Node&)>;

    virtual ~Repository() = default;

    virtual const Metrics& package() const = 0;
    virtual const Metrics& clazz() const = 0;
    virtual const Metrics& function() const = 0;
    virtual const Metrics& variable() const = 0;

    virtual RealCalculation packageInstabilityFunction() const = 0;
};


}
