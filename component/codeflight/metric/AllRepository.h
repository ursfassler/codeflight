/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Repository.h"
#include "generic/Instability.h"
#include "package/Fan.h"
#include "packageelement/Fan.h"


namespace component::codeflight::metric
{


class AllRepository :
    public Repository
{
  public:
    AllRepository(const ast::repository::Neighbor&);

    const Metrics& package() const override;
    const Metrics& clazz() const override;
    const Metrics& function() const override;
    const Metrics& variable() const override;

    RealCalculation packageInstabilityFunction() const override;


  private:
    using WholeCalculation = std::function<std::size_t(const ast::Node&)>;
    using BoolCalculation = std::function<bool(const ast::Node&)>;

    static std::string calcWhole(const WholeCalculation&, const ast::Node&);
    static std::string calcReal(const RealCalculation&, const ast::Node&);
    static std::string calcBool(const BoolCalculation&, const ast::Node&);

    struct Instabilty
    {
        Instabilty(const generic::Fan* fanIn, const generic::Fan* fanOut);

        WholeCalculation fanInFunc;
        WholeCalculation fanOutFunc;
        generic::Instability instability;
        RealCalculation instabilityFunc;
    };

    const package::FanIn packageFanIn;
    const package::FanOut packageFanOut;
    const Instabilty packageInstability;
    const Metrics packageMetrics;

    const packageelement::FanIn packageelementFanIn;
    const packageelement::FanOut packageelementFanOut;
    const Instabilty packageelementInstability;

    const Metrics classMetrics;
    const Metrics functionMetrics;
    const Metrics variableMetrics;
};


}
