/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/library/Path.h"


namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::ast::parents
{


library::Path nodePath(const Node&);


}
