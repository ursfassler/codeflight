/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "parents.h"
#include "component/codeflight/ast/Node.h"
#include "component/codeflight/ast/query/ifType.h"


namespace component::codeflight::ast::parents
{


std::optional<const Node*> findAncestor(const Node* node, const NodeSpecification& spec)
{
  for (std::optional<const Node*> itr = { node }; itr; itr = findParent(*itr)) {
    if (spec(*itr)) {
      return *itr;
    }
  }

  return {};
}

std::optional<const Node*> findParent(const Node* node)
{
  return node->parent;
}

const Project* getProject(const Node* node)
{
  while (node->parent) {
    node = *node->parent;
  }

  const Project* result{};

  query::ifType<Project>(*node, [&result](const Project& project){
    result = &project;
  }, [](const Node&){
    throw std::runtime_error("root node is not of project type");
  });

  return result;
}


}
