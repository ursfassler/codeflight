/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "nodePath.h"
#include "component/codeflight/ast/Project.h"
#include "component/codeflight/ast/specification/Project.h"
#include "parents.h"


namespace component::codeflight::ast::parents
{


library::Path nodePath(const Node& node)
{
  std::vector<std::string> path{};

  const ast::Node* itr = &node;

  while (itr) {
    path.push_back(itr->name);

    const auto parent = parents::findParent(itr);
    if (!parent || specification::Project()(*parent)) {
      break;
    }

    itr = *parent;
  }

  std::reverse(path.begin(), path.end());

  return {path};
}


}
