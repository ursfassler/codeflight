/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/ast/NodeSpecification.h"
#include <optional>

namespace component::codeflight::ast
{

class Project;

}

namespace component::codeflight::ast::parents
{


std::optional<const Node*> findAncestor(const Node*, const NodeSpecification&);
std::optional<const Node*> findParent(const Node*);
const Project* getProject(const Node*);


}
