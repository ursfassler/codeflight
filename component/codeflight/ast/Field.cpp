/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Field.h"
#include "MutableVisitor.h"
#include "Visitor.h"


namespace component::codeflight::ast
{


void Field::accept(Visitor &visitor) const
{
  visitor.visit(*this);
}

void Field::accept(MutableVisitor &visitor)
{
  visitor.visit(*this);
}


}
