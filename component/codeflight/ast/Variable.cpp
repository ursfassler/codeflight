/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Variable.h"
#include "MutableVisitor.h"
#include "Visitor.h"


namespace component::codeflight::ast
{


void Variable::accept(Visitor &visitor) const
{
  visitor.visit(*this);
}

void Variable::accept(MutableVisitor &visitor)
{
  visitor.visit(*this);
}


}
