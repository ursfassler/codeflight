/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "types.h"


namespace component::codeflight::ast::graph
{

class Graph;

}

namespace component::codeflight::ast::graph::cycle
{


NodeSets tarjan(const Graph&);


}
