/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "types.h"
#include <functional>

namespace component::codeflight::ast::graph
{

class Graph;

}

namespace component::codeflight::ast::graph::cycle
{


using CyleReporter = std::function<void(const NodeList&)>;


void johnson(const Graph&, const CyleReporter&);


}
