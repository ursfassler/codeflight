/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "dfsHasCycles.h"
#include "component/codeflight/ast/graph/graph.h"
#include <set>


namespace component::codeflight::ast::graph::cycle
{
namespace
{


struct Data
{
  const Graph& graph;
  bool hasCycles{false};

  std::set<const Node*> visited{};
  std::set<const Node*> stack{};
};


void dfs(const Node* node, Data& d)
{
  if (d.hasCycles) {
    return;
  }

  if (d.stack.find(node) != d.stack.end()) {
    d.hasCycles = true;
    return;
  }

  if (d.visited.find(node) != d.visited.end()) {
    return;
  }

  d.visited.insert(node);
  d.stack.insert(node);
  d.graph.foreach(node, [&d](const ast::Node* dest){
    dfs(dest, d);
  });
  d.stack.erase(node);
}


}


bool hasCycles(const Graph& graph)
{
  Data data{graph};

  graph.foreach([&data](const Node* node){
    dfs(node, data);
  });

  return data.hasCycles;
}


}
