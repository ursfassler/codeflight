/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace component::codeflight::ast::graph
{

class Graph;

}

namespace component::codeflight::ast::graph::cycle
{


bool hasCycles(const Graph&);


}
