/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "tarjan.h"
#include "component/codeflight/ast/graph/graph.h"
#include <map>
#include <set>
#include <vector>


namespace component::codeflight::ast::graph::cycle
{
namespace
{


struct Data
{
  const Graph& graph;
  NodeSets nodeSets{};
  unsigned index = 0;
  std::map<const Node*, unsigned> nodeIndex{};
  std::map<const Node*, unsigned> lowlink{};
  std::vector<const Node*> stack{};
  std::set<const Node*> onStack{};
};


void strongconnect(const Node* v, Data& d)
{
  // Set the depth index for v to the smallest unused index
  d.nodeIndex[v] = d.index;
  d.lowlink[v] = d.index;
  d.index = d.index + 1;
  d.stack.push_back(v);
  d.onStack.insert(v);

  // Consider successors of v
  d.graph.foreach(v, [v, &d](const Node* w){
    if (d.nodeIndex.find(w) == d.nodeIndex.end()) {
      // Successor w has not yet been visited; recurse on it
      strongconnect(w, d);
      d.lowlink[v] = std::min(d.lowlink[v], d.lowlink[w]);
    } else if (d.onStack.contains(w)) {
      // Successor w is in stack S and hence in the current SCC
      // If w is not on stack, then (v, w) is an edge pointing to an SCC already found and must be ignored
      // Note: The next line may look odd - but is correct.
      // It says w.index not w.lowlink; that is deliberate and from the original paper
      d.lowlink[v] = std::min(d.lowlink[v], d.nodeIndex[w]);
    }
  });

  // If v is a root node, pop the stack and generate an SCC
  if (d.lowlink[v] == d.nodeIndex[v]) {
    // start a new strongly connected component
    NodeSet scc{};
    const Node* w = {};
    do {
      w = d.stack.back();
      d.stack.pop_back();
      d.onStack.erase(w);
      // add w to current strongly connected component
      scc.insert(w);
    } while (w != v);
    // output the current strongly connected component
    if (scc.size() > 1) {
      d.nodeSets.insert(scc);
    }
  }
}


}


NodeSets tarjan(const Graph& graph)
{
  Data d{graph};

  d.graph.foreach([&d](const Node* v){
    if (d.nodeIndex.find(v) == d.nodeIndex.end()){
      strongconnect(v, d);
    }
  });

  return d.nodeSets;
}


}
