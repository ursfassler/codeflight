/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "johnson.h"
#include "component/codeflight/ast/graph/graph.h"
#include <limits>
#include <list>
#include <map>
#include <set>


namespace component::codeflight::ast::graph::cycle
{
namespace
{


class JohnsonSimpleCycles
{
    // D.B.Johnson, Finding all the elementary circuits of a directed graph, SIAM J. Comput., 4 (1975), pp. 77-84.
    // from https://github.com/jgrapht/jgrapht/blob/master/jgrapht-core/src/main/java/org/jgrapht/alg/cycle/JohnsonSimpleCycles.java
    // author Nikolay Ognyanov
    // licenced EPL-2.0 OR LGPL-2.1-or-later

  private:
    using V = const Node *;

    // The graph.
    const Graph& graph;
    const CyleReporter& reporter;

    // The main state of the algorithm.
    std::map<int, V> iToV{};
    std::map<V, int> vToI{};
    std::set<V> blocked{};
    std::map<V, std::set<V>> bSets{};
    std::vector<V> stack{};

    // The state of the embedded Tarjan SCC algorithm.
    std::list<NodeList> SCCs{};
    int index = 0;
    std::map<V, int> vIndex{};
    std::map<V, int> vLowlink{};
    std::vector<V> path{};
    std::set<V> pathSet{};

  public:
    JohnsonSimpleCycles(const Graph& graph_, const CyleReporter& reporter_) :
      graph{graph_},
      reporter{reporter_}
    {
    }

    void findSimpleCycles()
    {
      initState();

      int startIndex = 0;
      int size = graph.numberOfNodes();
      while (startIndex < size) {
        std::optional<std::pair<Graph, int>> minSCCGResult = findMinSCSG(startIndex);
        if (minSCCGResult.has_value()) {
          startIndex = minSCCGResult.value().second;
          Graph scg = minSCCGResult.value().first;
          V startV = iToV[startIndex];
          scg.foreach(startV, [this](const V v){
            blocked.erase(v);
            bSets[v].clear();
          });
          findCyclesInSCG(startIndex, startIndex, scg);
          startIndex++;
        } else {
          break;
        }
      }

      clearState();
    }

  private:
    std::optional<std::pair<Graph, int>> findMinSCSG(int startIndex)
    {
      // Per Johnson : "adjacency structure of strong component $K$ with least vertex in subgraph
      // of $G$ induced by $(s, s + 1, n)$". Or in contemporary terms: the strongly connected
      // component of the subgraph induced by $(v_1, \dotso ,v_n)$ which contains the minimum
      // (among those SCCs) vertex index. We return that index together with the graph.
      initMinSCGState();

      std::list<NodeList> SCCs = findSCCS(startIndex);

      // find the SCC with the minimum index
      int minIndexFound = std::numeric_limits<int>::max();
      std::optional<NodeList> minSCC{};
      for (const NodeList& scc : SCCs) {
        for (V v : scc) {
          int t = vToI[v];
          if (t < minIndexFound) {
            minIndexFound = t;
            minSCC = scc;
          }
        }
      }
      if (!minSCC.has_value()) {
        return {};
      }

      // build a graph for the SCC found
      Graph resultGraph{};
      for (V v : minSCC.value()) {
        resultGraph.add(v);
      }
      for (V v : minSCC.value()) {
        for (V w : minSCC.value()) {
          if (graph.edgeExists(v, w)) {
            resultGraph.add(v, w);
          }
        }
      }

      std::pair<Graph, int> result = {resultGraph, minIndexFound};
      clearMinSCCState();
      return result;
    }

    std::list<NodeList> findSCCS(int startIndex)
    {
      // Find SCCs in the subgraph induced
      // by vertices startIndex and beyond.
      // A call to StrongConnectivityAlgorithm
      // would be too expensive because of the
      // need to materialize the subgraph.
      // So - do a local search by the Tarjan's
      // algorithm and pretend that vertices
      // with an index smaller than startIndex
      // do not exist.
      graph.foreach([this, startIndex](const V v){
        int vI = vToI[v];
        if (vI >= startIndex) {
          const auto idx = vIndex.find(v);
          const auto contains = idx != vIndex.end();
          if (!contains) {
            getSCCs(startIndex, vI);
          }
        }
      });
      std::list<NodeList> result = SCCs;
      SCCs = {};
      return result;
    }

    void getSCCs(int startIndex, int vertexIndex)
    {
      V vertex = iToV[vertexIndex];
      vIndex[vertex] = index;
      vLowlink[vertex] = index;
      index++;
      path.push_back(vertex);
      pathSet.insert(vertex);

      graph.foreach(vertex, [this, startIndex, vertex](const V successor){
        int successorIndex = vToI[successor];
        if (successorIndex >= startIndex) {
          const auto containsKey = vIndex.find(successor) != vIndex.end();
          if (!containsKey) {
            getSCCs(startIndex, successorIndex);
            vLowlink[vertex] = std::min(vLowlink[vertex], vLowlink[successor]);
          } else {
            const auto contains = pathSet.find(successor) != pathSet.end();
            if (contains) {
              vLowlink[vertex] = std::min(vLowlink[vertex], vIndex[successor]);
            }
          }
        }
      });
      if (vLowlink[vertex] == vIndex[vertex]) {
        NodeList result{};
        V temp = nullptr;
        do {
          temp = path.back();
          path.pop_back();
          pathSet.erase(temp);
          result.push_back(temp);
        } while (vertex != temp);
        if (result.size() == 1) {
          V v = result[0];
          if (graph.edgeExists(vertex, v)) {
            SCCs.push_back(result);
          }
        } else {
          SCCs.push_back(result);
        }
      }
    }

    bool findCyclesInSCG(int startIndex, int vertexIndex, const Graph& scg)
    {
      // Find cycles in a strongly connected graph per Johnson.
      bool foundCycle = false;
      V vertex = iToV[vertexIndex];
      stack.push_back(vertex);
      blocked.insert(vertex);

      scg.foreach(vertex, [this, startIndex, &foundCycle, &scg](const V successor){
        int successorIndex = vToI[successor];
        if (successorIndex == startIndex) {
          reporter(stack);
          foundCycle = true;
        } else {
          const auto contains = blocked.find(successor) != blocked.end();
          if (!contains) {
            bool gotCycle = findCyclesInSCG(startIndex, successorIndex, scg);
            foundCycle = foundCycle || gotCycle;
          }
        }
      });
      if (foundCycle) {
        unblock(vertex);
      } else {
        scg.foreach(vertex, [this, vertex](const V w){
          bSets[w].insert(vertex);
        });
      }
      stack.pop_back();
      return foundCycle;
    }

    void unblock(V vertex)
    {
      blocked.erase(vertex);
      std::set<V>& bSet = bSets[vertex];
      while (bSet.size() > 0) {
        V w = *bSet.begin();
        bSet.erase(w);
        const auto contains = blocked.find(w) != blocked.end();
        if (contains) {
          unblock(w);
        }
      }
    }

    void initState()
    {
      clearState();

      int i = 0;
      graph.foreach([this, &i](const V v){
        iToV[i] = v;
        vToI[v] = i;
        i++;
      });
    }

    void clearState()
    {
      iToV.clear();
      vToI.clear();
      blocked.clear();
      bSets.clear();
      stack.clear();
    }

    void initMinSCGState()
    {
      index = 0;
      SCCs.clear();
      vIndex.clear();
      vLowlink.clear();
      path.clear();
      pathSet.clear();
    }

    void clearMinSCCState()
    {
      index = 0;
      SCCs.clear();
      vIndex.clear();
      vLowlink.clear();
      path.clear();
      pathSet.clear();
    }
};


}


void johnson(const ast::graph::Graph& graph, const CyleReporter& cycleReporter)
{
  JohnsonSimpleCycles cd{graph, cycleReporter};
  cd.findSimpleCycles();
}


}
