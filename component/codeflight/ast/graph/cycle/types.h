/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <set>
#include <vector>

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::ast::graph::cycle
{


using NodeList = std::vector<const Node*>;
using NodeSet = std::set<const Node*>;
using NodeSets = std::set<NodeSet>;


}
