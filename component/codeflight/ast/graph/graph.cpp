/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "graph.h"
#include "component/codeflight/ast/Node.h"
#include "component/codeflight/ast/graph/graph.h"
#include "component/codeflight/ast/parents/parents.h"
#include "component/codeflight/ast/specification/Package.h"
#include "component/codeflight/ast/traverse/type.h"
#include "component/codeflight/library/Set.h"
#include <stdexcept>


namespace component::codeflight::ast::graph
{
namespace
{


void addAllReachable(const Node* node, library::Set<const Node*>& visited, const std::map<const Node*, std::set<const Node*>>& graph)
{
  if (!visited.contains(node)) {
    visited.add(node);
    const auto idx = graph.find(node);
    if (idx == graph.end()) {
      throw std::invalid_argument("node not in graph");
    }
    for (const auto dest : idx->second) {
      addAllReachable(dest, visited, graph);
    }
  }
}

void forAllTargetAnchors(const Node& node, const Node* root, const std::function<void(const Node*)>& visitor)
{
  const auto parentIsRoot = [root](const Node* node){
    const auto parent = parents::findParent(node).value_or(nullptr);
    return parent == root;
  };

  for (const auto target : node.references) {
    const auto p = parents::findAncestor(target, parentIsRoot);

    if (p) {
      visitor(*p);
    }
  }
}

void forAllTargetParentsInGraph(const Node& node, const ast::graph::Graph& graph, const std::function<void(const Node*)>& visitor)
{
  for (const auto target : node.references) {
    const auto p = parents::findAncestor(target, [&graph](const Node* node){
      return graph.nodeExists(node);
    });

    if (p) {
      visitor(*p);
    }
  }
}


}


Graph buildDirectChildrenGraph(const Node& root, const NodeSpecification& spec)
{
  Graph graph{};

  traverse::children(root, [&graph, &spec](const Node& child){
    if (spec(&child)) {
      graph.add(&child);
    }
  });

  traverse::children(root, [&graph, &root](const Node& clazz){
    if (graph.nodeExists(&clazz)) {
      traverse::recursive(clazz, [&graph, &clazz, &root](const Node& node){
        forAllTargetAnchors(node, &root, [&graph, &clazz](const Node* p){
          if (graph.nodeExists(p)) {
            graph.add(&clazz, p);
          }
        });
      });
    }
  });

  return graph;
}

Graph buildPackageDependencyGraph(const Node& root)
{
  Graph graph{};

  graph.add(&root);
  traverse::children(root, [&graph](const Node& child){
    if (specification::Package()(&child)) {
      graph.add(&child);
    }
  });

  traverse::children(root, [&graph, &root](const Node& clazz){
    const bool isPackage = specification::Package()(&clazz);
    const Node& source = isPackage ? clazz : root;

    traverse::recursive(clazz, [&graph, &source](const Node& node){
      forAllTargetParentsInGraph(node, graph, [&graph, &source](const Node* p){
        graph.add(&source, p);
      });
    });
  });

  return graph;
}

//TODO rename / merge
Graph buildGraph(const Node& root, const NodeSpecification& specification)
{
  Graph graph{};

  traverse::recursive(root, [&specification, &graph](const ast::Node& node){
    if (specification(&node)) {
      graph.add(&node);
    }
  });

  const auto getParentPackage = [&graph](const ast::Node* node){
    const auto package = parents::findAncestor(node, [&graph](const Node* node){
      return graph.nodeExists(node);
    });
    return package;
  };

  traverse::recursive(root, [&graph, &getParentPackage](const Node& node){
    const std::optional<const Node*> source = getParentPackage(&node);
    if (source) {
      for (const auto target : node.references) {
        std::optional<const Node*> dest = getParentPackage(target);
        if (dest && graph.nodeExists(*dest)) {
          // we may have not started at the root node
          graph.add(*source, *dest);
        }
      }
    }
  });

  return graph;
}

void Graph::removeDuplicatedEdges()
{
  for (auto& source : map.keys()) {
    Destinations& dests = map.value(source);

    library::Set<const Node*> seen;
    const auto newEnd = std::remove_if(dests.begin(), dests.end(), [&seen](const Node* value){
      const auto isDuplicate = seen.contains(value);
      seen.add(value);
      return isDuplicate;
    });

    dests.erase(newEnd, dests.end());
  }
}

void Graph::removeEdgeToItself()
{
  for (auto& source : map.keys()) {
    Destinations& dests = map.value(source);

    const auto newEnd = std::remove_if(dests.begin(), dests.end(), [source](const ast::Node* dest){
      return source == dest;
    });

    dests.erase(newEnd, dests.end());
  }
}

std::size_t Graph::countConnectedComponents() const
{
  std::map<const Node*, std::set<const Node*>> undirected;

  foreach([&undirected](const Node* node){
    undirected[node];
  });
  foreach([&undirected](const Node* source, const Node* destination){
    undirected[source].insert(destination);
    undirected[destination].insert(source);
  });

  std::size_t count = 0;
  library::Set<const Node*> visited{};
  for (const auto& pair : undirected) {
    const auto isVisited = visited.contains(pair.first);
    if (!isVisited) {
      count++;
      addAllReachable(pair.first, visited, undirected);
    }
  }
  return count;
}

void Graph::assertThat(bool predicate) const
{
  if (!predicate) {
    throw std::runtime_error("assertion failed in " + std::string(__FILE__) + ":" + std::to_string(__LINE__));
  }
}

bool Graph::nodeExists(const Node* value) const
{
  return map.contains(value);
}

bool Graph::edgeExists(const Node* source, const Node* destination) const
{
  const auto srcIdx = map.find(source);
  if (srcIdx == map.end()) {
    return false;
  }
  const auto dstIdx = std::find_if(srcIdx->second.begin(), srcIdx->second.end(), [destination](const Node* entry){
    return destination == entry;
  });
  return dstIdx != srcIdx->second.end();
}

std::size_t Graph::numberOfNodes() const
{
  return map.size();
}

std::size_t Graph::countEdges() const
{
  std::size_t count{0};

  for (const auto& key : map.keys()) {
    count += map.value(key).size();
  }

  return count;
}

std::optional<Graph::Destinations*> Graph::find(const Node* source)
{
  auto idx = map.find(source);
  if (idx == map.end()) {
    return {};
  } else {
    Destinations* destinations = &idx->second;
    return {destinations};
  }
}

void Graph::add(const Node* node)
{
  assertThat(!nodeExists(node));

  map.add(node, {});
}

void Graph::add(const Node* source, const Node* destination)
{
  assertThat(nodeExists(source));
  assertThat(nodeExists(destination));

  auto res = find(source);
  res.value()->push_back(destination);
}

void Graph::foreach(const std::function<void(const Node*)>& visitor) const
{
  for (const auto& node : map.keys()) {
    visitor(node);
  }
}

void Graph::foreach(const Node* source, const std::function<void(const Node*)>& visitor) const
{
  auto idx = map.find(source);
  if (idx != map.end()){
    for (const auto& destination : idx->second) {
      visitor(destination);
    }
  }
}

void Graph::foreach(const std::function<void(const Node*, const Node*)>& visitor) const
{
  for (const auto& source : map.keys()) {
    for (const auto& dest : map.value(source)) {
      visitor(source, dest);
    }
  }
}


}
