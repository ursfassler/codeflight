/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/ast/NodeSpecification.h"
#include "component/codeflight/library/OrderedMap.h"
#include <optional>
#include <vector>

namespace component::codeflight::ast::graph
{


class Graph
{
  public:
    Graph() = default;
    Graph(const Graph&) = default;

    void add(const Node*);
    void add(const Node* source, const Node* destination);

    void removeDuplicatedEdges();
    void removeEdgeToItself();

    std::size_t countConnectedComponents() const;

    void foreach(const std::function<void(const Node*)>&) const;
    void foreach(const Node*, const std::function<void(const Node*)>&) const;
    void foreach(const std::function<void(const Node*, const Node*)>&) const;

    bool nodeExists(const Node*) const;
    bool edgeExists(const Node*, const Node*) const;
    std::size_t numberOfNodes() const;
    std::size_t countEdges() const;

  private:
    using Destinations = std::vector<const Node*>;

    library::OrderedMap<const Node*, Destinations> map{};

    std::optional<Destinations*> find(const Node*);

    void assertThat(bool) const;

};


Graph buildDirectChildrenGraph(const Node&, const NodeSpecification&);
Graph buildPackageDependencyGraph(const Node&);
Graph buildGraph(const Node&, const NodeSpecification&);


}
