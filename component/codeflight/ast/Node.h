/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <optional>
#include <string>
#include <vector>


namespace component::codeflight::ast
{


class Visitor;
class MutableVisitor;


class Node
{
public:
  virtual ~Node() = default;

  virtual void accept(Visitor&) const = 0;
  virtual void accept(MutableVisitor&) = 0;

  std::string name{};
  std::optional<Node*> parent{};
  std::vector<std::unique_ptr<Node>> children{};
  std::vector<Node*> references{};

};


}
