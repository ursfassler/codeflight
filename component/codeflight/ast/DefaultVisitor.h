/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Visitor.h"


namespace component::codeflight::ast
{

class Node;


class DefaultVisitor :
  public Visitor
{
public:
  void visit(const Project&) override;
  void visit(const Class&) override;
  void visit(const Package&) override;
  void visit(const Variable&) override;
  void visit(const Function&) override;
  void visit(const Field&) override;
  void visit(const Method&) override;
  virtual void default_(const Node&);
};


}
