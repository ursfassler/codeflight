/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Method.h"
#include "MutableVisitor.h"
#include "Visitor.h"


namespace component::codeflight::ast
{


void Method::accept(Visitor &visitor) const
{
  visitor.visit(*this);
}

void Method::accept(MutableVisitor &visitor)
{
  visitor.visit(*this);
}


}
