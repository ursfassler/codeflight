/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Package.h"
#include "MutableVisitor.h"
#include "Visitor.h"


namespace component::codeflight::ast
{


void Package::accept(Visitor &visitor) const
{
  visitor.visit(*this);
}

void Package::accept(MutableVisitor &visitor)
{
  visitor.visit(*this);
}


}
