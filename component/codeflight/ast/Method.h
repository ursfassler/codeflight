/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Node.h"


namespace component::codeflight::ast
{


class Method :
    public Node
{
public:
  void accept(Visitor& visitor) const override;
  void accept(MutableVisitor& visitor) override;
};


}
