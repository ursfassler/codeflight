/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once


namespace component::codeflight::ast
{


class Node;


struct Dependency
{
  const ast::Node* from;
  const ast::Node* to;
};


}
