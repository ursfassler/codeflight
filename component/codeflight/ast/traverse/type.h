/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/ast/DefaultVisitor.h"
#include "traverse.h"
#include <functional>

namespace component::codeflight::ast
{

class Node;


namespace traverse
{


template<typename T>
using KindVisitor = std::function<void(const T&)>;


template<typename T>
class KindVisitorDispatcher :
    public ast::DefaultVisitor
{
  public:
    KindVisitorDispatcher(const KindVisitor<T>& visitor_) :
      visitor{visitor_}
    {
    }

    void visit(const T& node) override
    {
      visitor(node);
    }

  private:
    const KindVisitor<T>& visitor;

};


template<typename T>
void recursive(const ast::Node& node, const KindVisitor<T>& visitor)
{
  KindVisitorDispatcher<T> vt{visitor};
  recursive(node, vt);
}

template<typename T>
void children(const ast::Node& node, const KindVisitor<T>& visitor)
{
  KindVisitorDispatcher<T> vt{visitor};
  children(node, vt);
}


}
}
