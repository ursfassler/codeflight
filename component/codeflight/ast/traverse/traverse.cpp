/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "traverse.h"
#include "component/codeflight/ast/Node.h"
#include "component/codeflight/ast/specification/All.h"


namespace component::codeflight::ast::traverse
{


void recursive(const Node& node, const ast::traverse::Visitor& visitor)
{
  visitor(node);

  for (const auto& child : node.children) {
    recursive(*child, visitor);
  }
}

void recursiveMutable(Node& node, const ast::traverse::MutableVisitor& visitor)
{
  visitor(node);

  for (const auto& child : node.children) {
    recursiveMutable(*child, visitor);
  }
}

void recursiveFor(const Node& node, const NodeSpecification& spec, const Visitor& visitor)
{
  if (spec(&node)) {
    visitor(node);
  }

  for (const auto& child : node.children) {
    recursiveFor(*child, spec, visitor);
  }
}

void recursive(const Node& node, ast::Visitor& visitor)
{
  recursive(node, [&visitor](const ast::Node& node){
    node.accept(visitor);
  });
}

void children(const Node& node, ast::Visitor& visitor)
{
  children(node, [&visitor](const ast::Node& node){
    node.accept(visitor);
  });
}

void children(const Node& node, const ast::traverse::Visitor& visitor)
{
  for (const auto& child : node.children) {
    visitor(*child);
  }
}


}
