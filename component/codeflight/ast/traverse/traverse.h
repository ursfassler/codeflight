/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/ast/NodeSpecification.h"
#include <functional>


namespace component::codeflight::ast
{

class Node;
class Visitor;


namespace traverse
{


using Visitor = std::function<void(const Node&)>;
using MutableVisitor = std::function<void(Node&)>;

void recursive(const Node&, const ast::traverse::Visitor&);
void recursiveMutable(Node&, const ast::traverse::MutableVisitor&);
void recursiveFor(const Node&, const NodeSpecification&, const Visitor&);
void recursive(const Node&, ast::Visitor&);

void children(const Node&, ast::Visitor&);
void children(const Node&, const ast::traverse::Visitor&);


}
}

