/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/ast/DefaultVisitor.h"
#include "component/codeflight/ast/Node.h"
#include <functional>

namespace component::codeflight::ast::query
{


template<typename T>
using BranchHandler = std::function<void(const T&)>;


template<typename T>
class TypeVisitorDispatcher :
    public ast::DefaultVisitor
{
  public:
    TypeVisitorDispatcher(
        const BranchHandler<T>& thenHandler_,
        const BranchHandler<Node>& elseHandler_
      ) :
      thenHandler{thenHandler_},
      elseHandler{elseHandler_}
    {
    }

    void visit(const T& node) override
    {
      thenHandler(node);
    }

    void default_(const Node& node) override
    {
      elseHandler(node);
    }

  private:
    const BranchHandler<T>& thenHandler;
    const BranchHandler<Node>& elseHandler;

};


template <typename T>
void ifType(const Node& node, const BranchHandler<T>& thenHandler, const BranchHandler<Node>& elseHandler)
{
  TypeVisitorDispatcher<T> visitor{thenHandler, elseHandler};
  node.accept(visitor);
}


}
