/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/ast/DefaultVisitor.h"
#include "component/codeflight/ast/Node.h"


namespace component::codeflight::ast::query
{
namespace
{

template<typename T>
class IsType :
    public DefaultVisitor
{
  public:
    void visit(const T&) override
    {
      is = true;
    }

    bool isType() const
    {
      return is;
    }

  private:
    bool is{false};
};

}


template<typename T>
bool isOfType(const Node* node)
{
  IsType<T> visitor{};
  node->accept(visitor);
  return visitor.isType();
}


}
