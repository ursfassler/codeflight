/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <vector>

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::ast::query
{


const Node* find(const std::string&, const Node*);
const Node* find(const std::vector<std::string>&, const Node*);


}
