/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::ast::name
{


std::string typeName(const ast::Node*);


}
