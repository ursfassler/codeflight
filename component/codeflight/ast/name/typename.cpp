/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "typename.h"
#include "component/codeflight/ast/Node.h"
#include "component/codeflight/ast/Visitor.h"


namespace component::codeflight::ast::name
{
namespace
{


class TypeName :
    public ast::Visitor
{
public:
  void visit(const ast::Variable&) override
  {
    name = "variable";
  }

  void visit(const ast::Function&) override
  {
    name = "function";
  }

  void visit(const ast::Field&) override
  {
    name = "field";
  }

  void visit(const ast::Method&) override
  {
    name = "method";
  }

  void visit(const ast::Class&) override
  {
    name = "class";
  }

  void visit(const ast::Package&) override
  {
    name = "package";
  }

  void visit(const ast::Project&) override
  {
    name = "project";
  }

  std::string name{};
};


}


std::string typeName(const ast::Node* node)
{
  TypeName visitor{};
  node->accept(visitor);
  return visitor.name;
}


}
