/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::ast::name
{


std::string formatName(const Node* node, const std::string& emptyPackageReplacement);


}
