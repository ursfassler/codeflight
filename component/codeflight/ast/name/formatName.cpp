/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "formatName.h"
#include "component/codeflight/ast/Node.h"
#include "component/codeflight/ast/specification/Project.h"


namespace component::codeflight::ast::name
{
namespace
{


std::string nameReplacement(const Node* node, const std::string& emptyPackageReplacement)
{
  if (ast::specification::Project()(node)) {
    return "Project";
  } else {
    return emptyPackageReplacement;
  }
}


}


std::string formatName(const Node* node, const std::string& emptyPackageReplacement)
{
  if (node->name == "") {
    return nameReplacement(node, emptyPackageReplacement);
  } else {
    return node->name;
  }
}


}
