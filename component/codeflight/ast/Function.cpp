/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Function.h"
#include "MutableVisitor.h"
#include "Visitor.h"


namespace component::codeflight::ast
{


void Function::accept(Visitor &visitor) const
{
  visitor.visit(*this);
}

void Function::accept(MutableVisitor &visitor)
{
  visitor.visit(*this);
}


}
