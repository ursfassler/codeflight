/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Neighbor.h"
#include "component/codeflight/ast/Node.h"
#include "component/codeflight/ast/parents/parents.h"
#include "component/codeflight/ast/specification/Package.h"
#include "component/codeflight/ast/specification/PackageElement.h"
#include <cassert>


namespace component::codeflight::ast::repository
{


Neighbor::Neighbor() :
  outgoingPackageElement{outgoing, specification::PackageElement()},
  incomingPackageElement{incoming, specification::PackageElement()},
  outgoingPackage{outgoing, specification::Package()},
  incomingPackage{incoming, specification::Package()}
{
}

void Neighbor::build(const Node *node)
{
  incoming[node];
  outgoing[node];

  for (const auto dest : node->references) {
    incoming[dest].push_back(node);
    outgoing[node].push_back(dest);
  }

  for (const auto& child : node->children) {
    build(child.get());
  }
}

void Neighbor::foreachOutgoingPackageElement(const Node *source, const Visitor &visitor) const
{
  foreach(outgoingPackageElement, source, visitor);
}

void Neighbor::foreachOutgoingPackage(const Node *source, const Visitor &visitor) const
{
  foreach(outgoingPackage, source, visitor);
}

void Neighbor::foreachIncomingPackageElement(const Node *destination, const Visitor &visitor) const
{
  foreach(incomingPackageElement, destination, visitor);
}

void Neighbor::foreachIncomingPackage(const Node *destination, const Visitor &visitor) const
{
  foreach(incomingPackage, destination, visitor);
}

void Neighbor::foreachOutgoing(const Node* source, const EdgeVisitor& visitor) const
{
  const auto idx = outgoing.find(source);
  assert(idx != outgoing.end());

  for (const Node* destination : idx->second) {
    visitor(source, destination);
  }

  for (const auto& child : source->children) {
    foreachOutgoing(child.get(), visitor);
  }
}

void Neighbor::foreachIncoming(const Node* destination, const EdgeVisitor& visitor) const
{
  const auto idx = incoming.find(destination);
  assert(idx != incoming.end());

  for (const Node* source : idx->second) {
    visitor(source, destination);
  }

  for (const auto& child : destination->children) {
    foreachIncoming(child.get(), visitor);
  }
}

void Neighbor::foreach(const Specification& spec, const Node *destination, const Visitor &visitor) const
{
  Edges visited{};
  foreach(spec, destination, visitor, visited);
}

void Neighbor::foreach(const Specification& spec, const Node* node, const Visitor &visitor, Edges &visited) const
{
  const auto idx = spec.vertices.find(node);
  assert(idx != spec.vertices.end());

  const auto nodeAncestor = ast::parents::findAncestor(node, specification::PackageElement());
  if (nodeAncestor.has_value()) {
    const auto ancestorNode = nodeAncestor.value();

    for (const Node* successor : idx->second) {
      const auto element = ast::parents::findAncestor(successor, specification::PackageElement());
      if (element.has_value()) {
        const auto elementNode = element.value();
        if (!visited[ancestorNode].contains(elementNode)) {
          visited[ancestorNode].add(elementNode);
          const auto ancestor = ast::parents::findAncestor(elementNode, spec.nodes);
          assert(ancestor.has_value());
          visitor(ancestor.value());
        }
      }
    }

  }

  for (const auto& child : node->children) {
    if (!spec.nodes(child.get())) {
      foreach(spec, child.get(), visitor, visited);
    }
  }
}


}
