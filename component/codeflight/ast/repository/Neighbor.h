/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/library/Set.h"
#include "component/codeflight/ast/NodeSpecification.h"
#include <functional>
#include <map>


namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::ast::repository
{


class Neighbor
{
  public:
    using Visitor = std::function<void(const Node*)>;
    using EdgeVisitor = std::function<void(const Node*, const Node*)>;

    using Vertices = std::map<const Node*, std::vector<const Node*>>;
    struct Specification
    {
        const Vertices& vertices;
        const NodeSpecification nodes;
    };

    Neighbor();

    void build(const Node* node);

    void foreachOutgoingPackageElement(const Node* source, const Visitor& visitor) const;
    void foreachIncomingPackageElement(const Node* destination, const Visitor& visitor) const;

    void foreachOutgoingPackage(const Node* source, const Visitor& visitor) const;
    void foreachIncomingPackage(const Node* destination, const Visitor& visitor) const;

    void foreachOutgoing(const Node* source, const EdgeVisitor& visitor) const;
    void foreachIncoming(const Node* destination, const EdgeVisitor& visitor) const;

    void foreach(const Specification&, const Node* destination, const Visitor& visitor) const;

    const Specification outgoingPackageElement;
    const Specification incomingPackageElement;
    const Specification outgoingPackage;
    const Specification incomingPackage;

private:
    Vertices incoming{};
    Vertices outgoing{};

    using Edges = std::map<const Node*, library::Set<const Node*>>;
    void foreach(const Specification&, const Node*, const Visitor& visitor, Edges& visited) const;

};


}
