/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once


namespace component::codeflight::ast
{


class Variable;
class Function;
class Field;
class Method;
class Class;
class Package;
class Project;


class MutableVisitor
{
public:
  virtual ~MutableVisitor() = default;

  virtual void visit(Variable&) = 0;
  virtual void visit(Function&) = 0;
  virtual void visit(Field&) = 0;
  virtual void visit(Method&) = 0;
  virtual void visit(Class&) = 0;
  virtual void visit(Package&) = 0;
  virtual void visit(Project&) = 0;

};


}
