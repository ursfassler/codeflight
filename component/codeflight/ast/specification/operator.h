/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/ast/NodeSpecification.h"


namespace component::codeflight::ast::specification
{


NodeSpecification operator||(NodeSpecification, NodeSpecification);


}
