/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "PackageElement.h"
#include "Class.h"
#include "Function.h"
#include "Variable.h"
#include "operator.h"


namespace component::codeflight::ast::specification
{


NodeSpecification PackageElement()
{
  return Class() || Function() || Variable();
}


}
