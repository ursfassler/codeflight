/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Class.h"
#include "component/codeflight/ast/query/isOfType.h"


namespace component::codeflight::ast::specification
{


NodeSpecification Class()
{
  return query::isOfType<ast::Class>;
}


}
