/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Field.h"
#include "component/codeflight/ast/Field.h"
#include "component/codeflight/ast/query/isOfType.h"


namespace component::codeflight::ast::specification
{


NodeSpecification Field()
{
  return query::isOfType<ast::Field>;
}


}
