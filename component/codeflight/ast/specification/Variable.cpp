/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Variable.h"
#include "component/codeflight/ast/query/isOfType.h"


namespace component::codeflight::ast::specification
{


NodeSpecification Variable()
{
  return query::isOfType<ast::Variable>;
}


}
