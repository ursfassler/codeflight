/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "All.h"


namespace component::codeflight::ast::specification
{


NodeSpecification All()
{
  return [](const ast::Node*){
    return true;
  };
}


}
