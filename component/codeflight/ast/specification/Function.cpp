/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Function.h"
#include "component/codeflight/ast/query/isOfType.h"


namespace component::codeflight::ast::specification
{


NodeSpecification Function()
{
  return query::isOfType<ast::Function>;
}


}
