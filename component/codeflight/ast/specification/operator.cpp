/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "operator.h"


namespace component::codeflight::ast::specification
{


NodeSpecification operator||(NodeSpecification left, NodeSpecification right)
{
  return [left, right](const Node* node){
    return left(node) || right(node);
  };
}


}
