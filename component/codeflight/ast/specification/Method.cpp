/*
 * (C) Copyright 202 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Method.h"
#include "component/codeflight/ast/Method.h"
#include "component/codeflight/ast/query/isOfType.h"


namespace component::codeflight::ast::specification
{


NodeSpecification Method()
{
  return query::isOfType<ast::Method>;
}


}
