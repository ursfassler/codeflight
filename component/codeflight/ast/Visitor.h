/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once


namespace component::codeflight::ast
{


class Variable;
class Function;
class Field;
class Method;
class Class;
class Package;
class Project;


class Visitor
{
public:
  virtual ~Visitor() = default;

  virtual void visit(const Variable&) = 0;
  virtual void visit(const Function&) = 0;
  virtual void visit(const Field&) = 0;
  virtual void visit(const Method&) = 0;
  virtual void visit(const Class&) = 0;
  virtual void visit(const Package&) = 0;
  virtual void visit(const Project&) = 0;

};


}
