/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Project.h"
#include "MutableVisitor.h"
#include "Visitor.h"


namespace component::codeflight::ast
{


void Project::accept(Visitor& visitor) const
{
  visitor.visit(*this);
}

void Project::accept(MutableVisitor &visitor)
{
  visitor.visit(*this);
}


}
