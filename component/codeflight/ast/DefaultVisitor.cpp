/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DefaultVisitor.h"

#include "Class.h"
#include "Field.h"
#include "Function.h"
#include "Method.h"
#include "Package.h"
#include "Project.h"
#include "Variable.h"


namespace component::codeflight::ast
{


void DefaultVisitor::visit(const Project& node)
{
  default_(node);
}

void DefaultVisitor::visit(const Class& node)
{
  default_(node);
}

void DefaultVisitor::visit(const Package& node)
{
  default_(node);
}

void DefaultVisitor::visit(const Variable& node)
{
  default_(node);
}

void DefaultVisitor::visit(const Function& node)
{
  default_(node);
}

void DefaultVisitor::visit(const Field& node)
{
  default_(node);
}

void DefaultVisitor::visit(const Method& node)
{
  default_(node);
}

void DefaultVisitor::default_(const Node&)
{
}


}
