/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <memory>


namespace component::codeflight::ast
{

class Project;
class Node;

}

namespace component::codeflight
{


class Namer
{
public:
  void name(ast::Project*);

private:
  unsigned unnamedNumber{0};

  void recursive(std::unique_ptr<ast::Node>&);
  void nameChildren(std::unique_ptr<ast::Node>&);
};


}
