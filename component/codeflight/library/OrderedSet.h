/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <set>
#include <vector>


namespace component::codeflight::library
{


/**
 * A set where the elements are ordered by the first time they are inserted.
 */
template<typename T>
class OrderedSet
{
  public:
    OrderedSet() = default;

    OrderedSet(const std::vector<T>& data) :
      list{data},
      set{data.begin(), data.end()}
    {
    }

    void add(T item)
    {
      if (!contains(item)) {
        list.push_back(item);
        set.insert(item);
      }
    }

    bool contains(T item) const
    {
      return set.find(item) != set.end();
    }

    std::size_t size() const
    {
      return list.size();
    }

    typename std::vector<T>::const_iterator begin() const
    {
      return list.begin();
    }

    typename std::vector<T>::const_iterator end() const
    {
      return list.end();
    }

  private:
    std::vector<T> list{};
    std::set<T> set{};
};


}
