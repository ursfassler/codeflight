/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <vector>


namespace component::codeflight::library
{


std::string join(const std::vector<std::string>&, const std::string&);
std::vector<std::string> split(const std::string& value, const std::string& separator);
std::string toString(double, std::size_t);
std::string toString(bool);


}
