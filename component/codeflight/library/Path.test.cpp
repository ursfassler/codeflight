/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Path.h"
#include <gmock/gmock.h>


namespace component::codeflight::library::unit_test
{
namespace
{

using namespace testing;


TEST(Path_Test, returns_true_when_comparing_identical_paths)
{
  const Path path{{std::string{"a"}, "b"}};

  EXPECT_TRUE(path == path);
}

TEST(Path_Test, returns_true_when_comparing_equal_paths)
{
  const Path lhs{{std::string{"a"}, "b"}};
  const Path rhs{{std::string{"a"}, "b"}};

  EXPECT_TRUE(lhs == rhs);
}

TEST(Path_Test, returns_false_when_comparing_different_paths_with_equal_length)
{
  const Path lhs{{std::string{"a"}, "x"}};
  const Path rhs{{std::string{"a"}, "y"}};

  EXPECT_FALSE(lhs == rhs);
}

TEST(Path_Test, returns_false_when_comparing_different_paths_with_different_length)
{
  const Path lhs{{std::string{"a"}, "x"}};
  const Path rhs{{std::string{"a"}}};

  EXPECT_FALSE(lhs == rhs);
}

TEST(Path_Test, compare_returns_0_when_paths_are_equal)
{
  const Path lhs{{std::string{"a"}, "x"}};
  const Path rhs{{std::string{"a"}, "x"}};

  EXPECT_EQ(0, lhs.compare(rhs));
}

TEST(Path_Test, compare_returns_less_than_0_when_first_name_of_first_path_is_lower)
{
  const Path lhs{{std::string{"a"}, "x"}};
  const Path rhs{{std::string{"b"}, "x"}};

  EXPECT_GT(0, lhs.compare(rhs));
}

TEST(Path_Test, compare_returns_greater_than_0_when_later_name_of_first_path_is_higher)
{
  const Path lhs{{std::string{"a"}, "z", "u"}};
  const Path rhs{{std::string{"a"}, "x", "u"}};

  EXPECT_LT(0, lhs.compare(rhs));
}

TEST(Path_Test, compare_returns_less_than_0_when_names_are_equal_but_first_is_shorter)
{
  const Path lhs{{std::string{"a"}, "b"}};
  const Path rhs{{std::string{"a"}, "b", "c"}};

  EXPECT_GT(0, lhs.compare(rhs));
}

TEST(Path_Test, compare_returns_more_than_0_when_names_are_equal_but_second_is_shorter)
{
  const Path lhs{{std::string{"a"}, "b", "c"}};
  const Path rhs{{std::string{"a"}, "b"}};

  EXPECT_LT(0, lhs.compare(rhs));
}

TEST(Path_Test, returns_only_suffix_when_requesting_subPath)
{
  const Path context{{std::string{"a"}, "b"}};
  const Path full{{std::string{"a"}, "b", "c", "d"}};
  const Path expected{{std::string{"c"}, "d"}};

  const auto suffix = full.subPathIn(context);

  EXPECT_EQ(expected, suffix);
}

TEST(Path_Test, suffix_is_empty_when_full_path_matches_the_context)
{
  const Path context{{std::string{"a"}, "b"}};
  const Path full{{std::string{"a"}, "b"}};
  const Path expected{};

  const auto suffix = full.subPathIn(context);

  EXPECT_EQ(expected, suffix);
}

TEST(Path_Test, returns_error_when_path_is_less_deep_than_context)
{
  const Path context{{std::string{"a"}, "b"}};
  const Path full{{std::string{"a"}}};

  EXPECT_THROW(full.subPathIn(context), std::invalid_argument);
}

TEST(Path_Test, returns_error_when_path_is_not_in_context)
{
  const Path context{{std::string{"a"}, "x"}};
  const Path full{{std::string{"a"}, "y", "c", "d"}};

  EXPECT_THROW(full.subPathIn(context), std::invalid_argument);
}


}
}
