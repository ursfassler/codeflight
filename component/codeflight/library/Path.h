/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <vector>


namespace component::codeflight::library
{


class Path;
using Paths = std::vector<Path>;


class Path
{
  public:
    Path() = default;
    Path(const std::vector<std::string>&);

    Path add(const std::string&) const;
    Path remove() const;

    Path subPathIn(const Path&) const;

    std::vector<std::string> raw() const;
    std::string to_string() const;

    int compare(const Path&) const;

  private:
    std::vector<std::string> path{};

};

bool operator==(const Path&, const Path&);
bool operator<(const Path&, const Path&);


}
