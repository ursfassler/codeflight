/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <map>
#include <vector>


namespace component::codeflight::library
{


/**
 * A map where the elements are ordered by the first time the key is inserted.
 */
template<typename K, typename V>
class OrderedMap
{
  public:
    void add(K key, V value)
    {
      if (!contains(key)) {
        list.push_back(key);
      }
      map[key] = value;
    }

    bool contains(K key) const
    {
      return map.find(key) != map.end();
    }

    std::size_t size() const
    {
      return map.size();
    }

    const std::vector<K>& keys() const
    {
      return list;
    }

    const V& value(K key) const
    {
      return map.at(key);
    }

    V& value(K key)
    {
      return map[key];
    }

    typename std::map<K, V>::const_iterator find(K key) const
    {
      return map.find(key);
    }

    typename std::map<K, V>::iterator find(K key)
    {
      return map.find(key);
    }

    typename std::map<K, V>::const_iterator begin() const
    {
      return map.begin();
    }

    typename std::map<K, V>::const_iterator end() const
    {
      return map.end();
    }

  private:
    std::vector<K> list{};
    std::map<K, V> map{};
};


}
