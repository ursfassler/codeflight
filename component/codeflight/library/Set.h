/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <set>


namespace component::codeflight::library
{


template<typename T>
class Set
{
public:
  void add(T value)
  {
    items.insert(value);
  }

  bool contains(T value) const
  {
    return items.find(value) != items.end();
  }

  std::size_t size() const
  {
      return items.size();
  }

private:
  std::set<T> items{};
};


}
