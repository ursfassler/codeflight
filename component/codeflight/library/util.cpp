/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "util.h"
#include <iomanip>
#include <sstream>


namespace component::codeflight::library
{


std::string join(const std::vector<std::string>& value, const std::string& separator)
{
  std::string result{};

  bool first = true;
  for (const auto& part : value) {
    if (first) {
      first = false;
    } else {
      result += separator;
    }
    result += part;
  }

  return result;
}

std::vector<std::string> split(const std::string& value, const std::string& separator)
{
  std::vector<std::string> result{};

  std::size_t start = 0;
  while (true) {
    const std::size_t end = value.find(separator, start);
    const auto part = value.substr(start, end-start);
    result.push_back(part);
    if (end == std::string::npos) {
      break;
    }
    start = end + separator.size();
  }

  return result;
}

std::string toString(double value, std::size_t precision)
{
  std::stringstream ss{};
  ss << std::fixed;
  ss << std::setprecision(precision);
  ss << value;
  return ss.str();
}

std::string toString(bool value)
{
  std::stringstream ss{};
  ss << (value ? "true" : "false");
  return ss.str();
}


}
