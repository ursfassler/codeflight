/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Path.h"
#include "util.h"
#include <stdexcept>


namespace component::codeflight::library
{


Path::Path(const std::vector<std::string>& path_) :
  path{path_}
{
}

Path Path::add(const std::string& value) const
{
  std::vector<std::string> np = path;
  np.push_back(value);
  return {np};
}

Path Path::remove() const
{
  std::vector<std::string> np = path;
  np.pop_back();
  return {np};
}

Path Path::subPathIn(const Path& context) const
{
  const auto contextSize = context.path.size();
  const auto thisSize = path.size();

  if (thisSize < contextSize) {
    throw std::invalid_argument(to_string() + " is not within context " + context.to_string());
  }

  const std::vector<std::string> prefix{path.cbegin(), path.cbegin()+contextSize};
  if (prefix != context.path) {
    throw std::invalid_argument(to_string() + " is not within context " + context.to_string());
  }

  const std::vector<std::string> suffix{path.cbegin()+contextSize, path.cend()};
  return {suffix};
}

std::vector<std::string> Path::raw() const
{
  return path;
}

std::string Path::to_string() const
{
  return library::join(path, "::");
}

int Path::compare(const Path& other) const
{
  const auto& lhs = path;
  const auto& rhs = other.path;

  const std::size_t len = std::min(lhs.size(), rhs.size());
  for (std::size_t i = 0; i < len; i++) {
    auto res = lhs.at(i).compare(rhs.at(i));
    if (res != 0) {
      return res;
    }
  }

  return lhs.size() - rhs.size();
}

bool operator==(const Path& lhs, const Path& rhs)
{
  return lhs.raw() == rhs.raw();
}

bool operator<(const Path& lhs, const Path& rhs)
{
  return lhs.raw() < rhs.raw();
}


}
