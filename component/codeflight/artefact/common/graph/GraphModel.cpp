/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "GraphModel.h"
#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"
#include "component/codeflight/ast/Node.h"

namespace component::codeflight::artefact::common::graph
{
namespace
{


void assertThat(bool predicate)
{
  if (!predicate) {
    throw std::runtime_error("assertion failed in " + std::string(__FILE__) + ":" + std::to_string(__LINE__));
  }
}


}


void GraphModel::addNode(const ast::Node* node)
{
  nodes.insert(node);
  nodesWithoutContainer.add(node);
}

void GraphModel::addEdge(const ast::Node* source, const ast::Node* destination)
{
  assertThat(nodes.contains(source));
  assertThat(nodes.contains(destination));

  edgesToDraw.add({source, destination});
}

GraphModel::GraphModel(
    library::Path context_
    ) :
  context{context_}
{
}

void GraphModel::setLabel(const ast::Node* source, const ast::Node* destination, const std::size_t& value)
{
  assertThat(nodes.contains(source));
  assertThat(nodes.contains(destination));

  edgeLabel[{source, destination}] = value;
}

void GraphModel::setEmphasized(const ast::Node* node)
{
  assertThat(nodes.contains(node));
  emphasizedNodes.insert(node);
}

void GraphModel::setLink(const ast::Node* node, const Link& link)
{
  assertThat(nodes.contains(node) || nodesInContainer.contains(node));
  nodeLink[node] = link;
}

void GraphModel::setLink(const Edge& edge, const Link& link)
{
  assertThat(nodes.contains(edge.first));
  assertThat(nodes.contains(edge.second));

  edgeLink[edge] = link;
}

library::Path GraphModel::getContext() const
{
  return context;
}

std::optional<GraphModel::Link> GraphModel::getLink(const ast::Node* node) const
{
  const auto idx = nodeLink.find(node);
  if (idx == nodeLink.end()) {
    return {};
  } else {
    return idx->second;
  }
}

std::optional<std::size_t> GraphModel::getLabel(const Edge& edge) const
{
  const auto idx = edgeLabel.find(edge);
  if (idx == edgeLabel.end()) {
    return {};
  } else {
    return idx->second;
  }
}

bool GraphModel::isEmphasized(const ast::Node* node) const
{
  return emphasizedNodes.contains(node);
}

const library::OrderedSet<GraphModel::Edge>& GraphModel::getEdges() const
{
  return edgesToDraw;
}

std::optional<GraphModel::Link> GraphModel::getLink(const Edge& edge) const
{
  const auto idx = edgeLink.find(edge);
  if (idx == edgeLink.end()) {
    return {};
  } else {
    return idx->second;
  }
}


void GraphModel::addContainer(const ast::Node* node)
{
  if (!nodesInContainer.contains(node)) {
    nodesInContainer.add(node, {});
  }
}

void GraphModel::addChildNode(const ast::Node* node, const ast::Node* container)
{
  assertThat(nodesInContainer.contains(container));

  nodes.insert(node);
  nodesInContainer.value(container).add(node);
}

const std::vector<const ast::Node*>& GraphModel::getContainers() const
{
  return nodesInContainer.keys();
}

const library::OrderedSet<const ast::Node*>& GraphModel::getNodesWithoutContainers() const
{
  return nodesWithoutContainer;
}

const library::OrderedSet<const ast::Node*>& GraphModel::getNodesByContainer(const ast::Node* container) const
{
  return nodesInContainer.value(container);
}


}
