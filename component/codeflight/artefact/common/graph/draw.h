/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once


namespace component::codeflight::artefact::interface
{

class GraphBuilder;

}

namespace component::codeflight::artefact::common::graph
{

class GraphModel;


enum class NameType
{
  Path,
  Leaf,
};

struct DrawOptions
{
  NameType nameType = NameType::Path;
  bool graphLabel = false;
};

void draw(const GraphModel&, interface::GraphBuilder&, const DrawOptions&);


}
