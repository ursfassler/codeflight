/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "GraphArtefact.h"
#include "component/codeflight/artefact/common/graph/GraphModel.h"
#include "component/codeflight/artefact/common/graph/draw.h"
#include "component/codeflight/artefact/overviewfactory/Factory.h"
#include "component/codeflight/ast/graph/graph.h"
#include "component/codeflight/ast/parents/nodePath.h"
#include <sstream>


namespace component::codeflight::artefact::common::graph
{


GraphArtefact::GraphArtefact(
    const overviewfactory::Factory& overviewFactory_
    ) :
  overviewFactory{overviewFactory_}
{
}

void GraphArtefact::build(const ast::Node* node, const ast::graph::Graph& graph, interface::GraphBuilder& builder) const
{
  const auto pg = create(node, graph);
  common::graph::draw(pg, builder, {common::graph::NameType::Path, true});
}

common::graph::GraphModel GraphArtefact::create(const ast::Node* node, const ast::graph::Graph& graph) const
{
  common::graph::GraphModel result{ast::parents::nodePath(*node)};

  graph.foreach([&result, this](const ast::Node* node){
    result.addNode(node);

    auto ai = overviewFactory.identifier(node);
    if (ai) {
      result.setLink(node, *ai);
    }
  });
  graph.foreach([&result](const ast::Node* source, const ast::Node* destination){
    if (source != destination) {
      result.addEdge(source, destination);
    }
  });

  return result;
}


}
