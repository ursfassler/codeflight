/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"
#include "component/codeflight/library/OrderedMap.h"
#include "component/codeflight/library/OrderedSet.h"
#include "component/codeflight/library/Path.h"
#include <map>
#include <optional>


namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::common::graph
{


class GraphModel
{
public:
  using Edge = std::pair<const ast::Node*, const ast::Node*>;
  using Link = interface::ArtefactIdentifier;


  GraphModel(
      library::Path context
      );

  void addNode(const ast::Node* node);
  void addEdge(const ast::Node* source, const ast::Node* destination);
  void setLabel(const ast::Node* source, const ast::Node* destination, const std::size_t&);
  void setEmphasized(const ast::Node*);

  void setLink(const ast::Node*, const Link&);
  void setLink(const Edge&, const Link&);

  library::Path getContext() const;
  std::optional<Link> getLink(const ast::Node*) const;
  bool isEmphasized(const ast::Node*) const;
  std::optional<std::size_t> getLabel(const Edge&) const;
  const library::OrderedSet<Edge>& getEdges() const;
  std::optional<Link> getLink(const Edge&) const;


  void addContainer(const ast::Node* node);
  void addChildNode(const ast::Node* node, const ast::Node* container);

  const std::vector<const ast::Node*>& getContainers() const;
  const library::OrderedSet<const ast::Node*>& getNodesWithoutContainers() const;
  const library::OrderedSet<const ast::Node*>& getNodesByContainer(const ast::Node*) const;

private:
  const library::Path context{};
  library::OrderedSet<const ast::Node*> nodesWithoutContainer{};
  library::OrderedMap<const ast::Node*, library::OrderedSet<const ast::Node*>> nodesInContainer{};
  std::set<const ast::Node*> nodes{};
  std::set<const ast::Node*> emphasizedNodes{};
  std::map<const ast::Node*, Link> nodeLink{};
  library::OrderedSet<Edge> edgesToDraw{};
  std::map<Edge, std::size_t> edgeLabel{};
  std::map<Edge, Link> edgeLink{};

};


}
