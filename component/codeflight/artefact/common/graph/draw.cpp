/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "draw.h"
#include "GraphModel.h"
#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"
#include "component/codeflight/artefact/interface/GraphBuilder.h"
#include "component/codeflight/ast/Node.h"
#include "component/codeflight/ast/parents/nodePath.h"

namespace component::codeflight::artefact::common::graph
{
namespace
{


library::Path getPath(const ast::Node* node, NameType type, const library::Path& context)
{
  switch (type) {
    case NameType::Leaf:
      return library::Path{{node->name}};

    case NameType::Path:
      auto p = ast::parents::nodePath(*node);
      auto sub = p.subPathIn(context);
      return sub;
  }

  throw std::runtime_error("unhandled value " + std::to_string(static_cast<int>(type)) + " in " + __func__);
}


std::optional<interface::GraphBuilder::Link> convertLink(const std::optional<GraphModel::Link>& link)
{
  if (link) {
    return {*link};
  } else {
    return {};
  }
}


}


void drawNode(const ast::Node* node, const library::Path& context, const GraphModel& model, interface::GraphBuilder& gvw, const DrawOptions& options)
{
  const auto emphasize = model.isEmphasized(node);
  const auto name = getPath(node, options.nameType, context);
  const auto link = model.getLink(node);
  gvw.node(node, name, emphasize, convertLink(link));
}

void drawEdge(const GraphModel::Edge& edge, const GraphModel& model, interface::GraphBuilder& gvw)
{
  const auto labelNr = model.getLabel(edge);
  const std::optional<std::string> labelText = labelNr ? std::to_string(*labelNr) :  std::optional<std::string>{};
  const auto link = model.getLink(edge);
  gvw.edge(edge.first, edge.second, labelText, convertLink(link));
}

void draw(const GraphModel& model, interface::GraphBuilder& gvw, const DrawOptions& options)
{
  const auto context = model.getContext();
  const std::optional<library::Path> label = options.graphLabel ? context : std::optional<library::Path>{};
  gvw.beginGraph(label);

  for (const auto node : model.getNodesWithoutContainers()) {
    drawNode(node, context, model, gvw, options);
  }

  for (const auto parent : model.getContainers()) {
    const auto label = getPath(parent, options.nameType, context);
    const auto link = model.getLink(parent);
    gvw.beginContainer(label, convertLink(link));

    const auto context = ast::parents::nodePath(*parent);
    for (const auto node : model.getNodesByContainer(parent)) {
      drawNode(node, context, model, gvw, options);
    }

    gvw.endContainer();
  }

  for (const auto& edge : model.getEdges()) {
    drawEdge(edge, model, gvw);
  }

  gvw.endGraph();
}


}
