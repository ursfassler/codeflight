/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>


namespace component::codeflight::artefact::overviewfactory
{

class Factory;

}

namespace component::codeflight::artefact::common::graph
{

class GraphModel;

}

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::ast::graph
{

class Graph;

}

namespace component::codeflight::artefact::interface
{

struct ArtefactIdentifier;
class GraphBuilder;

}

namespace component::codeflight::artefact::common::graph
{


class GraphArtefact
{
public:
  GraphArtefact(
      const overviewfactory::Factory&
      );

  void build(const ast::Node*, const ast::graph::Graph&, interface::GraphBuilder&) const;

private:
  const overviewfactory::Factory& overviewFactory;

  common::graph::GraphModel create(const ast::Node*, const ast::graph::Graph&) const;
};


}
