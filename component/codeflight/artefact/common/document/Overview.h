/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ArtefactList.h"

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::interface
{

class Factory;
class DocumentBuilder;

}

namespace component::codeflight::artefact::common::document
{


class Overview
{
public:
  void build(const ast::Node*, const FactoryList&, interface::DocumentBuilder&) const;

private:
  void printBody(const ast::Node*, const FactoryList&, interface::DocumentBuilder&) const;

};


}
