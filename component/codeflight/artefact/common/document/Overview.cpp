/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Overview.h"
#include "component/codeflight/artefact/interface/Artefact.h"
#include "component/codeflight/artefact/interface/DocumentBuilder.h"
#include "component/codeflight/artefact/interface/Factory.h"
#include "component/codeflight/ast/parents/nodePath.h"


namespace component::codeflight::artefact::common::document
{


void Overview::build(const ast::Node* node, const FactoryList& artefacts, interface::DocumentBuilder& xw) const
{
  xw.beginDocument(ast::parents::nodePath(*node), node);
  printBody(node, artefacts, xw);
  xw.endDocument();
}

void Overview::printBody(const ast::Node* node, const FactoryList& factories, interface::DocumentBuilder& xw) const
{
  xw.beginSection(ast::parents::nodePath(*node));

  xw.beginParagraph();
  printArtefactList(node, factories, xw);
  xw.endParagraph();

  xw.endSection();
}


}
