/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "metric.h"
#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"


namespace component::codeflight::artefact::common::document
{


void printMetricTable(const ast::Node& node, const metric::Metrics& metrics, interface::DocumentBuilder& xw)
{
  xw.beginTable({"metric", "value"}, interface::DocumentBuilder::TableType::StringNumbers);
  for (const auto& metric : metrics) {
    xw.beginTableRow();
    xw.tableCell(metric.name, {});
    xw.tableCell(metric.value(node), {});
    xw.endTableRow();
  }
  xw.endTable();
}


}
