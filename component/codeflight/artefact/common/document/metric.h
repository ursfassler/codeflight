/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/DocumentBuilder.h"
#include "component/codeflight/metric/Metrics.h"
#include <string>
#include <vector>


namespace component::codeflight::artefact::common::document
{


template<typename T>
interface::DocumentBuilder::Row metricHeader(const std::vector<std::string>& names, const std::vector<metric::GenericDescription<T>>& metrics)
{
  interface::DocumentBuilder::Row header = names;
  for (const auto& metric : metrics) {
    header.push_back(metric.name);
  }
  return header;
}


void printMetricTable(const ast::Node&, const metric::Metrics&, interface::DocumentBuilder&);


}
