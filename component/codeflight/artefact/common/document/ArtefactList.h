/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <vector>

namespace component::codeflight::artefact::interface
{

class NodeFactory;
class DocumentBuilder;

}

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::common::document
{


using FactoryList = std::vector<const interface::NodeFactory*>;

void printArtefactList(const ast::Node*, const FactoryList&, interface::DocumentBuilder&);


}
