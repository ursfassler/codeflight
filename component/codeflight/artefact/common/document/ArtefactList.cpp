/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ArtefactList.h"
#include "component/codeflight/artefact/interface/Artefact.h"
#include "component/codeflight/artefact/interface/DocumentBuilder.h"
#include "component/codeflight/artefact/interface/NodeFactory.h"

namespace component::codeflight::artefact::common::document
{


void printArtefactList(const ast::Node* node, const FactoryList& factories, interface::DocumentBuilder& xw)
{
  xw.beginUnnumberedList();

  for (auto& factory : factories) {
    xw.beginListItem();
    xw.link(factory->printName(), factory->identifier(node));
    xw.endListItem();
  }

  xw.endUnnumberedList();
}


}
