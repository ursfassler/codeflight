/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/DocumentArtefact.h"
#include "component/codeflight/artefact/interface/DocumentBuilder.h"
#include "component/codeflight/ast/NodeSpecification.h"
#include "component/codeflight/metric/Metrics.h"

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::interface
{

class Factory;

}

namespace component::codeflight::artefact::common::all_packageelement
{

class ArtefactFactory;


class Artefact :
    public interface::DocumentArtefact
{
  public:
    Artefact(
        const ast::Node*,
        const metric::Metrics&,
        const ast::NodeSpecification&,
        const ArtefactFactory&,
        const interface::Factory&
        );

    void build(interface::DocumentBuilder&) const override;

  private:
    const ast::Node* node;
    const metric::Metrics& metrics;
    const ast::NodeSpecification specification;
    const ArtefactFactory& artefactFactory;
    const interface::Factory& factory;

    void printBody(interface::DocumentBuilder &xw) const;

    using Link = std::pair<library::Path, interface::ArtefactIdentifier>;
    Link nodeOverview(const ast::Node*) const;
};


}
