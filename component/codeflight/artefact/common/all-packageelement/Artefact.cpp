/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "ArtefactFactory.h"
#include "component/codeflight/artefact/interface/MaybeFactory.h"
#include "component/codeflight/ast/parents/nodePath.h"
#include "component/codeflight/ast/specification/Package.h"
#include "component/codeflight/ast/traverse/traverse.h"
#include <cassert>


namespace component::codeflight::artefact::common::all_packageelement
{


Artefact::Artefact(
    const ast::Node* node_,
    const metric::Metrics& metrics_,
    const ast::NodeSpecification& specification_,
    const ArtefactFactory& artefactFactory_,
    const interface::Factory& factory_
    ) :
  node{node_},
  metrics{metrics_},
  specification{specification_},
  artefactFactory{artefactFactory_},
  factory{factory_}
{
}

void Artefact::build(interface::DocumentBuilder& xw) const
{
  xw.beginDocument(factory.printName(), {});
  xw.beginSection(factory.printName());
  printBody(xw);
  xw.endSection();
  xw.endDocument();
}

void Artefact::printBody(interface::DocumentBuilder& xw) const
{
  std::vector<std::string> head{"Name"};
  for (const auto& metric : metrics) {
    head.push_back(metric.name);
  }
  xw.beginTable(head, interface::DocumentBuilder::TableType::StringNumbers);

  ast::traverse::recursiveFor(*node, specification, [&xw, this](const ast::Node& variable){
    const auto overview = nodeOverview(&variable);

    xw.beginTableRow();

    xw.tableCell(overview.first, overview.second);

    for (const auto& metric : metrics) {
      const std::string value = metric.value(variable);
      xw.tableCell(value, {});
    }

    xw.endTableRow();
  });

  xw.endTable();
}

Artefact::Link Artefact::nodeOverview(const ast::Node* node) const
{
  const auto classPath = ast::parents::nodePath(*node);
  const auto ai = artefactFactory.getOverviewFactory().identifier(node);
  if (!ai) {
    throw std::runtime_error("expected oveview for node at " + classPath.to_string());
  }
  return {classPath, *ai};
}


}
