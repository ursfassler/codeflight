/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace component::codeflight::artefact::interface
{

class MaybeFactory;

}

namespace component::codeflight::artefact::common::all_packageelement
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual const interface::MaybeFactory& getOverviewFactory() const = 0;

};


}
