/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <cstdint>


namespace component::codeflight::artefact::common::package_cycles
{


struct Settings
{
  std::size_t maxNumberOfNodes{40};
  std::size_t maxNumberOfVertices{260};
};


}
