/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/ast/graph/cycle/types.h"
#include "Settings.h"
#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"
#include "component/codeflight/artefact/interface/DocumentBuilder.h"
#include "component/codeflight/metric/Metrics.h"
#include <map>
#include <vector>

namespace component::codeflight::ast
{

class Project;
class Package;

}

namespace component::codeflight::ast::graph
{

class Graph;

}

namespace component::codeflight::artefact::common::package_cycles
{

class ArtefactFactory;


struct PackageDependency
{
  const ast::Package* from;
  const ast::Package* to;
};

using PackageDependencyMetrics = std::vector<metric::GenericDescription<PackageDependency>>;


class Configuration
{
public:
  virtual PackageDependencyMetrics dependency() const = 0;
  virtual std::optional<metric::GenericDescription<PackageDependency>::Link> dependencyReferencesUrl(const ast::Project*) const = 0;
};


class Artefact
{
public:
  Artefact(
      const ArtefactFactory&,
      const Configuration&,
      const Settings&
      );

  void print(ast::graph::Graph, const ast::Node&, interface::DocumentBuilder&) const;

private:
  const ArtefactFactory& artefactFactory;
  const Configuration& configuration;
  const Settings settings;

  using NodesInCycles = std::map<const ast::Node*, std::size_t>;
  using DependenciesInCycles = std::map<const ast::Node*, std::map<const ast::Node*, std::size_t>>;
  using CycleReporter = std::function<void(const std::vector<const ast::Node*>&)>;
  using Packages = std::vector<const ast::Package*>;
  using Dependencies = std::vector<PackageDependency>;
  struct Component
  {
    Packages packages{};
    Dependencies dependencies{};
  };
  using ComponentLists = std::vector<Component>;
  struct CycleCount
  {
    std::size_t cycleCount{};
    NodesInCycles nodesInCycles{};
    DependenciesInCycles dependenciesInCycles{};
  };
  using CycleCountPerComponent = std::map<const Component*, CycleCount>;
  struct PackageData
  {
    std::size_t fanIn;
    std::size_t fanOut;
  };
  using PackagesData = std::map<const ast::Node*, PackageData>;
  using PackageMetrics = std::vector<metric::GenericDescription<ast::Package>>;
  using ReferenceCount = std::map<const ast::Node*, std::map<const ast::Node*, std::size_t>>;

  ReferenceCount countReferences(const ast::graph::Graph&) const;
  CycleCountPerComponent countCycles(const ComponentLists&) const;
  CycleCount countCycles(const Component&) const;
  std::optional<std::size_t> getTotalCycleCount(const ComponentLists&, const CycleCountPerComponent&) const;
  metric::Metrics getRootMetrics(const ComponentLists&, const ast::graph::Graph&) const;
  metric::Metrics getRootMetricsCycleCount(std::size_t) const;
  PackageMetrics getPackageMetrics(const PackagesData&) const;
  PackageMetrics getPackageMetricsCycleCount(const NodesInCycles&) const;
  PackageDependencyMetrics getDependencyMetrics(const ReferenceCount&, const ast::Project*) const;
  PackageDependencyMetrics getDependencyMetricsCycleCount(const DependenciesInCycles&) const;
  ComponentLists convertToComponents(const ast::graph::cycle::NodeSets&, const ast::graph::Graph&) const;
  PackagesData convertToPackagesData(const Component&, const ReferenceCount&) const;
  void printPackageList(const Packages&, const PackageMetrics&, const ast::Node& root, interface::DocumentBuilder&) const;
  void printDependencyList(const Dependencies&, const PackageDependencyMetrics&, const ast::Node& root, interface::DocumentBuilder&) const;
  interface::ArtefactIdentifier packageOverviewAi(const ast::Package*) const;
  library::Path packageName(const ast::Node& package, const ast::Node& root) const;
  int pathCompare(const ast::Node*, const ast::Node*) const;
  bool comparePackage(const ast::Node*, const ast::Node*) const;
  bool compareDependency(const PackageDependency&, const PackageDependency&) const;
  bool compareComponent(const Component&, const Component&) const;

};


}
