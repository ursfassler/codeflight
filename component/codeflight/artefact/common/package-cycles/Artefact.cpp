/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "ArtefactFactory.h"
#include "component/codeflight/artefact/common/document/metric.h"
#include "component/codeflight/artefact/interface/Artefact.h"
#include "component/codeflight/artefact/interface/NodeFactory.h"
#include "component/codeflight/ast/Package.h"
#include "component/codeflight/ast/Project.h"
#include "component/codeflight/ast/graph/cycle/johnson.h"
#include "component/codeflight/ast/graph/cycle/tarjan.h"
#include "component/codeflight/ast/graph/graph.h"
#include "component/codeflight/ast/parents/nodePath.h"
#include "component/codeflight/ast/parents/parents.h"
#include "component/codeflight/ast/query/ifType.h"
#include "component/codeflight/ast/specification/Project.h"
#include "component/codeflight/library/util.h"


namespace component::codeflight::artefact::common::package_cycles
{
namespace
{


template<typename T>
void insert(std::vector<T>& base, const std::vector<T>& toInsert)
{
  base.insert(base.end(), toInsert.begin(), toInsert.end());
}


}


Artefact::Artefact(
    const ArtefactFactory& artefactFactory_,
    const Configuration& configuration_,
    const Settings& settings_
    ) :
  artefactFactory{artefactFactory_},
  configuration{configuration_},
  settings{settings_}
{
}


void Artefact::print(ast::graph::Graph graph, const ast::Node& root, interface::DocumentBuilder& xw) const
{
  graph.removeEdgeToItself();
  const ReferenceCount references = countReferences(graph);
  graph.removeDuplicatedEdges();

  const auto cycles = ast::graph::cycle::tarjan(graph);

  const auto components = convertToComponents(cycles, graph);
  const CycleCountPerComponent countPerComponent = countCycles(components);
  const auto totalCycles = getTotalCycleCount(components, countPerComponent);

  metric::Metrics rootMetrics{};
  insert(rootMetrics, getRootMetrics(components, graph));
  if (totalCycles) {
    insert(rootMetrics, getRootMetricsCycleCount(*totalCycles));
  }
  common::document::printMetricTable(root, rootMetrics, xw);

  auto project = ast::parents::getProject(&root);

  std::size_t clusterNumber = 0;
  for (const auto& component : components) {
    PackageDependencyMetrics dependencyMetrics{};
    insert(dependencyMetrics, getDependencyMetrics(references, project));
    insert(dependencyMetrics, configuration.dependency());
    const auto packageCount = convertToPackagesData(component, references);
    PackageMetrics packageMetrics{};
    insert(packageMetrics, getPackageMetrics(packageCount));

    const auto countIdx = countPerComponent.find(&component);
    if (countIdx != countPerComponent.end()) {
      const auto& count = countIdx->second;
      insert(dependencyMetrics, getDependencyMetricsCycleCount(count.dependenciesInCycles));
      insert(packageMetrics, getPackageMetricsCycleCount(count.nodesInCycles));
    }

    clusterNumber++;
    xw.beginSection("Cycle cluster " + std::to_string(clusterNumber));

    printPackageList(component.packages, packageMetrics, root, xw);
    printDependencyList(component.dependencies, dependencyMetrics, root, xw);

    xw.endSection();
  }
}

Artefact::CycleCountPerComponent Artefact::countCycles(const ComponentLists& components) const
{
  CycleCountPerComponent result{};

  for (const auto& component : components) {
    if (component.packages.size() > settings.maxNumberOfNodes) {
      continue;
    }
    if (component.dependencies.size() > settings.maxNumberOfVertices) {
      continue;
    }

    CycleCount count = countCycles(component);
    result[&component] = count;
  }

  return result;
}

Artefact::CycleCount Artefact::countCycles(const Component& component) const
{
  CycleCount count{};

  const auto reporter = [&count](const ast::graph::cycle::NodeList& cycle){
    count.cycleCount++;

    for (const auto node : cycle) {
      count.nodesInCycles[node]++;
    }

    for (std::size_t i = 0; i < cycle.size(); i++) {
      const auto from = cycle.at(i);
      const auto to = cycle.at((i+1) % cycle.size());
      count.dependenciesInCycles[from][to]++;
    }
  };

  ast::graph::Graph cluster{};
  for (const auto node : component.packages) {
    cluster.add(node);
  }
  for (const auto& dependency : component.dependencies) {
    cluster.add(dependency.from, dependency.to);
  }

  ast::graph::cycle::johnson(cluster, reporter);

  return count;
}

std::optional<std::size_t> Artefact::getTotalCycleCount(const ComponentLists& components, const CycleCountPerComponent& countPerComponent) const
{
  std::size_t result = 0;

  for (const auto& component : components) {
    const auto idx = countPerComponent.find(&component);
    if (idx == countPerComponent.end()) {
      return {};
    }
    result += idx->second.cycleCount;
  }

  return {result};
}

metric::Metrics Artefact::getRootMetrics(const ComponentLists& components, const ast::graph::Graph& graph) const
{
  const bool hasCycles = !components.empty();
  std::size_t cycleDependencies = 0;
  std::size_t cyclePackages = 0;
  for (const auto& component : components) {
    cycleDependencies += component.dependencies.size();
    cyclePackages += component.packages.size();
  }
  const std::size_t totalPackages = graph.numberOfNodes();
  const double packageRatio = (totalPackages == 0) ? 0.0 : double(cyclePackages) / totalPackages;
  const auto totalDependencies = graph.countEdges();
  const double dependencyRatio = (totalDependencies == 0) ? 0.0 : double(cycleDependencies) / totalDependencies;

  return {
    {"Has cycles", [hasCycles](const ast::Node&){
       return library::toString(hasCycles);
    }},
    {"Number of involved packages", [cyclePackages](const ast::Node&){
       return std::to_string(cyclePackages);
    }},
    {"Total number of packages", [totalPackages](const ast::Node&){
       return std::to_string(totalPackages);
    }},
    {"Packages in cycles ratio", [packageRatio](const ast::Node&){
       return library::toString(packageRatio, 3);
    }},
    {"Number of involved dependencies", [cycleDependencies](const ast::Node&){
       return std::to_string(cycleDependencies);
    }},
    {"Total number of dependencies", [totalDependencies](const ast::Node&){
       return std::to_string(totalDependencies);
    }},
    {"Dependencies in cycles ratio", [dependencyRatio](const ast::Node&){
       return library::toString(dependencyRatio, 3);
    }},
  };
}

metric::Metrics Artefact::getRootMetricsCycleCount(std::size_t cycleCount) const
{
  return {
    {"Number of cycles", [cycleCount](const ast::Node&){
       return std::to_string(cycleCount);
    }},
  };
}

Artefact::PackageMetrics Artefact::getPackageMetrics(const PackagesData& metrics) const
{
  return {
    {
      "cycle fan-in",
      [&metrics](const ast::Package& package){
        return std::to_string(metrics.at(&package).fanIn);
      },
    },
    {
      "cycle fan-out",
      [&metrics](const ast::Package& node){
        return std::to_string(metrics.at(&node).fanOut);
      },
    },
  };
}

Artefact::PackageMetrics Artefact::getPackageMetricsCycleCount(const NodesInCycles& nodesInCycles) const
{
  return {
    {
      "in cycles",
      [&nodesInCycles](const ast::Package& package){
        return std::to_string(nodesInCycles.at(&package));
      },
    },
  };
}

PackageDependencyMetrics Artefact::getDependencyMetrics(const ReferenceCount& references, const ast::Project* project) const
{
  return {
    {
      "references", [&references](const PackageDependency& dependency){
        return std::to_string(references.at(dependency.from).at(dependency.to));
      },
      configuration.dependencyReferencesUrl(project),
    },
  };
}


PackageDependencyMetrics Artefact::getDependencyMetricsCycleCount(const DependenciesInCycles& dependenciesInCycles) const
{
  return {
    {
      "in cycles", [&dependenciesInCycles](const PackageDependency& dependency){
        return std::to_string(dependenciesInCycles.at(dependency.from).at(dependency.to));
      },
    },
  };
}

Artefact::ComponentLists Artefact::convertToComponents(const ast::graph::cycle::NodeSets& cycles, const ast::graph::Graph& graph) const
{
  ComponentLists components{};

  for (const auto& nodes : cycles) {
    Packages packages{};
    Dependencies dependencies{};

    std::map<const ast::Node*, const ast::Package*> packageFromNode{};
    for (const auto node : nodes) {
      ast::query::ifType<ast::Package>(*node, [&packages, node, &packageFromNode](const ast::Package& package){
        packages.push_back(&package);
        packageFromNode[node] = &package;
      }, [](const ast::Node& node){
        throw std::invalid_argument("expected type package for node " + node.name);
      });
    }
    for (const auto from : nodes) {
      const auto fromPackage = packageFromNode.at(from);
      graph.foreach(from, [&nodes, &fromPackage, &dependencies, &packageFromNode](const ast::Node* to){
        if (nodes.contains(to)) {
          const auto toPackage = packageFromNode.at(to);
          dependencies.push_back({fromPackage, toPackage});
        }
      });
    }

    std::sort(packages.begin(), packages.end(), std::bind(&Artefact::comparePackage, this, std::placeholders::_1, std::placeholders::_2));
    std::sort(dependencies.begin(), dependencies.end(), std::bind(&Artefact::compareDependency, this, std::placeholders::_1, std::placeholders::_2));

    components.push_back({packages, dependencies});
  }

  std::sort(components.begin(), components.end(), std::bind(&Artefact::compareComponent, this, std::placeholders::_1, std::placeholders::_2));

  return components;
}

Artefact::ReferenceCount Artefact::countReferences(const ast::graph::Graph& graph) const
{
  ReferenceCount references{};

  graph.foreach([&references](const ast::Node* source, const ast::Node* destination){
    references[source][destination]++;
  });

  return references;
}

Artefact::PackagesData Artefact::convertToPackagesData(const Component& component, const ReferenceCount& references) const
{
  PackagesData metrics{};

  for (const auto& dependency: component.dependencies) {
    const auto count = references.at(dependency.from).at(dependency.to);
    metrics[dependency.to].fanIn += count;
    metrics[dependency.from].fanOut += count;
  }

  return metrics;
}

void Artefact::printPackageList(const Packages& packages, const PackageMetrics& metrics, const ast::Node& root, interface::DocumentBuilder& xw) const
{
  xw.beginSection("Involved packages");

  const auto header = common::document::metricHeader({"Package"}, metrics);
  xw.beginTable(header, interface::DocumentBuilder::TableType::StringNumbers);

  for (const auto& package : packages) {
    const interface::ArtefactIdentifier url = packageOverviewAi(package);
    const library::Path name = packageName(*package, root);

    xw.beginTableRow();
    xw.tableCell(name, url);
    for (const auto& metric : metrics) {
      const auto value = metric.value(*package);
      if (metric.link) {
        const auto link = (*metric.link)(*package);
        xw.tableCell(value, link);
      } else {
        xw.tableCell(value, {});
      }
    }
    xw.endTableRow();
  }

  xw.endTable();
  xw.endSection();
}

void Artefact::printDependencyList(const Dependencies& dependencies, const PackageDependencyMetrics& metrics, const ast::Node& root, interface::DocumentBuilder& xw) const
{
  xw.beginSection("Involved dependencies");

  const auto header = common::document::metricHeader({"from", "to"}, metrics);
  xw.beginTable(header, interface::DocumentBuilder::TableType::StringStringNumbers);

  for (const auto& dependency: dependencies) {
    const interface::ArtefactIdentifier fromAi = packageOverviewAi(dependency.from);
    const interface::ArtefactIdentifier toAi = packageOverviewAi(dependency.to);
    const library::Path from = packageName(*dependency.from, root);
    const library::Path to = packageName(*dependency.to, root);

    xw.beginTableRow();
    xw.tableCell(from, fromAi);
    xw.tableCell(to, toAi);
    for (const auto& metric : metrics) {
      const auto value = metric.value(dependency);
      if (metric.link) {
        const auto link = (*metric.link)(dependency);
        xw.tableCell(value, link);
      } else {
        xw.tableCell(value, {});
      }
    }
    xw.endTableRow();
  }

  xw.endTable();
  xw.endSection();
}

interface::ArtefactIdentifier Artefact::packageOverviewAi(const ast::Package* package) const
{
  auto ai = artefactFactory.getPackageOverviewFactory().identifier(package);
  return ai;
}

library::Path Artefact::packageName(const ast::Node& package, const ast::Node& root) const
{
  const auto packagePath = ast::parents::nodePath(package);
  const auto isProject = ast::specification::Project()(&root);
  const auto path = isProject ? packagePath : packagePath.subPathIn(ast::parents::nodePath(root));
  return path;
}

int Artefact::pathCompare(const ast::Node* left, const ast::Node* right) const
{
  const auto leftPath = ast::parents::nodePath(*left);
  const auto rightPath = ast::parents::nodePath(*right);
  return leftPath.compare(rightPath);
}

bool Artefact::comparePackage(const ast::Node* left, const ast::Node* right) const
{
  return pathCompare(left, right) < 0;
}

bool Artefact::compareDependency(const PackageDependency& left, const PackageDependency& right) const
{
  const auto first = pathCompare(left.from, right.from);
  if (first != 0) {
    return first < 0;
  }

  return comparePackage(left.to, right.to);
}

bool Artefact::compareComponent(const Component& left, const Component& right) const
{
  return comparePackage(left.packages.at(0), right.packages.at(0));
}


}
