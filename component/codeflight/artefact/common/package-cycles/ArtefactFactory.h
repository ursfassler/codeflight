/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace component::codeflight::artefact::interface
{

class NodeFactory;

}

namespace component::codeflight::artefact::common::package_cycles
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual const interface::NodeFactory& getPackageOverviewFactory() const = 0;

};


}
