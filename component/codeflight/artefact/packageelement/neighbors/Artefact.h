/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/GraphArtefact.h"
#include "component/codeflight/library/OrderedSet.h"
#include <string>


namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::ast::repository
{

class Neighbor;

}

namespace component::codeflight::artefact::common::graph
{

class GraphModel;

}

namespace component::codeflight::artefact::packageelement::neighbors
{

class ArtefactFactory;


class Artefact :
    public interface::GraphArtefact
{
  public:
    Artefact(
        const ast::Node*,
        const ArtefactFactory&,
        const ast::repository::Neighbor&
        );

    void build(interface::GraphBuilder&) const override;

  private:
    const ast::Node* element;
    const ArtefactFactory& artefactFactory;
    const ast::repository::Neighbor& neighbors;

    common::graph::GraphModel create() const;
    void setNodeUrl(const ast::Node*, common::graph::GraphModel&) const;
    void setEdgeUrl(const ast::Node*, const ast::Node*, common::graph::GraphModel&) const;
    const ast::Node* getPackageOrElement(const ast::Node*) const;
};


}
