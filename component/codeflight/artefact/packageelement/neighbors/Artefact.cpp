/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "ArtefactFactory.h"
#include "component/codeflight/artefact/common/graph/GraphModel.h"
#include "component/codeflight/artefact/common/graph/draw.h"
#include "component/codeflight/artefact/interface/ListFactory.h"
#include "component/codeflight/artefact/interface/MaybeFactory.h"
#include "component/codeflight/ast/Node.h"
#include "component/codeflight/ast/parents/nodePath.h"
#include "component/codeflight/ast/parents/parents.h"
#include "component/codeflight/ast/repository/Neighbor.h"
#include "component/codeflight/ast/specification/Package.h"

namespace component::codeflight::artefact::packageelement::neighbors
{


Artefact::Artefact(
    const ast::Node* element_,
    const ArtefactFactory& artefactFactory_,
    const ast::repository::Neighbor& neighbors_
    ) :
  element{element_},
  artefactFactory{artefactFactory_},
  neighbors{neighbors_}
{
}

void Artefact::build(interface::GraphBuilder& builder) const
{
  auto graph = create();
  common::graph::draw(graph, builder, {});
}

common::graph::GraphModel Artefact::create() const
{
  common::graph::GraphModel graph{{}};

  const auto addWithContainer = [&graph, this](const ast::Node* node){
    const auto parent = ast::parents::findAncestor(node, ast::specification::Package());
    if (!parent) {
      throw std::runtime_error("element without package: " + node->name);
    }
    graph.addContainer(*parent);
    setNodeUrl(*parent, graph);

    graph.addChildNode(node, *parent);
    setNodeUrl(node, graph);
  };

  addWithContainer(element);
  neighbors.foreachOutgoingPackageElement(element, [&graph, &addWithContainer, this](const ast::Node* node){
    if (node != element) {
      addWithContainer(node);
      graph.addEdge(element, node);
      setEdgeUrl(element, node, graph);
    }
  });
  neighbors.foreachIncomingPackageElement(element, [&graph, &addWithContainer, this](const ast::Node* node){
    if (node != element) {
      addWithContainer(node);
      graph.addEdge(node, element);
      setEdgeUrl(node, element, graph);
    }
  });

  graph.setEmphasized(element);

  return graph;
}

void Artefact::setNodeUrl(const ast::Node* node, common::graph::GraphModel& graph) const
{
  auto ai = artefactFactory.getOverviewFactory().identifier(node);
  if (ai) {
    graph.setLink(node, *ai);
  }
}

void Artefact::setEdgeUrl(const ast::Node* source, const ast::Node* destination, common::graph::GraphModel& graph) const
{
  const auto sourceEp = getPackageOrElement(source);
  const auto destinationEp = getPackageOrElement(destination);

  if (sourceEp == destinationEp) {
    return;
  }

  auto project = ast::parents::getProject(element);
  auto sourcePath = ast::parents::nodePath(*sourceEp);
  auto destinationPath = ast::parents::nodePath(*destinationEp);
  const auto ai = artefactFactory.getDependenciesBetweenPackagesFactory().identifier(project, {sourcePath, destinationPath});
  graph.setLink({source, destination}, ai);
}

const ast::Node* Artefact::getPackageOrElement(const ast::Node* node) const
{
  const auto package = ast::parents::findAncestor(node, ast::specification::Package());
  return package ? *package : node;
}


}
