/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Factory.h"
#include "component/codeflight/library/Path.h"
#include <memory>
#include <string>
#include <vector>

namespace component::codeflight::ast
{

class Project;

}

namespace component::codeflight::artefact::interface
{

class Artefact;
struct ArtefactIdentifier;


class ListFactory :
    public Factory
{
public:
  ~ListFactory() override = default;

  virtual std::unique_ptr<Artefact> produce(const ast::Project*, const library::Paths&) const = 0;
  virtual ArtefactIdentifier identifier(const ast::Project*, const library::Paths&) const = 0;

};


}
