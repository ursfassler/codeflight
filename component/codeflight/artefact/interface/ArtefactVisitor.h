/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace component::codeflight::artefact::interface
{

class GraphArtefact;
class DocumentArtefact;


class ArtefactVisitor
{
public:
  virtual void visit(const GraphArtefact&) = 0;
  virtual void visit(const DocumentArtefact&) = 0;
};


}
