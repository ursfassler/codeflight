/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace component::codeflight::artefact::interface
{


class Factory
{
public:
  virtual ~Factory() = default;

  virtual std::string printName() const = 0;
  virtual std::string artefactName() const = 0;

};


}
