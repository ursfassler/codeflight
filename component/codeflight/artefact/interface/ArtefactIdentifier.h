/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/library/Path.h"


namespace component::codeflight::artefact::interface
{


struct ArtefactIdentifier
{
  library::Path path;
  std::string artefact;
  library::Paths arguments{};
};

bool operator<(const ArtefactIdentifier&, const ArtefactIdentifier&);


}
