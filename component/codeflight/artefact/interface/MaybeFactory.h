/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Factory.h"
#include "ArtefactIdentifier.h"
#include <memory>
#include <optional>

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::interface
{

class Artefact;


class MaybeFactory :
    public Factory
{
public:
  ~MaybeFactory() override = default;

  virtual std::unique_ptr<Artefact> produce(const ast::Node*) const = 0;
  virtual std::optional<ArtefactIdentifier> identifier(const ast::Node*) const = 0;

};


}
