/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Artefact.h"
#include "ArtefactVisitor.h"


namespace component::codeflight::artefact::interface
{

class GraphBuilder;


class GraphArtefact :
    public Artefact
{
public:
  virtual void build(GraphBuilder&) const = 0;

  void accept(ArtefactVisitor& visitor) const override
  {
    visitor.visit(*this);
  }
};


}
