/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace component::codeflight::artefact::interface
{

class ArtefactVisitor;


class Artefact
{
public:
  virtual ~Artefact() = default;

  virtual void accept(ArtefactVisitor&) const = 0;

};


}
