/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ArtefactIdentifier.h"


namespace component::codeflight::artefact::interface
{


bool operator<(const ArtefactIdentifier& lhs, const ArtefactIdentifier& rhs)
{
  if (lhs.path != rhs.path) {
    return lhs.path < rhs.path;
  }

  if (lhs.artefact != rhs.artefact) {
    return lhs.artefact < rhs.artefact;
  }

  if (lhs.arguments != rhs.arguments) {
    return lhs.arguments < rhs.arguments;
  }

  return false;
}


}
