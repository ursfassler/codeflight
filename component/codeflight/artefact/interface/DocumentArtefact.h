/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Artefact.h"
#include "ArtefactVisitor.h"


namespace component::codeflight::artefact::interface
{

class DocumentBuilder;


class DocumentArtefact :
    public Artefact
{
public:
  virtual void build(DocumentBuilder&) const = 0;

  void accept(ArtefactVisitor& visitor) const override
  {
    visitor.visit(*this);
  }
};


}
