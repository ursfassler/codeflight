/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ArtefactIdentifier.h"
#include <optional>
#include <string>
#include<vector>


namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::interface
{


class DocumentBuilder
{
public:
  virtual ~DocumentBuilder() = default;

  virtual void beginDocument(const library::Path&, const ast::Node*) = 0;
  virtual void beginDocument(const std::string&, const std::optional<const ast::Node*>&) = 0;
  virtual void endDocument() = 0;

  virtual void beginSection(const library::Path&) = 0;
  virtual void beginSection(const std::string&) = 0;
  virtual void endSection() = 0;

  virtual void beginParagraph() = 0;
  virtual void endParagraph() = 0;

  virtual void beginUnnumberedList() = 0;
  virtual void endUnnumberedList() = 0;

  virtual void beginListItem() = 0;
  virtual void endListItem() = 0;

  virtual void link(const std::string&, const interface::ArtefactIdentifier&) = 0;

  enum class TableType {
    StringNumbers,
    StringStringNumbers,
  };
  using Row = std::vector<std::string>;

  virtual void beginTable(const Row& header, TableType) = 0;
  virtual void endTable() = 0;

  virtual void beginTableRow() = 0;
  virtual void endTableRow() = 0;
  virtual void tableCell(const library::Path&, const interface::ArtefactIdentifier&) = 0;
  virtual void tableCell(const std::string&, const std::optional<interface::ArtefactIdentifier>&) = 0;

};


}
