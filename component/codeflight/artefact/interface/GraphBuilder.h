/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ArtefactIdentifier.h"
#include <optional>
#include <string>


namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::library
{

class Path;

}


namespace component::codeflight::artefact::interface
{


class GraphBuilder
{
public:
  using Link = ArtefactIdentifier;


  virtual ~GraphBuilder() = default;

  virtual void beginGraph(const std::optional<library::Path>& name) = 0;
  virtual void endGraph() = 0;

  virtual void beginContainer(const library::Path&, const std::optional<Link>&) = 0;
  virtual void endContainer() = 0;

  virtual void node(const ast::Node*, const library::Path&, bool emphasize, const std::optional<Link>&) = 0;
  virtual void edge(const ast::Node* from, const ast::Node* to, const std::optional<std::string>& label, const std::optional<Link>&) = 0;

};


}
