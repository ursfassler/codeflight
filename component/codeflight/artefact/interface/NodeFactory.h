/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Factory.h"
#include "Artefact.h"
#include "ArtefactIdentifier.h"
#include <memory>

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::interface
{


class NodeFactory :
    public Factory
{
public:
  ~NodeFactory() override = default;

  virtual std::unique_ptr<Artefact> produce(const ast::Node*) const = 0;
  virtual ArtefactIdentifier identifier(const ast::Node*) const = 0;
};


}
