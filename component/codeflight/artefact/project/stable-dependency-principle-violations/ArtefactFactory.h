/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace component::codeflight::artefact::interface
{

class ListFactory;
class NodeFactory;

}

namespace component::codeflight::artefact::project::stable_dependency_principle_violations
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual const interface::ListFactory& getDependenciesBetweenPackagesFactory() const = 0;
  virtual const interface::NodeFactory& getPackageOverviewFactory() const = 0;

};


}
