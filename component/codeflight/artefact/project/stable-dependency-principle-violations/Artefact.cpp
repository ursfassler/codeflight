/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "ArtefactFactory.h"
#include "component/codeflight/artefact/interface/DocumentBuilder.h"
#include "component/codeflight/artefact/interface/ListFactory.h"
#include "component/codeflight/artefact/interface/NodeFactory.h"
#include "component/codeflight/ast/Package.h"
#include "component/codeflight/ast/Project.h"
#include "component/codeflight/ast/graph/cycle/johnson.h"
#include "component/codeflight/ast/graph/graph.h"
#include "component/codeflight/ast/parents/nodePath.h"
#include "component/codeflight/ast/query/ifType.h"
#include "component/codeflight/ast/repository/Neighbor.h"
#include "component/codeflight/ast/traverse/type.h"
#include "component/codeflight/library/OrderedSet.h"
#include "component/codeflight/library/Set.h"
#include "component/codeflight/library/util.h"


namespace component::codeflight::artefact::project::stable_dependency_principle_violations
{
namespace
{


void row(const std::string& name, const std::string& value, interface::DocumentBuilder& xw)
{
  xw.beginTableRow();
  xw.tableCell(name, {});
  xw.tableCell(value, {});
  xw.endTableRow();
}


}


Artefact::Artefact(
    const ast::Project* root_,
    const ArtefactFactory& artefactFactory_,
    const InstabilityCalculator& instability_,
    const StabilityCalculator& stability_,
    const ast::repository::Neighbor& neighbors_
    ) :
  root{root_},
  artefactFactory{artefactFactory_},
  instability{instability_},
  stability{stability_},
  neighbors{neighbors_}
{
}

void Artefact::build(interface::DocumentBuilder& xw) const
{
  std::vector<Artefact::DependencyInfo> violations{};
  std::map<const ast::Node*, library::Set<const ast::Node*>> visited{};

  ast::traverse::recursive<ast::Package>(*root, [&visited, &violations, this](const ast::Package& source){
    neighbors.foreachOutgoingPackage(&source, [&visited, &violations, &source, this](const ast::Node* destination){
      if (visited[&source].contains(destination)) {
        return;
      }
      visited[&source].add(destination);

      const auto metric = stability({&source, destination});
      if (metric < 0) {
        ast::query::ifType<ast::Package>(*destination, [&violations, &source, &metric](const ast::Package& destination){
          violations.push_back(DependencyInfo{&source, &destination, metric});
        }, [](const ast::Node& node){
          throw std::invalid_argument("expected type package for node " + node.name);
        });
      }
    });
  });

  std::map<const ast::Package*, std::size_t> inViolations{};
  library::OrderedSet<const ast::Package*> packageSet{};

  for (const auto& dependency: violations) {
    packageSet.add(dependency.from);
    inViolations[dependency.from]++;
    packageSet.add(dependency.to);
    inViolations[dependency.to]++;
  }

  Packages packages{};
  for (const auto& package: packageSet) {
    packages.push_back({package, inViolations[package]});
  }

  std::size_t numberOfDependencies = 0;
  std::for_each(visited.begin(), visited.end(), [&numberOfDependencies](const auto& set){
    numberOfDependencies += set.second.size();
  });

  const std::string title = "Stable dependency principle violations";


  xw.beginDocument(title, {});
  xw.beginSection(title);

  printMetric(violations.size(), numberOfDependencies, packages.size(), xw);
  if (!violations.empty()) {
    printDependencyList(violations, xw);
    printPackages(packages, xw);
  }

  xw.endSection();
  xw.endDocument();
}

void Artefact::printMetric(std::size_t violations, std::size_t dependencies, std::size_t packages, interface::DocumentBuilder& xw) const
{
  const double ratio = (dependencies == 0) ? 0.0 : double(violations) / dependencies;

  xw.beginTable({"metric", "value"}, interface::DocumentBuilder::TableType::StringNumbers);
  row("Number of violations", std::to_string(violations), xw);
  row("Number of dependencies", std::to_string(dependencies), xw);
  row("Violation ratio", library::toString(ratio, 3), xw);
  row("Number of involved packages", std::to_string(packages), xw);
  xw.endTable();
}

void Artefact::printDependencyList(const std::vector<Artefact::DependencyInfo>& violations, interface::DocumentBuilder& xw) const
{
  xw.beginSection("Dependencies");

  xw.beginTable({"from", "to", "stability"}, interface::DocumentBuilder::TableType::StringStringNumbers);

  for (const auto& dependency: violations) {
    auto sourcePath = ast::parents::nodePath(*dependency.from);
    auto destinationPath = ast::parents::nodePath(*dependency.to);
    const auto link = artefactFactory.getDependenciesBetweenPackagesFactory().identifier(root, {sourcePath, destinationPath});
    const auto from = ast::parents::nodePath(*dependency.from);
    const auto to = ast::parents::nodePath(*dependency.to);
    const auto stability = library::toString(dependency.stability, 3);

    xw.beginTableRow();
    xw.tableCell(from, link);
    xw.tableCell(to, link);
    xw.tableCell(stability, {});
    xw.endTableRow();
  }

  xw.endTable();
  xw.endSection();
}

void Artefact::printPackages(const Packages& packages, interface::DocumentBuilder& xw) const
{
  xw.beginSection("Packages");

  xw.beginTable({"Package", "in violations", "instability"}, interface::DocumentBuilder::TableType::StringNumbers);

  for (const auto& package: packages) {
    const auto link = artefactFactory.getPackageOverviewFactory().identifier(package.first);
    const auto name = ast::parents::nodePath(*package.first);
    const auto inViolations = std::to_string(package.second);
    const auto instabilityStr = library::toString(instability(package.first), 3);

    xw.beginTableRow();
    xw.tableCell(name, link);
    xw.tableCell(inViolations, {});
    xw.tableCell(instabilityStr, {});
    xw.endTableRow();
  }

  xw.endTable();
  xw.endSection();
}


}
