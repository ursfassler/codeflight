/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/NodeFactory.h"
#include "Calculator.h"

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::ast::repository
{

class Neighbor;

}

namespace component::codeflight::artefact::project::stable_dependency_principle_violations
{

class ArtefactFactory;


class Factory :
    public interface::NodeFactory
{
public:
  Factory(
      const ArtefactFactory&,
      const InstabilityCalculator&,
      const StabilityCalculator&,
      const ast::repository::Neighbor&
      );

  std::string printName() const override;
  std::string artefactName() const override;
  std::unique_ptr<interface::Artefact> produce(const ast::Node*) const override;
  interface::ArtefactIdentifier identifier(const ast::Node*) const override;

private:
  const ArtefactFactory& artefactFactory;
  const InstabilityCalculator instability;
  const StabilityCalculator stability;
  const ast::repository::Neighbor& neighbors;
};


}

