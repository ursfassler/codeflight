/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/ast/Dependency.h"
#include <functional>


namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::project::stable_dependency_principle_violations
{


using InstabilityCalculator = std::function<double(const ast::Node*)>;
using StabilityCalculator = std::function<double(const ast::Dependency&)>;


}
