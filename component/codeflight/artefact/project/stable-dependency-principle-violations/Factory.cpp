/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "Artefact.h"
#include "component/codeflight/ast/parents/parents.h"

namespace component::codeflight::artefact::project::stable_dependency_principle_violations
{


Factory::Factory(
    const ArtefactFactory& artefactFactory_,
    const InstabilityCalculator& instability_,
    const StabilityCalculator& stability_,
    const ast::repository::Neighbor& neighbors_
    ) :
  artefactFactory{artefactFactory_},
  instability{instability_},
  stability{stability_},
  neighbors{neighbors_}
{
}

std::string Factory::printName() const
{
  return "Stable dependency principle violations";
}

std::string Factory::artefactName() const
{
  return "stable-dependency-principle-violations";
}

std::unique_ptr<interface::Artefact> Factory::produce(const ast::Node* node) const
{
  auto project = ast::parents::getProject(node);
  return std::make_unique<Artefact>(project, artefactFactory, instability, stability, neighbors);
}

interface::ArtefactIdentifier Factory::identifier(const ast::Node*) const
{
  return {{}, artefactName()};
}


}

