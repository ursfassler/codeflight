/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/DocumentArtefact.h"
#include "Calculator.h"
#include "component/codeflight/ast/graph/cycle/types.h"
#include "component/codeflight/library/Path.h"
#include <functional>
#include <set>
#include <string>


namespace component::codeflight::ast
{

class Project;
class Package;
class Node;

}

namespace component::codeflight::ast::graph
{

class Graph;

}

namespace component::codeflight::ast::repository
{

class Neighbor;

}

namespace component::codeflight::artefact::project::stable_dependency_principle_violations
{

class ArtefactFactory;


class Artefact :
    public interface::DocumentArtefact
{
  public:
    Artefact(
        const ast::Project*,
        const ArtefactFactory&,
        const InstabilityCalculator&,
        const StabilityCalculator&,
        const ast::repository::Neighbor&
        );

    void build(interface::DocumentBuilder&) const override;

  private:
    const ast::Project* root;
    const ArtefactFactory& artefactFactory;
    const InstabilityCalculator instability;
    const StabilityCalculator stability;
    const ast::repository::Neighbor& neighbors;

    struct DependencyInfo
    {
        const ast::Package* from;
        const ast::Package* to;
        double stability;
    };
    using Packages = std::vector<std::pair<const ast::Package*, std::size_t>>;

    void printMetric(std::size_t violations, std::size_t dependencies, std::size_t packages, interface::DocumentBuilder&) const;
    void printDependencyList(const std::vector<Artefact::DependencyInfo>&, interface::DocumentBuilder&) const;
    void printPackages(const Packages&, interface::DocumentBuilder&) const;

};


}
