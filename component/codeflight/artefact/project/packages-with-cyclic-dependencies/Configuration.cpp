/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Configuration.h"
#include "ArtefactFactory.h"
#include "component/codeflight/artefact/interface/ListFactory.h"
#include "component/codeflight/ast/Package.h"
#include "component/codeflight/ast/parents/nodePath.h"
#include "component/codeflight/library/util.h"


namespace component::codeflight::artefact::project::packages_with_cyclic_dependencies
{


Configuration::Configuration(
    const ArtefactFactory& artefactFactory_,
    const Stability& stabilityFunc_
    ) :
  artefactFactory{artefactFactory_},
  stabilityFunc{stabilityFunc_}
{
}

common::package_cycles::PackageDependencyMetrics Configuration::dependency() const
{
  return {
    {
      "stability", [this](const common::package_cycles::PackageDependency& dependency){
        return library::toString(stabilityFunc(dependency), 3);
      },
    },
  };
}

std::optional<metric::GenericDescription<common::package_cycles::PackageDependency>::Link> Configuration::dependencyReferencesUrl(const ast::Project* project) const
{
  return [project, this](const common::package_cycles::PackageDependency& dependency){
    auto sourcePath = ast::parents::nodePath(*dependency.from);
    auto destinationPath = ast::parents::nodePath(*dependency.to);
    interface::ArtefactIdentifier ai = artefactFactory.getDependenciesBetweenPackagesFactory().identifier(project, {sourcePath, destinationPath});
    return ai;
  };
}


}
