/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "Artefact.h"
#include "component/codeflight/ast/parents/parents.h"

namespace component::codeflight::artefact::project::packages_with_cyclic_dependencies
{


Factory::Factory(
    const ArtefactFactory& artefactFactory_,
    const common::package_cycles::ArtefactFactory& cyclesArtefactFactory,
    const Stability& stabilityFunc_,
    const common::package_cycles::Settings& limits_
    ) :
  configuration{
    artefactFactory_,
    stabilityFunc_
  },
  cycleArtefact{
    cyclesArtefactFactory,
    configuration,
    limits_
  }
{
}

std::string Factory::printName() const
{
  return "Packages with cylic dependencies";
}

std::string Factory::artefactName() const
{
  return "packages-with-cyclic-dependencies";
}

std::unique_ptr<interface::Artefact> Factory::produce(const ast::Node* node) const
{
  auto project = ast::parents::getProject(node);
  return std::make_unique<Artefact>(project, cycleArtefact);
}

interface::ArtefactIdentifier Factory::identifier(const ast::Node*) const
{
  return {{}, artefactName()};
}

}

