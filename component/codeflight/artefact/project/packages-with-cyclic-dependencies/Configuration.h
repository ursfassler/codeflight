/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/Artefact.h"
#include "Stability.h"
#include <functional>
#include <optional>

namespace component::codeflight::artefact::project::packages_with_cyclic_dependencies
{

class ArtefactFactory;


class Configuration :
    public common::package_cycles::Configuration
{
public:
  Configuration(
      const ArtefactFactory&,
      const Stability&
      );

  common::package_cycles::PackageDependencyMetrics dependency() const override;
  std::optional<metric::GenericDescription<common::package_cycles::PackageDependency>::Link> dependencyReferencesUrl(const ast::Project*) const override;

private:
  const ArtefactFactory& artefactFactory;
  const Stability stabilityFunc;

};


}
