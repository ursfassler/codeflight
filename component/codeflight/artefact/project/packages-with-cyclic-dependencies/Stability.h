/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/common/package-cycles/Artefact.h"
#include <functional>

namespace component::codeflight::artefact::project::packages_with_cyclic_dependencies
{


using Stability = std::function<double(const common::package_cycles::PackageDependency&)>;


}
