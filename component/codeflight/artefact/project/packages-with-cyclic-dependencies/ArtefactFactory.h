/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace component::codeflight::artefact::interface
{

class ListFactory;
class NodeFactory;

}

namespace component::codeflight::artefact::project::packages_with_cyclic_dependencies
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual const interface::ListFactory& getDependenciesBetweenPackagesFactory() const = 0;
  virtual const interface::NodeFactory& getPackageNeighborsFactory() const = 0;

};


}
