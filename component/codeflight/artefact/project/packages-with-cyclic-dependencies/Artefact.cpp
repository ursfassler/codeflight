/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "component/codeflight/artefact/common/package-cycles/Artefact.h"
#include "component/codeflight/artefact/interface/DocumentBuilder.h"
#include "component/codeflight/artefact/interface/Factory.h"
#include "component/codeflight/ast/Project.h"
#include "component/codeflight/ast/graph/graph.h"
#include "component/codeflight/ast/specification/Package.h"


namespace component::codeflight::artefact::project::packages_with_cyclic_dependencies
{


Artefact::Artefact(
    const ast::Project* root_,
    const common::package_cycles::Artefact& cycleArtefact_
    ) :
  root{root_},
  cycleArtefact{cycleArtefact_}
{
}

void Artefact::build(interface::DocumentBuilder& xw) const
{
  const auto title = "Packages with cyclic dependencies";
  xw.beginDocument(title, {});
  xw.beginSection(title);

  ast::graph::Graph graph = ast::graph::buildGraph(*root, ast::specification::Package());
  cycleArtefact.print(graph, *root, xw);

  xw.endSection();
  xw.endDocument();
}


}
