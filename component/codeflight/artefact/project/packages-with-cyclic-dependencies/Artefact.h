/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/DocumentArtefact.h"
#include <functional>
#include <string>


namespace component::codeflight::ast
{

class Project;

}

namespace component::codeflight::artefact::common::package_cycles
{

class Artefact;

}

namespace component::codeflight::artefact::project::packages_with_cyclic_dependencies
{


class Artefact :
    public interface::DocumentArtefact
{
  public:
    Artefact(
        const ast::Project*,
        const common::package_cycles::Artefact&
        );

    void build(interface::DocumentBuilder&) const override;

  private:
    const ast::Project* root;
    const common::package_cycles::Artefact& cycleArtefact;

};


}
