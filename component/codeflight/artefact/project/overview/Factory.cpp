/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "Artefact.h"

namespace component::codeflight::artefact::project::overview
{


Factory::Factory(
    const ArtefactFactory& artefactFactory_
    ) :
  artefactFactory{artefactFactory_}
{
}

std::string Factory::printName() const
{
  return "Project overview";
}

std::string Factory::artefactName() const
{
  return "index";
}

std::unique_ptr<interface::Artefact> Factory::produce(const ast::Node* project) const
{
  return std::make_unique<Artefact>(project, artefactFactory);
}

interface::ArtefactIdentifier Factory::identifier(const ast::Node*) const
{
  return {{}, artefactName()};
}


}

