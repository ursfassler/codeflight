/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/DocumentArtefact.h"

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::project::overview
{

class ArtefactFactory;


class Artefact :
    public interface::DocumentArtefact
{
  public:
    Artefact(
        const ast::Node*,
        const ArtefactFactory&
        );

    void build(interface::DocumentBuilder&) const override;

  private:
    const ast::Node* node;
    const ArtefactFactory& artefactFactory;

    void printBody(interface::DocumentBuilder&) const;
    void printPackagesIntro(interface::DocumentBuilder&) const;

};


}
