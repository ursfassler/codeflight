/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "ArtefactFactory.h"
#include "component/codeflight/artefact/common/document/ArtefactList.h"
#include "component/codeflight/artefact/interface/Artefact.h"
#include "component/codeflight/artefact/interface/DocumentBuilder.h"
#include "component/codeflight/artefact/interface/Factory.h"
#include "component/codeflight/ast/Node.h"


namespace component::codeflight::artefact::project::overview
{


Artefact::Artefact(
    const ast::Node* node_,
    const ArtefactFactory& artefactFactory_
    ) :
  node{node_},
  artefactFactory{artefactFactory_}
{
}

void Artefact::build(interface::DocumentBuilder& xw) const
{
  xw.beginDocument(node->name, {});
  printBody(xw);
  xw.endDocument();
}

void Artefact::printBody(interface::DocumentBuilder& xw) const
{
  xw.beginSection("Project");
  printPackagesIntro(xw);
  xw.endSection();
}

void Artefact::printPackagesIntro(interface::DocumentBuilder& xw) const
{
  const common::document::FactoryList factories{
    &artefactFactory.getProjectAllPackagesFactory(),
    &artefactFactory.getProjectAllClassesFactory(),
    &artefactFactory.getProjectAllFunctionsFactory(),
    &artefactFactory.getProjectAllVariablesFactory(),
    &artefactFactory.getPackagesWithCyclicDependenciesFactory(),
    &artefactFactory.getStableDependencyPrincipleViolationsFactory(),
  };

  xw.beginParagraph();
  common::document::printArtefactList(node, factories, xw);
  xw.endParagraph();
}


}
