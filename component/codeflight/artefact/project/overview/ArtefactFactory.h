/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace component::codeflight::artefact::interface
{

class NodeFactory;

}

namespace component::codeflight::artefact::project::overview
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual const interface::NodeFactory& getProjectAllPackagesFactory() const = 0;
  virtual const interface::NodeFactory& getProjectAllClassesFactory() const = 0;
  virtual const interface::NodeFactory& getProjectAllFunctionsFactory() const = 0;
  virtual const interface::NodeFactory& getProjectAllVariablesFactory() const = 0;
  virtual const interface::NodeFactory& getPackagesWithCyclicDependenciesFactory() const = 0;
  virtual const interface::NodeFactory& getStableDependencyPrincipleViolationsFactory() const = 0;

};


}
