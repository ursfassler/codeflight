/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/NodeFactory.h"

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::metric
{

class Repository;

}

namespace component::codeflight::artefact::common::all_packageelement
{

class ArtefactFactory;

}

namespace component::codeflight::artefact::project::all_classes
{


class Factory :
    public interface::NodeFactory
{
public:
  Factory(
      const common::all_packageelement::ArtefactFactory&,
      const metric::Repository&
      );

  std::string printName() const override;
  std::string artefactName() const override;
  std::unique_ptr<interface::Artefact> produce(const ast::Node*) const override;
  interface::ArtefactIdentifier identifier(const ast::Node*) const override;

private:
  const common::all_packageelement::ArtefactFactory& artefactFactory;
  const metric::Repository& metrics;
};


}

