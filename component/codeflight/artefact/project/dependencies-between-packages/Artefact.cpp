/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "ArtefactFactory.h"
#include "component/codeflight/artefact/common/graph/GraphModel.h"
#include "component/codeflight/artefact/common/graph/draw.h"
#include "component/codeflight/artefact/interface/ListFactory.h"
#include "component/codeflight/artefact/overviewfactory/Factory.h"
#include "component/codeflight/ast/Class.h"
#include "component/codeflight/ast/Project.h"
#include "component/codeflight/ast/graph/graph.h"
#include "component/codeflight/ast/parents/nodePath.h"
#include "component/codeflight/ast/parents/parents.h"
#include "component/codeflight/ast/repository/Neighbor.h"
#include "component/codeflight/ast/specification/Package.h"
#include "component/codeflight/ast/specification/PackageElement.h"
#include "component/codeflight/library/Path.h"


namespace component::codeflight::artefact::project::dependencies_between_packages
{
namespace
{


const ast::Node* getAncestorPackage(const ast::Node* node)
{
  auto ancestor = ast::parents::findAncestor(node, ast::specification::Package());
  if (!ancestor) {
    throw std::runtime_error("no parent package found for " + node->name);
  }
  return *ancestor;
}


}


Artefact::Artefact(
    const std::vector<const ast::Node*>& packages_,
    const ArtefactFactory& artefactFactory_,
    const ast::repository::Neighbor& neighbors_
    ) :
  packages{packages_},
  artefactFactory{artefactFactory_},
  neighbors{neighbors_}
{
}

void Artefact::build(interface::GraphBuilder& builder) const
{
  const auto elementGraph = createPackageChildrenGraph();
  const auto reduced = reduceGraph(elementGraph);
  const auto drawGraph = createDrawGraph(reduced);
  common::graph::draw(drawGraph, builder, {});
}

ast::graph::Graph Artefact::createPackageChildrenGraph() const
{
  struct ChildAndParent {
    const ast::Node* element;
    const ast::Node* package;
  };

  const auto inPackages = [this](const ast::Node* node){
    return packages.contains(node);
  };

  const auto findElementAndPackage = [inPackages](const ast::Node* node) -> std::optional<ChildAndParent> {
    const auto element = ast::parents::findAncestor(node, ast::specification::PackageElement());
    if (!element) {
      return {};
    }

    const auto package = ast::parents::findAncestor(*element, inPackages);
    if (!package) {
      return {};
    }

    return {{*element, *package}};
  };


  ast::graph::Graph childrenGraph{};

  for (const auto& package : packages) {
    const auto visitor = [&childrenGraph, &findElementAndPackage](const ast::Node* source, const ast::Node* destination) {
      const auto sourceNodes = findElementAndPackage(source);
      if (!sourceNodes) {
        return;
      }
      const auto destinationNodes = findElementAndPackage(destination);
      if (!destinationNodes) {
        return;
      }

      if (sourceNodes->package == destinationNodes->package) {
        return;
      }

      if (!childrenGraph.nodeExists(sourceNodes->element)) {
        childrenGraph.add(sourceNodes->element);
      }
      if (!childrenGraph.nodeExists(destinationNodes->element)) {
        childrenGraph.add(destinationNodes->element);
      }
      if (!childrenGraph.edgeExists(sourceNodes->element, destinationNodes->element)) {
        childrenGraph.add(sourceNodes->element, destinationNodes->element);
      }
    };

    neighbors.foreachOutgoing(package, visitor);
  }

  return childrenGraph;
}

ast::graph::Graph Artefact::reduceGraph(const ast::graph::Graph& graph) const
{
  ast::graph::Graph reduced{};

  const auto visitor = [&reduced, this](const ast::Node* source, const ast::Node* destination) {
    const auto sourceAncestor = getElementWithParentInPackages(source);
    const auto destinationAncestor = getElementWithParentInPackages(destination);

    if (!reduced.nodeExists(sourceAncestor)) {
      reduced.add(sourceAncestor);
    }
    if (!reduced.nodeExists(destinationAncestor)) {
      reduced.add(destinationAncestor);
    }
    reduced.add(sourceAncestor, destinationAncestor);
  };
  graph.foreach(visitor);

  return reduced;
}

common::graph::GraphModel Artefact::createDrawGraph(const ast::graph::Graph& graph) const
{
  common::graph::GraphModel draw{{}};

  for (const auto& package : packages) {
    draw.addContainer(package);
    setNodeLink(package, draw);
  }

  const auto addNode = [&draw, this](const ast::Node* node){
    draw.addChildNode(node, *node->parent);
    setNodeLink(node, draw);
  };

  using Edge = std::pair<const ast::Node *, const ast::Node *>;
  std::map<Edge, std::size_t> edgeCount{};

  graph.foreach([&draw, &addNode, &edgeCount, this](const ast::Node* source, const ast::Node* destination) {
    addNode(source);
    addNode(destination);
    draw.addEdge(source, destination);
    setEdgeLink(source, destination, draw);
    edgeCount[{source, destination}]++;
  });

  for (const auto& edge : edgeCount) {
    draw.setLabel(edge.first.first, edge.first.second, edge.second);
  }

  return draw;
}

void Artefact::setNodeLink(const ast::Node* node, common::graph::GraphModel& graph) const
{
  auto ai = artefactFactory.getOverviewFactory().identifier(node);
  if (ai) {
    graph.setLink(node, *ai);
  }
}

void Artefact::setEdgeLink(const ast::Node* source, const ast::Node* destination, common::graph::GraphModel& graph) const
{
  if (source == destination) {
    return;
  }

  auto sourcePackage = getAncestorPackage(source);
  auto destinationPackage = getAncestorPackage(destination);
  if (sourcePackage == destinationPackage) {
    return;
  }

  auto project = ast::parents::getProject(sourcePackage);
  auto sourcePath = ast::parents::nodePath(*sourcePackage);
  auto destinationPath = ast::parents::nodePath(*destinationPackage);
  auto ai = artefactFactory.getDependenciesBetweenPackagesFactory().identifier(project, {sourcePath, destinationPath});
  graph.setLink({source, destination}, ai);
}

const ast::Node* Artefact::getElementWithParentInPackages(const ast::Node* node) const
{
  const auto element = ast::parents::findAncestor(node, [this](const ast::Node* node){
    const auto parent = ast::parents::findParent(node);
    return parent && packages.contains(*parent);
  });

  if (!element) {
    throw std::runtime_error("ancestor not in packages: " + node->name);
  }

  return *element;
}


}
