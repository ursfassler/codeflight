/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/GraphArtefact.h"
#include "component/codeflight/ast/NodeSpecification.h"
#include "component/codeflight/library/OrderedSet.h"
#include <string>


namespace component::codeflight::ast
{

class Project;
class Node;

}

namespace component::codeflight::ast::graph
{

class Graph;

}

namespace component::codeflight::ast::repository
{

class Neighbor;

}

namespace component::codeflight::artefact::common::graph
{

class GraphModel;

}

namespace component::codeflight::artefact::project::dependencies_between_packages
{

class ArtefactFactory;


class Artefact :
    public interface::GraphArtefact
{
  public:
    Artefact(
        const std::vector<const ast::Node*>&,
        const ArtefactFactory&,
        const ast::repository::Neighbor&
        );

    void build(interface::GraphBuilder&) const override;

  private:
    const library::OrderedSet<const ast::Node*> packages;
    const ArtefactFactory& artefactFactory;
    const ast::repository::Neighbor& neighbors;

    ast::graph::Graph createPackageChildrenGraph() const;
    ast::graph::Graph reduceGraph(const ast::graph::Graph&) const;
    common::graph::GraphModel createDrawGraph(const ast::graph::Graph&) const;
    void writePackagesGraph(std::ostream &output, const std::vector<const ast::Node *> &included) const;
    const ast::Node* getElementWithParentInPackages(const ast::Node*) const;
    void setNodeLink(const ast::Node*, common::graph::GraphModel&) const;
    void setEdgeLink(const ast::Node*, const ast::Node*, common::graph::GraphModel&) const;
};


}
