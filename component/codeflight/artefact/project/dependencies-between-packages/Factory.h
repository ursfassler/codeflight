/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"
#include "component/codeflight/artefact/interface/ListFactory.h"
#include <vector>

namespace component::codeflight::ast
{

class Node;
class Project;

}

namespace component::codeflight::ast::repository
{

class Neighbor;

}

namespace component::codeflight::artefact::project::dependencies_between_packages
{

class ArtefactFactory;


class Factory :
    public interface::ListFactory
{
public:
  Factory(
      const ArtefactFactory&,
      const ast::repository::Neighbor&
      );

  std::string printName() const override;
  std::string artefactName() const override;
  std::unique_ptr<interface::Artefact> produce(const ast::Project*, const library::Paths&) const override;
  interface::ArtefactIdentifier identifier(const ast::Project*, const library::Paths&) const override;

private:
  const ArtefactFactory& artefactFactory;
  const ast::repository::Neighbor& neighbors;
  std::vector<const ast::Node*> getPackages(const ast::Project*, const library::Paths&) const;
};


}

