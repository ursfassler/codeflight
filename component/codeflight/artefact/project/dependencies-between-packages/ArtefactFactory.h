/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace component::codeflight::artefact::interface
{

class ListFactory;
class MaybeFactory;

}

namespace component::codeflight::artefact::project::dependencies_between_packages
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual const interface::ListFactory& getDependenciesBetweenPackagesFactory() const = 0;
  virtual const interface::MaybeFactory& getOverviewFactory() const = 0;

};


}
