/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "Artefact.h"
#include "component/codeflight/ast/Project.h"
#include "component/codeflight/ast/query/find.h"

namespace component::codeflight::artefact::project::dependencies_between_packages
{


Factory::Factory(
    const ArtefactFactory& artefactFactory_,
    const ast::repository::Neighbor& neighbors_
    ) :
  artefactFactory{artefactFactory_},
  neighbors{neighbors_}
{
}

std::string Factory::printName() const
{
  return "Dependencies between packages";
}

std::string Factory::artefactName() const
{
  return "dependencies-between-packages";
}

std::unique_ptr<interface::Artefact> Factory::produce(const ast::Project* project, const library::Paths& paths) const
{
  auto packages = getPackages(project, paths);
  return std::make_unique<project::dependencies_between_packages::Artefact>(packages, artefactFactory, neighbors);
}

interface::ArtefactIdentifier Factory::identifier(const ast::Project* project, const library::Paths& paths) const
{
  auto packages = getPackages(project, paths); // verify that paths are valid
  return {{}, artefactName(), paths};
}

std::vector<const ast::Node*> Factory::getPackages(const ast::Project* project, const library::Paths& paths) const
{
  std::vector<const ast::Node*> packages{};

  for (const auto& part : paths) {
    const auto partPath = part.raw();
    const auto package = ast::query::find(partPath, project);
    if (!package) {
      throw std::invalid_argument("package not found: " + part.to_string());
    }
    packages.push_back(package);
  }

  return packages;
}


}

