/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "component/codeflight/artefact/common/all-packageelement/Artefact.h"
#include "component/codeflight/ast/specification/Function.h"
#include "component/codeflight/metric/Repository.h"

namespace component::codeflight::artefact::project::all_functions
{


Factory::Factory(
    const common::all_packageelement::ArtefactFactory& artefactFactory_,
    const metric::Repository& metrics_
    ) :
  artefactFactory{artefactFactory_},
  metrics{metrics_}
{
}

std::string Factory::printName() const
{
  return "All functions";
}

std::string Factory::artefactName() const
{
  return "all-functions";
}

std::unique_ptr<interface::Artefact> Factory::produce(const ast::Node* project) const
{
  return std::make_unique<artefact::common::all_packageelement::Artefact>(project, metrics.function(), ast::specification::Function(), artefactFactory, *this);
}

interface::ArtefactIdentifier Factory::identifier(const ast::Node*) const
{
  return {{}, artefactName()};
}


}

