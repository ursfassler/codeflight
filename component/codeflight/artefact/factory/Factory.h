/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/class/overview/ArtefactFactory.h"
#include "component/codeflight/artefact/class/intra-dependencies/Factory.h"
#include "component/codeflight/artefact/class/neighbors/Factory.h"
#include "component/codeflight/artefact/class/overview/Factory.h"
#include "component/codeflight/artefact/common/all-packageelement/ArtefactFactory.h"
#include "component/codeflight/artefact/common/document/Overview.h"
#include "component/codeflight/artefact/common/graph/GraphArtefact.h"
#include "component/codeflight/artefact/common/package-cycles/ArtefactFactory.h"
#include "component/codeflight/artefact/common/package-cycles/Settings.h"
#include "component/codeflight/artefact/function/neighbors/Factory.h"
#include "component/codeflight/artefact/function/overview/ArtefactFactory.h"
#include "component/codeflight/artefact/function/overview/Factory.h"
#include "component/codeflight/artefact/interface/Factory.h"
#include "component/codeflight/artefact/overviewfactory/ArtefactFactory.h"
#include "component/codeflight/artefact/overviewfactory/Factory.h"
#include "component/codeflight/artefact/package/content-dependencies/ArtefactFactory.h"
#include "component/codeflight/artefact/package/content-dependencies/Factory.h"
#include "component/codeflight/artefact/package/neighbors/ArtefactFactory.h"
#include "component/codeflight/artefact/package/neighbors/Factory.h"
#include "component/codeflight/artefact/package/overview/ArtefactFactory.h"
#include "component/codeflight/artefact/package/overview/Factory.h"
#include "component/codeflight/artefact/package/package-cycles/ArtefactFactory.h"
#include "component/codeflight/artefact/package/package-cycles/Factory.h"
#include "component/codeflight/artefact/package/package-tree/Factory.h"
#include "component/codeflight/artefact/packageelement/neighbors/ArtefactFactory.h"
#include "component/codeflight/artefact/project/all-classes/Factory.h"
#include "component/codeflight/artefact/project/all-functions/Factory.h"
#include "component/codeflight/artefact/project/all-packages/Factory.h"
#include "component/codeflight/artefact/project/all-variables/Factory.h"
#include "component/codeflight/artefact/project/dependencies-between-packages/ArtefactFactory.h"
#include "component/codeflight/artefact/project/dependencies-between-packages/Factory.h"
#include "component/codeflight/artefact/project/overview/ArtefactFactory.h"
#include "component/codeflight/artefact/project/overview/Factory.h"
#include "component/codeflight/artefact/project/packages-with-cyclic-dependencies/ArtefactFactory.h"
#include "component/codeflight/artefact/project/packages-with-cyclic-dependencies/Factory.h"
#include "component/codeflight/artefact/project/stable-dependency-principle-violations/ArtefactFactory.h"
#include "component/codeflight/artefact/project/stable-dependency-principle-violations/Factory.h"
#include "component/codeflight/artefact/variable/neighbors/Factory.h"
#include "component/codeflight/artefact/variable/overview/ArtefactFactory.h"
#include "component/codeflight/artefact/variable/overview/Factory.h"
#include "component/codeflight/metric/InstabilityService.h"

namespace component::codeflight::ast
{

class Class;
class Function;
class Variable;
class Package;

}

namespace component::codeflight::ast::repository
{

class Neighbor;

}

namespace component::codeflight::metric
{

class Repository;

}

namespace component::codeflight::artefact::factory
{


class Factory :
    public class_::overview::ArtefactFactory,
    public common::all_packageelement::ArtefactFactory,
    public common::package_cycles::ArtefactFactory,
    public function::overview::ArtefactFactory,
    public overviewfactory::ArtefactFactory,
    public package::content_dependencies::ArtefactFactory,
    public package::neighbors::ArtefactFactory,
    public package::overview::ArtefactFactory,
    public package::package_cycles::ArtefactFactory,
    public packageelement::neighbors::ArtefactFactory,
    public project::dependencies_between_packages::ArtefactFactory,
    public project::overview::ArtefactFactory,
    public project::packages_with_cyclic_dependencies::ArtefactFactory,
    public project::stable_dependency_principle_violations::ArtefactFactory,
    public variable::overview::ArtefactFactory
{
public:
  Factory(
      const ast::repository::Neighbor&,
      const metric::Repository&,
      const common::package_cycles::Settings&
      );

  const interface::NodeFactory& getClassIntradependenciesFactory() const override;
  const interface::NodeFactory& getClassNeighborsFactory() const override;
  const interface::NodeFactory& getClassOverviewFactory() const override;
  const interface::ListFactory& getDependenciesBetweenPackagesFactory() const override;
  const interface::MaybeFactory& getOverviewFactory() const override;
  const interface::NodeFactory& getProjectOverviewFactory() const override;
  const interface::NodeFactory& getPackagesWithCyclicDependenciesFactory() const override;
  const interface::NodeFactory& getStableDependencyPrincipleViolationsFactory() const override;
  const interface::NodeFactory& getProjectAllPackagesFactory() const override;
  const interface::NodeFactory& getProjectAllClassesFactory() const override;
  const interface::NodeFactory& getProjectAllFunctionsFactory() const override;
  const interface::NodeFactory& getProjectAllVariablesFactory() const override;
  const interface::NodeFactory& getFunctionNeighborsFactory() const override;
  const interface::NodeFactory& getFunctionOverviewFactory() const override;
  const interface::NodeFactory& getVariableNeighborsFactory() const override;
  const interface::NodeFactory& getVariableOverviewFactory() const override;
  const interface::NodeFactory& getPackageContentDependenciesFactory() const override;
  const interface::NodeFactory& getPackageOverviewFactory() const override;
  const interface::NodeFactory& getPackagePackageCyclesFactory() const override;
  const interface::NodeFactory& getPackageNeighborsFactory() const override;
  const interface::NodeFactory& getPackagePackageTreeFactory() const override;

private:
  const ast::repository::Neighbor& neighbors;
  const metric::Repository& metrics;
  const metric::InstabilityService instability;
  const overviewfactory::Factory overviewFactory;
  const common::document::Overview overview;
  const common::graph::GraphArtefact graph;
  const common::package_cycles::Settings cycleSettings;
  const class_::intra_dependencies::Factory classIntradependenciesFactory;
  const class_::neighbors::Factory classNeighborsFactory;
  const class_::overview::Factory classOverviewFactory;
  const project::dependencies_between_packages::Factory dependenciesBetweenPackagesFactory;
  const project::overview::Factory projectOverviewFactory;
  const project::packages_with_cyclic_dependencies::Factory packagesWithCyclicDependenciesFactory;
  const project::stable_dependency_principle_violations::Factory stableDependencyPrincipleViolationsFactory;
  const project::all_packages::Factory projectAllPackages;
  const project::all_classes::Factory projectAllClasses;
  const project::all_functions::Factory projectAllFunctions;
  const project::all_variables::Factory projectAllVariables;
  const function::neighbors::Factory functionNeighborsFactory;
  const function::overview::Factory functionOverviewFactory;
  const variable::neighbors::Factory variableNeighborsFactory;
  const variable::overview::Factory variableOverviewFactory;
  const package::content_dependencies::Factory packageContentDependenciesFactory;
  const package::overview::Factory packageOverviewFactory;
  const package::package_cycles::Factory packagePackageCyclesFactory;
  const package::neighbors::Factory packageNeighborsFactory;
  const package::package_tree::Factory packagePackageTreeFactory;

};


}
