/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "component/codeflight/ast/Package.h"
#include "component/codeflight/metric/Repository.h"


namespace component::codeflight::artefact::factory
{


Factory::Factory(
    const ast::repository::Neighbor& neihbors_,
    const metric::Repository& metrics_,
    const common::package_cycles::Settings& cycleSettings_
  ) :
  neighbors{neihbors_},
  metrics{metrics_},
  instability{metrics.packageInstabilityFunction()},
  overviewFactory{*this},
  overview{},
  graph{overviewFactory},
  cycleSettings{cycleSettings_},
  classIntradependenciesFactory{graph},
  classNeighborsFactory{*this, neighbors},
  classOverviewFactory{*this, overview},
  dependenciesBetweenPackagesFactory{*this, neighbors},
  projectOverviewFactory{*this},
  packagesWithCyclicDependenciesFactory{*this, *this, [this](const common::package_cycles::PackageDependency& dependency){
    return instability.stability({dependency.from, dependency.to});
  }, cycleSettings},
  stableDependencyPrincipleViolationsFactory{*this, std::bind(&metric::InstabilityService::instability, &instability, std::placeholders::_1), std::bind(&metric::InstabilityService::stability, &instability, std::placeholders::_1), neighbors},
  projectAllPackages{*this, metrics},
  projectAllClasses{*this, metrics},
  projectAllFunctions{*this, metrics},
  projectAllVariables{*this, metrics},
  functionNeighborsFactory{*this, neighbors},
  functionOverviewFactory{*this, overview},
  variableNeighborsFactory{*this, neighbors},
  variableOverviewFactory{*this, overview},
  packageContentDependenciesFactory{*this, neighbors},
  packageOverviewFactory{*this, overview},
  packagePackageCyclesFactory{*this, *this, cycleSettings},
  packageNeighborsFactory{*this, neighbors},
  packagePackageTreeFactory{overviewFactory}
{
}

const interface::NodeFactory& Factory::getClassIntradependenciesFactory() const
{
  return classIntradependenciesFactory;
}

const interface::NodeFactory& Factory::getClassNeighborsFactory() const
{
  return classNeighborsFactory;
}

const interface::NodeFactory& Factory::getClassOverviewFactory() const
{
  return classOverviewFactory;
}

const interface::ListFactory& Factory::getDependenciesBetweenPackagesFactory() const
{
  return dependenciesBetweenPackagesFactory;
}

const interface::MaybeFactory& Factory::getOverviewFactory() const
{
  return overviewFactory;
}

const interface::NodeFactory& Factory::getProjectOverviewFactory() const
{
  return projectOverviewFactory;
}

const interface::NodeFactory& Factory::getPackagesWithCyclicDependenciesFactory() const
{
  return packagesWithCyclicDependenciesFactory;
}

const interface::NodeFactory& Factory::getStableDependencyPrincipleViolationsFactory() const
{
  return stableDependencyPrincipleViolationsFactory;
}

const interface::NodeFactory& Factory::getFunctionNeighborsFactory() const
{
  return functionNeighborsFactory;
}

const interface::NodeFactory& Factory::getFunctionOverviewFactory() const
{
  return functionOverviewFactory;
}

const interface::NodeFactory& Factory::getVariableNeighborsFactory() const
{
  return variableNeighborsFactory;
}

const interface::NodeFactory& Factory::getVariableOverviewFactory() const
{
  return variableOverviewFactory;
}

const interface::NodeFactory& Factory::getPackageContentDependenciesFactory() const
{
  return packageContentDependenciesFactory;
}

const interface::NodeFactory& Factory::getPackageOverviewFactory() const
{
  return packageOverviewFactory;
}

const interface::NodeFactory& Factory::getPackagePackageCyclesFactory() const
{
  return packagePackageCyclesFactory;
}

const interface::NodeFactory& Factory::getPackageNeighborsFactory() const
{
  return packageNeighborsFactory;
}

const interface::NodeFactory& Factory::getPackagePackageTreeFactory() const
{
  return packagePackageTreeFactory;
}

const interface::NodeFactory& Factory::getProjectAllPackagesFactory() const
{
  return projectAllPackages;
}

const interface::NodeFactory& Factory::getProjectAllClassesFactory() const
{
  return projectAllClasses;
}

const interface::NodeFactory& Factory::getProjectAllFunctionsFactory() const
{
  return projectAllFunctions;
}

const interface::NodeFactory& Factory::getProjectAllVariablesFactory() const
{
  return projectAllVariables;
}


}
