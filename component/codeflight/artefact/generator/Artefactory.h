/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/ast/DefaultVisitor.h"
#include "NodeAndArgumentFactory.h"
#include "component/codeflight/artefact/factory/Factory.h"
#include "component/codeflight/artefact/interface/Factory.h"
#include "component/codeflight/library/Path.h"
#include <functional>
#include <string>
#include <vector>


namespace component::codeflight::artefact::generator
{


using Factories = std::vector<const interface::NodeFactory*>;


class Artefactory :
    public ast::DefaultVisitor
{
  public:
    Artefactory(
        const std::string& name_,
        const library::Paths&,
        const factory::Factory&
        );

    void visit(const ast::Class& node) override;
    void visit(const ast::Function& node) override;
    void visit(const ast::Variable& node) override;
    void visit(const ast::Package& node) override;
    void visit(const ast::Project& node) override;

    std::unique_ptr<interface::Artefact> getMaybeArtefact();

  private:
    std::string name;
    const library::Paths argument;
    const factory::Factory& factory;
    std::unique_ptr<interface::Artefact> artefact{};

    const NoArgumentFactory projectOverview;
    const NoArgumentFactory projectCyclicDependencies;
    const NoArgumentFactory projectStableDependenciesViolations;
    const ArgumentFactory projectDependenciesBetweenPackages;
    const NoArgumentFactory projectAllPackages;
    const NoArgumentFactory projectAllClasses;
    const NoArgumentFactory projectAllFunctions;
    const NoArgumentFactory projectAllVariables;

    const Factories classFactories;
    const Factories functionFactories;
    const Factories variableFactories;
    const Factories packageFactories;
    const std::vector<const NodeAndArgumentFactory*> projectFactories;

};


}
