/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "NodeAndArgumentFactory.h"
#include "component/codeflight/artefact/interface/ListFactory.h"
#include "component/codeflight/artefact/interface/NodeFactory.h"
#include "component/codeflight/ast/Project.h"

namespace component::codeflight::artefact::generator
{


ArgumentFactory::ArgumentFactory(
    const interface::ListFactory& factory_
    ) :
  factory{factory_}
{
}

std::string ArgumentFactory::printName() const
{
  return factory.printName();
}

std::string ArgumentFactory::artefactName() const
{
  return factory.artefactName();
}

std::unique_ptr<interface::Artefact> ArgumentFactory::produce(const ast::Project* project, const library::Paths& argument) const
{
  return factory.produce(project, argument);
}



NoArgumentFactory::NoArgumentFactory(
    const interface::NodeFactory& factory_
    ) :
  factory{factory_}
{
}

std::string NoArgumentFactory::printName() const
{
  return factory.printName();
}

std::string NoArgumentFactory::artefactName() const
{
  return factory.artefactName();
}

std::unique_ptr<interface::Artefact> NoArgumentFactory::produce(const ast::Project* node, const library::Paths&) const
{
  return factory.produce(node);
}


}
