/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/Factory.h"
#include "component/codeflight/library/Path.h"
#include <memory>
#include <string>

namespace component::codeflight::ast
{

class Project;

}

namespace component::codeflight::artefact::interface
{

class Artefact;
class ListFactory;
class NodeFactory;

}

namespace component::codeflight::artefact::generator
{


class NodeAndArgumentFactory :
    public interface::Factory
{
public:
  ~NodeAndArgumentFactory() override = default;

  virtual std::unique_ptr<interface::Artefact> produce(const ast::Project*, const library::Paths&) const = 0;

};


class ArgumentFactory :
    public NodeAndArgumentFactory
{
public:
  ArgumentFactory(
      const interface::ListFactory&
      );

  std::string printName() const override;
  std::string artefactName() const override;
  std::unique_ptr<interface::Artefact> produce(const ast::Project*, const library::Paths&) const override;

private:
  const interface::ListFactory& factory;

};


class NoArgumentFactory :
    public NodeAndArgumentFactory
{
public:
  NoArgumentFactory(
      const interface::NodeFactory&
      );

  std::string printName() const override;
  std::string artefactName() const override;
  std::unique_ptr<interface::Artefact> produce(const ast::Project*, const library::Paths&) const override;

private:
  const interface::NodeFactory& factory;

};


}
