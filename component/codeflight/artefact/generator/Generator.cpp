/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Generator.h"
#include "Artefactory.h"
#include "component/codeflight/artefact/interface/Artefact.h"
#include "component/codeflight/ast/Project.h"


namespace component::codeflight::artefact::generator
{
namespace
{


std::optional<ast::Node*> find(const std::string& name, const std::vector<std::unique_ptr<ast::Node>>& items)
{
  for (const auto& item : items) {
    if (item->name == name) {
      return {item.get()};
    }
  }

  return {};
}


}


Generator::Generator(
    const ast::Project* project_,
    const factory::Factory& factory_
    ) :
  project{project_},
  factory{factory_}
{
}

std::unique_ptr<interface::Artefact> Generator::getMaybeArtefact(const interface::ArtefactIdentifier& ai) const
{
  const ast::Node* node = project;
  for (auto& part : ai.path.raw()) {
    const auto child = find(part, node->children);
    if (!child) {
      return {};
    }
    node = *child;
  }

  Artefactory artefactory{ai.artefact, ai.arguments, factory};
  node->accept(artefactory);
  return artefactory.getMaybeArtefact();
}


}
