/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefactory.h"
#include "component/codeflight/artefact/interface/Factory.h"
#include "component/codeflight/ast/Class.h"
#include "component/codeflight/ast/Function.h"
#include "component/codeflight/ast/Package.h"
#include "component/codeflight/ast/Project.h"
#include "component/codeflight/ast/Variable.h"


namespace component::codeflight::artefact::generator
{
namespace
{


template<typename T>
T findFactory(const std::string& artefactName, const std::vector<T>& factories)
{
  for (const auto factory : factories) {
    if (factory->artefactName() == artefactName) {
      return { factory };
    }
  }

  return {};
}

std::unique_ptr<interface::Artefact> artefactFromFactory(const std::string& artefactName, const ast::Node* node, const Factories& factories)
{
  const auto factory = findFactory(artefactName, factories);

  if (!factory) {
    return {};
  }

  return factory->produce(node);
}

std::unique_ptr<interface::Artefact> artefactFromFactory(const std::string& artefactName, const ast::Project* node, const library::Paths& argument, const std::vector<const NodeAndArgumentFactory*>& factories)
{
  const auto factory = findFactory(artefactName, factories);

  if (!factory) {
    return {};
  }

  return factory->produce(node, argument);
}


}


Artefactory::Artefactory(
    const std::string& name_,
    const library::Paths& argument_,
    const factory::Factory& factory_
    ) :
  name{name_},
  argument{argument_},
  factory{factory_},
  projectOverview{factory.getProjectOverviewFactory()},
  projectCyclicDependencies{factory.getPackagesWithCyclicDependenciesFactory()},
  projectStableDependenciesViolations{factory.getStableDependencyPrincipleViolationsFactory()},
  projectDependenciesBetweenPackages{factory.getDependenciesBetweenPackagesFactory()},
  projectAllPackages{factory.getProjectAllPackagesFactory()},
  projectAllClasses{factory.getProjectAllClassesFactory()},
  projectAllFunctions{factory.getProjectAllFunctionsFactory()},
  projectAllVariables{factory.getProjectAllVariablesFactory()},
  classFactories {
    &factory.getClassIntradependenciesFactory(),
    &factory.getClassNeighborsFactory(),
    &factory.getClassOverviewFactory(),
  },
  functionFactories {
    &factory.getFunctionNeighborsFactory(),
    &factory.getFunctionOverviewFactory(),
  },
  variableFactories {
    &factory.getVariableNeighborsFactory(),
    &factory.getVariableOverviewFactory(),
  },
  packageFactories {
    &factory.getPackageContentDependenciesFactory(),
    &factory.getPackageOverviewFactory(),
    &factory.getPackagePackageCyclesFactory(),
    &factory.getPackageNeighborsFactory(),
    &factory.getPackagePackageTreeFactory(),
  },
  projectFactories {
    &projectOverview,
    &projectCyclicDependencies,
    &projectStableDependenciesViolations,
    &projectDependenciesBetweenPackages,
    &projectAllPackages,
    &projectAllClasses,
    &projectAllFunctions,
    &projectAllVariables,
  }
{
}

void Artefactory::visit(const ast::Class &node)
{
  artefact = artefactFromFactory(name, &node, classFactories);
}

void Artefactory::visit(const ast::Function &node)
{
  artefact = artefactFromFactory(name, &node, functionFactories);
}

void Artefactory::visit(const ast::Variable &node)
{
  artefact = artefactFromFactory(name, &node, variableFactories);
}

void Artefactory::visit(const ast::Package &node)
{
  artefact = artefactFromFactory(name, &node, packageFactories);
}

void Artefactory::visit(const ast::Project &node)
{
  artefact = artefactFromFactory(name, &node, argument, projectFactories);
}

std::unique_ptr<interface::Artefact> Artefactory::getMaybeArtefact()
{
  return std::move(artefact);
}


}
