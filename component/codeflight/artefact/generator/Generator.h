/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <memory>

namespace component::codeflight::ast
{

class Project;

}

namespace component::codeflight::artefact::factory
{

class Factory;

}

namespace component::codeflight::artefact::interface
{

class Artefact;
struct ArtefactIdentifier;

}

namespace component::codeflight::artefact::generator
{

class Artefactory;


class Generator
{
public:
  Generator(
      const ast::Project*,
      const factory::Factory&
      );

  std::unique_ptr<interface::Artefact> getMaybeArtefact(const interface::ArtefactIdentifier&) const;

private:
  const ast::Project* const project;
  const factory::Factory& factory;
};


}
