/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "ArtefactFactory.h"
#include "component/codeflight/artefact/interface/NodeFactory.h"
#include "component/codeflight/ast/Class.h"
#include "component/codeflight/ast/DefaultVisitor.h"
#include "component/codeflight/ast/Function.h"
#include "component/codeflight/ast/Package.h"
#include "component/codeflight/ast/Project.h"
#include "component/codeflight/ast/Variable.h"
#include "component/codeflight/ast/repository/Neighbor.h"
#include "component/codeflight/library/OrderedSet.h"
#include "component/codeflight/library/Path.h"
#include <cassert>


namespace component::codeflight::artefact::overviewfactory
{
namespace
{


class OverviewFactoryVisitor :
    public ast::DefaultVisitor
{
  public:
    OverviewFactoryVisitor(
        const ArtefactFactory& artefactFactory_
        ) :
      artefactFactory{artefactFactory_}
    {
    }

    void visit(const ast::Project&) override
    {
      factory = &artefactFactory.getProjectOverviewFactory();
    }

    void visit(const ast::Package&) override
    {
      factory = &artefactFactory.getPackageOverviewFactory();
    }

    void visit(const ast::Class&) override
    {
      factory = &artefactFactory.getClassOverviewFactory();
    }

    void visit(const ast::Function&) override
    {
      factory = &artefactFactory.getFunctionOverviewFactory();
    }

    void visit(const ast::Variable&) override
    {
      factory = &artefactFactory.getVariableOverviewFactory();
    }

    const interface::NodeFactory* getFactory()
    {
      return factory;
    }

  private:
    const ArtefactFactory& artefactFactory;
    const interface::NodeFactory* factory{};
};


}


Factory::Factory(const ArtefactFactory& artefactFactory_) :
  artefactFactory{artefactFactory_}
{
}

std::string Factory::printName() const
{
  return "Overview";
}

std::string Factory::artefactName() const
{
  return "index";
}

std::unique_ptr<interface::Artefact> Factory::produce(const ast::Node* node) const
{
  OverviewFactoryVisitor visitor{artefactFactory};
  node->accept(visitor);
  const auto factory = visitor.getFactory();
  if (factory) {
    return factory->produce(node);
  } else {
    return {};
  }
}

std::optional<interface::ArtefactIdentifier> Factory::identifier(const ast::Node* node) const
{
  OverviewFactoryVisitor visitor{artefactFactory};
  node->accept(visitor);
  const auto factory = visitor.getFactory();
  if (factory) {
    return factory->identifier(node);
  } else {
    return {};
  }
}


}
