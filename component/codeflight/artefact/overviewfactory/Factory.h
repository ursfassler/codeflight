/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/MaybeFactory.h"
#include "component/codeflight/artefact/interface/Factory.h"
#include <memory>
#include <string>


namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::overviewfactory
{

class ArtefactFactory;

class Factory :
    public interface::MaybeFactory
{
  public:
    explicit Factory(const ArtefactFactory&);

    std::string printName() const override;
    std::string artefactName() const override;
    std::unique_ptr<interface::Artefact> produce(const ast::Node*) const override;
    std::optional<interface::ArtefactIdentifier> identifier(const ast::Node*) const override;

  private:
    const ArtefactFactory& artefactFactory;
};


}
