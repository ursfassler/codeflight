/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "component/codeflight/artefact/packageelement/neighbors/Artefact.h"
#include "component/codeflight/ast/Function.h"
#include "component/codeflight/ast/parents/nodePath.h"

namespace component::codeflight::artefact::function::neighbors
{


Factory::Factory(
    const packageelement::neighbors::ArtefactFactory& artefactFactory_,
    const ast::repository::Neighbor& neighbors_
    ) :
  artefactFactory{artefactFactory_},
  neighbors{neighbors_}
{
}

std::string Factory::printName() const
{
  return "Function neighbors";
}

std::string Factory::artefactName() const
{
  return "neighbors";
}

std::unique_ptr<interface::Artefact> Factory::produce(const ast::Node* node) const
{
  return std::make_unique<packageelement::neighbors::Artefact>(node, artefactFactory, neighbors);
}

interface::ArtefactIdentifier Factory::identifier(const ast::Node* node) const
{
  return {ast::parents::nodePath(*node), artefactName()};
}


}

