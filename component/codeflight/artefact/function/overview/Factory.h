/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/NodeFactory.h"

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::common::document
{

class Overview;

}

namespace component::codeflight::artefact::function::overview
{

class ArtefactFactory;


class Factory :
    public interface::NodeFactory
{
public:
  Factory(
      const ArtefactFactory&,
      const common::document::Overview&
      );

  std::string printName() const override;
  std::string artefactName() const override;
  std::unique_ptr<interface::Artefact> produce(const ast::Node*) const override;
  interface::ArtefactIdentifier identifier(const ast::Node*) const override;

private:
  const ArtefactFactory& artefactFactory;
  const common::document::Overview& overview;
};


}
