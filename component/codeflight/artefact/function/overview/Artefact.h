/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/DocumentArtefact.h"

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::common::document
{

class Overview;

}

namespace component::codeflight::artefact::function::overview
{

class ArtefactFactory;


class Artefact :
    public interface::DocumentArtefact
{
  public:
    Artefact(
        const ast::Node*,
        const ArtefactFactory&,
        const common::document::Overview&
        );

    void build(interface::DocumentBuilder&) const override;

  private:
    const ast::Node* node;
    const ArtefactFactory& artefactFactory;
    const common::document::Overview& overview;
};


}
