/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/common/package-cycles/Artefact.h"
#include "component/codeflight/metric/Metrics.h"
#include <optional>


namespace component::codeflight::ast
{

class Package;

}

namespace component::codeflight::artefact::package::package_cycles
{

class ArtefactFactory;


class Configuration :
    public common::package_cycles::Configuration
{
public:
  Configuration(
      const ArtefactFactory&
      );

  common::package_cycles::PackageDependencyMetrics dependency() const override;
  std::optional<metric::GenericDescription<common::package_cycles::PackageDependency>::Link> dependencyReferencesUrl(const ast::Project*) const override;

private:
  const ArtefactFactory& artefactFactory;

};


}
