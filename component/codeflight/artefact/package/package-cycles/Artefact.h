/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Configuration.h"
#include "component/codeflight/artefact/interface/DocumentArtefact.h"
#include <optional>
#include <string>


namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::common::package_cycles
{

class ArtefactFactory;
struct Settings;

}

namespace component::codeflight::artefact::package::package_cycles
{


class Artefact :
    public interface::DocumentArtefact
{
  public:
    Artefact(
        const ast::Node*,
        const common::package_cycles::Artefact&
        );

    void build(interface::DocumentBuilder&) const override;

private:
    const ast::Node* node;
    const common::package_cycles::Artefact& cycleArtefact;

};


}
