/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Configuration.h"
#include "component/codeflight/artefact/common/package-cycles/Artefact.h"
#include "component/codeflight/artefact/interface/NodeFactory.h"

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::package::package_cycles
{

class ArtefactFactory;


class Factory :
    public interface::NodeFactory
{
public:
  Factory(
      const ArtefactFactory&,
      const common::package_cycles::ArtefactFactory&,
      const common::package_cycles::Settings&
      );

  std::string printName() const override;
  std::string artefactName() const override;
  std::unique_ptr<interface::Artefact> produce(const ast::Node*) const override;
  interface::ArtefactIdentifier identifier(const ast::Node*) const override;

private:
  const Configuration configuration;
  const common::package_cycles::Artefact cycleArtefact;
};


}

