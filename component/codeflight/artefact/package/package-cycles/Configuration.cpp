/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Configuration.h"
#include "ArtefactFactory.h"
#include "component/codeflight/artefact/interface/Artefact.h"
#include "component/codeflight/artefact/interface/ListFactory.h"
#include "component/codeflight/ast/Package.h"
#include "component/codeflight/ast/parents/nodePath.h"
#include <memory>


namespace component::codeflight::artefact::package::package_cycles
{


Configuration::Configuration(
    const ArtefactFactory& artefactFactory_
    ) :
  artefactFactory{artefactFactory_}
{
}

common::package_cycles::PackageDependencyMetrics Configuration::dependency() const
{
  return {};
}

std::optional<metric::GenericDescription<common::package_cycles::PackageDependency>::Link> Configuration::dependencyReferencesUrl(const ast::Project* project) const
{
  return [project, this](const common::package_cycles::PackageDependency& dependency){
    auto sourcePath = ast::parents::nodePath(*dependency.from);
    auto destinationPath = ast::parents::nodePath(*dependency.to);
    interface::ArtefactIdentifier ai = artefactFactory.getDependenciesBetweenPackagesFactory().identifier(project, {sourcePath, destinationPath});
    return ai;
  };
}


}
