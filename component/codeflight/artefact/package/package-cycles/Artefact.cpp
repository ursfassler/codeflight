/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "component/codeflight/artefact/interface/DocumentBuilder.h"
#include "component/codeflight/ast/Package.h"
#include "component/codeflight/ast/graph/graph.h"
#include "component/codeflight/ast/parents/nodePath.h"


namespace component::codeflight::artefact::package::package_cycles
{


Artefact::Artefact(
    const ast::Node* node_,
    const common::package_cycles::Artefact& cycleArtefact_
    ) :
  node{node_},
  cycleArtefact{cycleArtefact_}
{
}

void Artefact::build(interface::DocumentBuilder& builder) const
{
  const std::string title = "Package cycles";
  builder.beginDocument(title, node);
  builder.beginSection(title);

  ast::graph::Graph graph = ast::graph::buildPackageDependencyGraph(*node);
  cycleArtefact.print(graph, *node, builder);

  builder.endSection();
  builder.endDocument();
}


}
