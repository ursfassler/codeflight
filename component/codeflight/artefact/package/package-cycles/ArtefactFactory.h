/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace component::codeflight::artefact::interface
{

class ListFactory;

}

namespace component::codeflight::artefact::package::package_cycles
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual const interface::ListFactory& getDependenciesBetweenPackagesFactory() const = 0;

};


}
