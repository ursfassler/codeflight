/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "Artefact.h"
#include "component/codeflight/ast/parents/nodePath.h"

namespace component::codeflight::artefact::package::package_cycles
{


Factory::Factory(
    const ArtefactFactory& artefactFactory_,
    const common::package_cycles::ArtefactFactory& cyclesArtefactFactory,
    const common::package_cycles::Settings& settings
    ) :
  configuration{
    artefactFactory_
  },
  cycleArtefact{
    cyclesArtefactFactory,
    configuration,
    settings
  }
{
}

std::string Factory::printName() const
{
  return "Package cycles";
}

std::string Factory::artefactName() const
{
  return "package-cycles";
}

std::unique_ptr<interface::Artefact> Factory::produce(const ast::Node* package) const
{
  return std::make_unique<Artefact>(package, cycleArtefact);
}

interface::ArtefactIdentifier Factory::identifier(const ast::Node* node) const
{
  return {ast::parents::nodePath(*node), artefactName()};
}


}

