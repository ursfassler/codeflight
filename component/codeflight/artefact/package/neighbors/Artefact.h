/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/GraphArtefact.h"
#include "component/codeflight/library/Set.h"
#include <map>
#include <string>


namespace component::codeflight::ast
{

class Node;
class Project;
class Class;
class Package;

}

namespace component::codeflight::ast::graph
{

class Graph;

}

namespace component::codeflight::ast::repository
{

class Neighbor;

}

namespace component::codeflight::artefact::common::graph
{

class GraphModel;

}

namespace component::codeflight::artefact::package::neighbors
{

class ArtefactFactory;


class Artefact :
    public interface::GraphArtefact
{
  public:
    Artefact(
        const ast::Node*,
        const ArtefactFactory&,
        const ast::repository::Neighbor&
        );

    void build(interface::GraphBuilder&) const override;

  private:
    const ast::Node* package;
    const ArtefactFactory& artefactFactory;
    const ast::repository::Neighbor& neighbors;

    common::graph::GraphModel create() const;

    ast::graph::Graph createNeighborsOfPackageChildren() const;
    ast::graph::Graph reduceToPackageOrCommonAncestorsChild(const ast::graph::Graph&) const;
    common::graph::GraphModel mapToDraw(const ast::graph::Graph&) const;
    void addNode(const ast::Node*, common::graph::GraphModel&) const;

    const ast::Node* ancestorWithAnyPackageAsParent(const ast::Node*) const;
    const ast::Node* thePackageIfSuccessorOrTopmostAncestorWithCommonAncestor(const ast::Node* node) const;
    bool parentIsAncestorOfThePackage(const ast::Node* node) const;
    mutable library::Set<const ast::Node*> packageAncestors{};
    void setEdgeUrl(const ast::Node*, const ast::Node*, common::graph::GraphModel&) const;
};


}
