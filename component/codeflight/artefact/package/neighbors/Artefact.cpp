/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "ArtefactFactory.h"
#include "component/codeflight/artefact/common/graph/GraphModel.h"
#include "component/codeflight/artefact/common/graph/draw.h"
#include "component/codeflight/artefact/interface/ListFactory.h"
#include "component/codeflight/artefact/interface/MaybeFactory.h"
#include "component/codeflight/ast/Package.h"
#include "component/codeflight/ast/graph/graph.h"
#include "component/codeflight/ast/parents/nodePath.h"
#include "component/codeflight/ast/parents/parents.h"
#include "component/codeflight/ast/repository/Neighbor.h"
#include "component/codeflight/ast/specification/Package.h"
#include "component/codeflight/ast/traverse/traverse.h"
#include "component/codeflight/library/OrderedSet.h"
#include "component/codeflight/library/Path.h"
#include <cassert>


namespace component::codeflight::artefact::package::neighbors
{


Artefact::Artefact(
    const ast::Node* package_,
    const ArtefactFactory& artefactFactory_,
    const ast::repository::Neighbor& neighbors_
    ) :
  package{package_},
  artefactFactory{artefactFactory_},
  neighbors{neighbors_}
{
}

void Artefact::build(interface::GraphBuilder& builder) const
{
  const auto graph = create();
  common::graph::draw(graph, builder, {common::graph::NameType::Path, false});
}

common::graph::GraphModel Artefact::create() const
{
  ast::graph::Graph neighborsOfPackageChildren = createNeighborsOfPackageChildren();
  ast::graph::Graph neighborsOfPackage = reduceToPackageOrCommonAncestorsChild(neighborsOfPackageChildren);
  common::graph::GraphModel draw = mapToDraw(neighborsOfPackage);
  return draw;
}

ast::graph::Graph Artefact::createNeighborsOfPackageChildren() const
{
  ast::graph::Graph graph{};

  const auto visitor = [&graph, this](const ast::Node* source, const ast::Node* destination) {
    const auto sourceElement = ancestorWithAnyPackageAsParent(source);
    if (!graph.nodeExists(sourceElement)) {
      graph.add(sourceElement);
    }

    const auto destinationElement = ancestorWithAnyPackageAsParent(destination);
    if (!graph.nodeExists(destinationElement)) {
      graph.add(destinationElement);
    }

    if (!graph.edgeExists(sourceElement, destinationElement)) {
      graph.add(sourceElement, destinationElement);
    }
  };

  neighbors.foreachOutgoing(package, visitor);
  neighbors.foreachIncoming(package, visitor);

  return graph;
}

ast::graph::Graph Artefact::reduceToPackageOrCommonAncestorsChild(const ast::graph::Graph& full) const
{
  ast::graph::Graph reduced{};

  full.foreach([&reduced, this](const ast::Node* source, const ast::Node* destination){
    const auto sourceAncestor = thePackageIfSuccessorOrTopmostAncestorWithCommonAncestor(source);
    const auto destinationAncestor = thePackageIfSuccessorOrTopmostAncestorWithCommonAncestor(destination);

    if (sourceAncestor == destinationAncestor) {
      return;
    }

    if (!reduced.nodeExists(sourceAncestor)) {
      reduced.add(sourceAncestor);
    }
    if (!reduced.nodeExists(destinationAncestor)) {
      reduced.add(destinationAncestor);
    }
    reduced.add(sourceAncestor, destinationAncestor);
  });

  return reduced;
}

common::graph::GraphModel Artefact::mapToDraw(const ast::graph::Graph& graph) const
{
  common::graph::GraphModel draw{{}};

  using Edge = std::pair<const ast::Node *, const ast::Node *>;
  std::map<Edge, std::size_t> edgeCount{};

  const auto addEdge = [&draw, &edgeCount, this](const Edge& edge){
    addNode(edge.first, draw);
    addNode(edge.second, draw);
    draw.addEdge(edge.first, edge.second);
    setEdgeUrl(edge.first, edge.second, draw);
    edgeCount[edge]++;
  };

  addNode(package, draw);

  graph.foreach([&addEdge](const ast::Node* source, const ast::Node* destination){
    addEdge({source, destination});
  });

  draw.setEmphasized(package);

  for (const auto& edge : edgeCount) {
    draw.setLabel(edge.first.first, edge.first.second, edge.second);
  }

  return draw;
}

void Artefact::addNode(const ast::Node* node, common::graph::GraphModel& graph) const
{
  graph.addNode(node);

  auto ai = artefactFactory.getOverviewFactory().identifier(node);
  if (ai) {
    graph.setLink(node, *ai);
  }
}

void Artefact::setEdgeUrl(const ast::Node* source, const ast::Node* destination, common::graph::GraphModel& graph) const
{
  if (source == destination) {
    return;
  }

  if (!ast::specification::Package()(source) || !ast::specification::Package()(destination)) {
    return;
  }

  auto project = ast::parents::getProject(source);
  auto sourcePath = ast::parents::nodePath(*source);
  auto destinationPath = ast::parents::nodePath(*destination);
  const auto ai = artefactFactory.getDependenciesBetweenPackagesFactory().identifier(project, {sourcePath, destinationPath});
  graph.setLink({source, destination}, ai);
}

const ast::Node* Artefact::ancestorWithAnyPackageAsParent(const ast::Node* node) const
{
  const auto packageElement = ast::parents::findAncestor(node, [](const ast::Node* node){
    const auto parent = ast::parents::findParent(node);
    return parent && ast::specification::Package()(*parent);
  });

  if (!packageElement) {
    throw std::runtime_error("element with no package as parent: " + node->name);
  }

  return *packageElement;
}

const ast::Node* Artefact::thePackageIfSuccessorOrTopmostAncestorWithCommonAncestor(const ast::Node* node) const
{
  const auto isSuccessor = ast::parents::findAncestor(node, [this](const ast::Node* node){
    return node == package;
  }).has_value();
  if (isSuccessor) {
    return package;
  }

  const auto ancestor = ast::parents::findAncestor(node, [this](const ast::Node* node){
    return parentIsAncestorOfThePackage(node);
  });

  if (!ancestor) {
    throw std::runtime_error("element with no common ancestor to package: " + node->name);
  }

  return *ancestor;
}

bool Artefact::parentIsAncestorOfThePackage(const ast::Node* node) const
{
  if (packageAncestors.size() == 0) {
    for (const ast::Node* itr = package; itr; itr = ast::parents::findParent(itr).value_or(nullptr)) {
      packageAncestors.add(itr);
    }
  }

  const auto parent = ast::parents::findParent(node).value_or(nullptr);
  return packageAncestors.contains(parent);
}


}
