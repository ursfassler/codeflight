/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "ArtefactFactory.h"
#include "component/codeflight/artefact/common/document/ArtefactList.h"
#include "component/codeflight/artefact/common/document/Overview.h"
#include "component/codeflight/ast/Package.h"


namespace component::codeflight::artefact::package::overview
{


Artefact::Artefact(
    const ast::Node* node_,
    const ArtefactFactory& artefactFactory_,
    const common::document::Overview& overview_
    ) :
  node{node_},
  artefactFactory{artefactFactory_},
  overview{overview_}
{
}

void Artefact::build(interface::DocumentBuilder& builder) const
{
  const common::document::FactoryList factories{
    &artefactFactory.getPackageContentDependenciesFactory(),
    &artefactFactory.getPackagePackageCyclesFactory(),
    &artefactFactory.getPackageNeighborsFactory(),
    &artefactFactory.getPackagePackageTreeFactory(),
  };

  overview.build(node, factories, builder);
}


}
