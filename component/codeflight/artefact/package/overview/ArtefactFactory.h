/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace component::codeflight::artefact::interface
{

class NodeFactory;

}

namespace component::codeflight::artefact::package::overview
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual const interface::NodeFactory& getPackageContentDependenciesFactory() const = 0;
  virtual const interface::NodeFactory& getPackagePackageCyclesFactory() const = 0;
  virtual const interface::NodeFactory& getPackageNeighborsFactory() const = 0;
  virtual const interface::NodeFactory& getPackagePackageTreeFactory() const = 0;

};


}
