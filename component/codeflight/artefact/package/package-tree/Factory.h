/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/NodeFactory.h"

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::interface
{

class MaybeFactory;

}

namespace component::codeflight::artefact::package::package_tree
{


class Factory :
    public interface::NodeFactory
{
public:
  Factory(
      const interface::MaybeFactory&
      );

  std::string printName() const override;
  std::string artefactName() const override;
  std::unique_ptr<interface::Artefact> produce(const ast::Node*) const override;
  interface::ArtefactIdentifier identifier(const ast::Node*) const override;

private:
  const interface::MaybeFactory& overviewFactory;
};


}

