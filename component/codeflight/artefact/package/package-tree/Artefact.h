/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/GraphArtefact.h"
#include <string>


namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::interface
{

class MaybeFactory;

}

namespace component::codeflight::artefact::common::graph
{

class GraphModel;

}

namespace component::codeflight::artefact::package::package_tree
{


class Artefact :
    public interface::GraphArtefact
{
  public:
    Artefact(
        const ast::Node*,
        const interface::MaybeFactory&
        );

    void build(interface::GraphBuilder&) const override;

  private:
    const ast::Node* package;
    const interface::MaybeFactory& overviewFactory;

    common::graph::GraphModel create() const;
    void addChildren(const ast::Node*, common::graph::GraphModel&) const;
    void addSiblings(const ast::Node*, common::graph::GraphModel&) const;
    void addParents(const ast::Node*, common::graph::GraphModel&) const;
    void addNode(const ast::Node*, common::graph::GraphModel&) const;
};


}
