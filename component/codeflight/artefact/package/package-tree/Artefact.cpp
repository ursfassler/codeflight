/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "component/codeflight/artefact/common/graph/GraphModel.h"
#include "component/codeflight/artefact/common/graph/draw.h"
#include "component/codeflight/artefact/interface/MaybeFactory.h"
#include "component/codeflight/ast/Node.h"
#include "component/codeflight/ast/Package.h"
#include "component/codeflight/ast/parents/nodePath.h"
#include "component/codeflight/ast/parents/parents.h"
#include "component/codeflight/ast/specification/Package.h"
#include "component/codeflight/library/Path.h"


namespace component::codeflight::artefact::package::package_tree
{


Artefact::Artefact(
    const ast::Node* package_,
    const interface::MaybeFactory& overviewFactory_
    ) :
  package{package_},
  overviewFactory{overviewFactory_}
{
}

void Artefact::build(interface::GraphBuilder& builder) const
{
  auto graph = create();
  common::graph::draw(graph, builder, {common::graph::NameType::Leaf, false});
}

common::graph::GraphModel Artefact::create() const
{
  common::graph::GraphModel graph{{}};

  addNode(package, graph);
  addParents(package, graph);
  addSiblings(package, graph);
  addChildren(package, graph);
  graph.setEmphasized(package);

  return graph;
}

void Artefact::addChildren(const ast::Node* root, common::graph::GraphModel& graph) const
{
  for (const auto& child : root->children) {
    if (ast::specification::Package()(child.get())) {
      addNode(child.get(), graph);
      graph.addEdge(root, child.get());
    }
  }
}

void Artefact::addSiblings(const ast::Node* node, common::graph::GraphModel& graph) const
{
  const auto parent = ast::parents::findParent(node);
  if (parent && ast::specification::Package()(*parent)) {
    addChildren(*parent, graph);
  }
}

void Artefact::addParents(const ast::Node* node, common::graph::GraphModel& graph) const
{
  for (auto parent = ast::parents::findParent(node); parent && ast::specification::Package()(*parent); node = *parent, parent = ast::parents::findParent(node)) {
    addNode(*parent, graph);
    addParents(*parent, graph);
    graph.addEdge(*parent, node);
  }
}

void Artefact::addNode(const ast::Node* node, common::graph::GraphModel& graph) const
{
  graph.addNode(node);

  auto ai = overviewFactory.identifier(node);
  if (ai) {
    graph.setLink(node, *ai);
  }
}


}
