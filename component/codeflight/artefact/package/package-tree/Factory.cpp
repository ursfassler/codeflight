/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "Artefact.h"
#include "component/codeflight/ast/parents/nodePath.h"

namespace component::codeflight::artefact::package::package_tree
{


Factory::Factory(
    const interface::MaybeFactory& overviewFactory_
    ) :
  overviewFactory{overviewFactory_}
{
}

std::string Factory::printName() const
{
  return "Package tree";
}

std::string Factory::artefactName() const
{
  return "package-tree";
}

std::unique_ptr<interface::Artefact> Factory::produce(const ast::Node* package) const
{
  return std::make_unique<Artefact>(package, overviewFactory);
}

interface::ArtefactIdentifier Factory::identifier(const ast::Node* node) const
{
  return {ast::parents::nodePath(*node), artefactName()};
}


}

