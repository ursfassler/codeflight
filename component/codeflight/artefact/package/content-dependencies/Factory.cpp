/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "Artefact.h"
#include "component/codeflight/ast/Class.h"
#include "component/codeflight/ast/parents/nodePath.h"

namespace component::codeflight::artefact::package::content_dependencies
{


Factory::Factory(
    const ArtefactFactory& artefactFactory_,
    const ast::repository::Neighbor& neighbors_
    ) :
  artefactFactory{artefactFactory_},
  neighbors{neighbors_}
{
}

std::string Factory::printName() const
{
  return "Package content dependencies";
}

std::string Factory::artefactName() const
{
  return "content-dependencies";
}

std::unique_ptr<interface::Artefact> Factory::produce(const ast::Node* packages) const
{
  return std::make_unique<Artefact>(packages, artefactFactory, neighbors);
}

interface::ArtefactIdentifier Factory::identifier(const ast::Node* node) const
{
  return {ast::parents::nodePath(*node), artefactName()};
}


}

