/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/GraphArtefact.h"


namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::ast::repository
{

class Neighbor;

}

namespace component::codeflight::artefact::common::graph
{

class GraphModel;

}

namespace component::codeflight::artefact::package::content_dependencies
{

class ArtefactFactory;


class Artefact :
    public interface::GraphArtefact
{
  public:
    Artefact(
        const ast::Node*,
        const ArtefactFactory&,
        const ast::repository::Neighbor&
        );

    void build(interface::GraphBuilder&) const override;

  private:
    const ast::Node* node;
    const ArtefactFactory& artefactFactory;
    const ast::repository::Neighbor& neighbors;

    common::graph::GraphModel createGraph() const;
};


}
