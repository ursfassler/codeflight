/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "ArtefactFactory.h"
#include "component/codeflight/artefact/common/graph/GraphModel.h"
#include "component/codeflight/artefact/common/graph/draw.h"
#include "component/codeflight/artefact/interface/Factory.h"
#include "component/codeflight/artefact/interface/ListFactory.h"
#include "component/codeflight/artefact/interface/MaybeFactory.h"
#include "component/codeflight/ast/Node.h"
#include "component/codeflight/ast/parents/nodePath.h"
#include "component/codeflight/ast/parents/parents.h"
#include "component/codeflight/ast/repository/Neighbor.h"
#include "component/codeflight/ast/specification/Package.h"
#include "component/codeflight/ast/traverse/traverse.h"
#include "component/codeflight/library/OrderedSet.h"


namespace component::codeflight::artefact::package::content_dependencies
{
namespace
{


struct Sucessors {
  const ast::Node* child{};
  std::optional<const ast::Node*> grandchild{};
};

std::optional<Sucessors> findChildAndGrandchild(const ast::Node* node, const library::OrderedSet<const ast::Node*> &nodes)
{
  Sucessors result{};
  std::optional<const ast::Node*> itr = {node};
  while (itr) {
    if (result.child) {
      result.grandchild = result.child;
    }
    result.child = *itr;

    if (nodes.contains(*itr)) {
      return {result};
    }

    itr = ast::parents::findParent(*itr);
  }

  return {};
}

const ast::Node* getAncestorPackage(const ast::Node* node)
{
  auto ancestor = ast::parents::findAncestor(node, ast::specification::Package());
  if (!ancestor) {
    throw std::runtime_error("no parent package found for " + node->name);
  }
  return *ancestor;
}


}


Artefact::Artefact(
    const ast::Node* node_,
    const ArtefactFactory& artefactFactory_,
    const ast::repository::Neighbor& neighbors_
    ) :
  node{node_},
  artefactFactory{artefactFactory_},
  neighbors{neighbors_}
{
}

void forAllTargetAnchors(const ast::Node& node, const library::OrderedSet<const ast::Node*>& nodes, const std::function<void(const ast::Node*)>& visitor)
{
  const auto isInNodes = [&nodes](const ast::Node* node){
    return nodes.contains(node);
  };

  for (const auto target : node.references) {
    const auto p = ast::parents::findAncestor(target, isInNodes);

    if (p) {
      visitor(*p);
    }
  }
}

void Artefact::build(interface::GraphBuilder& builder) const
{
  auto pg = createGraph();
  common::graph::draw(pg, builder, {common::graph::NameType::Path, true});
}

common::graph::GraphModel Artefact::createGraph() const
{
  library::OrderedSet<const ast::Node*> nodes{};

  ast::traverse::children(*node, [&nodes](const ast::Node& child){
    nodes.add(&child);
  });


  using Edge = std::pair<const ast::Node *, const ast::Node *>;
  library::OrderedSet<Edge> edges{};
  std::map<Edge, std::size_t> edgeCount{};

  for (const auto sourceChild : nodes) {
    library::OrderedSet<Edge> grandChildrenEdges{};

    neighbors.foreachOutgoing(sourceChild, [&edges, &grandChildrenEdges, &nodes](const ast::Node* source, const ast::Node* destination){
      const auto sr = findChildAndGrandchild(source, nodes);
      const auto dr = findChildAndGrandchild(destination, nodes);
      if (sr && dr) {
        if (sr->child != dr->child) {
          Edge edge{sr->child, dr->child};
          edges.add(edge);

          if (sr->grandchild && dr->grandchild) {
            grandChildrenEdges.add({*sr->grandchild, *dr->grandchild});
          }
        }
      }
    });

    for (const auto& grandChildrenEdge : grandChildrenEdges) {
      const auto sourceChild = ast::parents::findParent(grandChildrenEdge.first);
      const auto destinationChild = ast::parents::findParent(grandChildrenEdge.second);
      Edge edge{*sourceChild, *destinationChild};
      edgeCount[edge]++;
    }
  }

  common::graph::GraphModel pg{ast::parents::nodePath(*node)};

  for (auto node : nodes) {
    pg.addNode(node);

    auto ai = artefactFactory.getOverviewFactory().identifier(node);
    if (ai) {
      pg.setLink(node, *ai);
    }
  }
  auto project = ast::parents::getProject(node);
  for (auto edge : edges) {
    if (ast::specification::Package()(edge.first) && ast::specification::Package()(edge.second)) {
      pg.setLabel(edge.first, edge.second, edgeCount.at(edge));
    }

    pg.addEdge(edge.first, edge.second);

    auto sourcePackage = getAncestorPackage(edge.first);
    auto destinationPackage = getAncestorPackage(edge.second);
    if (sourcePackage != destinationPackage) {
      auto sourcePath = ast::parents::nodePath(*sourcePackage);
      auto destinationPath = ast::parents::nodePath(*destinationPackage);
      const auto ai = artefactFactory.getDependenciesBetweenPackagesFactory().identifier(project, {sourcePath, destinationPath});
      pg.setLink(edge, ai);
    }
  }

  return pg;
}


}
