/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "component/codeflight/artefact/common/graph/GraphArtefact.h"
#include "component/codeflight/artefact/interface/Factory.h"
#include "component/codeflight/ast/Class.h"
#include "component/codeflight/ast/graph/graph.h"
#include "component/codeflight/ast/specification/All.h"


namespace component::codeflight::artefact::class_::intra_dependencies
{


Artefact::Artefact(
    const ast::Node* clazz_,
    const common::graph::GraphArtefact& graph_
    ) :
  clazz{clazz_},
  graph{graph_}
{
}

void Artefact::build(interface::GraphBuilder& builder) const
{
  graph.build(clazz, ast::graph::buildDirectChildrenGraph(*clazz, ast::specification::All()), builder);
}


}
