/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/GraphArtefact.h"


namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::common::graph
{

class GraphArtefact;

}

namespace component::codeflight::artefact::class_::intra_dependencies
{


class Artefact :
    public interface::GraphArtefact
{
  public:
    Artefact(
        const ast::Node*,
        const common::graph::GraphArtefact&
        );

    void build(interface::GraphBuilder&) const override;

  private:
    const ast::Node* clazz;
    const common::graph::GraphArtefact& graph;
};


}
