/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "Artefact.h"
#include "component/codeflight/ast/parents/nodePath.h"


namespace component::codeflight::artefact::class_::intra_dependencies
{


Factory::Factory(
    const common::graph::GraphArtefact& graph_
    ) :
  graph{graph_}
{
}

std::string Factory::printName() const
{
  return "Member dependencies";
}

std::string Factory::artefactName() const
{
  return "intra-dependencies";
}

std::unique_ptr<interface::Artefact> Factory::produce(const ast::Node* clazz) const
{
  return std::make_unique<Artefact>(clazz, graph);
}

interface::ArtefactIdentifier Factory::identifier(const ast::Node* node) const
{
  return {ast::parents::nodePath(*node), artefactName()};
}


}
