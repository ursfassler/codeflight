/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/NodeFactory.h"
#include <memory>
#include <string>

namespace component::codeflight::ast
{

class Class;

}

namespace component::codeflight::artefact::common::graph
{

class GraphArtefact;

}


namespace component::codeflight::artefact::class_::intra_dependencies
{


class Factory :
    public interface::NodeFactory
{
public:
  Factory(
      const common::graph::GraphArtefact&
      );

  std::string printName() const override;
  std::string artefactName() const override;
  std::unique_ptr<interface::Artefact> produce(const ast::Node*) const override;
  interface::ArtefactIdentifier identifier(const ast::Node*) const override;

private:
  const common::graph::GraphArtefact& graph;
};


}

