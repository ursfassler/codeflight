/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "Artefact.h"
#include "component/codeflight/ast/parents/nodePath.h"


namespace component::codeflight::artefact::class_::overview
{


Factory::Factory(
    const ArtefactFactory& artefactFactory_,
    const common::document::Overview& overview_
    ) :
  artefactFactory{artefactFactory_},
  overview{overview_}
{
}

std::string Factory::printName() const
{
  return "Class overview";
}

std::string Factory::artefactName() const
{
  return "index";
}

std::unique_ptr<interface::Artefact> Factory::produce(const ast::Node* clazz) const
{
  return std::make_unique<Artefact>(clazz, artefactFactory, overview);
}

interface::ArtefactIdentifier Factory::identifier(const ast::Node* node) const
{
  return {ast::parents::nodePath(*node), artefactName()};
}


}
