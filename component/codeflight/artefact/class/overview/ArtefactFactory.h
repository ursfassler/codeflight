/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace component::codeflight::artefact::interface
{

class NodeFactory;

}

namespace component::codeflight::artefact::class_::overview
{


class ArtefactFactory
{
public:
  virtual ~ArtefactFactory() = default;

  virtual const interface::NodeFactory& getClassIntradependenciesFactory() const = 0;
  virtual const interface::NodeFactory& getClassNeighborsFactory() const = 0;

};


}
