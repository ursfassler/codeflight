/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Codeflight.h"
#include "Namer.h"
#include "component/codeflight/ast/Project.h"


namespace component::codeflight
{
namespace
{


void cleanup(ast::Project* project)
{
  Namer namer{};
  namer.name(project);
}


}


std::unique_ptr<Codeflight> Codeflight::produce(
  std::unique_ptr<ast::Project>& project,
  const artefact::common::package_cycles::Settings& cycleSettings,
  const std::optional<const MetricRepoFactory>& metricRepoFactory
)
{
  cleanup(project.get());

  auto neighbors = std::make_unique<ast::repository::Neighbor>();
  neighbors->build(project.get());
  auto metricRepo = metricRepoFactory ? (*metricRepoFactory)() : std::make_unique<metric::AllRepository>(*neighbors);

  return std::make_unique<Codeflight>(
               project,
               cycleSettings,
               neighbors,
               metricRepo
  );
}

Codeflight::Codeflight(
    std::unique_ptr<ast::Project>& project_,
    const artefact::common::package_cycles::Settings& cycleSettings_,
    std::unique_ptr<ast::repository::Neighbor>& neighbors_,
    std::unique_ptr<metric::Repository>& metricRepo_
    ) :
  project{std::move(project_)},
  cycleSettings{cycleSettings_},
  neighbors{std::move(neighbors_)},
  metricRepo{std::move(metricRepo_)},
  factory{*neighbors, *metricRepo, cycleSettings}
{
}

artefact::interface::ArtefactIdentifier Codeflight::rootAi() const
{
  return factory.getProjectOverviewFactory().identifier(project.get());
}


}
