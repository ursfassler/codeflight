/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/common/package-cycles/Settings.h"
#include "component/codeflight/artefact/factory/Factory.h"
#include "component/codeflight/ast/Project.h"
#include "component/codeflight/ast/repository/Neighbor.h"
#include "component/codeflight/metric/AllRepository.h"
#include <memory>

namespace component::codeflight::artefact::common::package_cycles
{

struct Settings;

}

namespace component::codeflight
{


using MetricRepoFactory = std::function<std::unique_ptr<metric::Repository>()>;

class Codeflight
{
public:
  static std::unique_ptr<Codeflight> produce(
      std::unique_ptr<ast::Project>&,
      const artefact::common::package_cycles::Settings&,
      const std::optional<const MetricRepoFactory>&
      );

  Codeflight(
      std::unique_ptr<ast::Project>&,
      const artefact::common::package_cycles::Settings&,
      std::unique_ptr<ast::repository::Neighbor>&,
      std::unique_ptr<metric::Repository>&
      );

  const ast::Project* getProject()
  {
    return project.get();
  }

  const artefact::factory::Factory& getFactory()
  {
    return factory;
  }

  const ast::repository::Neighbor& getNeighbors()
  {
    return *neighbors;
  }

  artefact::interface::ArtefactIdentifier rootAi() const;

private:
  std::unique_ptr<ast::Project> project;
  const artefact::common::package_cycles::Settings& cycleSettings;

  std::unique_ptr<ast::repository::Neighbor> neighbors{};

  std::unique_ptr<metric::Repository> metricRepo{};
  artefact::factory::Factory factory;

};


}
