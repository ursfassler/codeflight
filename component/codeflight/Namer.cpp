/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Namer.h"
#include "component/codeflight/ast/Node.h"
#include "component/codeflight/ast/Project.h"
#include <algorithm>
#include <functional>


namespace component::codeflight
{


void Namer::name(ast::Project* root)
{
  std::for_each(root->children.begin(), root->children.end(), std::bind(&Namer::nameChildren, this, std::placeholders::_1));
}

void Namer::nameChildren(std::unique_ptr<ast::Node>& node)
{
  std::for_each(node->children.begin(), node->children.end(), std::bind(&Namer::recursive, this, std::placeholders::_1));
}

void Namer::recursive(std::unique_ptr<ast::Node>& node)
{
  if (node->name == "") {
    unnamedNumber++;
    std::string name = "-unnamed" + std::to_string(unnamedNumber) + "-";
    node->name = name;
  }

  nameChildren(node);
}


}
