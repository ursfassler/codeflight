/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/generator/Generator.h"
#include <optional>

namespace component::web::library
{

class Url;

}

namespace component::web::resource
{

class Factory;

}

namespace component::web::generator
{

class WebHandler;


class WebGenerator
{
public:
  WebGenerator(
      const resource::Factory&,
      const codeflight::artefact::generator::Generator&,
      WebHandler&
      );

  bool maybeGenerate(const library::Url&) const;
  bool maybeGenerate(const codeflight::artefact::interface::ArtefactIdentifier&) const;

private:
  const resource::Factory& resources;
  const codeflight::artefact::generator::Generator& generator;
  WebHandler& handler;

  std::optional<codeflight::artefact::interface::ArtefactIdentifier> createAi(const library::Url&) const;
  bool generateResource(const library::Url&) const;
};


}
