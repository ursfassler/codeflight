/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace component::codeflight::artefact::interface
{

class Artefact;
struct ArtefactIdentifier;

}

namespace component::web::resource
{

class Artefact;

}

namespace component::web::generator
{


class WebHandler
{
public:
  virtual ~WebHandler() = default;

  virtual void resource(const resource::Artefact&) = 0;
  virtual void handle(const codeflight::artefact::interface::ArtefactIdentifier&, const codeflight::artefact::interface::Artefact&) = 0;

};


}
