/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "WebGenerator.h"
#include "WebHandler.h"
#include "component/codeflight/artefact/interface/Artefact.h"
#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"
#include "component/web/library/pathUtil.h"
#include "component/web/resource/Artefact.h"
#include "component/web/resource/Factory.h"


namespace component::web::generator
{


WebGenerator::WebGenerator(
    const resource::Factory& resources_,
    const codeflight::artefact::generator::Generator& generator_,
    WebHandler& handler_
    ) :
  resources{resources_},
  generator{generator_},
  handler{handler_}
{
}

bool WebGenerator::maybeGenerate(const library::Url &url) const
{
  const auto resource = generateResource(url);
  if (resource) {
    return true;
  }

  auto ai = createAi(url);
  if (!ai) {
    return false;
  }

  return maybeGenerate(*ai);
}

bool WebGenerator::maybeGenerate(const codeflight::artefact::interface::ArtefactIdentifier& ai) const
{
  auto artefact = generator.getMaybeArtefact(ai);

  if (artefact) {
    handler.handle(ai, *artefact);
  }

  return !!artefact;
}

bool WebGenerator::generateResource(const library::Url& url) const
{
  auto artefact = resources.produce(url);
  if (artefact) {
    handler.resource(*artefact);
  }
  return !!artefact;
}

std::optional<codeflight::artefact::interface::ArtefactIdentifier> WebGenerator::createAi(const library::Url& url) const
{
  auto path = url.getPath();
  if (path.empty()) {
    return {};
  }
  if (path.front() == "-") {
    path.front() = "";
  }

  if (path.front() == "") {
    auto name = path.back();
    path.pop_back();

    return {{path, name}};
  } else {
    auto name = path.front();
    path = {path.begin()+1, path.end()};

    if (path.size() > 1) {
      throw std::invalid_argument("expected exactly 0 or 1 argument, got " + std::to_string(path.size()));
    }
    const auto paths = path.empty() ? codeflight::library::Paths{} : library::decodePathList(path.front());

    return {{{}, name, paths}};
  }
}


}
