/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <filesystem>
#include <functional>
#include <istream>
#include <set>
#include <string>


namespace component::web::io
{


class ResourceFilesystem
{
public:
  virtual ~ResourceFilesystem() = default;

  virtual void read(const std::filesystem::path&, const std::function<void(std::istream&)>&) const = 0;
  virtual std::set<std::filesystem::path> files(const std::filesystem::path&) const = 0;

};


}
