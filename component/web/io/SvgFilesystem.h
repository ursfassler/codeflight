/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <filesystem>
#include <functional>
#include <string>

namespace component::web::io
{


class SvgFilesystem
{
public:
  virtual ~SvgFilesystem() = default;

  virtual void write(const std::filesystem::path&, const std::function<void(std::ostream&)>&) = 0;
  virtual bool fileExists(const std::filesystem::path&) const = 0;
  virtual void read(const std::filesystem::path&, const std::function<void(std::istream&)>&) const = 0;

};


}
