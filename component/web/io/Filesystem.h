/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ResourceFilesystem.h"
#include "SvgFilesystem.h"

namespace component::web::io
{


class Filesystem :
    public ResourceFilesystem,
    public SvgFilesystem
{
};


}
