/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace component::web::io
{


class Process
{
public:
  virtual ~Process() = default;

  virtual void execute(const std::string&) = 0;

};


}
