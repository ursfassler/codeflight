/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <ostream>


namespace component::web::xml
{


class Writer
{
public:
  virtual ~Writer() = default;

  virtual void text(const std::string&) = 0;
  virtual void attribute(const std::string& name, const std::string& value) = 0;
  virtual void beginNode(const std::string&) = 0;
  virtual void endNode() = 0;

  virtual void serialize(std::ostream&) const = 0;
};

class WriterFactory
{
public:
  virtual ~WriterFactory() = default;

  virtual std::unique_ptr<Writer> produce() const = 0;
};


}
