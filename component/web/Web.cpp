/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Web.h"
#include "component/codeflight/Codeflight.h"
#include "component/web/io/Filesystem.h"
#include "component/web/library/aiString.h"
#include "component/web/library/urlFromAi.h"
#include "component/web/resource/find.h"

namespace component::web
{


std::unique_ptr<Web> Web::produce(
  codeflight::Codeflight& codeflight,
  const xml::WriterFactory& xmlFactory,
  const std::optional<std::filesystem::path>& resourcesDirectory,
  std::filesystem::path outdir,
  io::Filesystem& fs,
  io::Process& process
)
{
  const auto resources = resource::produceFactory(resourcesDirectory, fs);
  return std::make_unique<Web>(
        codeflight,
        xmlFactory,
        resources,
        outdir,
        fs,
        process
        );
}

Web::Web(
    codeflight::Codeflight& codeflight_,
    const xml::WriterFactory& xmlFactory,
    const resource::Factory resources_,
    std::filesystem::path outdir,
    io::Filesystem& fs,
    io::Process& process
    ) :
  codeflight{codeflight_},
  env{codeflight.getProject()->language, outdir, fs, process},
  resources{resources_},
  generator{codeflight.getProject(), codeflight.getFactory()},
  typeQuery{generator},
  htmlFactory{xmlFactory, codeflight.getFactory().getOverviewFactory(), resources, typeQuery, env},
  writer{htmlFactory, typeQuery, env},
  webGenerator{resources, generator, writer}
{
}

void Web::addLinkHook(library::LinkHook* value)
{
  writer.addLinkHook(value);
}

library::Url Web::rootUrl() const
{
  auto root = codeflight.rootAi();

  auto type = typeQuery.getType(root);
  if (!type) {
    throw std::runtime_error("unknown root artefact: " + library::aiString(root));
  }

  return library::urlFromAi(codeflight.rootAi(), *type);
}


}
