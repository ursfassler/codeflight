/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"
#include "ArtefactType.h"
#include "component/web/library/Url.h"

namespace component::web::library
{


Url urlFromAi(const codeflight::artefact::interface::ArtefactIdentifier&, ArtefactType);


}
