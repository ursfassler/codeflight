/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace component::web::library
{


enum class ArtefactType
{
  Document,
  Graph,
};


}
