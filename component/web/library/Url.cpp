/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Url.h"
#include "component/codeflight/library/util.h"
#include <algorithm>
#include <cassert>
#include <iomanip>


namespace component::web::library
{
namespace
{


const std::string DirectorySeparator = "/";

bool isValidUrlCharacter(char c)
{
  return isalnum(c) || (c == '-') || (c == '_') || (c == '.') || (c == '~') || (c == '!') || (c == ',');
}

std::string to_hex(char value)
{
  std::stringstream ss;
  ss << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << (int)value;
  return ss.str();
}

std::string escape(const std::string& value)
{
  std::string result{};

  for (const auto& sym : value) {
    if (isValidUrlCharacter(sym)) {
      result += sym;
    } else {
      result += "%" + to_hex(sym);
    }
  }

  return result;
}

std::optional<uint_least8_t> parseHexDigit(char value)
{
  char letter = std::tolower(value);

  if (('0' <= letter) && (letter <= '9')) {
    return letter - '0';
  }

  if (('a' <= letter) && (letter <= 'f')) {
    return letter - 'a' + 10;
  }

  return {};
}

std::optional<char> to_char(const std::string& value)
{
  if (value.length() != 2) {
    return {};
  }

  char result = 0;

  for (char sym : value)   {
    const auto digit = parseHexDigit(sym);
    if (!digit){
      return {};
    }
    result = (result << 4) | *digit;
  }

  return { result };
}

std::optional<std::string> unescape(const std::string& value)
{
  constexpr auto HexStringSize = 2;

  std::string result{};

  std::size_t start = 0;

  while (true) {
    const auto end = value.find('%', start);
    if (end == std::string::npos) {
      break;
    }

    result += value.substr(start, end - start);

    const auto sym = to_char(value.substr(end + 1, HexStringSize));
    if (!sym) {
      return {};
    }
    result += *sym;

    start = end + 1 + HexStringSize;
  }

  result += value.substr(start);

  return { result };
}

Url::Path escapePath(const Url::Path& value)
{
  Url::Path result{};
  std::transform(value.cbegin(), value.cend(), std::back_inserter(result), escape);
  return result;
}

std::optional<Url> parseEscaped(const std::string& value)
{
  const std::optional<Url> EmptyUrlResult = { Url{} };
  const std::optional<Url> ErrorResult = {};

  auto split = codeflight::library::split(value, "/");
  std::vector<std::string> path{};

  for (const auto& part : split) {
    if (part != "") {
      path.push_back(part);
    }
  }

  if (path.empty()) {
    return EmptyUrlResult;
  }

  std::string& name = path.back();
  const auto sep = name.find_last_of('.');
  std::string ext{};
  if (sep != std::string::npos) {
    ext = name.substr(sep);
    name.erase(sep);
  }

  for (auto& name : path) {
    const auto part = unescape(name);
    if (!part) {
      return ErrorResult;
    }
    name = *part;
  }

  return { { path, ext } };
}


}


Url::Url(const Path& path_, const std::string& representation_) :
  path{path_},
  representation{representation_}
{
}

std::optional<Url> Url::parse(const std::string& value)
{
  return parseEscaped(value);
}

Url Url::fromFilename(const std::filesystem::path& value)
{
  std::vector<std::string> path{};
  for (const auto& element : value.parent_path()) {
    path.push_back(element);
  }
  path.push_back(value.stem());

  std::string extension = value.extension();

  return {path, extension};
}

std::string Url::escaped() const
{
  return codeflight::library::join(escapePath(path), DirectorySeparator) + representation;
}

std::filesystem::path Url::filename() const
{
  std::filesystem::path result{};
  for (const auto& part : path) {
    result /= part;
  }
  result += representation;
  return result;
}

std::string Url::getRepresentation() const
{
  return representation;
}

Url::Path Url::getPath() const
{
  return path;
}

Url Url::append(const std::string& part) const
{
  Path np = path;
  np.push_back(part);
  return {np, representation};
}

Url Url::asRepresentation(const std::string& representation) const
{
  return {path, representation};
}

Url Url::relativeTo(const Url& root) const
{
  //TODO cleanup
  const auto rootPath = root.filename().parent_path();
  const auto linkFile = filename();
  const auto relativeFile = linkFile.lexically_relative(rootPath).replace_extension("");

  Path path{};
  for (const auto& part : relativeFile) {
    path.push_back(part);
  }

  return {path, representation};
}

bool Url::operator<(const Url& rhs) const
{
  if (path == rhs.path) {
    return representation < rhs.representation;
  } else {
    return path < rhs.path;
  }
}

bool Url::operator==(const Url& rhs) const
{
  return !(*this < rhs) && !(rhs < *this);
}


}
