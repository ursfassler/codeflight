/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ArtefactTypeQuery.h"
#include "component/codeflight/artefact/generator/Generator.h"
#include "component/codeflight/artefact/interface/Artefact.h"
#include "component/codeflight/artefact/interface/ArtefactVisitor.h"


namespace component::web::library
{
namespace
{


class TypeVisitor :
    public codeflight::artefact::interface::ArtefactVisitor
{
public:
  void visit(const codeflight::artefact::interface::GraphArtefact&) override
  {
    type = ArtefactType::Graph;
  }

  void visit(const codeflight::artefact::interface::DocumentArtefact&) override
  {
    type = ArtefactType::Document;
  }

  std::optional<ArtefactType> type{};
};


}


ArtefactTypeQuery::ArtefactTypeQuery(
    const codeflight::artefact::generator::Generator& generator_
    ) :
  generator{generator_}
{
}

std::optional<ArtefactType> ArtefactTypeQuery::getType(const codeflight::artefact::interface::ArtefactIdentifier& ai) const
{
  auto artefact = generator.getMaybeArtefact(ai); //TODO find way with less overhead

  if (artefact) {
    TypeVisitor visitor{};
    artefact->accept(visitor);
    return visitor.type;
  } else {
    return {};
  }
}


}
