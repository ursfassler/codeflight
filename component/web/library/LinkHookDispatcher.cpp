/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "LinkHookDispatcher.h"

namespace component::web::library
{


void LinkHookDispatcher::referenced(const codeflight::artefact::interface::ArtefactIdentifier& value)
{
  for (auto hook : hooks) {
    hook->referenced(value);
  }
}

void LinkHookDispatcher::add(LinkHook* value)
{
  hooks.push_back(value);
}


}
