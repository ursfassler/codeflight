/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "WebEnvironment.h"
#include "component/codeflight/ast/name/formatName.h"
#include "component/web/io/Filesystem.h"
#include "component/web/library/pathUtil.h"
#include <map>

namespace component::web::library
{
namespace
{


const std::map<std::string, std::string> DisplaySeparator
{
  {"C++", "::"},
  {"Python", "."},
  {"TypeScript", "/"},
};

std::string getDisplaySeparator(const std::string& language)
{
  const auto idx = DisplaySeparator.find(language);
  if (idx == DisplaySeparator.end()) {
    return DisplaySeparator.at("C++");
  }
  return idx->second;
}


}


WebEnvironment::WebEnvironment(
    std::string language,
    const std::filesystem::path& outdir,
    io::Filesystem& fs_,
    io::Process& process_
    ) :
  displaySeparator{getDisplaySeparator(language)},
  rootDirectory{outdir},
  fs{fs_},
  process{process_}
{
}

std::string WebEnvironment::formatPath(const codeflight::library::Path& path) const
{
  return library::formatPath(path, displaySeparator);
}

std::string WebEnvironment::formatName(const codeflight::ast::Node* node) const
{
  return codeflight::ast::name::formatName(node, displaySeparator);
}

std::filesystem::path WebEnvironment::getRootDirectory() const
{
  return rootDirectory;
}

io::SvgFilesystem& WebEnvironment::getSvgFilesystem() const
{
  return fs;
}

io::ResourceFilesystem& WebEnvironment::getResourceFilesystem() const
{
  return fs;
}

io::Process& WebEnvironment::getProcess() const
{
  return process;
}


}
