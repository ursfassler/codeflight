/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <filesystem>
#include <string>

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::library
{

class Path;

}

namespace component::web::io
{

class Process;
class Filesystem;
class SvgFilesystem;
class ResourceFilesystem;

}

namespace component::web::library
{


class WebEnvironment
{
public:
  WebEnvironment(
      std::string language,
      const std::filesystem::path& outdir,
      io::Filesystem&,
      io::Process&
      );

  std::string formatPath(const codeflight::library::Path&) const;
  std::string formatName(const codeflight::ast::Node*) const;
  std::filesystem::path getRootDirectory() const;
  io::SvgFilesystem& getSvgFilesystem() const;
  io::ResourceFilesystem& getResourceFilesystem() const;
  io::Process& getProcess() const;

private:
  std::string displaySeparator;
  const std::filesystem::path rootDirectory;
  io::Filesystem& fs;
  io::Process& process;
};


}
