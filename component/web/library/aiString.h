/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace component::codeflight::artefact::interface
{

struct ArtefactIdentifier;

}

namespace component::web::library
{


std::string aiString(const codeflight::artefact::interface::ArtefactIdentifier&);


}
