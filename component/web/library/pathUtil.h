/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/library/Path.h"
#include <vector>

namespace component::web::library
{


std::vector<std::string> notEmptyPath(const codeflight::library::Path&);

std::string formatPath(const codeflight::library::Path&, const std::string& separator);

std::string encodePathList(const codeflight::library::Paths&);
codeflight::library::Paths decodePathList(const std::string&);


}
