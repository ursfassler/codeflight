/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Url.h"
#include <gmock/gmock.h>


namespace component::web::library::unit_test
{
namespace
{

using namespace testing;


TEST(Url_Test, is_equal_to_itself)
{
  const Url url{{"path", "to", "file"}, ".html"};

  ASSERT_TRUE(url == url);
}

TEST(Url_Test, is_equal_to_other_url_with_same_content)
{
  const Url url1{{"path", "to", "file"}, ".html"};
  const Url url2{{"path", "to", "file"}, ".html"};

  ASSERT_TRUE(url1 == url2);
}

TEST(Url_Test, is_not_equal_when_path_differs_in_length)
{
  const Url url1{{"path", "to", "file"}, ".html"};
  const Url url2{{"path", "to"}, ".html"};

  ASSERT_FALSE(url1 == url2);
}

TEST(Url_Test, is_not_equal_when_path_differs_in_content)
{
  const Url url1{{"path", "to", "file"}, ".html"};
  const Url url2{{"path", "from", "file"}, ".html"};

  ASSERT_FALSE(url1 == url2);
}

TEST(Url_Test, is_not_equal_when_representation_is_different)
{
  const Url url1{{"path", "to", "file"}, ".html"};
  const Url url2{{"path", "to", "file"}, ".svg"};

  ASSERT_FALSE(url1 == url2);
}

TEST(Url_Test, parse_url)
{
  const Url url1 = *Url::parse("path/to/file.html");
  const Url url2{{"path", "to", "file"}, ".html"};

  ASSERT_TRUE(url1 == url2);
}

TEST(Url_Test, remove_leading_slash_when_parsing_url)
{
  const Url url1 = *Url::parse("/path/to/file.html");
  const Url url2{{"path", "to", "file"}, ".html"};

  ASSERT_TRUE(url1 == url2);
}

TEST(Url_Test, remove_multiple_slash_when_parsing_url)
{
  const Url url1 = *Url::parse("path//to///file.html");
  const Url url2{{"path", "to", "file"}, ".html"};

  ASSERT_TRUE(url1 == url2);
}

TEST(Url_Test, parse_encoded_character)
{
  const Url url = *Url::parse("/ab%3Ccd.html");

  ASSERT_EQ("ab<cd", url.getPath().at(0));
}

TEST(Url_Test, parse_multiple_encoded_characters)
{
  const Url url = *Url::parse("/%3C%3E%20%3A%26%28%29.html");

  ASSERT_EQ("<> :&()", url.getPath().at(0));
}

TEST(Url_Test, parse_encoded_and_unencoded_characters)
{
  const Url url = *Url::parse("/x%3Cy%3ez.html");

  ASSERT_EQ("x<y>z", url.getPath().at(0));
}

TEST(Url_Test, reports_error_when_encoded_value_is_too_short)
{
  const auto url = Url::parse("/x%3.html");

  ASSERT_FALSE(url);
}

TEST(Url_Test, reports_error_when_encoded_value_contains_wrong_value_on_first_position)
{
  const auto url = Url::parse("/x%Q1y.html");

  ASSERT_FALSE(url);
}

TEST(Url_Test, reports_error_when_encoded_value_contains_wrong_value_on_second_position)
{
  const auto url = Url::parse("/x%0Iy.html");

  ASSERT_FALSE(url);
}

TEST(Url_Test, from_filename)
{
  const Url url1 = Url::fromFilename("hello/world.html");
  const Url url2{{"hello", "world"}, ".html"};

  ASSERT_EQ(url1, url2);
}

TEST(Url_Test, correct_name_when_returning_as_filename_with_dot_in_name)
{
  const Url url{{"file.name"}, ".html"};

  const auto actual = url.filename();

  ASSERT_EQ(actual.string(), "file.name.html");
}

TEST(Url_Test, correct_extension_when_returning_as_filename_with_dot_in_name)
{
  const Url url{{"file.name"}, ".html"};

  const auto actual = url.filename().extension();

  ASSERT_EQ(actual.string(), ".html");
}


}
}
