/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "aiString.h"
#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"

namespace component::web::library
{
namespace
{


std::string to_string(const codeflight::library::Path& value)
{
  std::string result{};

  for (const auto& part : value.raw()) {
    result += "/" + part;
  }

  return result;
}


}


std::string aiString(const codeflight::artefact::interface::ArtefactIdentifier& value)
{
  std::string result{};

  result += to_string(value.path);

  result += ":" + value.artefact;

  if (!value.arguments.empty()) {
    for (const auto& path : value.arguments) {
      result += ":" + to_string(path);
    }
  }

  return result;
}


}
