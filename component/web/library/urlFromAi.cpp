/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "urlFromAi.h"
#include "pathUtil.h"

namespace component::web::library
{
namespace
{


std::string repr(ArtefactType type)
{
  switch (type) {
    case ArtefactType::Document:
      return ".html";
    case ArtefactType::Graph:
      return ".svg";
  }

  return ".unknown";
}


}


Url urlFromAi(const codeflight::artefact::interface::ArtefactIdentifier& ai, ArtefactType type)
{
  auto path = notEmptyPath(ai.path.add(ai.artefact));

  const std::string representation = repr(type);
  if (!ai.arguments.empty()) {
    const auto arguments = encodePathList(ai.arguments);
    path.push_back(arguments);
  }

  const Url url = {path, representation};
  return url;
}


}
