/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "ArtefactType.h"
#include <optional>

namespace component::codeflight::artefact::interface
{

struct ArtefactIdentifier;

}

namespace component::codeflight::artefact::generator
{

class Generator;

}

namespace component::web::library
{


class ArtefactTypeQuery
{
public:
  ArtefactTypeQuery(
      const codeflight::artefact::generator::Generator&
      );

  std::optional<ArtefactType> getType(const codeflight::artefact::interface::ArtefactIdentifier&) const;

private:
  const codeflight::artefact::generator::Generator& generator;
};

}
