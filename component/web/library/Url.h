/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <filesystem>
#include <optional>
#include <string>
#include <vector>


namespace component::web::library
{


class Url
{
  public:
    using Path = std::vector<std::string>;

    Url() = default;
    Url(const Path& name, const std::string& representation);

    static std::optional<Url> parse(const std::string&);
    static Url fromFilename(const std::filesystem::path&);

    std::string escaped() const;
    std::filesystem::path filename() const;
    std::string getRepresentation() const;
    Path getPath() const;

    Url append(const std::string&) const;
    Url asRepresentation(const std::string&) const;
    Url relativeTo(const Url&) const;

    bool operator<(const Url&) const;
    bool operator==(const Url&) const;

  private:
    Path path{};
    std::string representation{};

};


}
