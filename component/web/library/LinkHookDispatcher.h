/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "LinkHook.h"
#include <vector>

namespace component::web::library
{


class LinkHookDispatcher :
    public LinkHook
{
public:
  void referenced(const codeflight::artefact::interface::ArtefactIdentifier& value) override;

  void add(library::LinkHook* value);

private:
  std::vector<library::LinkHook*> hooks{};
};


}
