/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace component::codeflight::artefact::interface
{

struct ArtefactIdentifier;

}

namespace component::web::library
{


class LinkHook
{
public:
  virtual ~LinkHook() = default;

  virtual void referenced(const codeflight::artefact::interface::ArtefactIdentifier&) = 0;
};


}
