/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "pathUtil.h"
#include "component/codeflight/library/util.h"


namespace component::web::library
{
namespace
{


const auto EncodeSeperator = "~";
const auto ListSeparator = ",";
const std::string Prefix = "-";


std::vector<std::string> decode(const std::string& encoded)
{
  auto result = codeflight::library::split(encoded, EncodeSeperator);
  if ((result.size() >= Prefix.size()) && (result.front() == Prefix)) {
    result.front() = "";
  }
  return result;
}

std::string encodedPath(const codeflight::library::Path& path)
{
  auto result = codeflight::library::join(notEmptyPath(path), EncodeSeperator);
  return result;
}


}


std::vector<std::string> notEmptyPath(const codeflight::library::Path& path)
{
  auto result = path.raw();
  if (!result.empty() && (result.front() == "")) {
    result.front() = Prefix;
  }
  return result;
}

std::string formatPath(const codeflight::library::Path& path, const std::string& separator)
{
  const auto raw = path.raw();
  const auto size = raw.size();
  if (size == 0) {
    return separator;
  }
  if ((size >= 1) && (raw.front() == "")) {
    if (size == 1) {
      return separator;
    } else {
      return codeflight::library::join({raw.begin()+1, raw.end()}, separator);
    }
  }
  return codeflight::library::join(raw, separator);
}

std::string encodePathList(const codeflight::library::Paths& value)
{
  std::vector<std::string> encoded{};

  for (const auto& path : value) {
    const auto enc = encodedPath(path);
    encoded.push_back(enc);
  }

  auto result = codeflight::library::join(encoded, ListSeparator);
  return result;
}

codeflight::library::Paths decodePathList(const std::string& value)
{
  const auto parts = codeflight::library::split(value, ListSeparator);

  codeflight::library::Paths result{};
  for (const auto& part : parts) {
    const auto path = codeflight::library::Path{decode(part)};
    result.push_back(path);
  }

  return result;
}


}
