/*
 * (C) Copyright 2021 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/web/resource/Factory.h"
#include "component/web/generator/WebGenerator.h"
#include "component/web/library/ArtefactTypeQuery.h"
#include "component/web/library/WebEnvironment.h"
#include "component/web/output/WebWriter.h"
#include "component/web/output/html/WriterFactory.h"
#include <filesystem>
#include <memory>
#include <optional>

namespace component::codeflight
{

class Codeflight;

}

namespace component::web::io
{

class Filesystem;
class Process;

}

namespace component::web::xml
{

class WriterFactory;

}

namespace component::web
{


class Web
{
public:
  static std::unique_ptr<Web> produce(
      codeflight::Codeflight&,
      const xml::WriterFactory&,
      const std::optional<std::filesystem::path>& resourcesDirectory,
      std::filesystem::path outdir,
      io::Filesystem&,
      io::Process&
      );

  Web(
      codeflight::Codeflight&,
      const xml::WriterFactory&,
      const resource::Factory,
      std::filesystem::path outdir,
      io::Filesystem&,
      io::Process&
      );

  const resource::Factory& getResources()
  {
    return resources;
  }

  output::WebWriter& getWriter()
  {
    return writer;
  }

  const output::html::WriterFactory& getWriterFactory()
  {
    return htmlFactory;
  }

  generator::WebGenerator& getWebGenerator()
  {
    return webGenerator;
  }

  void addLinkHook(library::LinkHook*);

  library::Url rootUrl() const;

private:
  codeflight::Codeflight& codeflight;
  library::WebEnvironment env;
  const resource::Factory resources;
  const codeflight::artefact::generator::Generator generator;
  library::ArtefactTypeQuery typeQuery;
  const output::html::WriterFactory htmlFactory;
  output::WebWriter writer;
  generator::WebGenerator webGenerator;

};


}
