/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "generateGraphvizSvg.h"
#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"
#include "component/web/io/Process.h"
#include "component/web/io/SvgFilesystem.h"
#include "component/web/library/Url.h"
#include "component/web/library/WebEnvironment.h"
#include <cassert>


namespace component::web::output::svg
{
namespace
{


bool shouldGenerate(const std::string& svgName, const std::string& graphvizName, const std::string& graph, io::SvgFilesystem& fs)
{
  const auto svgExists = fs.fileExists(svgName);
  if (!svgExists) {
    return true;
  }

  const auto graphvizExists = fs.fileExists(graphvizName);
  if (!graphvizExists) {
    return true;
  }

  std::string existing;
  fs.read(graphvizName, [&existing](std::istream& stream){
    std::string input((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
    existing = input;
  });

  if (existing != graph) {
    return true;
  }

  return false;
}


}


void generateGraphvizSvg(const library::Url& name, const std::string &graph, const library::WebEnvironment& env)
{
  assert(name.getRepresentation() == ".svg");

  const auto graphvizName = env.getRootDirectory() / name.asRepresentation(".gv").filename();
  const auto svgName = env.getRootDirectory() / name.filename();

  io::SvgFilesystem& fs = env.getSvgFilesystem();
  io::Process& process = env.getProcess();
  if (shouldGenerate(svgName, graphvizName, graph, fs)) {
    fs.write(graphvizName, [&graph](std::ostream& stream){
      stream << graph;
    });
    process.execute("dot -T svg -o \"" + svgName.string() + "\" \"" + graphvizName.string() + "\"");
  }
}


}
