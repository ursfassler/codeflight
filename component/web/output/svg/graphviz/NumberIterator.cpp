/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "NumberIterator.h"


namespace component::web::output::svg::graphviz
{


NumberIterator::NumberIterator()
{
  reset();
}

void NumberIterator::reset()
{
  actual = maximum();
}

void NumberIterator::next()
{
  actual = (actual == maximum()) ? 0 : (actual + 1);
}

unsigned NumberIterator::current() const
{
  return actual;
}

unsigned NumberIterator::maximum() const
{
  return std::numeric_limits<decltype(actual)>::max();
}


}
