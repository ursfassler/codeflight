/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "StreamWriter.h"


namespace component::web::output::svg::graphviz
{


StreamWriter::StreamWriter(std::ostream& output_) :
  output{output_}
{
}

void StreamWriter::inc()
{
  level++;
}

void StreamWriter::dec()
{
  level--;
}

void StreamWriter::write(const std::string &value)
{
  output << indent(level);
  output << value;
  output << std::endl;
}

std::string StreamWriter::indent(unsigned level)
{
  return std::string(4*level, ' ');
}


}
