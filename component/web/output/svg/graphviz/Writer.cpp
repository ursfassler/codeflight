/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Writer.h"
#include "StreamWriter.h"
#include <functional>
#include <map>


namespace component::web::output::svg::graphviz
{
namespace
{


const std::map<char, std::string> EscapeMap
{
  {'"', "&quot;"},
  {'\'', "&apos;"},
  {'<', "&lt;"},
  {'>', "&gt;"},
  {'&', "&amp;"},
};

std::string escape(const std::string& value)
{
  std::string result;

  for (const auto& sym : value) {
    const auto idx = EscapeMap.find(sym);
    if (idx == EscapeMap.end()) {
      result += sym;
    } else {
      result += idx->second;
    }
  }

  return result;
}

const std::map<Shape, std::string> ShapeStrings
{
  {Shape::Box, "box"},
  {Shape::Circle, "circle"},
  {Shape::Cds, "cds"},
  {Shape::Tab, "tab"},
};

const std::map<Style, std::string> StyleStrings
{
  {Style::Rounded, "rounded"},
  {Style::Dashed, "dashed"},
  {Style::Bold, "bold"},
};

template<typename T>
std::string to_string(T value, const std::map<T, std::string>& map)
{
  const auto idx = map.find(value);
  if (idx == map.end()) {
    throw std::runtime_error("string for value not found: " + std::to_string(static_cast<int>(value)));
  }
  return idx->second;
}

std::string to_string(Shape value)
{
  return to_string(value, ShapeStrings);
}

std::string to_string(Style value)
{
  return to_string(value, StyleStrings);
}


}


Writer::Writer(StreamWriter &writer_) :
  writer{writer_}
{
}

void Writer::beginGraph()
{
  nodeName.clear();
  subgraphNr.reset();
  nodeNr.reset();

  writer.write("digraph {");
  writer.inc();
  writer.write("rankdir=LR");
}

void Writer::endGraph()
{
  writer.dec();
  writer.write("}");
}

void Writer::beginNode(Shape shape, const void* id)
{
  nodeNr.next();

  const auto name = currentNodeName();
  nodeName[id] = name;

  writer.write(name + " [");
  writer.inc();
  writer.write("shape=\"" + to_string(shape) + "\"");
}

void Writer::endNode()
{
  writer.dec();
  writer.write("]");
}

void Writer::label(const std::string& caption)
{
  writer.write("label=\"" +  escape(caption) + "\"");
}

void Writer::url(const std::string& value)
{
  writer.write("URL=\"" +  escape(value) + "\"");
}

void Writer::beginSubgraph()
{
  subgraphNr.next();
  writer.write("subgraph " + currentSubraphName() + " {");
  writer.inc();
}

void Writer::endSubgraph()
{
  writer.dec();
  writer.write("}");
}

void Writer::edge(const void* source, const void* destination)
{
  writer.write(toString(source) + " -> " + toString(destination));
}

void Writer::beginEdge(const void *source, const void *destination)
{
  writer.write(toString(source) + " -> " + toString(destination) + " [");
  writer.inc();
}

void Writer::endEdge()
{
  writer.dec();
  writer.write("]");
}

void Writer::style(Style value)
{
  writer.write("style=\"" + to_string(value) + "\"");
}

void Writer::style(const Styles& values)
{
  std::string style{};
  bool first = true;
  for (const auto value : values) {
    if (first) {
      first = false;
    } else {
      style += ", ";
    }
    style += to_string(value);
  }
  writer.write("style=\"" + style + "\"");
}

std::string Writer::toString(const void* id) const
{
  std::string result{};

  const auto nodeIdx = nodeName.find(id);
  if (nodeIdx != nodeName.end()) {
    result = nodeIdx->second;
  } else {
    throw std::invalid_argument("edge to/from undefined node");
  }

  return result;
}

std::string Writer::currentNodeName() const
{
  return "_node" + std::to_string(nodeNr.current());
}

std::string Writer::currentSubraphName() const
{
  return "cluster_" + std::to_string(subgraphNr.current());
}


}
