/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "NumberIterator.h"
#include <map>
#include <string>
#include <vector>


namespace component::web::output::svg::graphviz
{

class StreamWriter;


enum class Shape
{
  Box,
  Circle,
  Cds,
  Tab,
};

enum class Style
{
  Rounded,
  Dashed,
  Bold,
};
using Styles = std::vector<Style>;


class Writer
{
public:
  Writer(StreamWriter& writer_);

  void beginNode(Shape, const void*);
  void endNode();
  void label(const std::string&);
  void url(const std::string&);
  void beginSubgraph();
  void endSubgraph();
  void beginGraph();
  void endGraph();
  void edge(const void* source, const void* destination);
  void beginEdge(const void* source, const void* destination);
  void endEdge();
  void style(Style);
  void style(const Styles&);


private:
  StreamWriter& writer;

  NumberIterator subgraphNr{};
  NumberIterator nodeNr{};
  std::map<const void*, std::string> nodeName{};

  std::string toString(const void*) const;
  std::string currentNodeName() const;
  std::string currentSubraphName() const;
};


}
