/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <ostream>
#include <string>


namespace component::web::output::svg::graphviz
{


class StreamWriter
{
  public:
    StreamWriter(std::ostream& output_);

    void inc();
    void dec();
    void write(const std::string&);

  private:
    std::ostream& output;
    unsigned level{0};

    std::string indent(unsigned level);

};


}
