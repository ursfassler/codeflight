/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <limits>


namespace component::web::output::svg::graphviz
{


class NumberIterator
{
public:
  NumberIterator();

  void reset();
  void next();
  unsigned current() const;

private:
  unsigned actual;
  unsigned maximum() const;

};


}
