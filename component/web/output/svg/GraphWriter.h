/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/GraphBuilder.h"
#include "component/codeflight/library/Path.h"
#include "component/web/library/Url.h"
#include "component/web/output/svg/graphviz/Writer.h"
#include <functional>
#include <optional>
#include <string>


namespace component::codeflight::ast
{

class Node;

}

namespace component::web::library
{

class WebEnvironment;
class LinkHook;
class ArtefactTypeQuery;

}

namespace component::web::output::svg
{


class GraphWriter :
    public codeflight::artefact::interface::GraphBuilder
{
public:
  GraphWriter(
      const library::Url& self_,
      graphviz::Writer&,
      const library::ArtefactTypeQuery&,
      library::LinkHook&,
      const library::WebEnvironment&
      );

  void beginGraph(const std::optional<codeflight::library::Path>& name) override;
  void endGraph() override;

  void beginContainer(const codeflight::library::Path&, const std::optional<Link>&) override;
  void endContainer() override;

  void node(const codeflight::ast::Node*, const codeflight::library::Path&, bool emphasize, const std::optional<Link>&) override;
  void edge(const codeflight::ast::Node* from, const codeflight::ast::Node* to, const std::optional<std::string>& label, const std::optional<Link>&) override;

private:
  const library::Url self;
  graphviz::Writer& gvw;
  const library::ArtefactTypeQuery& typeQuery;
  library::LinkHook& linkHook;
  const library::WebEnvironment& env;

  graphviz::Shape shapeFor(const codeflight::ast::Node*) const;
  graphviz::Styles styleFor(const codeflight::ast::Node*) const;
  library::Url urlFromAi(const Link&) const;
};


}
