/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>

namespace component::codeflight::artefact::interface
{

struct ArtefactIdentifier;

}

namespace component::web::library
{

class WebEnvironment;
class Url;

}

namespace component::web::output::svg
{


void generateGraphvizSvg(const library::Url&, const std::string& graph, const library::WebEnvironment& env);


}
