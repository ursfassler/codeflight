/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "GraphWriter.h"
#include "component/codeflight/ast/specification/Field.h"
#include "component/codeflight/ast/specification/Function.h"
#include "component/codeflight/ast/specification/Method.h"
#include "component/codeflight/ast/specification/Package.h"
#include "component/codeflight/ast/specification/Variable.h"
#include "component/codeflight/ast/specification/operator.h"
#include "component/web/library/ArtefactTypeQuery.h"
#include "component/web/library/LinkHook.h"
#include "component/web/library/Url.h"
#include "component/web/library/WebEnvironment.h"
#include "component/web/library/aiString.h"
#include "component/web/library/urlFromAi.h"


namespace component::web::output::svg
{
namespace
{


using codeflight::ast::specification::operator||;


const codeflight::ast::NodeSpecification FieldOrVariable =
    codeflight::ast::specification::Field() ||
    codeflight::ast::specification::Variable();

const codeflight::ast::NodeSpecification MethodOrFunction =
    codeflight::ast::specification::Method() ||
    codeflight::ast::specification::Function();


}


GraphWriter::GraphWriter(
    const library::Url& self_,
    graphviz::Writer& gvw_,
    const library::ArtefactTypeQuery& typeQuery_,
    library::LinkHook& linkHook_,
    const library::WebEnvironment& env_
    ) :
  self{self_},
  gvw{gvw_},
  typeQuery{typeQuery_},
  linkHook{linkHook_},
  env{env_}
{
}

void GraphWriter::beginGraph(const std::optional<codeflight::library::Path>& name)
{
  gvw.beginGraph();
  if (name) {
    const std::string label = env.formatPath(*name);
    gvw.label(label);
  }
}

void GraphWriter::endGraph()
{
  gvw.endGraph();
}

void GraphWriter::beginContainer(const codeflight::library::Path& label, const std::optional<Link>& link)
{
  gvw.beginSubgraph();
  gvw.label(env.formatPath(label));
  if (link) {
    const auto url = urlFromAi(*link);
    gvw.url(url.escaped());
  }
}

void GraphWriter::endContainer()
{
  gvw.endSubgraph();
}

void GraphWriter::node(const codeflight::ast::Node* node, const codeflight::library::Path& label, bool emphasize, const std::optional<Link>& link)
{
  const auto shape = shapeFor(node);
  graphviz::Styles style = styleFor(node);
  if (emphasize) {
    style.push_back(graphviz::Style::Bold);
  }

  gvw.beginNode(shape, node);
  if (!style.empty()) {
    gvw.style(style);
  }
  gvw.label(env.formatPath(label));
  if (link) {
    const auto url = urlFromAi(*link);
    gvw.url(url.escaped());
  }
  gvw.endNode();
}

void GraphWriter::edge(const codeflight::ast::Node* from, const codeflight::ast::Node* to, const std::optional<std::string>& label, const std::optional<Link>& link)
{
  if (label || link) {
    gvw.beginEdge(from, to);
    if (label) {
      gvw.label(*label);
    }
    if (link) {
      const auto url = urlFromAi(*link);
      gvw.url(url.escaped());
    }
    gvw.endEdge();
  } else {
    gvw.edge(from, to);
  }
}

graphviz::Shape GraphWriter::shapeFor(const codeflight::ast::Node* node) const
{
  if (FieldOrVariable(node)) {
    return graphviz::Shape::Cds;
  }
  if (codeflight::ast::specification::Package()(node)) {
    return graphviz::Shape::Tab;
  }
  return graphviz::Shape::Box;
}

graphviz::Styles GraphWriter::styleFor(const codeflight::ast::Node* node) const
{
  if (MethodOrFunction(node)) {
    return {graphviz::Style::Rounded};
  } else {
    return {};
  }
}

library::Url GraphWriter::urlFromAi(const Link& link) const
{
  const auto type = typeQuery.getType(link);
  if (!type) {
    throw std::runtime_error("no type found for artefact identifier " + library::aiString(link));
  }

  linkHook.referenced(link);
  const auto url = library::urlFromAi(link, *type).relativeTo(self);
  return url;
}


}
