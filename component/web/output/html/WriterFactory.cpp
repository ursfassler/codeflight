/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "WriterFactory.h"
#include "Writer.h"


namespace component::web::output::html
{


WriterFactory::WriterFactory(
    const xml::WriterFactory& xmlFactory_,
    const codeflight::artefact::interface::MaybeFactory& overviewFactory_,
    const resource::Factory& resources_,
    const library::ArtefactTypeQuery& typeQuery_,
    const library::WebEnvironment& env_
    ) :
  xmlFactory{xmlFactory_},
  overviewFactory{overviewFactory_},
  resources{resources_},
  typeQuery{typeQuery_},
  env{env_}
{
}

std::unique_ptr<Writer> WriterFactory::produce(const library::Url& url, library::LinkHook& linkHook) const
{
  auto wf = xmlFactory.produce();
  return std::make_unique<Writer>(url, wf, typeQuery, linkHook, overviewFactory, resources, env);
}


}
