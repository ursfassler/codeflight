/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Writer.h"
#include <memory>

namespace component::web::library
{

class Url;

}

namespace component::web::resource
{

class Factory;

}

namespace component::web::xml
{

class WriterFactory;

}

namespace component::web::output::html
{


class WriterFactory
{
public:
  explicit WriterFactory(
      const xml::WriterFactory&,
      const codeflight::artefact::interface::MaybeFactory&,
      const resource::Factory&,
      const library::ArtefactTypeQuery&,
      const library::WebEnvironment&
      );

  std::unique_ptr<Writer> produce(const library::Url&, library::LinkHook&) const;

private:
  const xml::WriterFactory& xmlFactory;
  const codeflight::artefact::interface::MaybeFactory& overviewFactory;
  const resource::Factory& resources;
  const library::ArtefactTypeQuery& typeQuery;
  const library::WebEnvironment& env;

};


}
