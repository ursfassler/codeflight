/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/codeflight/artefact/interface/ArtefactIdentifier.h"
#include "component/codeflight/artefact/interface/DocumentBuilder.h"
#include "component/web/library/Url.h"
#include "component/web/xml/Writer.h"
#include <functional>
#include <optional>
#include <ostream>
#include <set>
#include <vector>


namespace component::web::library
{

class WebEnvironment;
class LinkHook;
class ArtefactTypeQuery;

}

namespace component::web::resource
{

class Factory;

}

namespace component::codeflight::ast
{

class Node;

}

namespace component::codeflight::artefact::interface
{

class MaybeFactory;

}

namespace component::web::output::html
{


class Writer :
    public codeflight::artefact::interface::DocumentBuilder
{
  public:
    Writer(
        const library::Url&,
        std::unique_ptr<xml::Writer>&,
        const library::ArtefactTypeQuery&,
        library::LinkHook&,
        const codeflight::artefact::interface::MaybeFactory&,
        const resource::Factory&,
        const library::WebEnvironment&
        );

    void beginDocument(const codeflight::library::Path&, const codeflight::ast::Node*) override;
    void beginDocument(const std::string&, const std::optional<const codeflight::ast::Node*>&) override;
    void endDocument() override;

    void beginSection(const codeflight::library::Path&) override;
    void beginSection(const std::string&) override;
    void endSection() override;

    void beginParagraph() override;
    void endParagraph() override;

    void beginUnnumberedList() override;
    void endUnnumberedList() override;

    void beginListItem() override;
    void endListItem() override;

    void link(const std::string&, const codeflight::artefact::interface::ArtefactIdentifier&) override;

    void beginTable(const Row& header, TableType) override;
    void endTable() override;

    void beginTableRow() override;
    void endTableRow() override;
    void tableCell(const codeflight::library::Path&, const codeflight::artefact::interface::ArtefactIdentifier&) override;
    void tableCell(const std::string&, const std::optional<codeflight::artefact::interface::ArtefactIdentifier>&) override;

    void text(const std::string&);

    void serialize(std::ostream&) const;

  private:
    std::unique_ptr<xml::Writer> xw;
    const library::ArtefactTypeQuery& typeQuery;
    library::LinkHook& linkHook;

    unsigned sectionLevel{0};

    const library::Url thisUrl;
    const codeflight::artefact::interface::MaybeFactory& overviewFactory;
    const resource::Factory& resources;
    const library::WebEnvironment& env;

    void printResources();
    bool hasResources() const;
    void printBreadcrumbs(const codeflight::ast::Node*);
    std::optional<codeflight::artefact::interface::ArtefactIdentifier> getLinkWhenPossible(const codeflight::ast::Node* node) const;
};


}
