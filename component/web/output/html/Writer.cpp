/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Writer.h"
#include "component/codeflight/artefact/interface/MaybeFactory.h"
#include "component/codeflight/ast/parents/parents.h"
#include "component/web/library/ArtefactTypeQuery.h"
#include "component/web/library/LinkHook.h"
#include "component/web/library/WebEnvironment.h"
#include "component/web/library/aiString.h"
#include "component/web/library/urlFromAi.h"
#include "component/web/resource/Artefact.h"
#include "component/web/resource/Factory.h"
#include <cassert>
#include <list>
#include <map>


namespace component::web::output::html
{


Writer::Writer(
    const library::Url& thisUrl_,
    std::unique_ptr<xml::Writer>& xw_,
    const library::ArtefactTypeQuery& typeQuery_,
    library::LinkHook& linkHook_,
    const codeflight::artefact::interface::MaybeFactory& overviewFactory_,
    const resource::Factory& resources_,
    const library::WebEnvironment& env_
    ) :
  xw{std::move(xw_)},
  typeQuery{typeQuery_},
  linkHook{linkHook_},
  thisUrl{thisUrl_},
  overviewFactory{overviewFactory_},
  resources{resources_},
  env{env_}
{
}

void Writer::beginDocument(const codeflight::library::Path& path, const codeflight::ast::Node* node)
{
  const std::string caption = env.formatPath(path);
  beginDocument(caption, {node});
}

void Writer::beginDocument(const std::string &title, const std::optional<const codeflight::ast::Node*>& node)
{
  xw->beginNode("html");
  xw->attribute("lang", "en");

  xw->beginNode("head");

  xw->beginNode("meta");
  xw->attribute("charset", "utf-8");
  xw->endNode();

  printResources();

  xw->beginNode("title");
  xw->text(title);
  xw->endNode();

  xw->endNode();
  xw->beginNode("body");

  if (node) {
    printBreadcrumbs(*node);
  }
}

void Writer::printResources()
{
  resources.foreach([this](const resource::Artefact& resource){
    const auto relativeFilename = resource.url().relativeTo(thisUrl).escaped();

    switch (resource.getType()) {
      case resource::Type::Stylesheet:
        xw->beginNode("link");
        xw->attribute("rel", "stylesheet");
        xw->attribute("href", relativeFilename);
        xw->endNode();
        break;

      case resource::Type::JavaScript:
        xw->beginNode("script");
        xw->attribute("src", relativeFilename);
        xw->text("");
        xw->endNode();
        break;
    }
  });
}

bool Writer::hasResources() const
{
  return !resources.empty();
}

void Writer::printBreadcrumbs(const codeflight::ast::Node* node)
{
  std::list<const codeflight::ast::Node*> ancestors{};

  for (std::optional<const codeflight::ast::Node*> crumb = {node}; crumb; crumb = codeflight::ast::parents::findParent(*crumb)) {
    ancestors.push_front(*crumb);
  }

  xw->beginNode("nav");

  bool first = true;
  for (const auto node : ancestors) {
    if (first) {
      first = false;
    } else {
      text(">");
    }
    const auto name = env.formatName(node);
    const auto ai = getLinkWhenPossible(node);
    if (ai) {
      link(name, *ai);
    } else {
      text(name);
    }
  }

  xw->endNode();
}

std::optional<codeflight::artefact::interface::ArtefactIdentifier> Writer::getLinkWhenPossible(const codeflight::ast::Node* node) const
{
  return overviewFactory.identifier(node);
}

void Writer::endDocument()
{
  xw->endNode();
  xw->endNode();
}

void Writer::beginSection(const codeflight::library::Path& path)
{
  const std::string caption = env.formatPath(path);
  beginSection(caption);
}

void Writer::beginSection(const std::string& title)
{
  sectionLevel++;

  xw->beginNode("h" + std::to_string(sectionLevel));
  xw->text(title);
  xw->endNode();
}

void Writer::endSection()
{
  assert(sectionLevel > 0);
  sectionLevel--;
}

void Writer::beginParagraph()
{
  xw->beginNode("p");
}

void Writer::endParagraph()
{
  xw->endNode();
}

void Writer::beginUnnumberedList()
{
  xw->beginNode("ul");
}

void Writer::endUnnumberedList()
{
  xw->endNode();
}

void Writer::beginListItem()
{
  xw->beginNode("li");
}

void Writer::endListItem()
{
  xw->endNode();
}

void Writer::link(const std::string& caption, const codeflight::artefact::interface::ArtefactIdentifier& link)
{
  const auto type = typeQuery.getType(link);
  if (!type) {
    throw std::runtime_error("no type found for artefact identifier " + library::aiString(link));
  }

  linkHook.referenced(link);
  const auto relativeFilename = library::urlFromAi(link, *type).relativeTo(thisUrl).escaped();

  xw->beginNode("a");
  xw->attribute("href", relativeFilename);
  xw->text(caption);
  xw->endNode();
}

namespace
{

const std::map<Writer::TableType, std::string> TableTypeNames
{
  {Writer::TableType::StringNumbers, "string-numbers"},
  {Writer::TableType::StringStringNumbers, "string-string-numbers"},
};

std::string tableTypeName(Writer::TableType type)
{
  const auto idx = TableTypeNames.find(type);
  assert(idx != TableTypeNames.end());
  return idx->second;
}

}

void Writer::beginTable(const Row& header, TableType type)
{
  xw->beginNode("table");

  if (hasResources()) {
    xw->attribute("class", tableTypeName(type));
  }

  beginTableRow();
  for (const auto& value : header) {
    xw->beginNode("th");
    xw->text(value);
    xw->endNode();
  }
  endTableRow();
}

void Writer::endTable()
{
  xw->endNode();
}

void Writer::beginTableRow()
{
  xw->beginNode("tr");
}

void Writer::endTableRow()
{
  xw->endNode();
}

void Writer::tableCell(const codeflight::library::Path& path, const codeflight::artefact::interface::ArtefactIdentifier& link)
{
  const std::string caption = env.formatPath(path);
  tableCell(caption, {link});
}

void Writer::tableCell(const std::string& caption, const std::optional<codeflight::artefact::interface::ArtefactIdentifier>& link)
{
  xw->beginNode("td");
  if (link) {
    this->link(caption, *link);
  } else {
    xw->text(caption);
  }
  xw->endNode();
}

void Writer::text(const std::string& value)
{
  xw->text(value);
}

void Writer::serialize(std::ostream &output) const
{
  xw->serialize(output);
}


}
