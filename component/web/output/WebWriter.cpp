/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "WebWriter.h"
#include "component/codeflight/artefact/interface/ArtefactVisitor.h"
#include "component/codeflight/artefact/interface/DocumentArtefact.h"
#include "component/codeflight/artefact/interface/GraphArtefact.h"
#include "component/web/io/SvgFilesystem.h"
#include "component/web/library/WebEnvironment.h"
#include "component/web/library/urlFromAi.h"
#include "component/web/output/html/WriterFactory.h"
#include "component/web/output/svg/GraphWriter.h"
#include "component/web/output/svg/generateGraphvizSvg.h"
#include "component/web/output/svg/graphviz/StreamWriter.h"
#include "component/web/output/svg/graphviz/Writer.h"
#include "component/web/resource/Artefact.h"


namespace component::web::output
{
namespace
{


class ArtefactDispatcher :
    public codeflight::artefact::interface::ArtefactVisitor
{
public:
  ArtefactDispatcher(
      const codeflight::artefact::interface::ArtefactIdentifier id_,
      const html::WriterFactory& htmlFactory_,
      const library::ArtefactTypeQuery& typeQuery_,
      const library::WebEnvironment& env_,
      library::LinkHookDispatcher& linkDispatcher_
      ) :
    id{id_},
    htmlFactory{htmlFactory_},
    typeQuery{typeQuery_},
    env{env_},
    linkDispatcher{linkDispatcher_}
  {
  }

  void visit(const codeflight::artefact::interface::GraphArtefact& artefact) override
  {
    const auto url = library::urlFromAi(id, library::ArtefactType::Graph);

    std::stringstream output{};
    svg::graphviz::StreamWriter writer{output};
    svg::graphviz::Writer gw{writer};
    svg::GraphWriter gvw{url, gw, typeQuery, linkDispatcher, env};

    artefact.build(gvw);

    svg::generateGraphvizSvg(url, output.str(), env);
  }

  void visit(const codeflight::artefact::interface::DocumentArtefact& artefact) override
  {
    const auto url = library::urlFromAi(id, library::ArtefactType::Document);

    auto xw = htmlFactory.produce(url, linkDispatcher);
    artefact.build(*xw);

    const auto fullname = env.getRootDirectory() / url.filename();
    env.getSvgFilesystem().write(fullname, [&xw](std::ostream& output){
      xw->serialize(output);
    });
  }

private:
  const codeflight::artefact::interface::ArtefactIdentifier id;
  const html::WriterFactory& htmlFactory;
  const library::ArtefactTypeQuery& typeQuery;
  const library::WebEnvironment& env;
  library::LinkHookDispatcher& linkDispatcher;

};


}



WebWriter::WebWriter(
    const html::WriterFactory& htmlFactory_,
    const library::ArtefactTypeQuery& typeQuery_,
    const library::WebEnvironment& env_
    ) :
  htmlFactory{htmlFactory_},
  typeQuery{typeQuery_},
  env{env_}
{
}

void WebWriter::resource(const resource::Artefact& artefact)
{
  const auto name = artefact.url();

  const auto fullname = env.getRootDirectory() / name.filename();
  env.getSvgFilesystem().write(fullname, [&artefact](std::ostream& output){
    std::string strout{};
    artefact.build(strout);
    output << strout;
  });
}

void WebWriter::handle(const codeflight::artefact::interface::ArtefactIdentifier& ai, const codeflight::artefact::interface::Artefact& artefact)
{
  ArtefactDispatcher dispatcher{ai, htmlFactory, typeQuery, env, linkDispatcher};
  artefact.accept(dispatcher);
}

void WebWriter::addLinkHook(library::LinkHook* value)
{
  linkDispatcher.add(value);
}


}
