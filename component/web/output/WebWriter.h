/*
 * (C) Copyright 2022 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "component/web/generator/WebHandler.h"
#include "component/web/library/LinkHookDispatcher.h"
#include <vector>

namespace component::web::output::html
{

class WriterFactory;

}

namespace component::web::library
{

class WebEnvironment;
class LinkHook;
class ArtefactTypeQuery;

}

namespace component::codeflight::artefact::interface
{

class Artefact;
class GraphArtefact;
class DocumentArtefact;

}

namespace component::web::resource
{

class Artefact;

}

namespace component::web::output
{


class WebWriter :
    public generator::WebHandler
{
public:
  WebWriter(const WebWriter&) = delete;

  WebWriter(
      const html::WriterFactory&,
      const library::ArtefactTypeQuery&,
      const library::WebEnvironment&
      );

  void handle(const codeflight::artefact::interface::ArtefactIdentifier&, const codeflight::artefact::interface::Artefact&) override;
  void resource(const resource::Artefact&) override;
  void addLinkHook(library::LinkHook*);

private:
  const html::WriterFactory& htmlFactory;
  const library::ArtefactTypeQuery& typeQuery;
  const library::WebEnvironment& env;
  library::LinkHookDispatcher linkDispatcher{};
};


}
