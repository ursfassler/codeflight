/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Artefact.h"
#include "component/web/io/ResourceFilesystem.h"

namespace component::web::resource
{


Artefact::Artefact(
    const Resource& resource_,
    const std::string& prefix_,
    const std::filesystem::path& sourceDir_,
    io::ResourceFilesystem& fs_
    ) :
  resource{resource_},
  prefix{prefix_},
  sourceDir{sourceDir_},
  fs{fs_}
{
}

void Artefact::build(std::string& output) const
{
  const auto sourceFile = sourceDir / resource.filename;
  fs.read(sourceFile, [&output](std::istream& stream){
    const std::string content(std::istreambuf_iterator<char>(stream), {});
    output = content;
  });
}

library::Url Artefact::url() const
{
  auto filename = prefix / resource.filename;
  auto url = library::Url::fromFilename(filename);
  return url;
}

Type Artefact::getType() const
{
  return resource.type;
}


}
