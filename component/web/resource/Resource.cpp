/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Resource.h"


namespace component::web::resource
{


bool operator==(const Resource& lhs, const Resource& rhs)
{
  return (lhs.type == rhs.type) && (lhs.filename == rhs.filename);
}


}
