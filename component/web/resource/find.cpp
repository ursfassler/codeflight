/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "find.h"
#include "component/web/io/ResourceFilesystem.h"
#include <map>


namespace component::web::resource
{
namespace
{


const std::map<std::string, Type> ResourceExtension
{
  {".css", Type::Stylesheet},
  {".js", Type::JavaScript},
};


}


std::vector<Resource> find(const std::filesystem::path& resources, io::ResourceFilesystem& fs)
{
  std::vector<Resource> result{};

  const auto files = fs.files(resources);
  for (const std::filesystem::path& file : files) {
    const auto idx = ResourceExtension.find(file.extension());
    if (idx != ResourceExtension.end()) {
      result.push_back({idx->second, file});
    }
  }

  return result;
}

Factory produceFactory(const std::optional<std::filesystem::path>& resources, io::ResourceFilesystem& fs)
{
  if (!resources) {
    return Factory{fs};
  }

  std::vector<Resource> res = find(*resources, fs);
  Factory result{res, *resources, fs};
  return result;
}


}
