/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <filesystem>


namespace component::web::resource
{


enum class Type {
  Stylesheet,
  JavaScript,
};

struct Resource
{
  Type type;
  std::filesystem::path filename;
};

bool operator==(const Resource& lhs, const Resource& rhs);


}
