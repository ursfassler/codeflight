/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Resource.h"
#include "Factory.h"
#include <filesystem>
#include <optional>
#include <vector>

namespace component::web::io
{

class ResourceFilesystem;

}

namespace component::web::resource
{


std::vector<Resource> find(const std::filesystem::path&, io::ResourceFilesystem&);
Factory produceFactory(const std::optional<std::filesystem::path>& resources, io::ResourceFilesystem&);


}
