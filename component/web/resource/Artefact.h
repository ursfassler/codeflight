/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Resource.h"
#include "component/web/library/Url.h"
#include <filesystem>

namespace component::web::io
{

class ResourceFilesystem;

}

namespace component::web::resource
{


class Artefact
{
public:
  Artefact(
      const Resource&,
      const std::string& prefix,
      const std::filesystem::path& sourceDir,
      io::ResourceFilesystem&
      );
  Artefact(const Artefact&) = default;

  void build(std::string&) const;
  library::Url url() const;
  Type getType() const;

private:
  Resource resource;
  const std::string prefix;
  const std::filesystem::path sourceDir;
  io::ResourceFilesystem& fs;

};


}
