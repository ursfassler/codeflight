/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "Artefact.h"
#include "component/web/io/ResourceFilesystem.h"


namespace component::web::resource
{


Factory::Factory(
    io::ResourceFilesystem& fs_
    ) :
  fs{fs_}
{
}

Factory::Factory(
    const std::vector<Resource>& sources_,
    const std::filesystem::path& sourceDir_,
    io::ResourceFilesystem& fs_
    ):
  sources{sources_},
  sourceDir{sourceDir_},
  fs{fs_}
{
}

bool Factory::empty() const
{
  return sources.empty();
}

void Factory::foreach(const Visitor& visitor) const
{
  for (const auto& resource : sources) {
    visitor(resource::Artefact{resource, artefactName(), sourceDir, fs});
  }
}

std::string Factory::artefactName() const
{
  return "resources";
}

std::unique_ptr<Artefact> Factory::produce(const library::Url& url) const
{
  const auto prefix = artefactName();
  std::filesystem::path fp = url.filename();

  for (const auto& resource : sources) {
    if (fp == prefix / resource.filename) {
      return std::make_unique<Artefact>(resource, prefix, sourceDir, fs);
    }
  }

  return {};
}



}
