/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "find.h"
#include "component/web/io/ResourceFilesystem.h"
#include <filesystem>
#include <gmock/gmock.h>


namespace component::web::resource::unit_test
{
namespace
{

using namespace testing;


class MemoryFilesystem :
    public io::ResourceFilesystem
{
public:
  void touch(const std::filesystem::path& path)
  {
    fs.insert(path);
  }

  std::set<std::filesystem::path> files(const std::filesystem::path& root) const override
  {
    std::set<std::filesystem::path> result{};

    for (const auto& file : fs) {
      const auto relative = std::filesystem::relative(file, root);
      const auto parent_path = relative.parent_path();
      if (parent_path.empty()) {
        result.insert(relative);
      }
    }

    return result;
  }

  void read(const std::filesystem::path&, const std::function<void(std::istream&)>&) const override
  {
    throw std::runtime_error("not implemented: " + std::string(__func__));
  }

private:
  std::set<std::filesystem::path> fs{};

};


class find_Test :
    public Test
{
  public:
    MemoryFilesystem fs{};

    std::vector<Resource> resources(const std::vector<std::string>& files) const
    {
      std::vector<Resource> result{};

      for (const std::string& file : files) {
        result.push_back({Type::Stylesheet, file});
      }

      return result;
    }
};


TEST_F(find_Test, return_empty_list_when_no_files_are_available)
{
  const auto expected{resources({})};

  const auto actual = find({}, fs);

  ASSERT_EQ(expected, actual);
}

TEST_F(find_Test, return_available_stylesheets)
{
  const auto expected{resources({"style1.css", "style2.css", "xyz.css"})};
  fs.touch("style1.css");
  fs.touch("style2.css");
  fs.touch("xyz.css");

  const auto actual = find({}, fs);

  ASSERT_EQ(expected, actual);
}

TEST_F(find_Test, does_not_return_other_files)
{
  const auto expected{resources({"style.css"})};
  fs.touch("text.txt");
  fs.touch("style.css");

  const auto actual = find({}, fs);

  ASSERT_EQ(expected, actual);
}

TEST_F(find_Test, returns_file_from_specific_path)
{
  const auto expected{resources({"style2.css"})};
  fs.touch("style1.css");
  fs.touch("test/style2.css");
  fs.touch("other/style3.css");

  const auto actual = find("test", fs);

  ASSERT_EQ(expected, actual);
}

TEST_F(find_Test, can_specify_directory_with_or_without_trailing_slash)
{
  const auto expected{resources({"style.css"})};
  fs.touch("test/style.css");

  ASSERT_EQ(expected, find("test", fs));
  ASSERT_EQ(expected, find("test/", fs));
}

TEST_F(find_Test, does_not_return_files_from_subdirectories)
{
  const auto expected{resources({})};
  fs.touch("sub1/style.css");
  fs.touch("sub1/sub2/style.css");

  const auto actual = find({}, fs);

  ASSERT_EQ(expected, actual);
}


}
}
