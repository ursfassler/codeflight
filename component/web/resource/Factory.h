/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Resource.h"
#include <filesystem>
#include <functional>
#include <memory>
#include <vector>

namespace component::web::io
{

class ResourceFilesystem;

}

namespace component::web::library
{

class Url;

}

namespace component::web::resource
{

class Artefact;


class Factory
{
public:
  Factory(
      io::ResourceFilesystem&
      );
  Factory(
      const std::vector<Resource>&,
      const std::filesystem::path& sourceDir,
      io::ResourceFilesystem&
      );

  bool empty() const;
  using Visitor = std::function<void(const resource::Artefact&)>;
  void foreach(const Visitor&) const;

  std::string artefactName() const;
  std::unique_ptr<Artefact> produce(const library::Url&) const;

private:
  std::vector<Resource> sources{};
  const std::filesystem::path sourceDir{};
  io::ResourceFilesystem& fs;
};


}
