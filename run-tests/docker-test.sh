#!/bin/sh
set -e

reldir=`dirname "$0"`
scriptdir=`realpath ${reldir}`
sourcedir=`realpath ${scriptdir}/../`

docker build -t codeflight-test ${scriptdir}
docker run --rm -v "${sourcedir}:/home" codeflight-test ./run-tests/clean-test.sh

