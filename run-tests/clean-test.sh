#!/bin/sh
set -e

reldir=`dirname "$0"`
scriptdir=`realpath ${reldir}`

dir=`mktemp -d`
${scriptdir}/build-run-tests.sh ${dir}
rm -rf ${dir}

