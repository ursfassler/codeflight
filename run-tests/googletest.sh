#!/bin/sh
set -e

git clone https://github.com/google/googletest.git

cd googletest
git checkout release-1.8.1

cmake . -DBUILD_GMOCK=ON -DINSTALL_GTEST=ON
make -j `nproc`
make install

cd ..

