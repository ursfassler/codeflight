#!/bin/sh
set -e

reldir=`dirname "$0"`
srcdir=`realpath ${reldir}/../`
builddir=`realpath ${1}`
echo "Source directory: ${srcdir}"
echo "Build directory: ${builddir}"

cd ${builddir}

cmake ${srcdir} -DBUILD_TESTS=ON
make -j `nproc`


echo "unit tests"
./unit-tests


echo "acceptance tests: features/feature-tests"
./features/feature-tests &
cd ${srcdir}
cucumber --format=progress
cd ${builddir}


echo "acceptance tests: component/codeflight/features/codeflight-features"
./component/codeflight/features/codeflight-features &
cd ${srcdir}/component/codeflight
cucumber --format=progress
cd ${builddir}

