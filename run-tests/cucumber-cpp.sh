#!/bin/sh
set -e

git clone https://github.com/cucumber/cucumber-cpp.git

cd cucumber-cpp

cmake . \
  -DCUKE_USE_STATIC_BOOST=OFF \
  -DCUKE_USE_STATIC_GTEST=ON \
  -DCUKE_ENABLE_BOOST_TEST=OFF \
  -DCUKE_ENABLE_EXAMPLES=OFF \
  -DCUKE_ENABLE_GTEST=ON \
  -DCUKE_ENABLE_QT=OFF \
  -DCUKE_TESTS_E2E=OFF \
  -DCUKE_TESTS_UNIT=OFF \
  -DCUKE_TESTS_VALGRIND=OFF \
  -DGMOCK_SRC_DIR=../googletest/ \
  -DGMOCK_VER="1.8.1"

make -j `nproc`
make install

cd ..

